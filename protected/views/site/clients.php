<?php

if (is_null($clients)) {
	echo 'Клиент не найден.';
}
else {
	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'clients',
		//'enableAjaxValidation' => true
	));

	if (!$clients->isNewRecord) {
		echo CHtml::link('Настройка рассылки', Yii::app()->createUrl('site/subscrAdd', ['clientId' => $clients->clientId]));
	}
?>
<div class="form">
	<span class="red"><?=$form->errorSummary($clients, '');?></span>
	<div class="row">
		<?=$form->labelEx($clients, 'regionId'); ?>
		<?=$form->dropDownList($clients, 'regionId', CHtml::listData($regions, 'regionId', 'title'));?>
		<?=$form->error($clients, 'regionId'); ?>
	</div>
	<div class="row">
		<?=$form->labelEx($clients, 'branchId'); ?>
		<?=$form->dropDownList($clients, 'branchId', CHtml::listData($branches, 'branchId', 'title'));?>
		<?=$form->error($clients, 'branchId'); ?>
	</div>
	<div class="row">
		<?=$form->labelEx($clients, 'inn'); ?>
		<?php $this->widget('CMaskedTextField', [
			'model'     => $clients,
			'attribute' => 'inn',
			'mask'      => '9999999999'
		]); ?>
		<?=$form->error($clients, 'inn'); ?>
	</div>
	<div class="row">
		<?=$form->labelEx($clients, 'kpp'); ?>
		<?php $this->widget('CMaskedTextField', [
			'model'     => $clients,
			'attribute' => 'kpp',
			'mask'      => '999999999'
		]); ?>
		<?=$form->error($clients, 'kpp'); ?>
	</div>
	<div class="row">
		<?=$form->labelEx($clients, 'title'); ?>
		<?=$form->textField($clients, 'title');?>
		<?=$form->error($clients, 'title'); ?>
	</div>
	<div class="row">
		<?=$form->labelEx($clients, 'email'); ?>
		<?=$form->emailField($clients, 'email');?>
		<?=$form->error($clients, 'email'); ?>
	</div>
	<div class="row">
		<?=$form->labelEx($clients, 'fio'); ?>
		<?=$form->textField($clients, 'fio');?>
		<?=$form->error($clients, 'fio'); ?>
	</div>
	<div class="row">
		<?=$form->labelEx($clients, 'post'); ?>
		<?=$form->textField($clients, 'post');?>
		<?=$form->error($clients, 'post'); ?>
	</div>
	<div class="row">
		<?=$form->labelEx($clients, 'trueAddress'); ?>
		<?=$form->textField($clients, 'trueAddress');?>
		<?=$form->error($clients, 'trueAddress'); ?>
	</div>
	<div class="row">
		<?=$form->labelEx($clients, 'jureAddress'); ?>
		<?=$form->textField($clients, 'jureAddress');?>
		<?=$form->error($clients, 'jureAddress'); ?>
	</div>
	<div class="row">
		<?=$form->labelEx($clients, 'phone'); ?>
		<?php $this->widget('CMaskedTextField', [
			'model'     => $clients,
			'attribute' => 'phone',
			'mask'      => '9999999999'
		]); ?>
		<?=$form->error($clients, 'phone'); ?>
	</div>
	<div class="row buttons">
		<?=CHtml::submitButton($clients->isNewRecord ? 'Добавить клиента' : 'Сохранить'); ?>
	</div>
	<?php $this->endWidget(); ?>
</div>
	<?php if ($clients->isNewRecord) {?>
<div>Список клиентов
	<?php
		$this->widget('zii.widgets.grid.CGridView', [
			'dataProvider' => new CActiveDataProvider('Clients'),
			'columns' => [
				[
					'class' => 'CLinkColumn',
					'header' => 'Клиент',
					'labelExpression' => '$data->title',
					'urlExpression' => 'Yii::app()->createUrl("site/clients", ["id" => $data->clientId])'
				],
				'region.title',
				'branch.title'
			],
			'ajaxVar' => false,
			'ajaxUpdate' => false
		]);
	}
	?>
</div>
<?php } ?>
