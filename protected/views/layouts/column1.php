<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div id="content">
	<? if( ! empty($this->title)): ?><h1><?= $this->title ?></h1><? endif; ?>
	<?php echo $content; ?>
</div><!-- content -->
<?php $this->endContent(); ?>
