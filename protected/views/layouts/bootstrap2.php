<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/bootstrap'); ?>
<div class="row-fluid">
    <div class="span9">
        <?php echo $content; ?>
    </div>
    <div class="span3">
        <div class="well well-small">
            <strong>Операции</strong>
            <?php
            $this->widget('zii.widgets.CMenu', array(
                    'items'=>$this->menu,
                    'htmlOptions'=>array('class'=>'nav nav-list'),
                ));
            ?>
        </div>
    </div>

</div>
<?php $this->endContent(); ?>
