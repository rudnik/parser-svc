<?php
/**
 * Интерфейс класса, формирующего отчет о результате обработки сущностей
 */
interface IEntityProcessReportBuilder
{    
    /**
     * Увеличить счетчик общего числа сущностей, подлежащих обработке
     * 
     * @param integer $count - значение, на которое увеличивается счетчик
     */
    public function addTotalCount($count = 1);
    
    /**
     * Увеличить счетчик количества успешных обработок
     *  
     * @param integer $count - значение, на которое увеличивается счетчик
     */
    public function addSuccess($count = 1);
    
    /**
     * Увеличить счетчик количества обработок со сбоем
     *  
     * @param integer $count - значение, на которое увеличивается счетчик
     */
    public function addFail($count = 1);
    
    /**
     * Получить количество успешных обработок
     * 
     * @return integer
     */
    public function getSuccessCount();
    
    /**
     * Получить количество обработок со сбоем
     *  
     * @return integer
     */
    public function getFailCount();
    
    /**
     * Получить общее число сущностей, подлежащих обработке
     *  
     * @return integer
     */
    public function getTotalCount();
    
    /**
     * Сброс всех счетчиков
     */
    public function reset();
    
    /**
     * Возвращает строку, содержащую отчет.
     * @param array $params - парамтры (см. конкретную реализацию)
     * @return string
     */
    public function getReport(array $params = null);
}