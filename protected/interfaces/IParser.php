<?php

/**
 * Интерфейс адаптера парсера портала
 */
interface IParser
{

    /**
     * Конструктор
     * @param type $db - Класс-коннектор для работы с БД через фреймворк Yii.
     * @param type $params - Основные параметры портала.
     */
    public function __construct($db, $params);

    /**
     * Сохраняет файлы для парсинга из источника на сервер
     * @param string $period
     * @param boolean $rmFiles
     */
    public function saveFiles($period = 'day', $periodNum = 0);

    /**
     * Парсит файлы и записывает полезные данные в БД
     * @param boolean $rmFiles
     */
    public function parse($rmFiles = true);
}
