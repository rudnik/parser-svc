<?php
class DateSelectWidget extends CWidget {
    public $model = null;
    public $field = null;
    public $date = null;
    public $template = 'default';
    
    public function init() {
	Yii::app()->clientScript->registerScriptFile(
		    Yii::app()->assetManager->publish( Yii::getPathOfAlias('application.widgets.dateSelect.assets')) . '/js/dateSelectWidget.js'
	);
    }
    
    public function run() {
	if (is_numeric($this->date)) 
	    $this->date = date("d.m.Y", $this->date);
	elseif (is_string($this->date)) 
	    $this->date = date("d.m.Y", strtotime($this->date));
	else 
	    $this->date = date("d.m.Y");
	
	$dateTemp = explode('.', $this->date);
	$date['day'] = intval($dateTemp[0]);
	$date['month'] = intval($dateTemp[1]);
	$date['year'] = intval($dateTemp[2]);
	$modelName = $this->model !== null ? get_class($this->model) : '';
	$field = $this->field !== null ? $this->field : '';
	$this->render($this->template, [
	    'date' => $date,
	    'prefix' => "{$modelName}_{$field}",
	]);
    }
}