<?
    $month = [ 1=>'января', 2=>'февраля', 3=>'марта', 4=>'апреля', 5=>'мая', 6=>'июня', 7=>'июля', 
		8=>'августа', 9=>'сентября', 10=>'октября', 11=>'ноября', 12=>'декабря'];
    $year = [];
    foreach(range($date['year'] - 3, date('Y') + 3) as $item) $year[$item] = $item;
    
    echo CHtml::dropDownList("{$prefix}__day", $date['day'] - 1, range(1, 31), ['class' => 'js-date-select-widget', 'targetId' => $prefix]);
    echo CHtml::dropDownList("{$prefix}__month", $date['month'], $month, ['class' => 'js-date-select-widget', 'targetId' => $prefix]);
    echo CHtml::dropDownList("{$prefix}__year", $date['year'], $year, ['class' => 'js-date-select-widget', 'targetId' => $prefix]);
?>