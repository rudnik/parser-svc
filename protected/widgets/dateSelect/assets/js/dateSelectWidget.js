var dateSelectWidget = {};

$(function(){
  $('body').on('change', '.js-date-select-widget', function(){
      dateSelectWidget.updateModelField(this);
  });
});

// Записывает unixtimestamp в input модели
dateSelectWidget.updateModelField = function(initiator) {
    var targetId = $(initiator).attr('targetId'); 
    var day = $('#' + targetId + '__day').val(); day++;
    var month = $('#' + targetId + '__month').val(); month--;
    var year = $('#' + targetId + '__year').val();
    var oDate = new Date;
    oDate.setFullYear(year, month, day, 0, 0, 0);
    var newDate = Math.round(oDate.getTime() / 1000); 
    $('#' + targetId).attr('value', newDate);
};