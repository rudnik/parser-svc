<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m130906_060615_add_doctype_purchaseRejection extends CDbMigration {
	public function safeUp() {
		$this->insert('bids_doctypes', [
		    'portalId' => 2,
		    'name' => 'purchaseRejection',
		    'descr' => 'Извещение об отказе от проведения закупки',
		]);
	}

	public function safeDown() {
		$this->delete('bids_doctypes', 'name = "purchaseRejection"');
	}
}
