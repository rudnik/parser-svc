<?php

/**
 * Миграция добавления полей даты начала публикации и даты окончания публикации.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m130812_073354_add_publication_date_fields_range extends CDbMigration {
	public function safeUp() {
		// Добавление полей даты начала публикации и даты окончания публикации
		$this->dbConnection->createCommand("
                    ALTER TABLE `clients-subscriptions`
                        ADD COLUMN `publication_date_start` TIMESTAMP NULL DEFAULT '000-00-00 00:00:00' COMMENT 'Дата публикации \"c\"' AFTER `pubdate_to_end`,
                        ADD COLUMN `publication_date_end` TIMESTAMP NULL DEFAULT '000-00-00 00:00:00' COMMENT 'Дата публикации \"по\"' AFTER `publication_date_start`;
		")->execute();
	}

	public function safeDown() {
            $this->dbConnection->createCommand("
                ALTER TABLE `clients-subscriptions` DROP COLUMN `publication_date_start`, DROP COLUMN `publication_date_end`;
		")->execute();
	}
}
