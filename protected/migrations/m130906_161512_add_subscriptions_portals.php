<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m130906_161512_add_subscriptions_portals extends CDbMigration {
	public function safeUp() {
		$this->dbConnection->createCommand("
		CREATE TABLE `clients_subscriptions_portals` (
			`subscription_id` INT(11) UNSIGNED NOT NULL COMMENT 'id рассылки',
			`portal_id` MEDIUMINT(6) UNSIGNED NOT NULL COMMENT 'id портала',
			UNIQUE INDEX `subscription_id_portal_id` (`subscription_id`, `portal_id`),
			INDEX `FK_clients_subscriptions_portals_clients-subscriptions` (`subscription_id`),
			INDEX `FK_clients_subscriptions_portals_portals` (`portal_id`),
			CONSTRAINT `FK_clients_subscriptions_portals_clients-subscriptions` FOREIGN KEY (`subscription_id`) REFERENCES `clients-subscriptions` (`subscrId`) ON DELETE CASCADE,
			CONSTRAINT `FK_clients_subscriptions_portals_portals` FOREIGN KEY (`portal_id`) REFERENCES `portals` (`portalId`) ON DELETE CASCADE
		)
		COMMENT='порталы (законы) рассылки'
		COLLATE='utf8_general_ci'
		ENGINE=InnoDB;
		")->execute();
	}

	public function safeDown() {
	    $this->dbConnection->createCommand('SET FOREIGN_KEY_CHECKS = 0;')->execute();
	    $this->dropTable('clients_subscriptions_portals');
	}
}
