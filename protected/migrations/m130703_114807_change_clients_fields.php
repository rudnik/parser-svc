<?php

/**
 * Миграция изменения состава таблицы клиентов.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013
 */
class m130703_114807_change_clients_fields extends CDbMigration {
	public function safeUp() {
		$this->dropIndex('client', 'clients');
		$this->getDbConnection()->createCommand('
			ALTER TABLE `clients` MODIFY COLUMN `inn` bigint(10) unsigned NOT NULL DEFAULT \'0\' COMMENT \'ИНН\'
		')->execute();
		$this->createIndex('inn', 'clients', 'inn');
		$this->createIndex('kpp', 'clients', 'kpp');
	}

	public function safeDown() {
		echo 'm130703_114807_change_clients_fields does not support migration down.'."\n";
		return true;
	}
}
