<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m130902_111301_rename_zakupki_doctypes_to_bids_doctypes extends CDbMigration {
	public function safeUp() {
	    $this->dbConnection->createCommand("
		    RENAME TABLE `zakupki-doctypes` TO `bids_doctypes`;
		")->execute();
	    
	}

	public function safeDown() {
		$this->dbConnection->createCommand("
		    RENAME TABLE `bids_doctypes` TO `zakupki-doctypes`;
		")->execute();
	}
}
