<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m130919_071002_rename_tables extends CDbMigration {
	public function safeUp() {
        $this->dropTable('clients-protocols');
        $this->renameTable('bids-data', 'bids_data');
        $this->renameTable('bids-data-date', 'bids_data_date');
        $this->renameTable('bids-data-date-search', 'bids_data_date_search');
        $this->renameTable('bids-data-num', 'bids_data_num');
        $this->renameTable('bids-data-num-search', 'bids_data_num_search');
        $this->renameTable('bids-data-text', 'bids_data_text');
        $this->renameTable('bids-data-text-search', 'bids_data_text_search');
        $this->renameTable('clients-subscriptions', 'clients_subscriptions');
        $this->renameTable('client_emails', 'clients_emails');
        $this->renameTable('client_phones', 'clients_phones');
        $this->renameTable('client_protocol', 'clients_protocols');
        $this->renameTable('client_sended_bids', 'clients_sended_bids');
        $this->renameTable('client_subscribe_emails', 'clients_subscribe_emails');
	}

	public function safeDown() {
		echo 'm130919_071002_rename_tables does not support migration down.'."\n";

		return true;
	}
}
