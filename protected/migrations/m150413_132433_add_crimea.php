<?php

/**
 * Миграция .
 *
 * @package    TendersParser
 * @subpackage Migrate
 * @author     Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright  2013-2015 BST//soft
 */
class m150413_132433_add_crimea extends CDbMigration
{
    public function safeUp()
    {
        $insert_region = $this->dbConnection->createCommand(
            'INSERT INTO `regions` (`parentId`, `title`) VALUES (:parent_id, :title)'
        );
        $this->insert('regions', ['title' => 'Крымский федеральный округ']);
        $okrug_id = $this->dbConnection->getLastInsertID();
        $insert_region->execute([':parent_id' => $okrug_id, ':title' => 'Республика Крым']);
        $insert_region->execute([':parent_id' => $okrug_id, ':title' => 'Город Севастополь']);
    }

    public function safeDown()
    {
        echo 'm150413_132433_add_crimea does not support migration down.' . "\n";
        return true;
    }
}
