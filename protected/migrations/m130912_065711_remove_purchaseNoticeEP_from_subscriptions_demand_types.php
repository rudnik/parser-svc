<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m130912_065711_remove_purchaseNoticeEP_from_subscriptions_demand_types extends CDbMigration {
	public function safeUp() {
		$this->dbConnection->createCommand("
		    DELETE FROM `clients_subscriptions_demand_types` WHERE `demand_type_id` = 4;
		")->execute();
	}

	public function safeDown() {
		echo 'm130912_065711_remove_purchaseNoticeEP_from_subscriptions_demand_types does not support migration down.'."\n";

		return true;
	}
}
