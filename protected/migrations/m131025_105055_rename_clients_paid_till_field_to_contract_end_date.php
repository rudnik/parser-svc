<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m131025_105055_rename_clients_paid_till_field_to_contract_end_date extends CDbMigration {
	public function safeUp() {
		Yii::app()->db->createCommand("
            ALTER TABLE `clients`
              CHANGE COLUMN `paid_till` `contract_end_date` INT(10) UNSIGNED NULL DEFAULT NULL COMMENT 'Дата окончания контракта (unix timestamp)' AFTER `user_id`;
		")->execute();
	}

	public function safeDown() {
		Yii::app()->db->createCommand("
		    ALTER TABLE `clients`
              CHANGE COLUMN `contract_end_date` `paid_till` INT(10) UNSIGNED NULL DEFAULT NULL COMMENT 'Оплачен до (unix timestamp)' AFTER `user_id`;
		")->execute();
	}
}
