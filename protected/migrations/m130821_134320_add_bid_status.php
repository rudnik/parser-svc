<?php

/**
 * Миграция добавления статуса заявки.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013
 */
class m130821_134320_add_bid_status extends CDbMigration {
	public function safeUp() {
		$this->getDbConnection()->createCommand('
			ALTER TABLE `bids`
				ADD COLUMN `status` enum(\'canceled\') DEFAULT NULL COMMENT \'Статус закупки\',
				ADD KEY `status` (`status`)
		')->execute();
	}

	public function safeDown() {
		echo 'm130821_134320_add_bid_status does not support migration down.'."\n";
		return true;
	}
}
