<?php

/**
 * Миграция создание таблиц clients, client_emails, client_importance, 
 * client_phones, client_protocol, client_subscribe_emails
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m130815_085303_add_tables_for_model_clients extends CDbMigration {

    public function safeUp() {
	# переименование clients в clients_old
	$this->renameTable('clients', 'clients_old');
	
	# создание таблицы уровней важности
	$this->dbConnection->createCommand("
			-- Дамп структуры для таблица tenders.client_importance
			DROP TABLE IF EXISTS `client_importance`;
			CREATE TABLE IF NOT EXISTS `client_importance` (
			  `id` tinyint(1) unsigned NOT NULL AUTO_INCREMENT,
			  `title` varchar(50) DEFAULT NULL COMMENT 'Заголовок уровня важности',
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Важность в блоке \"Продажа\"';")->execute();
	# наполнение таблицы уровней важности
	$this->insert('client_importance', ['title' => 'Минимальная важность']);
	$this->insert('client_importance', ['title' => 'Высокая важность']);
	$this->insert('client_importance', ['title' => 'Край географии']);
	
	# создание новой таблицы клиентов
	$this->dbConnection->createCommand("CREATE TABLE `clients` (
			  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id клиента',
			  `title` varchar(256) NOT NULL COMMENT 'Наименование клиента',
			  `email_comment` text COMMENT 'Комментарий к E-mail',
			  `contact_name1` varchar(256) DEFAULT NULL COMMENT 'Контактное лицо 1',
			  `contact_name2` varchar(256) DEFAULT NULL COMMENT 'Контактное лицо 2',
			  `contact_name3` varchar(256) DEFAULT NULL COMMENT 'Контактное лицо 3',
			  `branch_id` smallint(5) unsigned DEFAULT NULL COMMENT 'id отрасли',
			  `career` varchar(256) DEFAULT NULL COMMENT 'Род деятельности',
			  `website` varchar(256) DEFAULT NULL COMMENT 'Веб-сайт',
			  `region_id` tinyint(3) unsigned DEFAULT NULL COMMENT 'id региона',
			  `city_id` int(10) unsigned DEFAULT NULL COMMENT 'id города',
			  `address` varchar(256) DEFAULT NULL COMMENT 'Адрес',
			  `legal_address` varchar(256) DEFAULT NULL COMMENT 'Юридический адрес',
			  `inn` varchar(12) DEFAULT NULL COMMENT 'ИНН',
			  `kpp` varchar(12) DEFAULT NULL COMMENT 'КПП',
			  `manager_id` smallint(5) unsigned DEFAULT NULL COMMENT 'id менеджера',
			  `paid_till` int(10) unsigned DEFAULT NULL COMMENT 'Оплачен до (unix timestamp)',
			  `is_duplication` tinyint(1) unsigned DEFAULT NULL COMMENT 'Дубль',
			  `comment` text COMMENT 'Комментарий',
			  `importance_id` tinyint(1) unsigned DEFAULT NULL COMMENT 'Важность',
			  `next_contact` int(10) unsigned DEFAULT NULL COMMENT 'Дата следующего контакта',
			  PRIMARY KEY (`id`),
			  KEY `FK_clients_client_importance` (`importance_id`),
			  KEY `FK_clients_managers` (`manager_id`),
			  KEY `FK_clients_regions` (`region_id`),
			  KEY `FK_clients_branches` (`branch_id`),
			  KEY `FK_clients_cities` (`city_id`),
			  CONSTRAINT `FK_clients_branches` FOREIGN KEY (`branch_id`) REFERENCES `branches` (`branchId`),
			  CONSTRAINT `FK_clients_cities` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`),
			  CONSTRAINT `FK_clients_client_importance` FOREIGN KEY (`importance_id`) REFERENCES `client_importance` (`id`),
			  CONSTRAINT `FK_clients_managers` FOREIGN KEY (`manager_id`) REFERENCES `managers` (`managerId`),
			  CONSTRAINT `FK_clients_regions` FOREIGN KEY (`region_id`) REFERENCES `regions` (`regionId`)
			) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COMMENT='Клиенты';")->execute();

	# создание таблицы хранения email'ов клиентов
	$this->dbConnection->createCommand("
			-- Дамп структуры для таблица tenders.client_emails
			DROP TABLE IF EXISTS `client_emails`;
			CREATE TABLE IF NOT EXISTS `client_emails` (
			  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
			  `client_id` int(10) unsigned NOT NULL COMMENT 'id клиента',
			  `email` varchar(256) DEFAULT NULL COMMENT 'email клиента',
			  PRIMARY KEY (`id`),
			  KEY `FK_client_emails_clients` (`client_id`),
			  CONSTRAINT `FK_client_emails_clients` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE
			) ENGINE=InnoDB AUTO_INCREMENT=142 DEFAULT CHARSET=utf8 COMMENT='Email''ы клиента';")->execute();

	# создание таблицы хранения телефонов клиентов
	$this->dbConnection->createCommand("
			-- Дамп структуры для таблица tenders.client_phones
			DROP TABLE IF EXISTS `client_phones`;
			CREATE TABLE IF NOT EXISTS `client_phones` (
			  `id` int(10) NOT NULL AUTO_INCREMENT,
			  `client_id` int(10) unsigned NOT NULL COMMENT 'id клиента',
			  `phone` varchar(20) DEFAULT NULL COMMENT 'Номер телефона клиента',
			  PRIMARY KEY (`id`),
			  KEY `FK_client_phones_clients` (`client_id`),
			  CONSTRAINT `FK_client_phones_clients` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE
			) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8 COMMENT='Номера телефонов клиента';")->execute();

	# создание таблицы хранения протоколов общения менеджеров с клиентами
	$this->dbConnection->createCommand("
			-- Дамп структуры для таблица tenders.client_protocol
			DROP TABLE IF EXISTS `client_protocol`;
			CREATE TABLE IF NOT EXISTS `client_protocol` (
			  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
			  `client_id` int(10) unsigned NOT NULL COMMENT 'id клиента',
			  `manager_id` smallint(5) unsigned NOT NULL COMMENT 'id менеджера',
			  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата создания комментария',
			  `comment` text NOT NULL COMMENT 'Комментарий менеджера',
			  PRIMARY KEY (`id`),
			  KEY `FK_client_protocol_clients` (`client_id`),
			  KEY `FK_client_protocol_managers` (`manager_id`),
			  CONSTRAINT `FK_client_protocol_clients` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`),
			  CONSTRAINT `FK_client_protocol_managers` FOREIGN KEY (`manager_id`) REFERENCES `managers` (`managerId`)
			) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Протокол общения с клиентом';")->execute();

	# создание таблицы хранения email'ов рассылки 
	$this->dbConnection->createCommand("
			-- Дамп структуры для таблица tenders.client_subscribe_emails
			DROP TABLE IF EXISTS `client_subscribe_emails`;
			CREATE TABLE IF NOT EXISTS `client_subscribe_emails` (
			  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
			  `client_id` int(10) unsigned NOT NULL COMMENT 'id клиента',
			  `email` varchar(256) DEFAULT NULL COMMENT 'Email для рассылки',
			  PRIMARY KEY (`id`),
			  KEY `FK_client_subscribe_emails_clients` (`client_id`),
			  CONSTRAINT `FK_client_subscribe_emails_clients` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE
			) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8 COMMENT='Email''ы клиента для рассылки';")->execute();
    }

    public function safeDown() {
	$this->dropTable('client_subscribe_emails');
	$this->dropTable('client_protocol');
	$this->dropTable('client_phones');
	$this->dropTable('client_emails');
	$this->dropTable('clients');
	$this->dropTable('client_importance');
	$this->renameTable('clients_old', 'clients');
    }
}
