<?php

/**
 * Миграция переноса наименования закупки.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013
 */
class m130709_141721_diveide_ordername extends CDbMigration {
	public function safeUp() {
		$datas = $this->getDbConnection()->createCommand('
			SELECT
				`bd`.`dataId`
			FROM
				`bids-data` AS `bd`
				JOIN `zakupki-fields` AS `f`
					ON `f`.`fieldId`=`bd`.`fieldId`
					AND `f`.`name`="orderName"
				JOIN `bids` AS `b`
					ON `b`.`bidId`=`bd`.`bidId`
					AND `b`.`created`>=STR_TO_DATE("2013-07-01 00:00:00", "%Y-%m-%d %H:%i:%s")
		')->queryAll();
		$updated_fields = [];

		foreach ($datas as $data) {
			$data_id = (int) $data['dataId'];
			$this->getDbConnection()->createCommand('
				INSERT INTO
					`bids-data-text-search` (`dataId`, `value`)
				SELECT
					'.$data_id.',
					`value`
				FROM
					`bids-data-text`
				WHERE
					`dataId`='.$data_id
			)->execute();
			$this->delete('bids-data-text', 'dataId='.$data_id);
		}
	}

	public function safeDown() {
		echo 'm130709_141721_diveide_ordername does not support migration down.'."\n";
		return true;
	}
}
