<?php

/**
 * Миграция удаления полей дат рассылки.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013
 */
class m130820_092651_remove_subscr_start_date extends CDbMigration {
	public function safeUp() {
		$this->dropColumn('clients-subscriptions', 'publication_date_start');
		$this->dropColumn('clients-subscriptions', 'publication_date_end');
	}

	public function safeDown() {
		echo 'm130820_092651_remove_subscr_start_date does not support migration down.'."\n";
		return true;
	}
}
