<?php

/**
 * Миграция добавления Байконура.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013
 */
class m130614_134037_add_bajkonur_region extends CDbMigration {
	public function safeUp() {
		$this->insert('regions', ['title' => 'Байконур']);
	}

	public function safeDown() {
		echo 'm130614_134037_add_bajkonur_region does not support migration down.'."\n";
		return true;
	}
}
