<?php

/**
 * Миграция создания сводной таблицы для Sphinx.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m130808_122552_create_sphinx_data_table extends CDbMigration {
	public function safeUp() {
		// создание сводной таблицы для Sphinx
		$this->dbConnection->createCommand("
                        CREATE TABLE `sphinx_data` (
                            `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Уникальный Id для Sphinx',
                            `model_name` varchar(64) NOT NULL COMMENT 'Название модели, к которой относятся данные',
                            `model_data_id` bigint(20) unsigned NOT NULL COMMENT 'id записи, которой соответствуют приведенные в этой таблице данные',
                            `title` varchar(2000) NOT NULL COMMENT 'Заголовок заказа',
                            `description` text NOT NULL COMMENT 'Описание заказа',
                            `region_id` tinyint(3) unsigned DEFAULT NULL COMMENT 'Регион заказа',
                            `publication_date` int(10) unsigned DEFAULT NULL COMMENT 'Дата публикации заказа',
                            `quote_date_start` int(10) unsigned DEFAULT NULL COMMENT 'Дата начала подачи котировочных заявок',
                            `quote_date_end` int(10) unsigned DEFAULT NULL COMMENT 'Дата окончания подачи котировочных заявок',
                            `max_cost` decimal(14,2) unsigned DEFAULT NULL COMMENT 'Начальная (максимальная) цена контракта',
                        PRIMARY KEY (`id`),
                        UNIQUE KEY `model_name_model_data_id` (`model_name`,`model_data_id`),
                        KEY `FK_sphinx_data_regions` (`region_id`),
                        CONSTRAINT `FK_sphinx_data_regions` FOREIGN KEY (`region_id`) REFERENCES `regions` (`regionId`)
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Данные для индекса сфинкса, собранные из моделей парсеров.';"
		)->execute();
	}

	public function safeDown() {
		$this->dropTable('sphinx_data');
	}
}
