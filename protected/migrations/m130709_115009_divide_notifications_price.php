<?php

/**
 * Миграция выделения цена извещений.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013
 */
class m130709_115009_divide_notifications_price extends CDbMigration {
	public function safeUp() {
		$datas = $this->getDbConnection()->createCommand('
			SELECT
				`bd`.`dataId`,
				`f`.`fieldId`
			FROM
				`bids-data` AS `bd`
				JOIN `zakupki-fields` AS `f`
					ON `f`.`fieldId`=`bd`.`fieldId`
					AND `f`.`name` IN ("lots/lot/customerRequirements/customerRequirement/maxPrice")
				JOIN `bids` AS `b`
					ON `b`.`bidId`=`bd`.`bidId`
					AND `b`.`created`>=STR_TO_DATE("2013-07-01 00:00:00", "%Y-%m-%d %H:%i:%s")
		')->queryAll();
		$updated_fields = [];

		foreach ($datas as $data) {
			$data_id = (int) $data['dataId'];
			$field_id = (int) $data['fieldId'];

			if (!in_array($field_id, $updated_fields)) {
				$updated_fields[]= $field_id;
				$this->update('zakupki-fields', ['type' => 'num'], 'fieldId='.$field_id);
			}

			$this->getDbConnection()->createCommand('
				INSERT INTO
					`bids-data-num-search` (`dataId`, `value`)
				SELECT
					'.$data_id.',
					`value`
				FROM
					`bids-data-text`
				WHERE
					`dataId`='.$data_id
			)->execute();
			$this->delete('bids-data-text', 'dataId='.$data_id);
		}

		$this->delete('bids-data-date',        'value="0000-00-00 00:00:00"');
		$this->delete('bids-data-date-search', 'value="0000-00-00 00:00:00"');
	}

	public function safeDown() {
		echo 'm130709_115009_divide_notifications_price does not support migration down.'."\n";
		return true;
	}
}
