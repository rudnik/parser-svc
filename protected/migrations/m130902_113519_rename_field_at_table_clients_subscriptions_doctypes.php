<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m130902_113519_rename_field_at_table_clients_subscriptions_doctypes extends CDbMigration {
	public function safeUp() {
		$this->dbConnection->createCommand('SET FOREIGN_KEY_CHECKS = 0;')->execute();
		
		$this->dbConnection->createCommand("
		    ALTER TABLE `clients_subscriptions_doctypes`
			DROP FOREIGN KEY `FK_client_subscriptions_doctypes_zakupki-doctypes`;
		")->execute();
		
		$this->dbConnection->createCommand("
		    ALTER TABLE `clients_subscriptions_doctypes`
			ALTER `zakupki_doctype_id` DROP DEFAULT;
		")->execute();
		
		$this->dbConnection->createCommand("
		    ALTER TABLE `clients_subscriptions_doctypes`
			CHANGE COLUMN `zakupki_doctype_id` `bid_doctype_id` TINYINT(2) UNSIGNED NOT NULL COMMENT 'id типа документа с zakupki.gov.ru.' AFTER `client_subscription_id`;
		")->execute();
	
		$this->dbConnection->createCommand("
		    ALTER TABLE `clients_subscriptions_doctypes`
			ADD CONSTRAINT `FK_clients_subscriptions_doctypes_bids_doctypes` FOREIGN KEY (`bid_doctype_id`) REFERENCES `bids_doctypes` (`doctypeId`) ON DELETE CASCADE;
		")->execute();
	}

	public function safeDown() {
		echo 'm130902_113519_rename_field_at_table_clients_subscriptions_doctypes does not support migration down.'."\n";

		return true;
	}
}
