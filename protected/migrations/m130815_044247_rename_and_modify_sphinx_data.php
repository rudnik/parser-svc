<?php

/**
 * Миграция изменения поисковой таблицы.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013
 */
class m130815_044247_rename_and_modify_sphinx_data extends CDbMigration {
	public function safeUp() {
		$this->truncateTable('sphinx_data');
		$this->renameTable('sphinx_data', 'bids_data_search');
		$this->renameColumn('bids_data_search', 'id', 'search_id');
		$this->dropColumn('bids_data_search', 'model_name');
		$this->dropIndex('model_name_model_data_id', 'bids_data_search');
		$this->renameColumn('bids_data_search', 'model_data_id', 'bid_id');
		$this->createIndex('bid', 'bids_data_search', 'bid_id');
		$this->addForeignKey('fk_bid_search', 'bids_data_search', 'bid_id', 'bids', 'bidId');
		$this->getDbConnection()->createCommand('
			ALTER TABLE `bids_data_search`
				MODIFY COLUMN `search_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT \'Идентификатор поисковой записи\',
				MODIFY COLUMN `bid_id` bigint(20) unsigned NOT NULL COMMENT \'Идентификатор заявки\'
		')->execute();
	}

	public function safeDown() {
		$this->renameTable('bids_data_search', 'sphinx_data');
	}
}
