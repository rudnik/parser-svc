<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m131129_103558_add_additional_fields_to_clients_table extends CDbMigration {
	public function safeUp() {
		Yii::app()->db->createCommand("
            ALTER TABLE `clients`
                ADD COLUMN `client_status` ENUM('Лид','Клиент') NOT NULL DEFAULT 'Лид' COMMENT 'Статус клиента' AFTER `next_contact`,
                ADD COLUMN `action_description` TEXT NOT NULL COMMENT 'Описание деятельности компании' AFTER `client_status`,
                ADD COLUMN `payment_status` ENUM('Оплачено','Не оплачено') NOT NULL DEFAULT 'Не оплачено' COMMENT 'Статус оплаты' AFTER `action_description`;
		")->execute();
	}

	public function safeDown() {
		Yii::app()->db->createCommand("
            ALTER TABLE `clients`
                DROP COLUMN `client_status`,
                DROP COLUMN `action_description`,
                DROP COLUMN `payment_status`;
		")->execute();
	}
}
