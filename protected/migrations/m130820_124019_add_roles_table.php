<?php

/**
 * Миграция добавления таблицы ролей.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m130820_124019_add_roles_table extends CDbMigration {
	# добавление таблицы ролей
	public function safeUp() {
	    $this->dbConnection->createCommand("CREATE TABLE `roles` (
		`id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id роли',
		`code` VARCHAR(64) NOT NULL COMMENT 'Симаольный код роди',
		`title` VARCHAR(80) NOT NULL COMMENT 'Название роли',
		PRIMARY KEY (`id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Роли пользователя'")->execute();
	    $this->insert('roles', ['code' => 'admin', 'title' => 'Администратор']);
	    $this->insert('roles', ['code' => 'manager', 'title' => 'Менеджер']);
	    
	}

	public function safeDown() {
	    $this->dbConnection->createCommand("DROP TABLE `roles`;")->execute();
	}
}
