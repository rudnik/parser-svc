<?php

/**
 * Миграция добавления поля doctypeId.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m130812_122444_add_doctypeId_field_at_table_sphinx_data extends CDbMigration {
	public function safeUp() {
		// добавление поля doctypeId
		$this->dbConnection->createCommand("
                        ALTER TABLE `sphinx_data`
                            ADD COLUMN `doctypeId` TINYINT(2) UNSIGNED NOT NULL COMMENT 'Id типа документа' AFTER `model_data_id`,
                            ADD CONSTRAINT `FK_sphinx_data_zakupki-doctypes` FOREIGN KEY (`doctypeId`) REFERENCES `zakupki-doctypes` (`doctypeId`);
		")->execute();
	}

	public function safeDown() {
            $this->dbConnection->createCommand("
                       ALTER TABLE `sphinx_data`
                            DROP COLUMN `doctypeId`,
                            DROP FOREIGN KEY `FK_sphinx_data_zakupki-doctypes`;
		")->execute();
            
		$this->dropTable('clients_subscriptions_doctypes');
	}
}
