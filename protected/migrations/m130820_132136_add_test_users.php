<?php

/**
 * Миграция добавления тестовых пользователей.
 * 1. admin:123
 * 2. tender:TpCLYdfv9L
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013
 */
class m130820_132136_add_test_users extends CDbMigration {
	# добавление тестовых пользователей
	public function safeUp() {
	    $this->insert('managers', ['email' => 'admin@tender.svc', 
				    'name' => 'Администратор', 
				    'password' => '5067ae4441a48186f47174f64bc4d858', // TpCLYdfv9L1
				    'role_id' => 1,
	    ]);
	    
	    $this->insert('managers', ['email' => 'tender@tender.svc', 
				    'name' => 'Менеджер', 
				    'password' => '822cf14355a47a6d7a76a49ba5f5b923', // TpCLYdfv9L
				    'role_id' => 2,
	    ]);
	}

	public function safeDown() {
	    $this->delete('managers', 'email = :email AND password = :password', [':email' => 'admin@tender.svc', ':password' => '5067ae4441a48186f47174f64bc4d858']);	    
	    $this->delete('managers', 'email = :email AND password = :password', [':email' => 'tender@tender.svc', ':password' => '822cf14355a47a6d7a76a49ba5f5b923']);    
	}
}
