<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author
 * @copyright 2013
 */
class m130625_075718_divide_bids_data_types extends CDbMigration {
	public function safeUp() {
		// типы полей
		$field_types_num = [
			'id', 'versionNumber', 'price', 'priceChangeReason/id',
			'singleCustomerReason/id', 'execution/month', 'execution/year',
			'finances/budgetary/month', 'finances/budgetary/year',
			'finances/budgetary/substageMonth', 'finances/budgetary/substageYear',
			'finances/budgetary/price', 'finances/extrabudgetary/month',
			'finances/extrabudgetary/year', 'finances/extrabudgetary/substageMonth',
			'finances/extrabudgetary/substageYear', 'finances/extrabudgetary/price',
			'products/product/sid', 'products/product/price', 'products/product/quantity',
			'products/product/sum', 'documentMetas/documentMeta/sid'
		];
		$field_types_date = [
			'publishDate', 'signDate', 'protocolDate'
		];
		// добавляем тип поля
 		$this->dbConnection->createCommand('
 			ALTER TABLE `zakupki-fields` ADD COLUMN `type` enum(\'num\', \'text\', \'date\') NOT NULL DEFAULT \'text\' COMMENT \'Тип поля\'
 		')->execute();
 		// таблицы типов данных
 		$this->createTable('bids-data-num', [
 			'valueId' => 'bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT \'Идентификатор значения\'',
 			'dataId'  => 'bigint(20) unsigned NOT NULL DEFAULT \'0\' COMMENT \'Идентификатор данных\'',
 			'value'   => 'decimal(25, 2) NOT NULL DEFAULT \'0\' COMMENT \'Значение\'',
 			'PRIMARY KEY (`valueId`)',
 			'KEY `data` (`dataId`)',
 			'CONSTRAINT `fk_bdn_bd` FOREIGN KEY (`dataId`) REFERENCES `bids-data` (`dataId`) ON DELETE CASCADE ON UPDATE NO ACTION',
 		], 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=\'Числовые данные\'');
 		$this->createTable('bids-data-text', [
 			'valueId' => 'bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT \'Идентификатор значения\'',
 			'dataId'  => 'bigint(20) unsigned NOT NULL DEFAULT \'0\' COMMENT \'Идентификатор данных\'',
 			'value'   => 'text DEFAULT NULL COMMENT \'Значение\'',
 			'PRIMARY KEY (`valueId`)',
 			'KEY `data` (`dataId`)',
 			'KEY `value` (`value`(10))',
 			'CONSTRAINT `fk_bdt_bd` FOREIGN KEY (`dataId`) REFERENCES `bids-data` (`dataId`) ON DELETE CASCADE ON UPDATE NO ACTION',
 		], 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=\'Текстовые данные\'');
 		$this->createTable('bids-data-date', [
 			'valueId' => 'bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT \'Идентификатор значения\'',
 			'dataId'  => 'bigint(20) unsigned NOT NULL DEFAULT \'0\' COMMENT \'Идентификатор данных\'',
 			'value'   => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT \'Значение\'',
 			'PRIMARY KEY (`valueId`)',
 			'KEY `data` (`dataId`)',
 			'CONSTRAINT `fk_bdd_bd` FOREIGN KEY (`dataId`) REFERENCES `bids-data` (`dataId`) ON DELETE CASCADE ON UPDATE NO ACTION',
 		], 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=\'Данные дат\'');
 		$data = $this->dbConnection->createCommand('
 			SELECT
 				`f`.`fieldId`, `f`.`name`, `bd`.`dataId`, `bdv`.`value`
			FROM
 				`zakupki-fields` AS `f`
 				JOIN `bids-data` AS `bd`
 					ON `bd`.`fieldId`=`f`.`fieldId`
 				JOIN `bids-data-values` AS `bdv`
 					ON `bdv`.`dataId`=`bd`.`dataId`
 		')->queryAll();

 		foreach ($data as $d) {
 			$type = in_array($d['name'], $field_types_date)
 				? 'date'
 				: (in_array($d['name'], $field_types_num) ? 'num' : 'text');
 			$this->dbConnection->createCommand('
 				UPDATE `zakupki-fields` SET `type`="'.$type.'" WHERE `fieldId`='.(int) $d['fieldId']
 			)->execute();

 			$table = 'bids-data-'.$type;
 			$value = $d['value'];

 			if ('date' == $type) {
 				$ts = new DateTime($value);
 				$value = $ts->format('Y-m-d H:i:s');
 			}

 			$this->dbConnection->createCommand('
 				INSERT INTO `'.$table.'`
 				SET
 					`dataId`='.(int) $d['dataId'].',
 					`value`='.$this->dbConnection->quoteValue($value).'
 			')->execute();
 		}

 		$this->dropTable('bids-data-values');
	}

	public function safeDown() {
		$this->dropTable('bids-data-date');
		$this->dropTable('bids-data-text');
		$this->dropTable('bids-data-num');
	}
}
