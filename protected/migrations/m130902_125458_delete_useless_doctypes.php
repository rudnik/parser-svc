<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m130902_125458_delete_useless_doctypes extends CDbMigration {
	public function safeUp() {
	    $this->dbConnection->createCommand("DELETE FROM `bids_doctypes` WHERE `name` NOT IN (
		'notificationOK',
		'notificationEF',
		'notificationZK',
		'notificationPO',
		'notificationSZ',
		'notificationCancel',
		'protocolOK1',
		'protocolOK2',
		'protocolOK3',
		'protocolEF2',
		'protocolEF3',
		'protocolZK1',
		'protocolZK5',
		'protocolPO1',
		'contract'
		);")->execute();
	}

	public function safeDown() {
		echo 'm130902_125458_delete_useless_doctypes does not support migration down.'."\n";

		return true;
	}
}
