<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m130917_101920_add_client_subscription_sended_bids_table extends CDbMigration {
	public function safeUp() {
		$this->dbConnection->createCommand('SET FOREIGN_KEY_CHECKS = 0;')->execute();
        $this->dbConnection->createCommand("
            CREATE TABLE `client_sended_bids` (
                `id` BIGINT(19) UNSIGNED NOT NULL AUTO_INCREMENT,
                `clientId` INT(11) UNSIGNED NOT NULL COMMENT 'id клиента',
                `bidId` BIGINT(19) UNSIGNED NOT NULL COMMENT 'id заявки',
                `date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Время отправки заявки',
                PRIMARY KEY (`id`),
                UNIQUE INDEX `clientId_bidId` (`clientId`, `bidId`),
                CONSTRAINT `FK_client_sended_bids_clients` FOREIGN KEY (`clientId`) REFERENCES `clients` (`id`) ON DELETE CASCADE,
                CONSTRAINT `FK_client_sended_bids_bids` FOREIGN KEY (`bidId`) REFERENCES `bids` (`bidId`) ON DELETE CASCADE
            )
            COMMENT='Зяавки, отправленный клиенту'
            COLLATE='utf8_general_ci'
            ENGINE=InnoDB;
        ")->execute();
	}

	public function safeDown() {
        $this->dbConnection->createCommand('SET FOREIGN_KEY_CHECKS = 0;')->execute();
        $this->dropTable('client_sended_bids');
	}
}
