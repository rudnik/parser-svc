<?php

/**
 * Миграция создания общей таблицы для данных.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013
 */
class m130816_090438_add_all_data_table extends CDbMigration {
	public function safeUp() {
		$this->createTable('bids_data_values', [
			'valueId' => 'bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT \'Идентификатор значения\'',
			'dataId'  => 'bigint(20) unsigned NOT NULL DEFAULT \'0\' COMMENT \'Идентификатор данных\'',
			'value'   => 'longtext DEFAULT NULL COMMENT \'Значение\'',
			'PRIMARY KEY (`valueId`)',
			'KEY `data` (`dataId`)',
			'CONSTRAINT `fk_bdv_bd` FOREIGN KEY (`dataId`) REFERENCES `bids-data` (`dataId`) ON DELETE CASCADE ON UPDATE NO ACTION',
		], 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=\'Все данные закупки\'');
	}

	public function safeDown() {
		$this->dropTable('bids_data_values');
	}
}
