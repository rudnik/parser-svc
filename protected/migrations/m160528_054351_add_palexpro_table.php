<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013-2016 BST//soft
 */
class m160528_054351_add_palexpro_table extends CDbMigration
{
    public function safeUp()
    {
        $client_id = 2192;
        $this->createTable('bids_export', [
            'bidId'    => 'bigint(19) unsigned NOT NULL COMMENT "Идентификатор заявки"',
            'clientId' => 'int(11) unsigned NOT NULL COMMENT "Идентификатор клиента"',
            'PRIMARY KEY (`bidId`, `clientId`)',
            'CONSTRAINT `fk_bid_export` FOREIGN KEY (`bidId`) REFERENCES `bids` (`bidId`) ON DELETE CASCADE ON UPDATE NO ACTION',
            'CONSTRAINT `fk_client_export` FOREIGN KEY (`clientId`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION'
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT="Экспортируемые данные для других источников"');
        $bids = $this->dbConnection->createCommand('SELECT bidId FROM clients_sended_bids WHERE clientId=:client_id')
            ->queryColumn([':client_id' => $client_id]);
        $client_bids = $this->dbConnection->createCommand('SELECT bids_cache FROM clients_subscriptions WHERE clientId=:client_id')
            ->queryAll(true, [':client_id' => $client_id]);
        foreach ($client_bids as $client_bid) {
            $cbids = explode(',', $client_bid['bids_cache']);
            $bids = array_merge($bids, $cbids);
        }
        $bids = array_unique($bids);
        foreach ($bids as $bid) {
            $bid_id = $this->dbConnection->createCommand('SELECT bidId FROM bids WHERE bidId=:bid_id')
                ->queryScalar([':bid_id' => $bid]);
            if ($bid_id) {
                $this->dbConnection->createCommand('INSERT IGNORE INTO bids_export VALUES(:bid_id, :client_id)')
                    ->execute([':bid_id' => $bid_id, ':client_id' => $client_id]);
            }
        }
    }

    public function safeDown()
    {
        $this->dropTable('bids_export');

        return true;
    }
}
