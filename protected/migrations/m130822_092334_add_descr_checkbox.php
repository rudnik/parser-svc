<?php

/**
 * Миграция добавления чекбокса поиска по дополнительным полям.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013
 */
class m130822_092334_add_descr_checkbox extends CDbMigration {
	public function safeUp() {
		$this->getDbConnection()->createCommand('
			ALTER TABLE `clients-subscriptions`
				ADD COLUMN `keywords_advance` tinyint(1) unsigned NOT NULL DEFAULT \'0\' COMMENT \'Поиск по расширенным полям\' AFTER `keywords_exc`
		')->execute();
	}

	public function safeDown() {
		echo 'm130822_092334_add_descr_checkbox does not support migration down.'."\n";
		return true;
	}
}
