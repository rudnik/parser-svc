<?php

/**
 * Миграция конвертации регионов из вида хранения в одном поле через запятую в связующую таблицу.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m130812_093353_convert_client_subcription_regions_to_table_client_subscriptions_regions extends CDbMigration {
	public function safeUp() {
		$clientSubscriptionsModels = ClientsSubscriptions::model()->findAll();
                # Перебираем все подписки
                foreach($clientSubscriptionsModels as $clientSubscriptionsModel) {
                    $subscriptionRegions = explode(',', $clientSubscriptionsModel->regionId);
                    # Перебираем все регионы подписки
                    foreach($subscriptionRegions as $subscriptionRegion) {
                        # Формируем связующую таблицу
                        $subscriptionsRegions = new ClientsSubscriptionsRegions();
                        $subscriptionsRegions->client_subscription_id = $clientSubscriptionsModel->subscrId;
                        $subscriptionsRegions->region_id = intval($subscriptionRegion);
                        if( ! $subscriptionsRegions->save()) return false;
                    }
                }
	}

	public function safeDown() {
		ClientsSubscriptionsRegions::model()->deleteAll();
		return true;
	}
}
