<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013
 */
class m130820_124857_add_role_field_to_managers_table extends CDbMigration {
	public function safeUp() {
	    $this->dbConnection->createCommand("SET FOREIGN_KEY_CHECKS=0;")->execute();
	    $this->dbConnection->createCommand("ALTER TABLE `managers`
		ADD COLUMN `role_id` TINYINT(3) UNSIGNED NOT NULL COMMENT 'Роль пользователя' AFTER `password`,
		ADD CONSTRAINT `FK_managers_roles` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);
	    ")->execute();
	}

	public function safeDown() {
	    $this->dbConnection->createCommand("SET FOREIGN_KEY_CHECKS=0;")->execute();
	    $this->dbConnection->createCommand("ALTER TABLE `managers`
		DROP COLUMN `role_id`,
		DROP FOREIGN KEY `FK_managers_roles`;;")->execute();
	}
}
