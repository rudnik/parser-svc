<?php

/**
 * Миграция обновления адресов.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013-2015 BST//soft
 */
class m150427_085524_update_addresses extends CDbMigration
{
    public $connect = null;

    public $columns = array(
        'AOLEVEL',
        'AREACODE',
        'AUTOCODE',
        'CITYCODE',
        'CTARCODE',
        'EXTRCODE',
        'NEXTID',
        'OFFNAME',
        'PARENTGUID',
        'PLACECODE',
        'POSTALCODE',
        'PREVID',
        'REGIONCODE',
        'SEXTCODE',
        'SHORTNAME',
        'STREETCODE'
    );

    public function __construct(){
        $this->connect = mysqli_connect('localhost','tender','tZq9Deof9e','tenders','3306') or die("Error " . mysqli_error($this->connect));
        if (mysqli_connect_error()) {
            die('Ошибка подключения (' . mysqli_connect_errno() . ') '
                . mysqli_connect_error());
        } else {
            $this->connect->query('SET NAMES UTF8');
        }
    }

    public function safeUp()
    {
        return true;
        $filename = __DIR__ . '/ADDROBJ.DBF';

        if (!file_exists($filename)) {
            echo 'File with addresses not found.' . "\n";
            return false;
        }

        $initAddresses = function($row, $action = 'insert'){
            if ($action === 'insert'){
                $sql = 'INSERT INTO `tenders`.`addresses` ('.implode(',' , $this->columns).',AOGUID,AOID'.') VALUES('.
                    mysqli_real_escape_string($this->connect, intval($row['AOLEVEL'])).', '.
                    mysqli_real_escape_string($this->connect, intval($row['AREACODE'])).', '.
                    mysqli_real_escape_string($this->connect, intval($row['AUTOCODE'])).', '.
                    mysqli_real_escape_string($this->connect, intval($row['CITYCODE'])).', '.
                    mysqli_real_escape_string($this->connect, intval($row['CTARCODE'])).', '.
                    mysqli_real_escape_string($this->connect, intval($row['EXTRCODE'])).', '.
                    "'".mysqli_real_escape_string($this->connect, trim($row['NEXTID']))."', ".
                    "'".mysqli_real_escape_string($this->connect, trim($row['OFFNAME']))."', ".
                    "'".mysqli_real_escape_string($this->connect, trim($row['PARENTGUID']))."', ".
                    mysqli_real_escape_string($this->connect, intval($row['PLACECODE'])).', '.
                    mysqli_real_escape_string($this->connect, intval($row['POSTALCODE'])).', '.
                    "'".mysqli_real_escape_string($this->connect, trim($row['PREVID']))."', ".
                    mysqli_real_escape_string($this->connect, intval($row['REGIONCODE'])).', '.
                    mysqli_real_escape_string($this->connect, intval($row['SEXTCODE'])).', '.
                    "'".mysqli_real_escape_string($this->connect, trim($row['SHORTNAME']))."', ".
                    mysqli_real_escape_string($this->connect, intval($row['STREETCODE'])).', '.
                    "'".mysqli_real_escape_string($this->connect, trim($row['AOGUID']))."', ".
                    "'".mysqli_real_escape_string($this->connect, trim($row['AOID']))."');"
                ;
            } elseif ($action === 'update') {
                $sql = $sql = 'UPDATE `tenders`.`addresses` SET '.
                    'AOLEVEL = '. mysqli_real_escape_string($this->connect, intval($row['AOLEVEL'])).', '.
                    'AREACODE = '. mysqli_real_escape_string($this->connect, intval($row['AREACODE'])).', '.
                    'AUTOCODE = '. mysqli_real_escape_string($this->connect, intval($row['AUTOCODE'])).', '.
                    'CITYCODE = '. mysqli_real_escape_string($this->connect, intval($row['CITYCODE'])).', '.
                    'CTARCODE = '. mysqli_real_escape_string($this->connect, intval($row['CTARCODE'])).', '.
                    'EXTRCODE = '. mysqli_real_escape_string($this->connect, intval($row['EXTRCODE'])).', '.
                    "NEXTID =  '".mysqli_real_escape_string($this->connect, trim($row['NEXTID']))."', ".
                    "OFFNAME =  '".mysqli_real_escape_string($this->connect, trim($row['OFFNAME']))."', ".
                    "PARENTGUID =  '".mysqli_real_escape_string($this->connect, trim($row['PARENTGUID']))."', ".
                    'PLACECODE = '. mysqli_real_escape_string($this->connect, intval($row['PLACECODE'])).', '.
                    'POSTALCODE = '. mysqli_real_escape_string($this->connect, intval($row['POSTALCODE'])).', '.
                    "PREVID = '".mysqli_real_escape_string($this->connect, trim($row['PREVID']))."', ".
                    'REGIONCODE = '. mysqli_real_escape_string($this->connect, intval($row['REGIONCODE'])).', '.
                    'SEXTCODE = '. mysqli_real_escape_string($this->connect, intval($row['SEXTCODE'])).', '.
                    "SHORTNAME = '".mysqli_real_escape_string($this->connect, trim($row['SHORTNAME']))."', ".
                    'STREETCODE = '. mysqli_real_escape_string($this->connect, intval($row['STREETCODE'])).
                    " WHERE AOGUID = '" . $row['AOGUID']."';"
                ;
            } else {
                $sql = false;
            }

            //echo $sql; exit;

            return $sql;

        };

        $db = dbase_open($filename, 0);
        $new = $exists = $saved = 0;
        if ($db && $this->connect) {
            $record_numbers = dbase_numrecords($db);

            for ($i = 1; $i <= $record_numbers; $i++) {
                $row = dbase_get_record_with_names($db, $i);

                foreach ($row as $key => &$value) {
                    if (in_array($key, ['ACTSTATUS', 'CENTSTATUS', 'CODE', 'CURRSTATUS', 'ENDDATE', 'FORMALNAME', 'IFNSFL'])) {
                        unset($row[$key]);
                    }
                    $value = mb_convert_encoding($value, 'utf8', 'ibm866');
                }

                $sql = 'SELECT COUNT(*) as cnt FROM `tenders`.`addresses` WHERE AOGUID = \'' . trim($row['AOGUID']) . '\'';

                /** @var mysqli_result $result */
                $resultExist = $this->connect->query($sql);

                if ($resultExist instanceof mysqli_result){
                    if ($resultExist->fetch_object()->cnt > 0){
                        //Если адрес уже существует
                        $exists++;
                        if ($sql = $initAddresses($row, 'update')){
                            //echo $sql; exit;
                            $result = $this->connect->query($sql);
                        } else {
                            $result = false;
                        }
                    } else {
                        $new++;
                        if ($sql = $initAddresses($row)){
                            //echo $sql; exit;
                            $result = $this->connect->query($sql);
                        } else {
                            $result = false;
                        }
                    }
                    $resultExist->free_result();
                } else {
                    $result = false;
                }

                if ($result){
                    if ($result instanceof mysqli_result){
                        $result->free_result();
                    }
                    $saved++;
                } else {
                    die ("\n\nError insert or update row: " . (mysqli_error($this->connect)));
                }

                unset($address);

                print 'Estimated: '. ($record_numbers - $i) . ". Exists: $exists. New: $new. Saved: $saved          \r";
            }

            dbase_close($db);
            $this->connect->close();
        }
        //die("\n\n$record_numbers - $exists - $new\n");
        return true;
    }

    public function safeDown() {
        echo 'm150427_085524_update_addresses does not support migration down.'."\n";

        return true;
    }

//    public function safeUp() {
//        $filename = __DIR__ . '/ADDROBJ.DBF';
//        if (!file_exists($filename)) {
//            echo 'File with addresses not found.' . "\n";
//            return false;
//        }
//
//        $initAddresses = function(Addresses &$address, $row){
//            $address->AOLEVEL    = intval($row['AOLEVEL']);
//            $address->AREACODE   = intval($row['AREACODE']);
//            $address->AUTOCODE   = intval($row['AUTOCODE']);
//            $address->CITYCODE   = intval($row['CITYCODE']);
//            $address->CTARCODE   = intval($row['CTARCODE']);
//            $address->EXTRCODE   = intval($row['EXTRCODE']);
//            $address->NEXTID     = $row['NEXTID'];
//            $address->OFFNAME    = $row['OFFNAME'];
//            $address->PARENTGUID = $row['PARENTGUID'];
//            $address->PLACECODE  = intval($row['PLACECODE']);
//            $address->POSTALCODE = intval($row['POSTALCODE']);
//            $address->PREVID     = $row['PREVID'];
//            $address->REGIONCODE = intval($row['REGIONCODE']);
//            $address->SEXTCODE   = intval($row['SEXTCODE']);
//            $address->SHORTNAME  = $row['SHORTNAME'];
//            $address->STREETCODE = intval($row['STREETCODE']);
//        };
//
//        $db = dbase_open($filename, 0);
//        $new = $exists = $saved = 0;
//        if ($db) {
//            $record_numbers = dbase_numrecords($db);
//
//            for ($i = 1; $i <= $record_numbers; $i++) {
//                $row = dbase_get_record_with_names($db, $i);
//
//                foreach ($row as $key => &$value) {
//                    if (in_array($key, ['ACTSTATUS', 'CENTSTATUS', 'CODE', 'CURRSTATUS', 'ENDDATE', 'FORMALNAME', 'IFNSFL'])) {
//                        unset($row[$key]);
//                    }
//                    $value = mb_convert_encoding($value, 'utf8', 'ibm866');
//                }
//
//                /** @var Addresses $address */
//                $address = Addresses::model()->find(
//                    'AOGUID=:AOGUID',
//                    [':AOGUID' => trim($row['AOGUID'])]
//                );
//
//                if ($address){
//                    $exists++;
//                    $initAddresses($address, $row);
//                } else {
//                    $address = new Addresses();
//                    $address->AOID = $row['AOID'];
//                    $address->AOGUID = $row['AOGUID'];
//                    $initAddresses($address, $row);
//                    $new++;
//                }
//
//                if ($address->save()){
//                    $saved++;
//                } else {
//                    die ("\n\nError seve or update row: " . print_r($address->getErrors()));
//                }
//
//                unset($address);
//
//                print 'Estimated: '. ($record_numbers - $i) . ". Exists: $exists. New: $new. Saved: $saved\r";
//            }
//
//            dbase_close($db);
//        }
//        die("\n\n$record_numbers - $exists - $new\n");
//        return false;
//    }
}
