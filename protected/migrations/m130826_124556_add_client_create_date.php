<?php

/**
 * Миграция добавления поля даты создания клиента.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013
 */
class m130826_124556_add_client_create_date extends CDbMigration {
	public function safeUp() {
		$this->getDbConnection()->createCommand('
			ALTER TABLE `clients`
				ADD COLUMN `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT \'Дата создания клиента\'
		')->execute();
		$this->update('clients', ['created' => date('Y-m-d H:i:s')]);
	}

	public function safeDown() {
		echo 'm130826_124556_add_client_create_date does not support migration down.'."\n";
		return true;
	}
}
