<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m130823_152004_add_subscriptions_emails_table extends CDbMigration {
	public function safeUp() {
		$this->dbConnection->createCommand("CREATE TABLE `clients_subscriptions_emails` (
			`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
			`client_subscription_id` INT(10) UNSIGNED NOT NULL COMMENT 'id рассылки',
			`email` VARCHAR(256) NULL DEFAULT NULL COMMENT 'email рассылки',
			PRIMARY KEY (`id`),
			INDEX `FK_clients_subscriptions_emails_clients_subscriptions` (`client_subscription_id`),
			CONSTRAINT `FK_clients_subscriptions_emails_clients_subscriptions` FOREIGN KEY (`client_subscription_id`) REFERENCES `clients-subscriptions` (`subscrId`) ON DELETE CASCADE
		)
		COMMENT='Email\'ы рассылки'
		COLLATE='utf8_general_ci'
		ENGINE=InnoDB;")->execute();
		
		$this->dbConnection->createCommand("
		    ALTER TABLE `clients-subscriptions`
			    DROP COLUMN `emails`;")->execute();
		
		
	}

	public function safeDown() {
		echo 'm130823_152004_add_subscriptions_emails_table does not support migration down.'."\n";
		return true;
	}
}
