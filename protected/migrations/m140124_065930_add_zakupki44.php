<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m140124_065930_add_zakupki44 extends CDbMigration {
	public function safeUp() {
        Yii::app()->db->createCommand("INSERT INTO `portals` (`portalId`, `name`, `title`, `url`) VALUES (3, 'zakupki44', 'Портал госзакупок 44ФЗ', 'http://zakupki.gov.ru/wps/portal/base/topmain/home');")->execute();
        Yii::app()->db->createCommand("INSERT INTO `bids_doctypes` (`portalId`, `name`, `descr`) VALUES (3, 'fcsNotificationCancel', 'Извещение об отмене определения поставщика');")->execute();
        Yii::app()->db->createCommand("INSERT INTO `bids_doctypes` (`portalId`, `name`, `descr`) VALUES (3, 'fcsNotificationLotCancel', 'Извещение об отмене определения поставщика в части лота');")->execute();
        Yii::app()->db->createCommand("INSERT INTO `bids_doctypes` (`portalId`, `name`, `descr`) VALUES (3, 'fcsNotificationEF', 'Электронный аукцион');")->execute();
        Yii::app()->db->createCommand("INSERT INTO `bids_doctypes` (`portalId`, `name`, `descr`) VALUES (3, 'fcsNotificationEP', 'Закупка единственного поставщика');")->execute();
        Yii::app()->db->createCommand("INSERT INTO `bids_doctypes` (`portalId`, `name`, `descr`) VALUES (3, 'fcsNotificationISM', 'Иной способ (ISM)');")->execute();
        Yii::app()->db->createCommand("INSERT INTO `bids_doctypes` (`portalId`, `name`, `descr`) VALUES (3, 'fcsNotificationISO', 'Иной способ (ISO)');")->execute();
        Yii::app()->db->createCommand("INSERT INTO `bids_doctypes` (`portalId`, `name`, `descr`) VALUES (3, 'fcsNotificationOKD', 'Двухэтапный конкурс');")->execute();
        Yii::app()->db->createCommand("INSERT INTO `bids_doctypes` (`portalId`, `name`, `descr`) VALUES (3, 'fcsNotificationOKOU', 'Конкурс с ограниченным участием');")->execute();
        Yii::app()->db->createCommand("INSERT INTO `bids_doctypes` (`portalId`, `name`, `descr`) VALUES (3, 'fcsNotificationOK', 'Открытый конкурс');")->execute();
        Yii::app()->db->createCommand("INSERT INTO `bids_doctypes` (`portalId`, `name`, `descr`) VALUES (3, 'fcsNotificationPO', 'Предварительный отбор');")->execute();
        Yii::app()->db->createCommand("INSERT INTO `bids_doctypes` (`portalId`, `name`, `descr`) VALUES (3, 'fcsNotificationZakA', 'Закрытый аукцион');")->execute();
        Yii::app()->db->createCommand("INSERT INTO `bids_doctypes` (`portalId`, `name`, `descr`) VALUES (3, 'fcsNotificationZakKD', 'Закрытый двухэтапный конкурс');")->execute();
        Yii::app()->db->createCommand("INSERT INTO `bids_doctypes` (`portalId`, `name`, `descr`) VALUES (3, 'fcsNotificationZakKOU', 'Закрытый конкурс с ограниченным участием');")->execute();
        Yii::app()->db->createCommand("INSERT INTO `bids_doctypes` (`portalId`, `name`, `descr`) VALUES (3, 'fcsNotificationZakK', 'Закрытый конкурс');")->execute();
        Yii::app()->db->createCommand("INSERT INTO `bids_doctypes` (`portalId`, `name`, `descr`) VALUES (3, 'fcsNotificationZKBI', 'Общая информация об объекте закупки (ЗК-БИ)');")->execute();
        Yii::app()->db->createCommand("INSERT INTO `bids_doctypes` (`portalId`, `name`, `descr`) VALUES (3, 'fcsNotificationZK', 'Запрос котировок');")->execute();
        Yii::app()->db->createCommand("INSERT INTO `bids_doctypes` (`portalId`, `name`, `descr`) VALUES (3, 'fcsNotificationZP', 'Запрос предложений');")->execute();
        Yii::app()->db->createCommand("INSERT INTO `bids_doctypes` (`portalId`, `name`, `descr`) VALUES (3, 'fcsNotificationLotModificationType', 'Извещение о внесении изменений в извещение в части лота');")->execute();
	}

	public function safeDown() {
		echo 'm140124_065930_add_zakupki44 does not support migration down.'."\n";

		return true;
	}
}
