<?php

/**
 * Миграция создания таблиц рассылок клиента.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013
 */
class m130624_072727_create_subscriptions_table extends CDbMigration {
	public function safeUp() {
		$this->createTable('clients-subscriptions', [
			'subscrId' => 'int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT \'Идентификатор рассылки\'',
			'clientId' => 'int(11) unsigned NOT NULL DEFAULT \'0\' COMMENT \'Идентификатор клиента\'',
			'emails'   => 'varchar(1024) NOT NULL DEFAULT \'\' COMMENT \'Список адресов рассылки\'',
			'PRIMARY KEY (`subscrId`)',
			'KEY `client` (`clientId`)',
			'CONSTRAINT `fk_cs_client` FOREIGN KEY (`clientId`) REFERENCES `clients` (`clientId`) ON DELETE CASCADE ON UPDATE NO ACTION',
		], 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=\'Рассылки клиентов\'');
		$this->createTable('clients-subscriptions-values', [
			'valueId'  => 'bigint(12) unsigned NOT NULL AUTO_INCREMENT COMMENT \'Идентификатор данных рассылки\'',
			'subscrId' => 'int(11) unsigned NOT NULL DEFAULT \'0\' COMMENT \'Идентификатор рассылки\'',
			'fieldId'  => 'smallint(5) unsigned NOT NULL DEFAULT \'0\' COMMENT \'Идентификатор поля\'',
			'value'    => 'varchar(1024) NOT NULL DEFAULT \'\' COMMENT \'Значение поиска\'',
			'PRIMARY KEY (`valueId`)',
			'KEY `subscr` (`subscrId`)',
			'KEY `field` (`fieldId`)',
			'CONSTRAINT `fk_csv_subscr` FOREIGN KEY (`subscrId`) REFERENCES `clients-subscriptions` (`subscrId`) ON DELETE CASCADE ON UPDATE NO ACTION',
			'CONSTRAINT `fk_csv_fields` FOREIGN KEY (`fieldId`) REFERENCES `zakupki-fields` (`fieldId`) ON DELETE CASCADE ON UPDATE NO ACTION',
		], 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=\'Параметры поиска для рассылок\'');
	}

	public function safeDown() {
		$this->dropTable('clients-subscriptions-values');
		$this->dropTable('clients-subscriptions');
	}
}
