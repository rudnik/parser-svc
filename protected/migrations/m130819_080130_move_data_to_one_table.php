<?php

/**
 * Миграция перемещения всех данных в одну таблицу.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013
 */
class m130819_080130_move_data_to_one_table extends CDbMigration {
	public function safeUp() {
		foreach (['date', 'date-search', 'num', 'num-search', 'text', 'text-search'] as $table) {
			$this->getDbConnection()->createCommand('
				INSERT INTO
					`bids_data_values` (`dataId`, `value`)
				SELECT `dataId`, `value` FROM `bids-data-'.$table.'`
			')->execute();
		}
	}

	public function safeDown() {
		echo 'm130819_080130_move_data_to_one_table does not support migration down.'."\n";
		return true;
	}
}
