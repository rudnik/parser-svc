<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m131214_113238_add_mail_signature_to_users extends CDbMigration {
	public function safeUp() {
		Yii::app()->db->createCommand("
		    ALTER TABLE `users`
	            ADD COLUMN `mail_signature` TEXT NOT NULL DEFAULT '' COMMENT 'Подпись, добавляемая в конец письма рассылки' AFTER `password`;
		")->execute();
	}

	public function safeDown() {
		$this->dropColumn('users', 'mail_signature');
	}
}
