<?php

/**
 * Миграция изменения поисковой таблицы.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013
 */
class m130821_130037_change_search_table extends CDbMigration {
	public function safeUp() {
		$this->truncateTable('bids_data_search');
		$this->getDbConnection()->createCommand('
			ALTER TABLE `bids_data_search`
				MODIFY COLUMN `publication_date` int(10) unsigned NOT NULL DEFAULT \'0\' COMMENT \'Дата публикации заказа\',
				MODIFY COLUMN `quote_date_end` int(10) unsigned NOT NULL DEFAULT \'0\' COMMENT \'Дата окончания подачи котировочных заявок\',
				MODIFY COLUMN `max_cost` decimal(14,2) unsigned NOT NULL DEFAULT \'0\' COMMENT \'Начальная (максимальная) цена контракта\',
				DROP KEY `FK_sphinx_data_regions`,
				DROP KEY `FK_sphinx_data_zakupki-doctypes`,
				DROP KEY `bid`,
				DROP FOREIGN KEY `fk_bid_search`,
				DROP FOREIGN KEY `FK_sphinx_data_regions`,
				DROP FOREIGN KEY `FK_sphinx_data_zakupki-doctypes`,
				ADD KEY `bid` (`bid_id`),
				ADD KEY `region` (`region_id`),
				ADD KEY `doctype` (`doctypeId`),
				ADD CONSTRAINT `fk_search_bid` FOREIGN KEY (`bid_id`) REFERENCES `bids` (`bidId`) ON DELETE CASCADE ON UPDATE NO ACTION,
				ADD CONSTRAINT `fk_search_region` FOREIGN KEY (`region_id`) REFERENCES `regions` (`regionId`) ON DELETE CASCADE ON UPDATE NO ACTION,
				ADD CONSTRAINT `fk_search_doctype` FOREIGN KEY (`doctypeId`) REFERENCES `zakupki-doctypes` (`doctypeId`) ON DELETE CASCADE ON UPDATE NO ACTION
		')->execute();
	}

	public function safeDown() {
		echo 'm130821_130037_change_search_table does not support migration down.'."\n";
		return true;
	}
}
