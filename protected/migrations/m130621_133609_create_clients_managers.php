<?php

/**
 * Миграция создания клиентов, менеджеров, отраслей.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013
 */
class m130621_133609_create_clients_managers extends CDbMigration {
	public function safeUp() {
		// таблица отраслей
		$this->createTable('branches', [
			'branchId' => 'smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT \'Идентификатор отрасли\'',
			'parentId' => 'smallint(5) unsigned NOT NULL DEFAULT \'0\' COMMENT \'Идентификатор родительской отрасли\'',
			'title'    => 'varchar(255) NOT NULL DEFAULT \'\' COMMENT \'Наименование\'',
			'PRIMARY KEY (`branchId`)'
		], 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=\'Отрасли\'');
		// таблица менеджеров
		$this->createTable('managers', [
			'managerId' => 'smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT \'Идентификатор менеджера\'',
			'name'      => 'varchar(255) NOT NULL DEFAULT \'\' COMMENT \'ФИО\'',
			'PRIMARY KEY (`managerId`)'
		], 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=\'Менеджеры\'');
		// таблица клиентов
		$this->createTable('clients', [
			'clientId'    => 'int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT \'Идентификатор клиента\'',
			'regionId'    => 'tinyint(3) unsigned NOT NULL DEFAULT \'0\' COMMENT \'Идентификатор региона\'',
			'branchId'    => 'smallint(5) unsigned NOT NULL DEFAULT \'0\' COMMENT \'Идентификатор отрасли\'',
			'inn'         => 'bigint(12) unsigned NOT NULL DEFAULT \'0\' COMMENT \'ИНН\'',
			'kpp'         => 'bigint(9) unsigned NOT NULL DEFAULT \'0\' COMMENT \'КПП\'',
			'title'       => 'varchar(255) NOT NULL DEFAULT \'\' COMMENT \'Название\'',
			'email'       => 'varchar(255) NOT NULL DEFAULT \'\' COMMENT \'E-mail\'',
			'fio'         => 'varchar(100) NOT NULL DEFAULT \'\' COMMENT \'ФИО отвественного\'',
			'post'        => 'varchar(50) NOT NULL DEFAULT \'\' COMMENT \'Должность\'',
			'trueAddress' => 'varchar(255) NOT NULL DEFAULT \'\' COMMENT \'Фактический адрес\'',
			'jureAddress' => 'varchar(255) NOT NULL DEFAULT \'\' COMMENT \'Юридический адрес\'',
			'phone'       => 'varchar(20) NOT NULL DEFAULT \'\' COMMENT \'Телефон\'',
			'PRIMARY KEY (`clientId`)',
			'UNIQUE KEY `client` (`inn`, `kpp`)',
			'KEY `region` (`regionId`)',
			'KEY `branch` (`branchId`)',
			'KEY `title` (`title`(5))',
			'CONSTRAINT `fk_client_region` FOREIGN KEY (`regionId`) REFERENCES `regions` (`regionId`) ON DELETE CASCADE ON UPDATE NO ACTION',
			'CONSTRAINT `fk_client_branch` FOREIGN KEY (`branchId`) REFERENCES `branches` (`branchId`) ON DELETE CASCADE ON UPDATE NO ACTION'
		], 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=\'Клиенты\'');
		// таблица протоколов клиентов
		$this->createTable('clients-protocols', [
			'protocolId' => 'int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT \'Иденификатор протокола\'',
			'managerId'  => 'smallint(5) unsigned NOT NULL DEFAULT \'0\' COMMENT \'Идентификатор менеджера\'',
			'clientId'   => 'int(11) unsigned NOT NULL DEFAULT \'0\' COMMENT \'Иденификатор клиента\'',
			'descr'      => 'text DEFAULT NULL COMMENT \'Описание\'',
			'created'    => 'timestamp DEFAULT CURRENT_TIMESTAMP COMMENT \'Дата создания\'',
			'PRIMARY KEY (`protocolId`)',
			'KEY `manager` (`managerId`)',
			'KEY `client` (`clientId`)',
			'CONSTRAINT `fk_cp_manager` FOREIGN KEY (`managerId`) REFERENCES `managers` (`managerId`) ON DELETE CASCADE ON UPDATE NO ACTION',
			'CONSTRAINT `fk_cp_client` FOREIGN KEY (`clientId`) REFERENCES `clients` (`clientId`) ON DELETE CASCADE ON UPDATE NO ACTION'
		], 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=\'Протоколы клиентов\'');
	}

	public function safeDown() {
		$this->dropTable('clients-protocols');
		$this->dropTable('clients');
		$this->dropTable('managers');
		$this->dropTable('branches');
	}
}
