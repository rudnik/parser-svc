<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013
 */
class m140417_070511_add_clients_statuses extends CDbMigration {
	public function safeUp() {
		Yii::app()->db->createCommand("
		    ALTER TABLE `clients`
	            CHANGE COLUMN `client_status` `client_status` ENUM('Лид','Клиент', 'Клиент Сириус', 'Клиент ИТК', 'Клиент SVC') NOT NULL DEFAULT 'Лид' COMMENT 'Статус клиента' AFTER `next_contact`;
		")->execute();
	}

	public function safeDown() {
		echo 'm140417_070511_add_clients_statuses does not support migration down.'."\n";

		return true;
	}
}
