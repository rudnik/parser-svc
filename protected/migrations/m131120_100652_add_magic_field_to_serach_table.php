<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m131120_100652_add_magic_field_to_serach_table extends CDbMigration {
	public function safeUp() {
		Yii::app()->db->createCommand("
            ALTER TABLE `bids_data_search`
              ADD COLUMN `magic` VARCHAR(3) NOT NULL DEFAULT 'aaa' COMMENT 'Вспомогательное поле для возможности корректного поиска только по словам-исключениям' AFTER `inn`;
		")->execute();
	}

	public function safeDown() {
		Yii::app()->db->createCommand("
            ALTER TABLE `bids_data_search`
              DROP COLUMN `magic`;
		")->execute();
	}
}
