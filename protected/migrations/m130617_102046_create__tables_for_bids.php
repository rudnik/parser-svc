<?php

/**
 * Миграция создания таблиц для учёта заявок госзакупок.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013
 */
class m130617_102046_create__tables_for_bids extends CDbMigration {
	public function safeUp() {
		// таблица всех закупок. связана с регионом и типом документа
		$this->createTable('bids', [
			'bidId'     => 'bigint(19) unsigned NOT NULL AUTO_INCREMENT COMMENT \'Идентификатор закупки\'',
			'regionId'  => 'tinyint(3) unsigned NOT NULL DEFAULT \'0\' COMMENT \'Идентификатор региона\'',
			'portalId'  => 'mediumint(6) unsigned NOT NULL DEFAULT \'0\' COMMENT \'Идентификатор портала\'',
			'doctypeId' => 'tinyint(2) unsigned NOT NULL DEFAULT \'0\' COMMENT \'Идентификатор типа документа\'',
			'id'        => 'varchar(100) NOT NULL DEFAULT \'\' COMMENT \'Уникальный идентификатор в типе документа\'',
			'created'   => 'timestamp DEFAULT CURRENT_TIMESTAMP COMMENT \'Дата создания\'',
			'PRIMARY KEY (`bidId`)',
			'UNIQUE KEY `bid` (`regionId`, `portalId`, `doctypeId`, `id`)',
			'CONSTRAINT `fk_bids_region` FOREIGN KEY (`regionId`) REFERENCES `regions` (`regionId`) ON DELETE CASCADE ON UPDATE NO ACTION',
			'CONSTRAINT `fk_bids_portal` FOREIGN KEY (`portalId`) REFERENCES `portals` (`portalId`) ON DELETE CASCADE ON UPDATE NO ACTION',
			'CONSTRAINT `fk_bids_doctype` FOREIGN KEY (`doctypeId`) REFERENCES `zakupki-doctypes` (`doctypeId`) ON DELETE CASCADE ON UPDATE NO ACTION'
		], 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=\'Закупки\'');
		// поля закупок. связаны с порталом и типом документа
		$this->createTable('zakupki-fields', [
			'fieldId'   => 'smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT \'Идентификатор поля\'',
			'doctypeId' => 'tinyint(2) unsigned NOT NULL DEFAULT \'0\' COMMENT \'Идентификатор типа документа\'',
			'name'      => 'varchar(100) NOT NULL DEFAULT \'\' COMMENT \'Имя поля\'',
			'unique'    => 'tinyint(1) unsigned NOT NULL DEFAULT \'0\' COMMENT \'Уникальное поле\'',
			'PRIMARY KEY (`fieldId`)',
			'UNIQUE KEY `field` (`doctypeId`, `name`)',
			'KEY `name` (`name`(5))',
			'CONSTRAINT `fk_fields_doctype` FOREIGN KEY (`doctypeId`) REFERENCES `zakupki-doctypes` (`doctypeId`) ON DELETE CASCADE ON UPDATE NO ACTION'
		], 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=\'Поля закупок\'');
		// данные полей закупки
		$this->createTable('bids-data', [
			'dataId'  => 'bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT \'Идентификатор данных\'',
			'bidId'   => 'bigint(19) unsigned NOT NULL DEFAULT \'0\' COMMENT \'Идентификатор закупки\'',
			'fieldId' => 'smallint(5) unsigned NOT NULL DEFAULT \'0\' COMMENT \'Идентификатор поля\'',
			'PRIMARY KEY (`dataId`)',
			'UNIQUE KEY `bid_field` (`bidId`, `fieldId`)',
			'CONSTRAINT `fk_bd_bids` FOREIGN KEY (`bidId`) REFERENCES `bids` (`bidId`) ON DELETE CASCADE ON UPDATE NO ACTION',
			'CONSTRAINT `fk_bd_fields` FOREIGN KEY (`fieldId`) REFERENCES `zakupki-fields` (`fieldId`) ON DELETE CASCADE ON UPDATE NO ACTION'
		], 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=\'Данные закупок\'');
		// значения данных полей
		$this->createTable('bids-data-values', [
			'valueId' => 'bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT \'Идентификатор значений\'',
			'dataId'  => 'bigint(20) unsigned NOT NULL DEFAULT \'0\' COMMENT \'Идентификатор данных\'',
			'value'   => 'text DEFAULT NULL COMMENT \'Значение\'',
			'PRIMARY KEY (`valueId`)',
			'KEY `data` (`dataId`)',
			'KEY `value` (`value`(10))',
			'CONSTRAINT `fk_bdv_bd` FOREIGN KEY (`dataId`) REFERENCES `bids-data` (`dataId`) ON DELETE CASCADE ON UPDATE NO ACTION',
		], 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=\'Значения данных закупок\'');
	}

	public function safeDown() {
		$this->dropTable('bids-data-values');
		$this->dropTable('bids-data');
		$this->dropTable('zakupki-fields');
		$this->dropTable('bids');
	}
}
