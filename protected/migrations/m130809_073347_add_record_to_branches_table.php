<?php

/**
 * Миграция добавления записи в таблицу отраслей.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m130809_073347_add_record_to_branches_table extends CDbMigration {
	public function safeUp() {
            $this->dbConnection->createCommand("
                INSERT INTO `branches` (`branchId`, `parentId`, `title`) VALUES (1, 0, 'Пустая отрасль');"
            )->execute();
	}

	public function safeDown() {
		$this->dbConnection->createCommand("
                DELETE FROM `branches` WHERE `branchId` = 1
                ")->execute();
	}
}
