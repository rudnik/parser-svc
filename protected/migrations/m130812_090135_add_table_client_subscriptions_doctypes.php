<?php

/**
 * Миграция создания связующей таблицы подписок и типов документов.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m130812_090135_add_table_client_subscriptions_doctypes extends CDbMigration {
	public function safeUp() {
		// создание связующей таблицы подписок и типов документов
		$this->dbConnection->createCommand("
                        CREATE TABLE `clients_subscriptions_doctypes` (
                                `client_subscription_id` int(10) unsigned NOT NULL COMMENT 'id подписки',
                                `zakupki_doctype_id` tinyint(2) unsigned NOT NULL COMMENT 'id типа документа с zakupki.gov.ru. !!!Нужно избаиться от связи с gov.ru!!!',
                                UNIQUE KEY `client_subscription_id_zakupki_doctype_id` (`client_subscription_id`,`zakupki_doctype_id`),
                                KEY `FK_client_subscriptions_doctypes_zakupki-doctypes` (`zakupki_doctype_id`),
                                CONSTRAINT `FK_client_subscriptions_doctypes_clients-subscriptions` FOREIGN KEY (`client_subscription_id`) REFERENCES `clients-subscriptions` (`subscrId`),
                                CONSTRAINT `FK_client_subscriptions_doctypes_zakupki-doctypes` FOREIGN KEY (`zakupki_doctype_id`) REFERENCES `zakupki-doctypes` (`doctypeId`)
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Типы документов для фильтрации результатов поиска'
		")->execute();
	}

	public function safeDown() {
		$this->dropTable('clients_subscriptions_doctypes');
	}
}
