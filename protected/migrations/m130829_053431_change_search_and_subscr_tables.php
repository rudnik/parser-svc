<?php

/**
 * Миграция удаления ненужного поля в таблице рассылок и правка полей в таблице поиска.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013
 */
class m130829_053431_change_search_and_subscr_tables extends CDbMigration {
	public function safeUp() {
		$this->dropColumn('clients-subscriptions', 'search_in');
		$this->getDbConnection()->createCommand('
			ALTER TABLE `bids_data_search`
				MODIFY COLUMN `title` mediumtext DEFAULT NULL COMMENT \'Заголовок заказа\',
  				MODIFY COLUMN `description` mediumtext DEFAULT NULL COMMENT \'Описание заказа\'
		')->execute();
	}

	public function safeDown() {
		echo 'm130829_053431_change_search_and_subscr_tables does not support migration down.'."\n";

		return true;
	}
}
