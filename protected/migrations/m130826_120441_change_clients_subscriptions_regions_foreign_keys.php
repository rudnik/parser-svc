<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m130826_120441_change_clients_subscriptions_regions_foreign_keys extends CDbMigration {
	public function safeUp() {
		$this->dbConnection->createCommand("
		    ALTER TABLE `clients_subscriptions_regions`
			DROP FOREIGN KEY `FK_clients-subscriptions-regions_clients-subscriptions`,
			DROP FOREIGN KEY `FK_clients-subscriptions-regions_regions`;")->execute();
		
		$this->dbConnection->createCommand("
		    ALTER TABLE `clients_subscriptions_regions`
			ADD CONSTRAINT `FK_clients-subscriptions-regions_clients-subscriptions` FOREIGN KEY (`client_subscription_id`) REFERENCES `clients-subscriptions` (`subscrId`) ON DELETE CASCADE,
			ADD CONSTRAINT `FK_clients-subscriptions-regions_regions` FOREIGN KEY (`region_id`) REFERENCES `regions` (`regionId`) ON DELETE CASCADE;
		")->execute();
	}

	public function safeDown() {
		echo 'm130826_120441_change_clients_subscriptions_regions_foreign_keys does not support migration down.'."\n";
		return true;
	}
}
