<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m131202_124435_add_field_send_from_default_email_to_subscriptions extends CDbMigration {
    public function safeUp() {
        Yii::app()->db->createCommand("
            ALTER TABLE `clients_subscriptions`
                ADD COLUMN `send_from_default_email` TINYINT UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Отправлять письма с девлотного email' AFTER `theme`;
		")->execute();
    }

    public function safeDown() {
        Yii::app()->db->createCommand("
            ALTER TABLE `clients_subscriptions`
                DROP COLUMN `send_from_default_email`
		")->execute();
    }
}
