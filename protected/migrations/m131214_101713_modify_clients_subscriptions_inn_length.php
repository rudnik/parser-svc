<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m131214_101713_modify_clients_subscriptions_inn_length extends CDbMigration {
	public function safeUp() {
		Yii::app()->db->createCommand("
            ALTER TABLE `clients_subscriptions`
	            CHANGE COLUMN `inn` `inn` TEXT NULL DEFAULT NULL COMMENT 'ИНН заказчика' AFTER `pubdate_to_end`;
		")->execute();
	}

	public function safeDown() {
		Yii::app()->db->createCommand("
		    ALTER TABLE `clients_subscriptions`
	            CHANGE COLUMN `inn` `inn` VARCHAR(12) NULL DEFAULT NULL COMMENT 'ИНН заказчика' AFTER `pubdate_to_end`;
		")->execute();
	}
}
