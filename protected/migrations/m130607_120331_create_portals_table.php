<?php

/**
 * Миграция создания таблицы порталов.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013 BSTsoft
 */
class m130607_120331_create_portals_table extends CDbMigration {
	public function safeUp() {
		// добавление таблицы порталов
		$this->dbConnection->createCommand('
			CREATE TABLE `portals` (
				`portalId` mediumint(6) unsigned NOT NULL AUTO_INCREMENT COMMENT \'Идентификатор\',
				`name` varchar(50) NOT NULL DEFAULT \'\' COMMENT \'Имя\',
				`title` varchar(50) NOT NULL DEFAULT \'\' COMMENT \'Заголовок\',
				`url` varchar(255) NOT NULL DEFAULT \'\' COMMENT \'Ссылка на портал\',
				PRIMARY KEY (`portalId`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=\'Порталы закупок\''
		)->execute();
	}

	public function safeDown() {
		$this->dropTable('portals');
	}
}
