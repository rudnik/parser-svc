<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m130906_161526_add_subscriptions_demand_types extends CDbMigration {
	public function safeUp() {
		$this->dbConnection->createCommand("
		CREATE TABLE `clients_subscriptions_demand_types` (
			`subscription_id` INT(11) UNSIGNED NOT NULL COMMENT 'id рассылки',
			`demand_type_id` MEDIUMINT(6) UNSIGNED NOT NULL COMMENT 'id типа спроса',
			UNIQUE INDEX `subscription_id_demand_type_id` (`subscription_id`, `demand_type_id`),
			INDEX `FK_clients_subscriptions_demand_types_clients-subscriptions` (`subscription_id`),
			CONSTRAINT `FK_clients_subscriptions_demand_types_clients-subscriptions` FOREIGN KEY (`subscription_id`) REFERENCES `clients-subscriptions` (`subscrId`) ON DELETE CASCADE
		)
		COMMENT='типы спроса рассылки'
		COLLATE='utf8_general_ci'
		ENGINE=InnoDB;
		")->execute();
	}

	public function safeDown() {
	    $this->dbConnection->createCommand('SET FOREIGN_KEY_CHECKS = 0;')->execute();
	    $this->dropTable('clients_subscriptions_demand_types');
	}
}
