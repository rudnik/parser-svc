<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m130903_085648_add_portalId_field_to_bids_doctypes extends CDbMigration {
	public function safeUp() {
	    $this->dbConnection->createCommand('SET FOREIGN_KEY_CHECKS = 0;')->execute();
	    $this->dbConnection->createCommand("
		ALTER TABLE `bids_doctypes`
		    ADD COLUMN `portalId` MEDIUMINT(6) UNSIGNED NOT NULL COMMENT 'id портала' AFTER `doctypeId`,
		    ADD CONSTRAINT `FK_bids_doctypes_portals` FOREIGN KEY (`portalId`) REFERENCES `portals` (`portalId`) ON DELETE CASCADE;
		")->execute();
	    $this->dbConnection->createCommand('UPDATE `bids_doctypes` SET `portalId` = 1;')->execute();
	}

	public function safeDown() {
	    $this->dbConnection->createCommand('SET FOREIGN_KEY_CHECKS = 0;')->execute();
	    $this->dbConnection->createCommand("
		ALTER TABLE `bids_doctypes`
		    DROP COLUMN `portalId`,
		    DROP FOREIGN KEY `FK_bids_doctypes_portals`;
		")->execute();
	}
}
