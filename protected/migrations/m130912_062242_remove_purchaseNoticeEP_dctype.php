<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m130912_062242_remove_purchaseNoticeEP_dctype extends CDbMigration {
	public function safeUp() {
		$this->dbConnection->createCommand("
		    DELETE FROM `bids_doctypes` WHERE `name` = 'purchaseNoticeEP';
		")->execute();
	}

	public function safeDown() {
		echo 'm130912_062242_remove_purchaseNoticeEP_dctype does not support migration down.'."\n";

		return true;
	}
}
