<?php

/**
 * Миграция переноса данных в поисковые таблицы.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013
 */
class m130709_081406_move_data_to_search extends CDbMigration {
	public function safeUp() {
		$this->moveData('num',  ['price']);
		$this->moveData('date', ['publishDate', 'notificationCommission/p1Date', 'notificationCommission/p2Date']);
		$this->moveData('text', ['products/product/name', 'lots/lot/products/product/name']);
	}

	private function moveData($type, $fields) {
		$fields = array_map(function($field) { return $this->getDbConnection()->quoteValue($field); }, $fields);
		$datas = $this->getDbConnection()->createCommand('
			SELECT
				`bd`.`dataId`
			FROM
				`bids-data` AS `bd`
				JOIN `zakupki-fields` AS `f`
					ON `f`.`fieldId`=`bd`.`fieldId`
					AND `f`.`name` IN ('.implode(', ', $fields).')
				JOIN `bids` AS `b`
					ON `b`.`bidId`=`bd`.`bidId`
					AND `b`.`created`>=STR_TO_DATE("2013-07-01 00:00:00", "%Y-%m-%d %H:%i:%s")
		')->queryAll();

		foreach ($datas as $data) {
			$data_id = (int) $data['dataId'];
			$this->getDbConnection()->createCommand('
				INSERT INTO
					`bids-data-'.$type.'-search` (`dataId`, `value`)
				SELECT
					'.$data_id.',
					`value`
				FROM
					`bids-data-'.$type.'`
				WHERE
					`dataId`='.$data_id
			)->execute();
			$this->delete('bids-data-'.$type, 'dataId='.$data_id);
		}

		unset($datas);
	}

	public function safeDown() {
		echo 'm130709_081406_move_data_to_search does not support migration down.'."\n";
		return true;
	}
}
