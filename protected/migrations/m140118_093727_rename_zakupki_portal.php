<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Serrgey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m140118_093727_rename_zakupki_portal extends CDbMigration {
	public function safeUp() {
		$this->update('portals', ['name' => 'zakupki94'], 'name="zakupki"');
	}

	public function safeDown() {
        $this->update('portals', ['name' => 'zakupki'], 'name="zakupki94"');
	}
}
