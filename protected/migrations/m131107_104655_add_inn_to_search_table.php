<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m131107_104655_add_inn_to_search_table extends CDbMigration {
	public function safeUp() {
		Yii::app()->db->createCommand("
            ALTER TABLE `bids_data_search`
                ADD COLUMN `inn` VARCHAR(12) NULL COMMENT 'ИНН Заказчика' AFTER `max_cost`;
		")->execute();
	}

	public function safeDown() {
        Yii::app()->db->createCommand("
            ALTER TABLE `bids_data_search`
                DROP COLUMN `inn`;
		")->execute();
	}
}
