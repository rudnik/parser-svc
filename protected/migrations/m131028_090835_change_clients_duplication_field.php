<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m131028_090835_change_clients_duplication_field extends CDbMigration {
	public function safeUp() {
        Yii::app()->db->createCommand("
            ALTER TABLE `clients`
                CHANGE COLUMN `is_duplication` `is_duplication` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Дубль' AFTER `contract_end_date`;
        ")->execute();
	}

	public function safeDown() {
        Yii::app()->db->createCommand("
            ALTER TABLE `clients`
                CHANGE COLUMN `is_duplication` `is_duplication` TINYINT(1) UNSIGNED NULL DEFAULT NULL COMMENT 'Дубль' AFTER `contract_end_date`;
        ")->execute();
	}
}
