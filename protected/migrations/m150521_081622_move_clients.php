<?php

/**
 * Миграция переназначения клиентов и рассылок на другого менеджера.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013-2015 BST//soft
 */
class m150521_081622_move_clients extends CDbMigration
{
    public function safeUp()
    {
        $old_user_id = 44;
        $new_user_id = 48;

        if (is_null(Users::model()->findByPk($old_user_id)) || is_null(Users::model()->findByPk($new_user_id))) {
            return true;
        }

        // отправляем все рассылки с info
        $this->dbConnection->commandBuilder->createSqlCommand(
            'UPDATE
                ' . ClientsSubscriptions::model()->tableName() . '
            SET
                send_from_default_email=1
            WHERE
                clientId in (
                    SELECT
                        id
                    FROM
                        ' . Clients::model()->tableName() . '
                    WHERE
                        user_id=:old_user_id
                ) AND
                send_from_default_email=0'
        )->execute([':old_user_id' => $old_user_id]);
        // меняем менеджера клиента
        $this->update(
            Clients::model()->tableName(),
            ['user_id' => $new_user_id],
            'user_id=:old_user_id',
            ['old_user_id' => $old_user_id]
        );
    }

    public function safeDown()
    {
        echo 'm150521_081622_move_clients does not support migration down.'."\n";

        return true;
    }
}
