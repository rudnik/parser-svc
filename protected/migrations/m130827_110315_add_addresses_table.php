<?php

/**
 * Миграция создания талицы регионов и населенных пунктов.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013
 */
class m130827_110315_add_addresses_table extends CDbMigration {
	public function safeUp() {
		$this->dbConnection->createCommand('
			CREATE TABLE `tenders`.`addresses` (
				`AOID` varchar(36) NOT NULL COMMENT \'Идентификатор\',
				`AOGUID` varchar(36) DEFAULT NULL COMMENT \'Глобальный уникальный идентификатор адресного объекта\',
				`PARENTGUID` varchar(36) DEFAULT NULL COMMENT \'Идентификатор объекта родительского объекта\',
				`OFFNAME` varchar(145) DEFAULT NULL COMMENT \'Официальное наименование\',
				`SHORTNAME` varchar(14) DEFAULT NULL COMMENT \'Краткое наименование типа объекта\',
				`AOLEVEL` tinyint(1) DEFAULT NULL COMMENT \'Уровень адресного объекта\',
				`REGIONCODE` tinyint(2) DEFAULT NULL COMMENT \'Код региона\',
				`AREACODE` tinyint(3) DEFAULT NULL COMMENT \'Код района\',
				`AUTOCODE` tinyint(1) DEFAULT NULL COMMENT \'Код автономии\',
				`CITYCODE` tinyint(3) DEFAULT NULL COMMENT \'Код города\',
				`CTARCODE` tinyint(3) DEFAULT NULL COMMENT \'Код внутригородского района\',
				`PLACECODE` tinyint(3) DEFAULT NULL COMMENT \'Код населенного пункта\',
				`STREETCODE` tinyint(4) DEFAULT NULL COMMENT \'Код улицы\',
				`EXTRCODE` tinyint(4) DEFAULT NULL COMMENT \'Код дополнительного адресообразующего элемента\',
				`SEXTCODE` tinyint(3) DEFAULT NULL COMMENT \'Код подчиненного дополнительного адресообразующего элемента\',
				`NEXTID` varchar(36) DEFAULT NULL COMMENT \'Идентификатор записи связывания с последующей исторической записью\',
				`PREVID` varchar(36) DEFAULT NULL COMMENT \'Идентификатор записи связывания с предыдушей исторической записью\',
				`POSTALCODE` int(6) DEFAULT NULL COMMENT \'Почтовый индекс\',
				PRIMARY KEY(`AOID`),
				KEY `AOGUID` (`AOGUID`),
				KEY `PARENTGUID` (`PARENTGUID`),
				KEY `OFFNAME` (`OFFNAME`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
		')
		->execute();
	}

	public function safeDown() {
		$this->dropTable('addresses');
	}
}
