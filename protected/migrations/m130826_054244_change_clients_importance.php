<?php

/**
 * Миграция изменения хранение значения важности клиента.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m130826_054244_change_clients_importance extends CDbMigration {
	public function safeUp() {
		$this->dbConnection->createCommand("
		   ALTER TABLE `clients`
			CHANGE COLUMN `importance_id` `importance` ENUM('Минимальная важность', 'Высокая важность', 'Край географии') NOT NULL DEFAULT 'Минимальная важность' COMMENT 'Важность' AFTER `comment`,
			DROP FOREIGN KEY `FK_clients_client_importance`;")->execute();
		
		$this->dbConnection->createCommand("DROP TABLE `client_importance`;")->execute();
	}

	public function safeDown() {
		echo 'm130826_054244_change_clients_importance does not support migration down.'."\n";
		return true;
	}
}
