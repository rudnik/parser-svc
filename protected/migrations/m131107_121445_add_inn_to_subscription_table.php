<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m131107_121445_add_inn_to_subscription_table extends CDbMigration {
	public function safeUp() {
		Yii::app()->db->createCommand("
            ALTER TABLE `clients_subscriptions`
                ADD COLUMN `inn` VARCHAR(12) NULL COMMENT 'ИНН заказчика' AFTER `pubdate_to_end`;
		")->execute();
	}

	public function safeDown() {
        Yii::app()->db->createCommand("
            ALTER TABLE `clients_subscriptions`
	            DROP COLUMN `inn`;
		")->execute();
	}
}
