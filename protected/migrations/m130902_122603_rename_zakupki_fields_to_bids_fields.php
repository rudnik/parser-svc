<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m130902_122603_rename_zakupki_fields_to_bids_fields extends CDbMigration {
	public function safeUp() {
	    $this->dbConnection->createCommand("
		    RENAME TABLE `zakupki-fields` TO `bids_fields`;
		")->execute();
	    
	}

	public function safeDown() {
		$this->dbConnection->createCommand("
		    RENAME TABLE `bids_fields` TO `zakupki-fields`;
		")->execute();
	}
}
