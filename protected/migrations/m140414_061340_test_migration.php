<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013
 */
class m140414_061340_test_migration extends CDbMigration {
	public function safeUp() {
		Yii::app()->db->createCommand("
		    SELECT 1;
		")->execute();
	}

	public function safeDown() {
		echo 'm140414_061340_test_migration does not support migration down.'."\n";

		return true;
	}
}
