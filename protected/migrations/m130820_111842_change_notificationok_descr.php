<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013
 */
class m130820_111842_change_notificationok_descr extends CDbMigration {
	public function safeUp() {
		$this->getDbConnection()->createCommand('
			UPDATE `zakupki-doctypes` SET `descr`="Открытый конкурс" WHERE `doctypeId`=1
		')->execute();
	}

	public function safeDown() {
		echo 'm130820_111842_change_notificationok_descr does not support migration down.'."\n";
		return true;
	}
}
