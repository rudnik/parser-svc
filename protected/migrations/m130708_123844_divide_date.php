<?php

/**
 * Миграция выделения дат окончания.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013
 */
class m130708_123844_divide_date extends CDbMigration {
	public function safeUp() {
		$datas = $this->getDbConnection()->createCommand('
			SELECT
				`bd`.`dataId`,
				`bd`.`fieldId`
			FROM
				`bids-data` AS `bd`
				JOIN `zakupki-fields` AS `f`
					ON `f`.`fieldId`=`bd`.`fieldId`
					AND `f`.`name` IN ("notificationCommission/p1Date", "notificationCommission/p2Date")
				JOIN `bids` AS `b`
					ON `b`.`bidId`=`bd`.`bidId`
					AND `b`.`created`>=STR_TO_DATE("2013-07-04 00:00:00", "%Y-%m-%d %H:%i:%s")
		')->queryAll();
		$updated_fields = [];

		foreach ($datas as $data) {
			$data_id = (int) $data['dataId'];
			$field_id = (int) $data['fieldId'];

			if (!in_array($field_id, $updated_fields)) {
				$updated_fields[]= $field_id;
				$this->update('zakupki-fields', ['type' => 'date'], 'fieldId='.$field_id);
			}

			$this->getDbConnection()->createCommand('
				INSERT INTO
					`bids-data-date-search` (`dataId`, `value`)
				SELECT
					'.$data_id.',
					`value`
				FROM
					`bids-data-text`
				WHERE
					`dataId`='.$data_id
			)->execute();
			$this->delete('bids-data-text', 'dataId='.$data_id);
		}
	}

	public function safeDown() {
		echo 'm130708_123844_divide_date does not support migration down.'."\n";
		return true;
	}
}
