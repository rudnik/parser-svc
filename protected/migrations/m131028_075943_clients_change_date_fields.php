<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m131028_075943_clients_change_date_fields extends CDbMigration {
	public function safeUp() {
		Yii::app()->db->createCommand("
            ALTER TABLE `clients`
                CHANGE COLUMN `contract_end_date` `contract_end_date` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Дата окончания контракта (unix timestamp)' AFTER `user_id`,
                CHANGE COLUMN `next_contact` `next_contact` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Дата следующего контакта' AFTER `importance`;
		")->execute();
	}

	public function safeDown() {
		Yii::app()->db->createCommand("
		    ALTER TABLE `clients`
                CHANGE COLUMN `contract_end_date` `contract_end_date` INT(10) UNSIGNED NULL DEFAULT NULL COMMENT 'Дата окончания контракта (unix timestamp)' AFTER `user_id`,
                CHANGE COLUMN `next_contact` `next_contact` INT(10) UNSIGNED NULL DEFAULT NULL COMMENT 'Дата следующего контакта' AFTER `importance`;
		")->execute();
	}
}
