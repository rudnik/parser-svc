<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m130904_114637_change_fz223_doctypes_list extends CDbMigration {
	public function safeUp() {
		$this->dbConnection->createCommand("DELETE FROM `bids_doctypes` WHERE `portalId` = 2;")->execute();
		# добавляем типы документов
		$this->insert('bids_doctypes', [
		    'portalId' => 2,
		    'name' => 'purchaseNoticeOK',
		    'descr' => 'Открытый конкурс',
		]);
		$this->insert('bids_doctypes', [
		    'portalId' => 2,
		    'name' => 'purchaseNoticeOA',
		    'descr' => 'Открытый аукцион',
		]);
		$this->insert('bids_doctypes', [
		    'portalId' => 2,
		    'name' => 'purchaseNoticeAE',
		    'descr' => 'Аукцион в электронной форме',
		]);
		$this->insert('bids_doctypes', [
		    'portalId' => 2,
		    'name' => 'purchaseNoticeZK',
		    'descr' => 'Запрос котировок',
		]);
		$this->insert('bids_doctypes', [
		    'portalId' => 2,
		    'name' => 'purchaseNoticeEP',
		    'descr' => 'Закупка у единственного поставщика',
		]);
		$this->insert('bids_doctypes', [
		    'portalId' => 2,
		    'name' => 'purchaseNotice',
		    'descr' => 'Иной способ',
		]);
	}

	public function safeDown() {
		echo 'm130904_114637_change_fz223_doctypes_list does not support migration down.'."\n";
		return true;
	}
}
