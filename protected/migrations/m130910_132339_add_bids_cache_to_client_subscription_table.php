<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m130910_132339_add_bids_cache_to_client_subscription_table extends CDbMigration {
	public function safeUp() {
		$this->dbConnection->createCommand("
		    ALTER TABLE `clients-subscriptions`
                ADD COLUMN `bids_cache` TEXT NOT NULL COMMENT 'Кэш заявок для рассылки' AFTER `pubdate_to_end`;
		")->execute();
	}

	public function safeDown() {
		$this->dbConnection->createCommand("
		    ALTER TABLE `clients-subscriptions`
                DROP COLUMN `bids_cache`;
		")->execute();
	}
}
