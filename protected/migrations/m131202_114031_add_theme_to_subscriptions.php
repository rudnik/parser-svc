<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m131202_114031_add_theme_to_subscriptions extends CDbMigration {
	public function safeUp() {
		Yii::app()->db->createCommand("
            ALTER TABLE `clients_subscriptions`
                ADD COLUMN `theme` VARCHAR(255) NOT NULL DEFAULT '' COMMENT 'Заголовок письма рассылки' AFTER `bids_cache`;
		")->execute();
	}

	public function safeDown() {
        Yii::app()->db->createCommand("
            ALTER TABLE `clients_subscriptions`
                DROP COLUMN `theme`
		")->execute();
	}
}
