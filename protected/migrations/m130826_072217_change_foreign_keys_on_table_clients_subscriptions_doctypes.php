<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m130826_072217_change_foreign_keys_on_table_clients_subscriptions_doctypes extends CDbMigration {
	public function safeUp() {
		$this->dbConnection->createCommand("
		    ALTER TABLE `clients_subscriptions_doctypes`
			DROP FOREIGN KEY `FK_client_subscriptions_doctypes_clients-subscriptions`,
			DROP FOREIGN KEY `FK_client_subscriptions_doctypes_zakupki-doctypes`;")->execute();
		
		$this->dbConnection->createCommand("
		    ALTER TABLE `clients_subscriptions_doctypes`
			ADD CONSTRAINT `FK_client_subscriptions_doctypes_clients-subscriptions` FOREIGN KEY (`client_subscription_id`) REFERENCES `clients-subscriptions` (`subscrId`) ON DELETE CASCADE,
			ADD CONSTRAINT `FK_client_subscriptions_doctypes_zakupki-doctypes` FOREIGN KEY (`zakupki_doctype_id`) REFERENCES `zakupki-doctypes` (`doctypeId`) ON DELETE CASCADE;
		")->execute();
	}

	public function safeDown() {
		echo 'm130826_072217_change_foreign_keys_on_table_clients_subscriptions_doctypes does not support migration down.'."\n";
		return true;
	}
}
