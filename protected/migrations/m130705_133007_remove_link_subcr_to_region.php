<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013
 */
class m130705_133007_remove_link_subcr_to_region extends CDbMigration {
	public function safeUp() {
		$this->dropColumn('clients-subscriptions', 'regionId');
		$this->getDbConnection()->createCommand('
			ALTER TABLE `clients-subscriptions`
				ADD COLUMN `regionId` varchar(255) NOT NULL DEFAULT \'\' COMMENT \'Список идентификаторов региона\' AFTER `subscrId`
		')->execute();
	}

	public function safeDown() {
		echo 'm130705_133007_remove_link_subcr_to_region does not support migration down.'."\n";
		return true;
	}
}
