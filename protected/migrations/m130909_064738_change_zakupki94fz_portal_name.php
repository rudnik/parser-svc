<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m130909_064738_change_zakupki94fz_portal_name extends CDbMigration {
	public function safeUp() {
		$this->dbConnection->createCommand("
		    UPDATE `portals` SET `title` = 'Портал госзакупок 94ФЗ' WHERE `portalId`=1;
		")->execute();
	}

	public function safeDown() {
		$this->dbConnection->createCommand("
		    UPDATE `portals` SET `title` = 'Портал госзакупок' WHERE `portalId`=1;
		")->execute();
	}
}
