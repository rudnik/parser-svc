<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m130903_101659_add_portal_zakupki223 extends CDbMigration {
	public function safeUp() {
	    # добавляем портал
	    $this->dbConnection->createCommand("
		INSERT INTO `portals` (`portalId`, `name`, `title`, `url`) VALUES (2, 'zakupki223', 'Портал госзакупок 223ФЗ', 'http://zakupki.gov.ru/wps/portal/base/topmain/home');
	    ")->execute();
	    
	    # добавляем типы документов
	    $this->insert('bids_doctypes', [
		'portalId' => 2,
		'name' => 'purchaseNoticeOK',
		'descr' => 'Открытый конкурс',
	    ]);
	    $this->insert('bids_doctypes', [
		'portalId' => 2,
		'name' => 'purchaseNoticeOA',
		'descr' => 'Открытый аукцион',
	    ]);
	    $this->insert('bids_doctypes', [
		'portalId' => 2,
		'name' => 'purchaseNoticeAE',
		'descr' => 'Аукцион в электронной форме',
	    ]);
	    $this->insert('bids_doctypes', [
		'portalId' => 2,
		'name' => 'purchaseNoticeZK',
		'descr' => 'Запрос котировок',
	    ]);
	    $this->insert('bids_doctypes', [
		'portalId' => 2,
		'name' => 'purchaseProtocolRZOK',
		'descr' => 'Протокол рассмотрения заявок для открытого конкурса',
	    ]);
	    $this->insert('bids_doctypes', [
		'portalId' => 2,
		'name' => 'purchaseProtocolRZOA',
		'descr' => 'Протокол рассмотрения заявок для открытого аукциона',
	    ]);
	    $this->insert('bids_doctypes', [
		'portalId' => 2,
		'name' => 'purchaseProtocolPAOA',
		'descr' => 'Протокол проведения аукциона для открытого аукциона',
	    ]);
	    $this->insert('bids_doctypes', [
		'portalId' => 2,
		'name' => 'purchaseProtocolRZAE',
		'descr' => 'Протокол рассмотрения заявок для аукциона в электронной форме',
	    ]);
	    $this->insert('bids_doctypes', [
		'portalId' => 2,
		'name' => 'purchaseProtocolPAAE',
		'descr' => 'Протокол проведения аукциона для открытого аукциона в электронной форме',
	    ]);
	    $this->insert('bids_doctypes', [
		'portalId' => 2,
		'name' => 'purchaseProtocolZK',
		'descr' => 'Протокол запроса котировок',
	    ]);
	}

	public function safeDown() {
		$this->dbConnection->createCommand("DELETE FROM `portals` WHERE `portalId` = 2;")->execute();
	}
}
