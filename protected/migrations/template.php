<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013-2015 BST//soft
 */
class {ClassName} extends CDbMigration
{
    public function safeUp()
    {
        // @todo
    }

    public function safeDown()
    {
        echo '{ClassName} does not support migration down.'."\n";

        return true;
    }
}
