<?php

/**
 * Миграция изменения столбца статуса закупки.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013
 */
class m130822_084401_change_bid_status_column extends CDbMigration {
	public function safeUp() {
		$this->getDbConnection()->createCommand('
			ALTER TABLE `bids`
				MODIFY COLUMN `status` enum(\'\', \'canceled\') NOT NULL DEFAULT \'\' COMMENT \'Статус закупки\'
		');
	}

	public function safeDown() {
		echo 'm130822_084401_change_bid_status_column does not support migration down.'."\n";
		return true;
	}
}
