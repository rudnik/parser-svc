<?php

/**
 * Миграция увеличения размера поля.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013
 */
class m130705_154708_increase_field_name extends CDbMigration {
	public function safeUp() {
		$this->getDbConnection()->createCommand('
			ALTER TABLE `zakupki-fields` MODIFY COLUMN `name` varchar(255) NOT NULL DEFAULT \'\' COMMENT \'Имя поля\'
		')->execute();
	}

	public function safeDown() {
		echo 'm130705_154708_increase_field_name does not support migration down.'."\n";
		return true;
	}
}
