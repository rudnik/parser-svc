<?php

/**
 * Миграция добавления описания типов документов.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013
 */
class m130710_064830_add_doctypes_descr extends CDbMigration {
	public function safeUp() {
		$this->getDbConnection()->createCommand('
			ALTER TABLE `zakupki-doctypes`
				ADD COLUMN `descr` varchar(255) NOT NULL DEFAULT \'\' COMMENT \'Описание\'
		')->execute();
		$this->update('zakupki-doctypes', ['descr' => 'Открытый аукцион'], 'name="notificationOK"');
		$this->update('zakupki-doctypes', ['descr' => 'Открытый аукцион в электронной форме'], 'name="notificationEF"');
		$this->update('zakupki-doctypes', ['descr' => 'Запрос котировок'], 'name="notificationZK"');
		$this->update('zakupki-doctypes', ['descr' => 'Предварительный отбор'], 'name="notificationPO"');
		$this->update('zakupki-doctypes', ['descr' => 'Заинтересованность проведения открытого конкурса'], 'name="notificationSZ"');
		$this->update('zakupki-doctypes', ['descr' => 'Время проведения открытого аукциона в электронной форме'], 'name="timeEF"');
		$this->update('zakupki-doctypes', ['descr' => 'Отказ от размещения заказа'], 'name="notificationCancel"');
		$this->update('zakupki-doctypes', ['descr' => 'Расмотрение первых частей заявок открытого аукциона в электронной форме'], 'name="protocolEF1"');
		$this->update('zakupki-doctypes', ['descr' => 'Запрос на разъяснение документации'], 'name="clarificationRequest"');
		$this->update('zakupki-doctypes', ['descr' => 'Разъяснение документации'], 'name="clarification"');
		$this->update('zakupki-doctypes', ['descr' => 'Протокол вскрытия конвертов'], 'name="protocolOK1"');
		$this->update('zakupki-doctypes', ['descr' => 'Протокол рассмотрения заявок'], 'name="protocolOK2"');
		$this->update('zakupki-doctypes', ['descr' => 'Протокол оценки и сопоставления заявок'], 'name="protocolOK3"');
		$this->update('zakupki-doctypes', ['descr' => 'Протокол проведения открытого аукциона в электронной форме'], 'name="protocolEF2"');
		$this->update('zakupki-doctypes', ['descr' => 'Протокол подведения итогов открытого аукциона в электронной форме'], 'name="protocolEF3"');
		$this->update('zakupki-doctypes', ['descr' => 'Протокол рассмотрения котировочных заявок'], 'name="protocolZK1"');
		$this->update('zakupki-doctypes', ['descr' => 'Протокол рассмотрения по главе 5 94-ФЗ'], 'name="protocolZK5"');
		$this->update('zakupki-doctypes', ['descr' => 'Протокол рассмотрения на участие в процедуре предварительного отбора'], 'name="protocolPO1"');
		$this->update('zakupki-doctypes', ['descr' => 'Протокол отказа от заключения контракта'], 'name="protocolEvasion"');
		$this->update('zakupki-doctypes', ['descr' => 'Отмена протокола'], 'name="protocolCancel"');
		$this->update('zakupki-doctypes', ['descr' => 'Информация о подписании государственного/муниципального контракта'], 'name="contractSign"');
		$this->update('zakupki-doctypes', ['descr' => 'Сведения о государственном или муниципальном контракте'], 'name="contract"');
		$this->update('zakupki-doctypes', ['descr' => 'Сведения об аннулировании контракта'], 'name="contractCancel"');
		$this->update('zakupki-doctypes', ['descr' => 'Сведения об исполнении государственного или муниципального контракта'], 'name="contractProcedure"');
		$this->update('zakupki-doctypes', ['descr' => 'Прикрепляемый к извещению файл'], 'name="attachment"');
		$this->update('zakupki-doctypes', ['descr' => 'Жалоба'], 'name="complaint"');
		$this->update('zakupki-doctypes', ['descr' => 'Решение и предписание по результатам рассмотрения жалобы'], 'name="decision"');
		$this->update('zakupki-doctypes', ['descr' => 'Информация об отзыве жалобы'], 'name="complaintCancel"');
		$this->update('zakupki-doctypes', ['descr' => 'План-график'], 'name="tenderPlan"');
		$this->update('zakupki-doctypes', ['descr' => 'Справочная информация'], 'name="masterDataBatch"');
		$this->update('zakupki-doctypes', ['descr' => 'Протокол загрузки информации'], 'name="confirmation"');
		$this->update('zakupki-doctypes', ['descr' => 'Сведения о контрактах'], 'name="contractBatch"');
	}

	public function safeDown() {
		echo 'm130710_064830_add_doctypes_descr does not support migration down.'."\n";
		return true;
	}
}
