<?php

/**
 * Миграция увеличения длины поля названия рассылки до 100 символов.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2014
 */
class m140414_054908_change_subscr_title_length extends CDbMigration {
	public function safeUp() {
		Yii::app()->db->createCommand("
		    ALTER TABLE `clients_subscriptions`
	            CHANGE COLUMN `title` `title` VARCHAR(100) NOT NULL DEFAULT '' COMMENT 'Наименование' AFTER `clientId`;
		")->execute();
	}

	public function safeDown() {
		echo 'm140414_054908_change_subscr_title_length does not support migration down.'."\n";

		return true;
	}
}
