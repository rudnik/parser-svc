<?php

/**
 * Миграция удаления ненужных таблиц.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013
 */
class m130828_122554_delete_useless_tables extends CDbMigration {
	public function safeUp() {
		$this->dbConnection->createCommand('SET FOREIGN_KEY_CHECKS = 0;')->execute();
		$this->dropTable('clients_old');
		$this->dropTable('clients-subscriptions-values');
		$this->dropTable('cities');
	}

	public function safeDown() {
		echo 'm130828_122554_delete_useless_tables does not support migration down.'."\n";

		return true;
	}
}
