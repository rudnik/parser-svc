<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m130828_075730_change_clients_protocols_foreign_key extends CDbMigration {
	public function safeUp() {
		$this->dbConnection->createCommand("
		    ALTER TABLE `client_protocol`
			DROP FOREIGN KEY `FK_client_protocol_clients`,
			DROP FOREIGN KEY `FK_client_protocol_managers`;")->execute();
		
		$this->dbConnection->createCommand("
		    ALTER TABLE `client_protocol`
			ADD CONSTRAINT `FK_client_protocol_clients` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE,
			ADD CONSTRAINT `FK_client_protocol_managers` FOREIGN KEY (`manager_id`) REFERENCES `managers` (`managerId`) ON DELETE CASCADE;

		")->execute();
	}

	public function safeDown() {
		echo 'm130828_075730_change_clients_protocols_foreign_key does not support migration down.'."\n";
		return true;
	}
}
