<?php

/**
 * Миграция привязки клиентов к регионам и городам.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013
 */
class m130828_043738_change_link_clients_addresses extends CDbMigration {
	public function safeUp() {
		$this->dropForeignKey('FK_clients_regions', 'clients');
		$this->dropForeignKey('FK_clients_cities', 'clients');
		$this->dropIndex('FK_clients_regions', 'clients');
		$this->dropIndex('FK_clients_cities', 'clients');
		$this->getDbConnection()->createCommand('
			ALTER TABLE `clients`
				MODIFY COLUMN `region_id` varchar(36) DEFAULT NULL COMMENT \'Регион\',
				MODIFY COLUMN `city_id` varchar(36) DEFAULT NULL COMMENT \'Город\'
		')->execute();
		$this->update('clients', ['region_id' => null]);
		$this->update('clients', ['city_id' => null]);
		$this->createIndex('region', 'clients', 'region_id');
		$this->createIndex('city', 'clients', 'city_id');
		$this->addForeignKey('fk_clients_regions', 'clients', 'region_id', 'addresses', 'AOID', 'NO ACTION', 'NO ACTION');
		$this->addForeignKey('fk_clients_cities', 'clients', 'city_id', 'addresses', 'AOID', 'NO ACTION', 'NO ACTION');
	}

	public function safeDown() {
		echo 'm130828_043738_change_link_clients_addresses does not support migration down.'."\n";

		return true;
	}
}
