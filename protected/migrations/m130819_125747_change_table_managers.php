<?php

/**
 * Миграция добавления в таблицу менеджеров полей email и password.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m130819_125747_change_table_managers extends CDbMigration {
	# добавления в таблицу менеджеров полей email и password
	public function safeUp() {
		$this->dbConnection->createCommand("ALTER TABLE `managers`
		    ADD COLUMN `email` VARCHAR(255) NOT NULL DEFAULT '' COMMENT 'email менеджера' AFTER `name`,
		    ADD COLUMN `password` VARCHAR(32) NOT NULL DEFAULT '' COMMENT 'md5 хэш пароля' AFTER `email`,
		    ADD UNIQUE INDEX `email` (`email`);")->execute();
	}

	public function safeDown() {
		$this->dbConnection->createCommand("ALTER TABLE `managers`
		    DROP COLUMN `email`,
		    DROP COLUMN `password`,
		    DROP INDEX `email`;")->execute();
	}
}
