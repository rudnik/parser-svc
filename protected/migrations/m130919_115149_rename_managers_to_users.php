<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m130919_115149_rename_managers_to_users extends CDbMigration {
	public function safeUp() {
        $this->dbConnection->createCommand('SET FOREIGN_KEY_CHECKS = 0;')->execute();
        
        # Удаляем внешние ключи
        $this->dbConnection->createCommand("
            ALTER TABLE `clients`
                DROP INDEX `FK_clients_managers`,
                DROP FOREIGN KEY `FK_clients_managers`;
        ")->execute();
        
        $this->dbConnection->createCommand("
            ALTER TABLE `clients_protocols`
                DROP INDEX `FK_client_protocol_managers`,
                DROP FOREIGN KEY `FK_client_protocol_managers`;
        ")->execute();
        
        
        
        # Переименовываем поля
        $this->dbConnection->createCommand("
            ALTER TABLE `managers`
                CHANGE COLUMN `managerId` `userId` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор пользователя' FIRST;
        ")->execute();
        
        $this->dbConnection->createCommand("
            ALTER TABLE `clients`
                CHANGE COLUMN `manager_id` `user_id` SMALLINT(5) UNSIGNED NULL DEFAULT NULL COMMENT 'id пользователя' AFTER `kpp`;
        ")->execute();        
        
        $this->dbConnection->createCommand("
            ALTER TABLE `clients_protocols`
                CHANGE COLUMN `manager_id` `user_id` SMALLINT(5) UNSIGNED NOT NULL COMMENT 'id пользователя' AFTER `client_id`;
        ")->execute();   
        
        # переименовываем таблицу
        $this->renameTable('managers', 'users');
        
        
        
        # Добавляем внешние ключи
        $this->dbConnection->createCommand("
            ALTER TABLE `clients_protocols`
                ADD CONSTRAINT `FK_clients_protocols_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`userId`) ON DELETE CASCADE;
        ")->execute();
        
        $this->dbConnection->createCommand("
            ALTER TABLE `clients`
                ADD CONSTRAINT `FK_clients_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`userId`);
        ")->execute();
	}

	public function safeDown() {
		echo 'm130919_071002_rename_tables does not support migration down.'."\n";

		return true;
	}
}
