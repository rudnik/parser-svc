<?php

/**
 * Миграция создания таблицы городов.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m130815_085055_add_table_cities extends CDbMigration {
	public function safeUp() {
		// создание таблицы городов
		$this->dbConnection->createCommand("
                        -- Дамп структуры для таблица tenders.cities
			DROP TABLE IF EXISTS `cities`;
			CREATE TABLE IF NOT EXISTS `cities` (
			  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
			  `region_id` tinyint(3) unsigned DEFAULT NULL COMMENT 'id региона',
			  `title` varchar(100) DEFAULT NULL COMMENT 'Название города',
			  PRIMARY KEY (`id`),
			  KEY `FK_cities_regions` (`region_id`),
			  CONSTRAINT `FK_cities_regions` FOREIGN KEY (`region_id`) REFERENCES `regions` (`regionId`) ON DELETE CASCADE
			) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Города';
		")->execute();
		$this->insert('cities', ['region_id' => 90, 'title' => 'Краснодар']);
		$this->insert('cities', ['region_id' => 90, 'title' => 'Краснодар 1']);
	}

	public function safeDown() {
		$this->dropTable('cities');
	}
}
