<?php

/**
 * Миграция создания связующей таблицы подписок и регионов.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m130812_090130_add_table_client_subscriptions_regions extends CDbMigration {
	public function safeUp() {
		// создание связующей таблицы подписок и регионов
		$this->dbConnection->createCommand("
                        CREATE TABLE `clients_subscriptions_regions` (
                                `client_subscription_id` int(10) unsigned DEFAULT NULL COMMENT 'id подписки',
                                `region_id` tinyint(3) unsigned DEFAULT NULL COMMENT 'id региона',
                                UNIQUE KEY `client_subscription_id_region_id` (`client_subscription_id`,`region_id`),
                                KEY `FK_clients-subscriptions-regions_regions` (`region_id`),
                                CONSTRAINT `FK_clients-subscriptions-regions_regions` FOREIGN KEY (`region_id`) REFERENCES `regions` (`regionId`),
                                CONSTRAINT `FK_clients-subscriptions-regions_clients-subscriptions` FOREIGN KEY (`client_subscription_id`) 
                                REFERENCES `clients-subscriptions` (`subscrId`)
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Регионы подписки'
		")->execute();
	}

	public function safeDown() {
		$this->dropTable('clients-subscriptions-regions');
	}
}



