<?php

/**
 * Миграция перемещения дат начала и окончания заявки в поисковые таблицы.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013
 */
class m130726_093532_move_all_date_to_search extends CDbMigration {
	public function safeUp() {
		$this->delete('bids', 'created<STR_TO_DATE("2013-07-01 00:00:00", "%Y-%m-%d %H:%i:%s")');
		$this->delete('bids', 'created BETWEEN STR_TO_DATE("2013-07-26 00:00:00", "%Y-%m-%d %H:%i:%s") AND STR_TO_DATE("2013-07-26 23:59:59", "%Y-%m-%d %H:%i:%s")');
		$this->delete('bids-data-date', 'value="0000-00-00 00:00:00"');
		$this->delete('bids-data-date-search', 'value="0000-00-00 00:00:00"');
		$datas = $this->getDbConnection()->createCommand('
			SELECT
				`bd`.`dataId`
			FROM
				`bids-data` AS `bd`
				JOIN `zakupki-fields` AS `f`
					ON `f`.`fieldId`=`bd`.`fieldId`
					AND `f`.`name` IN ("notificationCommission/p1Date", "notificationCommission/p2Date")
		')->queryAll();

		foreach ($datas as $data) {
			$data_id = (int) $data['dataId'];
			$value = $this->getDbConnection()->createCommand('
				SELECT
					`value`
				FROM
					`bids-data-text`
				WHERE
					`dataId`='.$data_id
			)->queryScalar();

			if ($value) {
				$date = new DateTime($value);
				$this->insert('bids-data-date-search', [
					'dataId' => $data_id,
					'value'  => $date->format('Y-m-d H:i:s')
				]);
			}

			$this->delete('bids-data-text', 'dataId='.$data_id);
		}
	}

	public function safeDown() {
		echo 'm130726_093532_move_all_date_to_search does not support migration down.'."\n";
		return true;
	}
}
