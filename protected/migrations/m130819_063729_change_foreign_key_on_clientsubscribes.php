<?php

/**
 * Миграция изменения внешнего ключа в таблице clients-subscriptions.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m130819_063729_change_foreign_key_on_clientsubscribes extends CDbMigration {
	# изменения внешнего ключа в таблице clients-subscriptions
	public function safeUp() {
		$this->dbConnection->createCommand("SET FOREIGN_KEY_CHECKS=0;")->execute();
		$this->dbConnection->createCommand("ALTER TABLE `clients`
		    CHANGE COLUMN `id` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id клиента' FIRST;")->execute();
		$this->dbConnection->createCommand("ALTER TABLE `clients-subscriptions` 
		    DROP FOREIGN KEY `fk_cs_client`;")->execute();
		$this->dbConnection->createCommand("ALTER TABLE `clients-subscriptions` 
		    ADD CONSTRAINT `fk_cs_client` 
		    FOREIGN KEY (`clientId`) REFERENCES `clients` (`id`) 
		    ON UPDATE NO ACTION ON DELETE CASCADE;")->execute();
	}

	public function safeDown() {
		$this->dbConnection->createCommand("SET FOREIGN_KEY_CHECKS=0;")->execute();
		$this->dbConnection->createCommand("ALTER TABLE `clients`
		    CHANGE COLUMN `id` `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id клиента' FIRST;")->execute();
		$this->dbConnection->createCommand("ALTER TABLE `clients-subscriptions`
		    DROP FOREIGN KEY `fk_cs_client`;")->execute();
		$this->dbConnection->createCommand("ALTER TABLE `clients-subscriptions`
		    ADD CONSTRAINT `fk_cs_client` 
		    FOREIGN KEY (`clientId`) REFERENCES `clients_old` (`clientId`) 
		    ON UPDATE NO ACTION ON DELETE CASCADE;")->execute();
	}
}
