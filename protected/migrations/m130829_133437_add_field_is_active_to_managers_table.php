<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m130829_133437_add_field_is_active_to_managers_table extends CDbMigration {
	public function safeUp() {
		$this->dbConnection->createCommand("
		    ALTER TABLE `managers`
			ADD COLUMN `is_active` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Активность менеджера' AFTER `role_id`;
		")->execute();
	}

	public function safeDown() {
		$this->dbConnection->createCommand("
		    ALTER TABLE `managers`
			DROP COLUMN `is_active`;
		")->execute();
	}
}
