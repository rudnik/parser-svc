<?php

/**
 * Миграция создания записи парсинга портала госзакупок и добавление справочника
 * выгружаемых порталом документов.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013 BSTsoft
 */
class m130607_121209_create_portal_zakupki extends CDbMigration {
	public function safeUp() {
		// выгружаемые документы
		$doctypes = [
			'notificationOK', 'notificationEF', 'notificationZK',
			'notificationPO', 'notificationSZ', 'timeEF', 'notificationCancel',
			'protocolEF1', 'clarificationRequest', 'clarification', 'protocolOK1',
			'protocolOK2', 'protocolOK3', 'protocolEF2', 'protocolEF3',
			'protocolZK1', 'protocolZK5', 'protocolPO1', 'protocolEvasion',
			'protocolCancel', 'contractSign', 'contract', 'contractCancel',
			'contractProcedure', 'attachment', 'complaint', 'decision',
			'complaintCancel', 'tenderPlan', 'masterDataBatch', 'confirmation',
			'contractBatch'
		];
		// добавляем госзакупки
		$this->insert('portals', [
			'name' => 'zakupki', 'title' => 'Портал госзакупок', 'url' => 'http://zakupki.gov.ru/wps/portal/base/topmain/home'
		]);
		// добавляем справочник выгружаемых документов
		$this->dbConnection->createCommand('
			CREATE TABLE `zakupki-doctypes` (
				`doctypeId` tinyint(2) unsigned NOT NULL AUTO_INCREMENT COMMENT \'Идентификатор\',
				`name` varchar(300) NOT NULL DEFAULT \'\' COMMENT \'Название документа\',
				PRIMARY KEY (`doctypeId`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=\'Типы XML документов\''
		)->execute();

		foreach ($doctypes as $type) {
			$this->insert('zakupki-doctypes', ['name' => $type]);
		}
	}

	public function safeDown() {
		$this->dropTable('zakupki-doctypes');
		$this->delete('portals', ['name=:name'], [':name' => 'zakupki']);
	}
}
