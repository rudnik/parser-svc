<?php

/**
 * Миграция удаления поискоковго поля.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013
 */
class m130820_064757_remove_search_start_date extends CDbMigration {
	public function safeUp() {
		$this->dropColumn('bids_data_search', 'quote_date_start');
	}

	public function safeDown() {
		echo 'm130820_064757_remove_search_start_date does not support migration down.'."\n";
		return true;
	}
}
