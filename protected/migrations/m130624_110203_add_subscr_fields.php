<?php

/**
 * Миграция добавления полей рассылки.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013
 */
class m130624_110203_add_subscr_fields extends CDbMigration {
	public function safeUp() {
		$this->dbConnection->createCommand('
			ALTER TABLE `clients-subscriptions`
				ADD COLUMN `regionId` tinyint(3) unsigned NOT NULL DEFAULT \'0\' COMMENT \'Идентификатор региона\' AFTER `subscrId`,
				ADD COLUMN `title` varchar(50) NOT NULL DEFAULT \'\' COMMENT \'Наименование\',
				ADD COLUMN `active` tinyint(1) NOT NULL DEFAULT \'0\' COMMENT \'Активен\',
				ADD COLUMN `activedate_from` timestamp NOT NULL DEFAULT \'0000-00-00 00:00\' COMMENT \'Дата действия "с"\',
				ADD COLUMN `activedate_to` timestamp NOT NULL DEFAULT \'0000-00-00 00:00\' COMMENT \'Дата действия "по"\',
				ADD COLUMN `keywords_inc` varchar(1024) NOT NULL DEFAULT \'\' COMMENT \'Ключевые слова\',
				ADD COLUMN `keywords_exc` varchar(1024) NOT NULL DEFAULT \'\' COMMENT \'Слова-исключения\',
				ADD COLUMN `search_in` enum(\'all\', \'title\', \'anons\', \'text\') NOT NULL DEFAULT \'all\' COMMENT \'Места поиска\',
				ADD COLUMN `price_min` varchar(20) NOT NULL DEFAULT \'\' COMMENT \'Минимальная сумма\',
				ADD COLUMN `price_max` varchar(20) NOT NULL DEFAULT \'\' COMMENT \'Максимальная сумма\',
				ADD COLUMN `pubdate_from_start` timestamp NOT NULL DEFAULT \'0000-00-00 00:00\' COMMENT \'Дата размещения "с"\',
				ADD COLUMN `pubdate_from_end` timestamp NOT NULL DEFAULT \'0000-00-00 00:00\' COMMENT \'Дата окончания размещения "с"\',
				ADD COLUMN `pubdate_to_start` timestamp NOT NULL DEFAULT \'0000-00-00 00:00\' COMMENT \'Дата размещения "по"\',
				ADD COLUMN `pubdate_to_end` timestamp NOT NULL DEFAULT \'0000-00-00 00:00\' COMMENT \'Дата окончания размещения "по"\'
		')->execute();
	}

	public function safeDown() {
		echo 'm130624_110203_add_subscr_fields does not support migration down.'."\n";
		return true;
	}
}
