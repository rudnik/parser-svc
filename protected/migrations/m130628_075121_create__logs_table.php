<?php

/**
 * Миграция создания таблицы логов.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013
 */
class m130628_075121_create__logs_table extends CDbMigration {
	public function safeUp() {
		// добавление таблицы порталов
		$this->dbConnection->createCommand("
			CREATE TABLE IF NOT EXISTS `logs` (
                            `id` int(11) NOT NULL AUTO_INCREMENT,
                            `level` varchar(128) DEFAULT NULL,
                            `category` varchar(128) DEFAULT NULL,
                            `logtime` int(11) DEFAULT NULL,
                            `message` text,
                        PRIMARY KEY (`id`)
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
		)->execute();
	}

	public function safeDown() {
		$this->dropTable('logs');
	}
}





