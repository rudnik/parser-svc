<?php

/**
 * Миграция создания поисковых таблиц.
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013
 */
class m130705_051824_create_search_tables extends CDbMigration {
	public function safeUp() {
		/* $this->getDbConnection()->createCommand('
			ALTER TABLE `bids-data-text` MODIFY COLUMN `value` mediumtext DEFAULT NULL COMMENT \'Значение\'
		')->execute(); */
		$this->createTable('bids-data-num-search', [
			'valueId' => 'bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT \'Идентификатор значения\'',
			'dataId'  => 'bigint(20) unsigned NOT NULL DEFAULT \'0\' COMMENT \'Идентификатор данных\'',
			'value'   => 'decimal(25, 2) NOT NULL DEFAULT \'0\' COMMENT \'Значение\'',
			'PRIMARY KEY (`valueId`)',
			'KEY `data` (`dataId`)',
			'CONSTRAINT `fk_bdns_bd` FOREIGN KEY (`dataId`) REFERENCES `bids-data` (`dataId`) ON DELETE CASCADE ON UPDATE NO ACTION',
		], 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=\'Числовые данные для поиска\'');
		$this->createTable('bids-data-text-search', [
			'valueId' => 'bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT \'Идентификатор значения\'',
			'dataId'  => 'bigint(20) unsigned NOT NULL DEFAULT \'0\' COMMENT \'Идентификатор данных\'',
			'value'   => 'mediumtext DEFAULT NULL COMMENT \'Значение\'',
			'PRIMARY KEY (`valueId`)',
			'KEY `data` (`dataId`)',
			'KEY `value` (`value`(10))',
			'CONSTRAINT `fk_bdts_bd` FOREIGN KEY (`dataId`) REFERENCES `bids-data` (`dataId`) ON DELETE CASCADE ON UPDATE NO ACTION',
		], 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=\'Текстовые данные для поиска\'');
		$this->createTable('bids-data-date-search', [
			'valueId' => 'bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT \'Идентификатор значения\'',
			'dataId'  => 'bigint(20) unsigned NOT NULL DEFAULT \'0\' COMMENT \'Идентификатор данных\'',
			'value'   => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT \'Значение\'',
			'PRIMARY KEY (`valueId`)',
			'KEY `data` (`dataId`)',
			'CONSTRAINT `fk_bdds_bd` FOREIGN KEY (`dataId`) REFERENCES `bids-data` (`dataId`) ON DELETE CASCADE ON UPDATE NO ACTION',
		], 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=\'Данные дат для поиска\'');

		$this->moveData('num',  ['price']);
		$this->moveData('date', ['publishDate']);
		$this->moveData('text', ['products/product/name']);
	}

	private function moveData($type, $fields) {
		$fields = array_map(function($field) { return $this->getDbConnection()->quoteValue($field); }, $fields);
		$datas = $this->getDbConnection()->createCommand('
			SELECT
				`bd`.`dataId`
			FROM
				`bids-data` AS `bd`
				JOIN `zakupki-fields` AS `f`
					ON `f`.`fieldId`=`bd`.`fieldId`
					AND `f`.`name` IN ('.implode(', ', $fields).')
				JOIN `bids` AS `b`
					ON `b`.`bidId`=`bd`.`bidId`
					AND `b`.`created` BETWEEN STR_TO_DATE("2013-07-04 00:00:00", "%Y-%m-%d %H:%i:%s") AND STR_TO_DATE("2013-07-04 23:59:59", "%Y-%m-%d %H:%i:%s")
		')->queryAll();

		foreach ($datas as $data) {
			$data_id = (int) $data['dataId'];
			$this->getDbConnection()->createCommand('
				INSERT INTO
					`bids-data-'.$type.'-search` (`dataId`, `value`)
				SELECT
					'.$data_id.',
					`value`
				FROM
					`bids-data-'.$type.'`
				WHERE
					`dataId`='.$data_id
			)->execute();
			$this->delete('bids-data-'.$type, 'dataId='.$data_id);
		}

		unset($datas);
	}

	public function safeDown() {
		//return true;
		$this->dropTable('bids-data-num-search');
		$this->dropTable('bids-data-date-search');
		$this->dropTable('bids-data-text-search');
	}
}
