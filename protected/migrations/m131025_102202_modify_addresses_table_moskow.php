<?php

/**
 * Миграция .
 *
 * @package TendersParser
 * @subpackage Migrate
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013
 */
class m131025_102202_modify_addresses_table_moskow extends CDbMigration {
	public function safeUp() {
		Yii::app()->db->createCommand("
          INSERT INTO `addresses` (`AOID`, `AOGUID`, `PARENTGUID`, `OFFNAME`, `SHORTNAME`, `AOLEVEL`, `REGIONCODE`, `AREACODE`, `AUTOCODE`, `CITYCODE`, `CTARCODE`, `PLACECODE`, `STREETCODE`, `EXTRCODE`, `SEXTCODE`, `NEXTID`, `PREVID`, `POSTALCODE`) VALUES ('moskow', '0c5b2444-70a0-4932-980c-b4dc0d3f02b5', '0c5b2444-70a0-4932-980c-b4dc0d3f02b5', 'Москва', 'г', 1, 77, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0);
		")->execute();

        Yii::app()->db->createCommand("
          UPDATE `addresses` SET `SHORTNAME` = '' WHERE `AOID` = '5c8b06f1-518e-496e-b683-7bf917e0d70b'
		")->execute();

	}

	public function safeDown() {
        Yii::app()->db->createCommand("
          DELETE FROM `addresses` WHERE `AOID` = 'moskow';
		")->execute();

        Yii::app()->db->createCommand("
          UPDATE `addresses` SET `SHORTNAME` = 'г' WHERE `AOID` = '5c8b06f1-518e-496e-b683-7bf917e0d70b'
		")->execute();
	}
}
