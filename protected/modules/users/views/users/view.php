<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	//'Пользователь'=>array('index'),
	$model->name,
);

$this->menu=array(
	//array('label'=>'Карточки пользователей', 'url'=>array('index')),
	array('label'=>'Добавить', 'url'=>array('create')),
	array('label'=>'Редактировать', 'url'=>array('update', 'id'=>$model->userId)),
	//array('label'=>'Удалить', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->userId),'confirm'=>'Вы уверены, что хотите удалить элемент?')),
	array('label'=>'Список пользователей', 'url'=>array('admin')),
);
?>

<h1>Карточка пользователя #<?php echo $model->userId; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'userId',
		[
		    'name' => 'is_active',
		    'value' => $model->is_active ? 'Да' : 'Нет',
		],
		'name',
		'email',
		[
		    'label' => 'Роль',
		    'name' => 'role.title',
		],
	),
)); ?>
