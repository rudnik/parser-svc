<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	//'Пользователи'=>array('index'),
	'Добавление',
);

$this->menu=array(
	//array('label'=>'Карточки пользователей', 'url'=>array('index')),
	array('label'=>'Список пользователей', 'url'=>array('admin')),
);
?>

<h1>Добавление пользователя</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>