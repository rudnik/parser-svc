<?php
/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */
?>

<div class="well">

<?php $form = $this->beginWidget('CActiveForm', array(
	'id'=>'users-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Поля, помеченные звездочкой <span class="required">*</span>, обязательны к заполнению.</p>
    <?if($model->isNewRecord):?>
    <p class="note">При добавлении пользователя вы можете не указывать пароль - от автоматически сгенерируется и отправится пользователю на email.</p>
    <?endif;?>

	<?php echo $form->errorSummary($model); ?>  

	<div class="row-fluid">
		<?php echo $form->labelEx($model,'is_active'); ?>
		<?php echo $form->dropDownList($model, 'is_active', ['0' => 'Нет', '1' => 'Да']); ?>
		<?php echo $form->error($model,'is_active'); ?>
	</div>

	<div class="row-fluid">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>
	
	<div class="row-fluid">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->emailField($model,'email',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row-fluid">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password',array('size'=>32,'maxlength'=>32)); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>
	<div class="row-fluid">
		<?php echo $form->labelEx($model,'password_confirm'); ?>
		<?php echo $form->passwordField($model,'password_confirm', array('size'=>32,'maxlength'=>32)); ?>
		<?php echo $form->error($model,'password_confirm'); ?>
	</div>

	<div class="row-fluid">
		<?php echo $form->labelEx($model,'role');?>
		<?php echo $form->dropdownList($model, 'role_id', CHtml::listData(Roles::model()->findAll(['order' => 'title DESC']), 'id', 'title')); ?>
		<?php echo $form->error($model,'role_id');?>
	</div>

    <div class="row-fluid">
        <?php echo $form->labelEx($model,'mail_signature');?>
        <?php echo $form->textArea($model, 'mail_signature', ['class' => 'span6', 'rows' => 5]);?>
        <?php echo $form->error($model,'mail_signature');?>
    </div>

	<div class="row-fluid buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => 'btn btn-primary']); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->