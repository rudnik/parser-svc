<?php
/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */
?>

<div class="well">

<?php 
if(1)
$form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row-fluid">
		<?php echo $form->label($model,'userId'); ?>
		<?php echo $form->textField($model,'userId'); ?>
	</div>

	<div class="row-fluid">
		<?php echo $form->label($model,'is_active'); ?>
		<?php echo $form->dropDownList($model, 'is_active', ['' => '--- не важно ---', '0' => 'Нет', '1' => 'Да']); ?>
	</div>

	<div class="row-fluid">
		<?php echo $form->label($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row-fluid">
		<?php echo $form->label($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255)); ?>
	</div>
	<div class="row-fluid">
		<?php echo $form->label($model,'role'); ?>
		<?php echo $form->dropDownList($model, 'role_id', CMap::mergeArray(['' => '--- любая роль ---'], CHtml::listData(Roles::model()->findAll(['order' => 'title ASC']), 'id', 'title')) ); ?>
	</div>

	<div class="row-fluid buttons">
		<?php echo CHtml::submitButton('Найти', ['class' => 'btn btn-primary']); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->