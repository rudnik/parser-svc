<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	//'Пользователи'=>array('index'),
	$model->name=>array('view','id'=>$model->userId),
	'Обновление',
);

$this->menu=array(
	//array('label'=>'Карточки пользователей', 'url'=>array('index')),
	array('label'=>'Добавить', 'url'=>array('create')),
	array('label'=>'Посмотреть карточку', 'url'=>array('view', 'id'=>$model->userId)),
	array('label'=>'Список пользователей', 'url'=>array('admin')),
);
?>

<h1>Обновление пользователя <?php echo $model->userId; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>