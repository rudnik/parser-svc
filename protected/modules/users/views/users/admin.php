<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	//'Пользователи'=>array('index'),
	'Список',
);

$this->menu=array(
	//array('label'=>'Карточки пользователей', 'url'=>array('index')),
	array('label'=>'Добавить', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#users-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Список пользователей</h1>

<p>
Вы можете вводить операторы сравнения (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
или <b>=</b>) в начале каждого поискового поля для соответствующей фильтрации результов поиска.
</p>

<?php echo CHtml::link('Расширенный поиск...','#',array('class'=>'btn btn-success search-button')); ?>
<div class="search-form" style="display:none">
<br>
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
    'id' => 'users-grid',
    'enableSorting' => false,
    'type' => 'striped bordered',
	'dataProvider'=>$model->search(),
	'filter' => $model,
	'columns' => array(
		'userId',
		[
		    'name' => 'is_active',
		    'value' => '$data->is_active ? "Да" : "Нет"',
		    'filter' => CHtml::dropDownList('Users[is_active]' , $model->is_active, ['' => '--- не важно ---', '0' => 'Нет', '1' => 'Да']),
		],
		'name',
		'email',
		[
		    'header' => 'Роль',
		    'name' => 'role.title',
		    'filter' => CHtml::dropDownList('Users[role_id]' , $model->role_id, CMap::mergeArray(['' => '--- любая роль ---'], CHtml::listData(Roles::model()->findAll(['order' => 'title ASC']), 'id', 'title')) ),
		],
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{view} {update}',
		),
	),
)); ?>
