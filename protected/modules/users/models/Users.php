<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $userId
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $mail_signature
 *
 * The followings are the available model relations:
 * @property ClientProtocol[] $clientProtocols
 * @property Clients[] $clients
 */
class Users extends CActiveRecord
{
    public $password_confirm;

    /**
     * Эмуляция отделов
     * todo: Убрать, когда грамотно реализуется иеррархическая структура менеджеров
     * @var array - [
     *  id_руководителя_отдела1 => [id_подчиненного1, id_подчиненного2, .... ],
     *  id_руководителя_отдела2 => [id_подчиненного56, id_подчиненного57, .... ],
     *  ...
     * ]
     */
    public static $usersRelations = [
        16 => [43, 42],
        43 => [18, 34, 23, 35, 36, 37, 38, 39, 40, 41],
        42 => [19]
    ];

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Users the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'users';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, email', 'length', 'max' => 255),
            array('email', 'email', 'message' => 'Неверный формат'),
            array('email', 'unique', 'message' => 'Пользователь с таким email уже существует.'),
            array('password', 'length', 'max' => 32),
            array('password_confirm', 'length', 'max' => 32),
            array('role_id', 'numerical', 'integerOnly' => true, 'min' => '0', 'max' => 255),
            array('role, is_active, mail_signature', 'safe'),
            # add
            ['name', 'required', 'message' => 'Поле Ф.И.О. не может быть пустым', 'on' => 'add'],
            ['email', 'required', 'message' => 'Поле Email не может быть пустым', 'on' => 'add'],
            ['password', 'required', 'message' => 'Поле Пароль не может быть пустым', 'on' => 'add'],
            [
                'password_confirm',
                'required',
                'message' => 'Поле Подтверждение пароля не может быть пустым',
                'on' => 'add'
            ],
            [
                'password_confirm',
                'compare',
                'compareAttribute' => 'password',
                'message' => 'Пароли должны совпадать',
                'on' => 'add'
            ],
            ['role_id', 'required', 'message' => 'Поле Роль не может быть пустым', 'on' => 'add'],
            # update
            ['name', 'required', 'message' => 'Поле Ф.И.О. не может быть пустым', 'on' => 'update'],
            ['email', 'required', 'message' => 'Поле Email не может быть пустым', 'on' => 'update'],
            ['role_id', 'required', 'message' => 'Поле Роль не может быть пустым', 'on' => 'update'],
            ['password', 'safe', 'on' => 'update'],
            ['password_confirm', 'safe', 'on' => 'update'],
            [
                'password_confirm',
                'compare',
                'compareAttribute' => 'password',
                'message' => 'Пароли должны совпадать',
                'on' => 'update'
            ],
            # edit
            [
                'password',
                'filter',
                'filter' => function ($value) {
                        return '';
                    },
                'on' => 'edit'
            ],
            # search
            array('userId, name, email, password, role_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'clientProtocols' => array(self::HAS_MANY, 'ClientProtocol', 'user_id'),
            'clients' => array(self::HAS_MANY, 'Clients', 'user_id'),
            'role' => array(self::BELONGS_TO, 'Roles', 'role_id'),
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'userId' => 'Id',
            'is_active' => 'Активен',
            'name' => 'Ф.И.О.',
            'email' => 'Email',
            'password' => 'Пароль',
            'password_confirm' => 'Подтверждение пароля',
            'role_id' => 'id роли',
            'role' => 'Роль',
            'mail_signature' => 'Подпись в рассылке',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('userId', $this->userId);
        if ($this->is_active !== "") {
            $criteria->compare('is_active', $this->is_active);
        }
        $criteria->compare('role_id', $this->role_id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('password', $this->password, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function beforeSave()
    {
        if (!parent::beforeSave()) {
            return false;
        }

        if (strlen($this->password) > 0) {
            $this->password = md5($this->password);
        }
        if ($this->scenario == 'update') {
            if ($this->password == null) {
                $tempModel = static::model()->findByPk($this->userId, ['select' => 'password']);
                $this->password = $tempModel->password;
            }
        }

        return true;
    }

    /**
     * Не удаляем менеджера, а деактвируем
     */
    public function delete()
    {
        $this->saveAttributes(['is_active' => 0]);
    }

    /**
     * Возвращает статус активности пользователя
     * @param integer $userId - id пользователя
     * @return boolean
     */
    public static function isActive($userId)
    {
        $user = static::model()->find(
            [
                'select' => 'is_active',
                'condition' => 'userId = :userId',
                'params' => [':userId' => $userId],
            ]
        );

        return is_null($user) ? false : $user->is_active;
    }

    public static function isMyUser($ownerId, $userId)
    {
        return false;//isset(static::$usersRelations[$ownerId]) && in_array($userId, static::$usersRelations[$ownerId]);
    }

    public static function getDropdownList(CWebUser $user)
    {
        $result = [];

        if ($user->checkAccess('admin')) {
            $result = CMap::mergeArray(
                ['' => '--- любой ---'],
                CHtml::listData(
                    Users::model()->findAll(['order' => 'name']),
                    'userId',
                    'name'
                )
            );
        } elseif (isset(Users::$usersRelations[$user->id])) {
            $result = CMap::mergeArray(
                ['' => '--- любой ---'],
                CHtml::listData(
                    Users::model()->findAll(
                        [
                            'order' => 'name',
                            'condition' => 'userId IN (' . implode(',', Users::$usersRelations[$user->id]) . ')'
                        ]
                    ),
                    'userId',
                    'name'
                )
            );
        }

        return $result;
    }
}
