<?php
/**
 * Интерфейс части письма
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013 BSTsoft
 */
interface IBidMailPart {
    /**
     * Контсруктор
     * @param Bids $bid
     */
    public function __construct(Bids $bid);
    
    /**
     * Возвращает отрендеренный HTML-код части письма
     * @return string
     */
    public function render();
}