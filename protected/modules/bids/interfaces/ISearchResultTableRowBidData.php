<?php
interface ISearchResultTableRowBidData
{
    public function getHref();
    public function getName();
    public function getPrice();
    public function getPublishDateStart();
    public function getPublishDateEnd();
}