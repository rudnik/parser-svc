<?php

/**
 * Модель полей закупки. Таблица "bids_fields".
 *
 * Доступные столбцы в таблице "bids_fields":
 * @property integer $fieldId Идентификатор поля.
 * @property integer $doctypeId Идентификатор типа документа.
 * @property string $name Имя поля.
 * @property bool $unique Уникальное поле.
 * @property string $type Тип поля.
 *
 * @package TendersParser
 * @subpackage bids
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013 BSTsoft
 */
class BidsFields extends CActiveRecord {
	/**
	 * Шаблон ссылки извещения на портале.
	 *
	 * @const string
	 */
	const LINK_NOTIFICATION = 'http://zakupki.gov.ru/pgz/public/action/orders/info/common_info/show?notificationId={id}';

	/**
	 * Шаблон ссылки контракта на портале.
	 *
	 * @const string
	 */
	const LINK_CONTRACT = 'http://zakupki.gov.ru/pgz/public/action/contracts/info/common_info/show?contractInfoId={id}';

	/**
	 * Тип поля числовых данных.
	 *
	 * @const string
	 */
	const TYPE_NUM = 'num';

	/**
	 * Тип поля текстовых данных.
	 *
	 * @const string
	 */
	const TYPE_TEXT = 'text';

	/**
	 * Тип поля данных дат
	 *
	 * @const string
	 */
	const TYPE_DATE = 'date';

	/**
	 * Тип организации, размещающей заказ.
	 *
	 * @var array
	 */
	public static $placerOrgType = [
		'Z' => 'Заказчик', 'O' => 'Организатор совместных торгов',
		'U' => 'Уполномоченный орган', 'S' => 'Специализированная организация'
	];
    
    /**
     * Поля, значения которых не обновляются
     * @var string[]
     */
    public static $notUpdatedFields = [
        'publishDate',
        'purchaseNotice/body/item/purchaseNoticeData/createDateTime',
	    'purchaseNoticeOA/body/item/purchaseNoticeOAData/createDateTime',
	    'purchaseNoticeOK/body/item/purchaseNoticeOKData/createDateTime',
	    'purchaseNoticeAE/body/item/purchaseNoticeAEData/createDateTime',
	    'purchaseNoticeZK/body/item/purchaseNoticeZKData/createDateTime',
	    'purchaseNoticeEP/body/item/purchaseNoticeEPData/createDateTime',
    ];

	/**
	 * Возвращает имя таблицы в базе данных.
	 *
	 * @return string Имя таблицы в базе данных.
	 */
	public function tableName() {
		return 'bids_fields';
	}

	/**
	 * Возвращает модель класса.
	 *
	 * @return BidsFields Модель класса.
	 */
	public static function model($className =__CLASS__) {
		return parent::model($className);
	}

	/**
	 * Возвращает массив правил валидации.
	 * @return array правила валидации
	 */
	public function rules() {
		return [
			['doctypeId, name', 'required'],
			['unique, type', 'safe']
		];
	}

	/**
	 * Возращает связи с другими таблицами.
	 *
	 * @return array Правила отношений.
	 */
	public function relations() {
		return [
			'bid_doctype' => [self::BELONGS_TO, 'BidsDoctypes', 'doctypeId'],
			'data'            => [self::HAS_MANY,   'BidsData',        'bidId']
		];
	}

	/**
	 * Список атрибутов модели.
	 *
	 * @return array Атрибуты.
	 */
	public function attributeLabels() {
		return [
			'fieldId'   => 'Идентификатор поля',
			'doctypeId' => 'Идентификатор типа документа',
			'name'      => 'Имя поля',
			'unique'    => 'Уникальное поле',
			'type'      => 'Тип поля'
		];
	}
}
