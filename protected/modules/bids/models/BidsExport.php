<?php

/**
 * Модель экспортных данных для внешних источников. Таблица "bids_export".
 *
 * Доступные столбцы в таблице "bids_export":
 *
 * @property integer $bidId    Идентификатор закупки.
 * @property integer $clientId Идентификатор клиента.
 *
 * @property Bids    $bid    Заявка.
 * @property Clients $client Клиент.
 *
 * @package    TendersParser
 * @subpackage bids
 * @author     Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright  2016 BSTsoft
 */
class BidsExport extends CActiveRecord
{
    /**
     * Возвращает модель класса.
     *
     * @param string $className Имя класса.
     *
     * @return BidsData Модель класса.
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Возвращает имя таблицы в базе данных.
     *
     * @return string Имя таблицы в базе данных.
     */
    public function tableName()
    {
        return 'bids_export';
    }

    /**
     * Возвращает массив правил валидации.
     *
     * @return array правила валидации
     */
    public function rules()
    {
        return [
            ['bidId, clientId', 'required']
        ];
    }

    /**
     * Возращает связи с другими таблицами.
     *
     * @return array Правила отношений.
     */
    public function relations()
    {
        return [
            'bid'    => [self::BELONGS_TO, 'Bids', 'bidId'],
            'client' => [self::BELONGS_TO, 'Clients', 'clientId']
        ];
    }

    /**
     * Список атрибутов модели.
     *
     * @return array Атрибуты.
     */
    public function attributeLabels()
    {
        return [
            'bidId'    => 'Идентификатор закупки',
            'clientId' => 'Идентификатор клиента'
        ];
    }

    /**
     * Сохранение модели с игнорированием ошибок.
     *
     * @param array   $bids     Массив id заявок, отправленных клиенту.
     * @param integer $clientId Идентфикатор клиента.
     *
     * @return bool
     *
     * @throws CDbException
     */
    public function saveExport($bids, $clientId)
    {
        return true;
    }
}
