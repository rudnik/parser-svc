<?php
class Zakupki94SearchResultTableRowBidData extends SearchResultTableRowBidData
{
    protected function getHrefFields()
    {
        return [
            '/href',
        ];
    }

    protected function getNameFields()
    {
        return [
            '/orderName',
        ];
    }

    protected function getPriceFields()
    {
        return [
            'lots/lot/customerRequirements/customerRequirement/maxPrice',
        ];
    }

    protected function getPublishDateStartFields()
    {
        return [
            '/publishDate',
        ];
    }

    protected function getPublishDateEndFields()
    {
        return [
            'notificationEF/notificationCommission/p1Date',
            'notificationZK/notificationCommission/p2Date',
            'notificationOK/competitiveDocumentProvisioning/deliveryTerm2',
        ];
    }

    public function getHref()
    {
        return CHtml::link('Перейти на сайт', $this->href, ['target' => '_blank']);
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getPublishDateStart()
    {
        return $this->publishDateStart;
    }

    public function getPublishDateEnd()
    {
        return $this->publishDateEnd;
    }
}