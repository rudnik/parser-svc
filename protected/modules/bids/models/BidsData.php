<?php

/**
 * Модель данных закупки. Таблица "bids_data".
 *
 * Доступные столбцы в таблице "bids":
 * @property integer $dataId Идентификатор данных.
 * @property integer $bidId Идентификатор закупки.
 * @property integer $fieldId Идентификатор поля.
 * @property BidsDataValues[] $values значения поля.
 *
 * @package TendersParser
 * @subpackage bids
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013 BSTsoft
 */
class BidsData extends CActiveRecord {
	/**
	 * Возвращает имя таблицы в базе данных.
	 *
	 * @return string Имя таблицы в базе данных.
	 */
	public function tableName() {
		return 'bids_data';
	}

	/**
	 * Возвращает модель класса.
	 *
	 * @return BidsData Модель класса.
	 */
	public static function model($className =__CLASS__) {
		return parent::model($className);
	}

	/**
	 * Возвращает массив правил валидации.
	 * @return array правила валидации
	 */
	public function rules() {
		return [
			['bidId, fieldId', 'required']
		];
	}

	/**
	 * Возращает связи с другими таблицами.
	 *
	 * @return array Правила отношений.
	 */
	public function relations() {
		return [
			'bid'                => [self::BELONGS_TO, 'Bids',               'bidId'],
			'field'              => [self::BELONGS_TO, 'BidsFields',      'fieldId'],
			'values'             => [self::HAS_MANY,   'BidsDataValues',     'dataId'],
			'values_num'         => [self::HAS_MANY,   'BidsDataNum',        'dataId'],
			'values_text'        => [self::HAS_MANY,   'BidsDataText',       'dataId'],
			'values_date'        => [self::HAS_MANY,   'BidsDataDate',       'dataId'],
			'values_num_search'  => [self::HAS_MANY,   'BidsDataNumSearch',  'dataId'],
			'values_text_search' => [self::HAS_MANY,   'BidsDataTextSearch', 'dataId'],
			'values_date_search' => [self::HAS_MANY,   'BidsDataDateSearch', 'dataId']
		];
	}

	/**
	 * Список атрибутов модели.
	 *
	 * @return array Атрибуты.
	 */
	public function attributeLabels() {
		return [
			'dataId'  => 'Идентификатор данных',
			'bidId'   => 'Идентификатор закупки',
			'fieldId' => 'Идентификатор поля'
		];
	}
}
