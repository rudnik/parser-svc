<?php

require_once 'sphinxapi.php';

/**
 * Класс поиска закупок.
 *
 * @package parser
 * @subpackage bids
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013
 */
class BidsSearch
{
    /**
     * Предел выборки элементов индекса.
     *
     * @const integer
     */
    const MATCH_LIMIT = 100000;

    /**
     * Объект поисковой машины.
     *
     * @var SphinxClient
     */
    private $sphinx = null;

    /**
     * Инициализация.
     */
    public function __construct()
    {
        $this->sphinx = new SphinxClient();
        $this->sphinx->SetServer('localhost', 9312);
        $this->sphinx->SetMatchMode(SPH_MATCH_EXTENDED);
        $this->sphinx->SetLimits(0, self::MATCH_LIMIT, self::MATCH_LIMIT);
    }

    /**
     * Поиск закупок.
     *
     * @param ClientSubscriptions $subscr Рассылка клиента.
     * @param integer $limit Ограничение количества.
     *
     * @return array Общее количество, список закупок, пагинатор.
     *
     * @throws InvalidArgumentException
     */
    public function search(ClientsSubscriptions $subscr, $limit = 100, $existingBidsIds = [])
    {
        // Подготавливаем параметры для фильтрации поискового результата
        $subscr->setScenario('beforeSearch');

        if (!$subscr->validate()) {
            throw new InvalidArgumentException(var_export($subscr->getErrors(), true));
        }

        // Устанавливаем фильтры
        $this->sphinx->ResetFilters();
        $this->sphinx->SetFilterRange(
            'publication_date',
            intval($subscr->pubdate_from_start),
            intval($subscr->pubdate_from_end)
        );

        $subscr->pubdate_to_start = (int) $subscr->pubdate_to_start;
        if($subscr->pubdate_to_start < 1) {
            $subscr->pubdate_to_start = strtotime(date("Y-m-d 00:00:00", time())) + 86400;
        }

        $this->sphinx->SetFilterRange(
            'quote_date_end',
            intval($subscr->pubdate_to_start),
            intval($subscr->pubdate_to_end)
        );
        $this->sphinx->SetFilterFloatRange(
            'max_cost',
            floatval($subscr->price_min),
            floatval($subscr->price_max)
        );

        if (count($subscr->regionsIdAsArray) > 0) {
            $this->sphinx->SetFilter('region_id', $subscr->regionsIdAsArray);
        }

        $this->sphinx->SetFilter(
            'doctypeId',
            DemandType::getPortalDemandsDoctypeIds($subscr->portalsIds, $subscr->demandTypesIds)
        );

        if (!empty($existingBidsIds)) {
            $this->sphinx->SetFilter('bid_id', $existingBidsIds, true);
        }

        $innList = $subscr->fieldAsArray('inn');
        if (!empty($innList)) {
            $this->sphinx->SetFilter('inn', $innList);
        }

        // Отправляем запрос Sphinx
        $result = $this->query($subscr->keywords_inc, $subscr->keywords_exc, (bool)$subscr->keywords_advance);

        $count = 0;
        $pages = new CPagination();
        $bids = [];

        if (!empty($result)) {
            $criteria = new CDbCriteria();
            $criteria->condition = 't.status!=:status';
            $criteria->params = [':status' => 'canceled'];
            $criteria->with = [
                'searchData' => [
                    'condition' => 'searchData.search_id IN (' . implode(
                            ', ',
                            array_keys($result)
                        ) . ' )'
                ]
            ];
            $criteria->order = 't.bidId DESC';
            $count = Bids::model()->count($criteria);

            if ($limit) {
                $pages->setItemCount($count);
                $pages->setPageSize($limit);
                $pages->applyLimit($criteria);
            }

            $bids = Bids::model()->findAll($criteria);
        }

        return [$count, $bids, $pages];
    }

    /**
     * Преобразование массива, возвращаемого Yii в обычный плоский массив
     *
     * @param array $data Данные.
     *
     * @return array $result
     */
    public function convertArray($data)
    {
        $result = [];

        foreach ($data as $d) {
            $result[] = array_shift($d);
        }

        return $result;
    }

    /**
     * Поиск данных.
     *
     * @param string $inc Слова, которые обязательны в тексте.
     * @param string $exc Слова, которых в тексте не должно быть.
     * @param bool $full Поиск по всем текстовым полям.
     *
     * @return array $found Список идентификаторов данных.
     *
     * @throws LogicException
     *
     * @see http://sphinxsearch.com/docs/manual-2.0.8.html#extended-syntax
     */
    private function query($inc, $exc, $full)
    {
        $params = [$inc, $exc];
        $inc = str_replace('!', '', $inc);
        $exc = str_replace('!', '', $exc);

        // собираем запрос для сфинкса. сначала обязательные слова, потом исключения
        foreach ($params as $i => &$item) {
            $item = trim($item);
            // разбиваем на условия ИЛИ
            $or = array_filter(explode("\n", $item));

            foreach ($or as &$or_item) {
                // разбиваем на условия И
                $and = array_filter(explode(',', $or_item));

                foreach ($and as &$and_item) {
                    $and_item = trim($and_item);

                    // встретили пробел - ищем как фразу, убрав лишние пробелы
                    if (false !== strpos($and_item, ' ')) {
                        $and_item = preg_replace('/\s+/', ' ', $and_item);
                        $and_item = '"' . $and_item . '"';
                    }
                }

                // склеиваем в И сфинкса
                if ($and) {
                    $or_item = '(' . implode(' ', $and) . ')';
                }
            }
            // склеиваем в ИЛИ сфинкса
            if ($or) {
                $item = ($i ? '!' : '') . '(' . implode(' | ', $or) . ')';
            } else {
                unset($params[$i]);
            }
        }

        // указываем в каких полях искать текст
        $query = ($full ? '@(title,description)' : '@title') . ' ' . implode(' & ', $params);

        // поиск без ключевых слов
        if (empty($params)) {
            $query = '@magic ((aaa))';
        } // поиск без ключевых слов по словам-исключениям
        elseif (empty($inc) && !empty($exc)) {
            $query = '@magic ((aaa)) & ' . $query;
        }

        $query = self::sphinxQueryCorrector($query);
        $result = $this->sphinx->Query($query);

        // Проверяем ошибки
        if (!$result) {
            //throw new LogicException($this->sphinx->_error);
            echo $this->sphinx->_error;
            return [];
        }

        if ($error = $this->sphinx->GetLastError()) {
            throw new LogicException($error);
        }

        if (!empty($result['error'])) {
            throw new LogicException($result['error']);
        }

        if (!isset($result['matches'])) {
            return [];
        }

        return $result['matches'];
    }

    /**
     * Корректирует запрос к sphinx перед выполнением
     * @param $query
     * @return mixed
     */
    public static function sphinxQueryCorrector($query)
    {
        # костыль номер один
        $query = str_replace('АТС', '*АТС', $query);

        return $query;
    }
}
