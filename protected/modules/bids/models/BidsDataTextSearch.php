<?php

/**
 * Модель поиска текстовых значений данных закупки. Таблица "bids_data-text-search".
 *
 * Доступные столбцы в таблице "bids_data-text-search":
 * @property integer $valueId Идентификатор значения.
 * @property integer $dataId Идентификатор данных.
 * @property string $value Значение.
 *
 * @package TendersParser
 * @subpackage bids
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013 BSTsoft
 */
class BidsDataTextSearch extends BidsDataBase {
	/**
	 * Возвращает имя таблицы в базе данных.
	 *
	 * @return string Имя таблицы в базе данных.
	 */
	public function tableName() {
		return 'bids_data_text_search';
	}
}
