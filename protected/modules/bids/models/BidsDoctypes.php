<?php

/**
 * Модель типов документов закупок. Таблица "bids_doctypes".
 *
 * Доступные столбцы в таблице "bids_doctypes":
 * @property integer $doctypeId Идентификатор типа документа.
 * @property string $name Название документа.
 * @property string $descr Описание.
 *
 * Доступные модели отношений:
 * @property Doctypes[] $doctypes Типы документов относящиеся к зажанному порталу
 * @package TendersParser
 * @subpackage bids
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013 BSTsoft
 */
class BidsDoctypes extends CActiveRecord {
	/**
	 * Возвращает имя таблицы в базе данных.
	 *
	 * @return string Имя таблицы в базе данных.
	 */
	public function tableName() {
		return 'bids_doctypes';
	}

	/**
	 * Возвращает модель класса.
	 *
	 * @return BidsDoctypes Модель класса.
	 */
	public static function model($className =__CLASS__) {
		return parent::model($className);
	}

	/**
	 * Возвращает массив правил валидации.
	 * @return array правила валидации
	 */
	public function rules() {
		return [
			['name', 'required'],
			['descr', 'safe']
		];
	}

	/**
	 * Возращает связи с другими таблицами.
	 *
	 * @return array Правила отношений.
	 */
	public function relations() {
		return [
			'bids'   => [self::HAS_MANY, 'Bids',          'bidId'],
			'fields' => [self::HAS_MANY, 'BidsFields', 'fieldId'],
			'portal' => [self::BELONGS_TO, 'Porta;', 'portalId'],
		];
	}

	/**
	 * Список атрибутов модели.
	 *
	 * @return array Атрибуты.
	 */
	public function attributeLabels() {
		return [
			'doctypeId' => 'Идентификатор типа документа',
			'name'      => 'Название документа',
			'descr'     => 'Описание'
		];
	}

	/**
	 * Тип документа - отмена заявки.
	 *
	 * @return bool
	 */
	public function isCanceled() {
		return in_array($this->name, [
			'notificationCancel',
			'purchaseRejection',
		    ]);
	}
}
