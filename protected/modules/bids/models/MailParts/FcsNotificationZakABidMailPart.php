<?php
class FcsNotificationZakABidMailPart extends BidMailPart44fz
{
    /**
     * Конструктор
     * @param Bids $bid - заявка
     */
    public function __construct(Bids $bid) {
        # имя шаблона
        $this->_template = 'Zakupki44Part';

        # обработчики полей заявки
        $this->addFieldHandler('fcsNotificationZakA/purchaseNumber', 'orderNumber');
        $this->addFieldHandler('fcsNotificationZakA/href', 'href', function($value) {
                return  CHtml::link('Перейти на сайт', $value, ['target' => '_blank']);
            });
        $this->addFieldHandler('fcsNotificationZakA/purchaseResponsible/responsibleOrg/fullName', 'customerName');
        $this->addFieldHandler('fcsNotificationZakA/purchaseObjectInfo', 'orderName');
        $this->addFieldHandler('fcsNotificationZakA/lots/lot/customerRequirements/customerRequirement/deliveryPlace', 'deliveryPlace');
        $this->addFieldHandler('fcsNotificationZakA/lots/lot/purchaseObjects/totalSum', 'maxPrice');
        $this->addFieldHandler('fcsNotificationZakA/purchaseDocumentation/payInfo/procedureInfo/collecting/EndDate', 'bidsEndFillingDate',
            function($value){
                return date("d.m.Y", strtotime($value));
            });
        $this->addFieldHandler('fcsNotificationZakA/purchaseDocumetation/payInfo/procedureInfo/bidding/date', 'closedAuctionStartDate',
            function($value){
                return date("d.m.Y H:i", strtotime($value));
            });

        parent::__construct($bid);
    }
}