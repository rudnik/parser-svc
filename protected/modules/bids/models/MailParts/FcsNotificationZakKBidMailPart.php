<?php
class FcsNotificationZakKBidMailPart extends BidMailPart44fz
{
    /**
     * Конструктор
     * @param Bids $bid - заявка
     */
    public function __construct(Bids $bid) {
        # имя шаблона
        $this->_template = 'Zakupki44Part';

        # обработчики полей заявки
        $this->addFieldHandler('fcsNotificationZakK/purchaseNumber', 'orderNumber');
        $this->addFieldHandler('fcsNotificationZakK/href', 'href', function($value) {
                return  CHtml::link('Перейти на сайт', $value, ['target' => '_blank']);
            });
        $this->addFieldHandler('fcsNotificationZakK/purchaseResponsible/responsibleOrg/fullName', 'customerName');
        $this->addFieldHandler('fcsNotificationZakK/purchaseObjectInfo', 'orderName');
        $this->addFieldHandler('fcsNotificationZakK/lots/lot/customerRequirements/customerRequirement/deliveryPlace', 'deliveryPlace');
        $this->addFieldHandler('fcsNotificationZakK/lots/lot/purchaseObjects/totalSum', 'maxPrice');
        $this->addFieldHandler('fcsNotificationZakK/purchaseDocumentation/payInfo/procedureInfo/collecting/EndDate', 'bidsEndFillingDate',
            function($value){
                return date("d.m.Y", strtotime($value));
            });
        parent::__construct($bid);
    }
}