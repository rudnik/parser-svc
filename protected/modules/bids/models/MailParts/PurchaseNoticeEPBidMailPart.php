<?php
/**
 * Модель часть письма - Закупка у единственного поставщика
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013 BSTsoft
 */
class PurchaseNoticeEPBidMailPart extends BidMailPart223fz {
   
    /**
     * Конструктор
     * @param Bids $bid - заявка
     */
    public function __construct(Bids $bid) {
	# имя шаблона
	$this->_template = 'purchaseNoticeEPPart';
	
	# обработчики полей заявки
	$this->addFieldHandler('purchaseNoticeEP/body/item/purchaseNoticeEPData/registrationNumber', 'orderNumber');
	$this->addFieldHandler('purchaseNoticeEP/body/item/purchaseNoticeEPData/customer/mainInfo/shortName', 'customerName');
	$this->addFieldHandler('purchaseNoticeEP/body/item/purchaseNoticeEPData/name', 'orderName');	
	$this->addFieldHandler('purchaseNoticeEP/body/item/purchaseNoticeEPData/lots/lot/deliveryPlace', 'deliveryPlace');
	$this->addFieldHandler('purchaseNoticeEP/body/item/purchaseNoticeEPData/lots/lot/initialSum', 'maxPrice');
	$this->addFieldHandler('purchaseNoticeEP/body/item/purchaseNoticeEPData/documentationDelivery/deliveryEndDateTime', 'bidsEndFillingDate', 
	    function($value){
		return date("d.m.Y", strtotime($value));
	    });
	parent::__construct($bid);
    }
}