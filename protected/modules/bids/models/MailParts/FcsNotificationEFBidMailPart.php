<?php
class FcsNotificationEFBidMailPart extends BidMailPart44fz
{
    /**
     * Конструктор
     * @param Bids $bid - заявка
     */
    public function __construct(Bids $bid) {
        # имя шаблона
        $this->_template = 'Zakupki44Part';

        # обработчики полей заявки
        $this->addFieldHandler('fcsNotificationEF/purchaseNumber', 'orderNumber');
        $this->addFieldHandler('fcsNotificationEF/href', 'href', function($value) {
                return  CHtml::link('Перейти на сайт', $value, ['target' => '_blank']);
            });
        $this->addFieldHandler('fcsNotificationEF/purchaseResponsible/responsibleOrg/fullName', 'customerName');
        $this->addFieldHandler('fcsNotificationEF/purchaseObjectInfo', 'orderName');
        $this->addFieldHandler('no data', 'deliveryPlace');
        $this->addFieldHandler('fcsNotificationEF/lot/purchaseObjects/totalSum', 'maxPrice');
        $this->addFieldHandler('fcsNotificationEF/procedureInfo/collecting/endDate', 'bidsEndFillingDate',
            function($value){
                return date("d.m.Y", strtotime($value));
            });

        $this->addFieldHandler('fcsNotificationEF/procedureInfo/bidding/date', 'auctionStartDate',
            function($value){
                return date("d.m.Y H:i", strtotime($value));
            });

        parent::__construct($bid);
    }
}