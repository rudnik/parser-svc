<?php
/**
 * Модель часть письма - Аукцион в электронной форме
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013 BSTsoft
 */
class PurchaseNoticeAEBidMailPart extends BidMailPart223fz {
   
    /**
     * Конструктор
     * @param Bids $bid - заявка
     */
    public function __construct(Bids $bid) {
	# имя шаблона
	$this->_template = 'purchaseNoticeAEPart';
	
	# обработчики полей заявки
	$this->addFieldHandler('purchaseNoticeAE/body/item/purchaseNoticeAEData/registrationNumber', 'orderNumber');
	$this->addFieldHandler('purchaseNoticeAE/body/item/purchaseNoticeAEData/customer/mainInfo/shortName', 'customerName');
	$this->addFieldHandler('purchaseNoticeAE/body/item/purchaseNoticeAEData/name', 'orderName');	
	$this->addFieldHandler('purchaseNoticeAE/body/item/purchaseNoticeAEData/lots/lot/deliveryPlace', 'deliveryPlace');
	$this->addFieldHandler('purchaseNoticeAE/body/item/purchaseNoticeAEData/lots/lot/initialSum', 'maxPrice');
	$this->addFieldHandler('purchaseNoticeAE/body/item/purchaseNoticeAEData/submissionCloseDateTime', 'bidsEndFillingDate', 
	    function($value){
		return date("d.m.Y", strtotime($value));
	    });
	    
	$this->addFieldHandler('purchaseNoticeAE/body/item/purchaseNoticeAEData/auctionTime', 'auctionStartDate', 
	    function($value){
		return date("d.m.Y H:i", strtotime($value));
	    });
	parent::__construct($bid);
    }
}