<?php
/**
 * Часть письма - Открытый аукцион в электронной форме
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013 BSTsoft
 */
class NotificationEFBidMailPart extends BidMailPart94fz {

    /**
     * Конструктор
     * @param Bids $bid - заявка
     */
    public function __construct(Bids $bid) {
	# имя шаблона
	$this->_template = 'notificationEFPart';

	# обработчики полей заявки
	# Дата окончания срока подачи заявок
	$this->addFieldHandler(
		'NotificationEF/notificationCommission/p1Date',
		'bidsEndFillingDate',
		function($value){
		    return date("d.m.Y", strtotime($value));
		}
	);

	# Дата и время проведения аукциона
	$this->addFieldHandler(
		'NotificationEF/notificationCommission/p3Date',
		'auctionStartDate',
		function($value){
		    return date("d.m.Y H:i", strtotime($value));
		}
	);

    # Добавление обработчиков полей, общих для всех типов документов.
    # Ссылка на сайт госзакупок
    $this->addFieldHandler('NotificationEF/href', 'url');
    # Номер заказа
    $this->addFieldHandler('NotificationEF/notificationNumber', 'orderNumber');

    /*
     * Заказчик
     *
     * Заказчиком может быть, как инициатор, так и плейсер.
     * Кто из них заказчик определяется после парсинга всех полей заявки
     */
    $this->addFieldHandler('NotificationEF/order/initiatorOrgRole', 'initiatorOrgRole');
    $this->addFieldHandler('NotificationEF/order/initiator/regNum', 'initiatorRegNum');
    $this->addFieldHandler('NotificationEF/order/initiator/fullName', 'initiatorFullName');
    $this->addFieldHandler('NotificationEF/order/placerOrgType', 'placerOrgType');
    $this->addFieldHandler('NotificationEF/order/placer/regNum', 'placerRegNum');
    $this->addFieldHandler('NotificationEF/order/placer/fullName', 'placerFullName');
    $this->addFieldHandler('NotificationEF/contactInfo/orgName', 'orgName');

    # Наименование заказа
    $this->addFieldHandler('NotificationEF/orderName', 'orderName');

    # Место поставки
    $this->addFieldHandler('NotificationEF/lots/lot/customerRequirements/customerRequirement/deliveryPlace', 'deliveryPlace');

    # Начальная (максимальная) цена
    $this->addFieldHandler('NotificationEF/lots/lot/customerRequirements/customerRequirement/maxPrice', 'maxPrice');

	parent::__construct($bid);
    }
}
