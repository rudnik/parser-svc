<?php
/**
 * Модель часть письма - Запрос котировок
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013 BSTsoft
 */
class PurchaseNoticeZKBidMailPart extends BidMailPart223fz {
   
    /**
     * Конструктор
     * @param Bids $bid - заявка
     */
    public function __construct(Bids $bid) {
	# имя шаблона
	$this->_template = 'purchaseNoticeZKPart';
	
	# обработчики полей заявки
	$this->addFieldHandler('purchaseNoticeZK/body/item/purchaseNoticeZKData/registrationNumber', 'orderNumber');
	$this->addFieldHandler('purchaseNoticeZK/body/item/purchaseNoticeZKData/customer/mainInfo/shortName', 'customerName');
	$this->addFieldHandler('purchaseNoticeZK/body/item/purchaseNoticeZKData/name', 'orderName');	
	$this->addFieldHandler('purchaseNoticeZK/body/item/purchaseNoticeZKData/lots/lot/deliveryPlace', 'deliveryPlace');
	$this->addFieldHandler('purchaseNoticeZK/body/item/purchaseNoticeZKData/lots/lot/initialSum', 'maxPrice');
	$this->addFieldHandler('purchaseNoticeZK/body/item/purchaseNoticeZKData/submissionCloseDateTime', 'bidsEndFillingDate', 
	    function($value){
		return date("d.m.Y", strtotime($value));
	    });
	parent::__construct($bid);
    }
}