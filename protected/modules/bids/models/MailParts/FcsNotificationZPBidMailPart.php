<?php
class FcsNotificationZPBidMailPart extends BidMailPart44fz
{
    /**
     * Конструктор
     * @param Bids $bid - заявка
     */
    public function __construct(Bids $bid) {
        # имя шаблона
        $this->_template = 'Zakupki44Part';

        # обработчики полей заявки
        $this->addFieldHandler('fcsNotificationZP/purchaseNumber', 'orderNumber');
        $this->addFieldHandler('fcsNotificationZP/href', 'href', function($value) {
                return  CHtml::link('Перейти на сайт', $value, ['target' => '_blank']);
            });
        $this->addFieldHandler('fcsNotificationZP/purchaseResponsible/responsibleOrg/fullName', 'customerName');
        $this->addFieldHandler('fcsNotificationZP/purchaseObjectInfo', 'orderName');
        $this->addFieldHandler('fcsNotificationZP/lot/customerRequirements/customerRequirement/deliveryPlace', 'deliveryPlace');
        $this->addFieldHandler('fcsNotificationZP/lot/purchaseObjects/totalSum', 'maxPrice');
        $this->addFieldHandler('fcsNotificationZP/procedureInfo/collecting/endDate', 'bidsEndFillingDate',
            function($value){
                return date("d.m.Y", strtotime($value));
            });

        parent::__construct($bid);
    }
}