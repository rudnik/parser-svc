<?php
class FcsNotificationISMBidMailPart extends BidMailPart44fz
{
    /**
     * Конструктор
     * @param Bids $bid - заявка
     */
    public function __construct(Bids $bid) {
        # имя шаблона
        $this->_template = 'Zakupki44Part';

        # обработчики полей заявки
        $this->addFieldHandler('fcsNotificationISM/purchaseNumber', 'orderNumber');
        $this->addFieldHandler('fcsNotificationISM/href', 'href', function($value) {
                return  CHtml::link('Перейти на сайт', $value, ['target' => '_blank']);
            });
        $this->addFieldHandler('fcsNotificationISM/purchaseResponsible/responsibleOrg/fullName', 'customerName');
        $this->addFieldHandler('fcsNotificationISM/purchaseObjectInfo', 'orderName');
        $this->addFieldHandler('no data', 'deliveryPlace');
        $this->addFieldHandler('fcsNotificationISM/lots/lot/maxPrice', 'maxPrice');
        $this->addFieldHandler('fcsNotificationISM/procedureInfo/collectingEndDate', 'bidsEndFillingDate',
            function($value){
                return date("d.m.Y", strtotime($value));
            });

        parent::__construct($bid);
    }
}