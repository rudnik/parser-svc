<?php
/**
 * Модель часть письма - Иной способ
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013 BSTsoft
 */
class PurchaseNoticeBidMailPart extends BidMailPart223fz {
   
    /**
     * Конструктор
     * @param Bids $bid - заявка
     */
    public function __construct(Bids $bid) {
	# имя шаблона
	$this->_template = 'purchaseNoticePart';
	
	# обработчики полей заявки
	$this->addFieldHandler('purchaseNotice/body/item/purchaseNoticeData/registrationNumber', 'orderNumber');
	$this->addFieldHandler('purchaseNotice/body/item/purchaseNoticeData/customer/mainInfo/shortName', 'customerName');
	$this->addFieldHandler('purchaseNotice/body/item/purchaseNoticeData/name', 'orderName');	
	$this->addFieldHandler('purchaseNotice/body/item/purchaseNoticeData/lots/lot/deliveryPlace', 'deliveryPlace');
	$this->addFieldHandler('purchaseNotice/body/item/purchaseNoticeData/lots/lot/initialSum', 'maxPrice');
	$this->addFieldHandler('purchaseNotice/body/item/purchaseNoticeData/submissionCloseDateTime', 'bidsEndFillingDate', 
	    function($value){
		return date("d.m.Y", strtotime($value));
	    });
	parent::__construct($bid);
    }
}