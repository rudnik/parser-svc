<?php
class BidMailPart223fz extends BidMailPart {

    protected function detectCustomer()
    {
        if(isset($this->_data['initiatorOrgRole']) && $this->_data['initiatorOrgRole'] == 'Z') {
            $this->_data['customerName'] = empty($this->_data['initiatorFullName']) ?
                $this->_data['initiatorRegNum'] : $this->_data['initiatorFullName'];
        }
        elseif(isset($this->_data['placerOrgType']) && $this->_data['placerOrgType'] == 'Z'){
            $this->_data['customerName'] = empty($this->_data['placerFullName']) ?
                $this->_data['placerRegNum'] : $this->_data['placerFullName'];
        }
    }
}