<?php
class FcsNotificationZKBidMailPart extends BidMailPart44fz
{
    /**
     * Конструктор
     * @param Bids $bid - заявка
     */
    public function __construct(Bids $bid) {
        # имя шаблона
        $this->_template = 'Zakupki44Part';

        # обработчики полей заявки
        $this->addFieldHandler('fcsNotificationZK/purchaseNumber', 'orderNumber');
        $this->addFieldHandler('fcsNotificationZK/href', 'href', function($value) {
                return  CHtml::link('Перейти на сайт', $value, ['target' => '_blank']);
            });
        $this->addFieldHandler('fcsNotificationZK/purchaseResponsible/responsibleOrg/fullName', 'customerName');
        $this->addFieldHandler('fcsNotificationZK/purchaseObjectInfo', 'orderName');
        $this->addFieldHandler('fcsNotificationZK/lot/customerRequirements/customerRequirement/deliveryPlace', 'deliveryPlace');
        $this->addFieldHandler('fcsNotificationZK/lot/purchaseObjects/totalSum', 'maxPrice');
        $this->addFieldHandler('fcsNotificationZK/procedureInfo/collecting/endDate', 'bidsEndFillingDate',
            function($value){
                return date("d.m.Y", strtotime($value));
            });

        parent::__construct($bid);
    }
}