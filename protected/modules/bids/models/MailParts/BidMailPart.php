<?php
/**
 * Часть письма
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013 BSTsoft
 */
abstract class BidMailPart implements IBidMailPart {
    /**
     * Массив моделей полей заявки
     * @var array
     */
    protected static $_fields = null;
    
    /**
     * Имя шаблона части письма
     * @var string
     */
    protected $_template = null;
    
    /**
     * Массив данных для шаблона письма
     * @var array
     */
    protected $_data;
    
    /**
     * Массив обработчиков полей заявки
     * @var array
     */
    protected $_fieldHandlers = [];
    
    /**
     * Конструктор.
     *  - Заполняет справочник моделей полей заявки.
     *  - Добавляет обработчики полей, общих для всех типов документов.
     *  - Парсит поля, необходимые для шаблона части письма
     * 
     * @param Bids $bid - заявка
     */
    public function __construct(Bids $bid) {	
	# Заполнение справочника моделей полей заявки
	if(is_null(static::$_fields)) {
        static::$_fields = BidsFields::model()->findAll(new CDbCriteria([
		'select' => ['fieldId', 'name', 'type'],
        'index' => 'fieldId',
	    ]));
	}

	# Парсинг полей, необходимых для шаблона части письма
	$this->_parseBid($bid);
    }
    
    /**
     * Добавление обработчика поля заявки
     * @param string $sourceField - название поля в заявке
     * @param string $destinationField - название поля в шаблоне
     * @param Closure $function - обработчик поля. function($value){ ...; return $newValue;}
     */
    protected function addFieldHandler($sourceField, $destinationField, Closure $function = null) {
	$this->_fieldHandlers[] = [
	    'sourceField' => $sourceField,
	    'destinationField' => $destinationField,
	    'function' => $function,
	];
    }
    
    /**
     * Парсинг полей, необходимых для шаблона части письма
     * @param Bids $bid - заявка
     */
    protected function _parseBid(Bids $bid) {
	# перебираем значения по каждому полю
	foreach($bid->data as $data) {
	    $field = static::$_fields[$data->fieldId];
	    $values = $data->values;
	    $firstValue = array_shift($values);
	    if( ! $firstValue instanceof BidsDataValues) continue;
	    $value =  $firstValue->value;

	    if(empty($value)) continue;

	    # Запуск обработчиков полей
	    foreach($this->_fieldHandlers as $handler) {
		if(strtolower($field->name) == strtolower($handler['sourceField'])) {
		    $fieldHandler = $handler['function'];
		    if(is_null($fieldHandler))
			$this->_data[$handler['destinationField']] = $value;
		    else
			if(is_callable($fieldHandler))		
			    $this->_data[$handler['destinationField']] = $fieldHandler($value);
		}
	    }
	    $this->_data['placingOrderMethod'] = $bid->doctype->descr;
	    
	    # Определяем название заказчика
        $this->detectCustomer();
	}

    }

    abstract protected function detectCustomer();

    /**
     * Возвращает отрендеренный шаблон
     * @return string
     * @throws CException
     */
    public function render() {
	if($this->_template === null) throw new CException('Имя шаблона не задано');
	$controller = new CController('BidMailPart');
	return $controller->renderInternal(
		Yii::getPathOfAlias('application.modules.bids.views.bids.mailParts') . '/' . $this->_template . '.php', 
		['mailPart' => (object)$this->_data], true
	);
    }    
    
    /**
     * Фабрика частей письма
     * @param Bids $bid - заявка
     * @return IBidMailPart
     */
    public static function factory(Bids $bid) {
	$mailPartClassName = ucfirst($bid->doctype->name) . 'BidMailPart' ;
	$classInsance = new $mailPartClassName($bid);
	if( ! $classInsance instanceof IBidMailPart)
	    throw new Exception ("Класс {$mailPartClassName} должен реализовывать интерфейс IBidMailPart");
	return $classInsance;
    }
}