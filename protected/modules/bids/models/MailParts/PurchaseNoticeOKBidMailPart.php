<?php
/**
 * Модель часть письма - Открытый конкурс
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013 BSTsoft
 */
class PurchaseNoticeOKBidMailPart extends BidMailPart223fz {
   
    /**
     * Конструктор
     * @param Bids $bid - заявка
     */
    public function __construct(Bids $bid) {
	# имя шаблона
	$this->_template = 'purchaseNoticeOKPart';
	
	# обработчики полей заявки
	$this->addFieldHandler('purchaseNoticeOK/body/item/purchaseNoticeOKData/registrationNumber', 'orderNumber');
	$this->addFieldHandler('purchaseNoticeOK/body/item/purchaseNoticeOKData/customer/mainInfo/shortName', 'customerName');
	$this->addFieldHandler('purchaseNoticeOK/body/item/purchaseNoticeOKData/name', 'orderName');	
	$this->addFieldHandler('purchaseNoticeOK/body/item/purchaseNoticeOKData/lots/lot/deliveryPlace', 'deliveryPlace');
	$this->addFieldHandler('purchaseNoticeOK/body/item/purchaseNoticeOKData/lots/lot/initialSum', 'maxPrice');
	$this->addFieldHandler('purchaseNoticeOK/body/item/purchaseNoticeOKData/submissionCloseDateTime', 'bidsEndFillingDate', 
	    function($value){
		return date("d.m.Y", strtotime($value));
	    });
	parent::__construct($bid);
    }
}