<?php
/**
 * Модель часть письма - Открытый конкурс
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013 BSTsoft
 */
class NotificationZKBidMailPart extends BidMailPart94fz {

    /**
     * Конструктор
     * @param Bids $bid - заявка
     */
    public function __construct(Bids $bid) {
	# имя шаблона
	$this->_template = 'notificationZKPart';

	# обработчики полей заявки
	# Дата окончания срока подачи заявок
	$this->addFieldHandler(
		'NotificationZK/notificationCommission/p2Date',
		'bidsEndFillingDate',
		function($value){
		    return date("d.m.Y", strtotime($value));
		}
	);

    # Добавление обработчиков полей, общих для всех типов документов.
    # Ссылка на сайт госзакупок
    $this->addFieldHandler('NotificationZK/href', 'url');
    # Номер заказа
    $this->addFieldHandler('NotificationZK/notificationNumber', 'orderNumber');

    /*
     * Заказчик
     *
     * Заказчиком может быть, как инициатор, так и плейсер.
     * Кто из них заказчик определяется после парсинга всех полей заявки
     */
    $this->addFieldHandler('NotificationZK/order/initiatorOrgRole', 'initiatorOrgRole');
    $this->addFieldHandler('NotificationZK/order/initiator/regNum', 'initiatorRegNum');
    $this->addFieldHandler('NotificationZK/order/initiator/fullName', 'initiatorFullName');
    $this->addFieldHandler('NotificationZK/order/placerOrgType', 'placerOrgType');
    $this->addFieldHandler('NotificationZK/order/placer/regNum', 'placerRegNum');
    $this->addFieldHandler('NotificationZK/order/placer/fullName', 'placerFullName');
    $this->addFieldHandler('NotificationZK/contactInfo/orgName', 'orgName');

    # Наименование заказаOK
    $this->addFieldHandler('NotificationZK/orderName', 'orderName');

    # Место поставкиOK
    $this->addFieldHandler('NotificationZK/lots/lot/customerRequirements/customerRequirement/deliveryPlace', 'deliveryPlace');

    # Начальная (максимальная) ценаOK
    $this->addFieldHandler('NotificationZK/lots/lot/customerRequirements/customerRequirement/maxPrice', 'maxPrice');

	parent::__construct($bid);
    }
}
