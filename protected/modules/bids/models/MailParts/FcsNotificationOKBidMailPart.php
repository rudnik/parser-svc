<?php
class FcsNotificationOKBidMailPart extends BidMailPart44fz
{
    /**
     * Конструктор
     * @param Bids $bid - заявка
     */
    public function __construct(Bids $bid) {
        # имя шаблона
        $this->_template = 'Zakupki44Part';

        # обработчики полей заявки
        $this->addFieldHandler('fcsNotificationOK/purchaseNumber', 'orderNumber');
        $this->addFieldHandler('fcsNotificationOK/href', 'href', function($value) {
                return  CHtml::link('Перейти на сайт', $value, ['target' => '_blank']);
            });
        $this->addFieldHandler('fcsNotificationOK/purchaseResponsible/responsibleOrg/fullName', 'customerName');
        $this->addFieldHandler('fcsNotificationOK/purchaseObjectInfo', 'orderName');
        $this->addFieldHandler('fcsNotificationOK/lots/lot/customerRequirements/customerRequirement/deliveryPlace', 'deliveryPlace');
        $this->addFieldHandler('fcsNotificationOK/lots/lot/purchaseObjects/totalSum', 'maxPrice');
        $this->addFieldHandler('fcsNotificationOK/procedureInfo/collecting/endDate', 'bidsEndFillingDate',
            function($value){
                return date("d.m.Y", strtotime($value));
            });

        parent::__construct($bid);
    }
}