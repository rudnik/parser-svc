<?php
/**
 * Модель часть письма - Открытый аукцион
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013 BSTsoft
 */
class PurchaseNoticeOABidMailPart extends BidMailPart223fz {
   
    /**
     * Конструктор
     * @param Bids $bid - заявка
     */
    public function __construct(Bids $bid) {
	# имя шаблона
	$this->_template = 'purchaseNoticeOAPart';
	
	# обработчики полей заявки
	$this->addFieldHandler('purchaseNoticeOA/body/item/purchaseNoticeOAData/registrationNumber', 'orderNumber');
	$this->addFieldHandler('purchaseNoticeOA/body/item/purchaseNoticeOAData/customer/mainInfo/shortName', 'customerName');
	$this->addFieldHandler('purchaseNoticeOA/body/item/purchaseNoticeOAData/name', 'orderName');	
	$this->addFieldHandler('purchaseNoticeOA/body/item/purchaseNoticeOAData/lots/lot/deliveryPlace', 'deliveryPlace');
	$this->addFieldHandler('purchaseNoticeOA/body/item/purchaseNoticeOAData/lots/lot/initialSum', 'maxPrice');
	$this->addFieldHandler('purchaseNoticeOA/body/item/purchaseNoticeOAData/submissionCloseDateTime', 'bidsEndFillingDate', 
	    function($value){
		return date("d.m.Y", strtotime($value));
	    });
	    
	$this->addFieldHandler('purchaseNoticeOA/body/item/purchaseNoticeOAData/auctionTime', 'auctionStartDate', 
	    function($value){
		return date("d.m.Y H:i", strtotime($value));
	    });
	parent::__construct($bid);
    }
}