<?php
class FcsNotificationOKDBidMailPart extends BidMailPart44fz
{
    /**
     * Конструктор
     * @param Bids $bid - заявка
     */
    public function __construct(Bids $bid) {
        # имя шаблона
        $this->_template = 'Zakupki44Part';

        # обработчики полей заявки
        $this->addFieldHandler('fcsNotificationOK/purchaseNumber', 'orderNumber');
        $this->addFieldHandler('fcsNotificationOKD/href', 'href', function($value) {
                return  CHtml::link('Перейти на сайт', $value, ['target' => '_blank']);
            });
        $this->addFieldHandler('fcsNotificationOKD/purchaseResponsible/responsibleOrg/fullName', 'customerName');
        $this->addFieldHandler('fcsNotificationOKD/purchaseObjectInfo', 'orderName');
        $this->addFieldHandler('fcsNotificationOKD/lots/lot/customerRequirements/ customerRequirement/deliveryPlace', 'deliveryPlace');
        $this->addFieldHandler('fcsNotificationOKD/lots/lot/purchaseObjects/totalSum', 'maxPrice');
        $this->addFieldHandler('fcsNotificationOKD/purchaseDocumentation/payInfo/procedureInfo/stageOne/collecting/EndDate', 'bidsEndFillingDate',
            function($value){
                return date("d.m.Y", strtotime($value));
            });
        $this->addFieldHandler('fcsNotificationOKD/purchaseDocumentation/payInfo/procedureInfo/stageOne/prequalification/date', 'qualifyFilter',
            function($value){
                return date("d.m.Y H:i", strtotime($value));
            });

        parent::__construct($bid);
    }
}