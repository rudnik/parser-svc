<?php
class FcsNotificationZakKDBidMailPart extends BidMailPart44fz
{
    /**
     * Конструктор
     * @param Bids $bid - заявка
     */
    public function __construct(Bids $bid) {
        # имя шаблона
        $this->_template = 'Zakupki44Part';

        # обработчики полей заявки
        $this->addFieldHandler('fcsNotificationZakKD/purchaseNumber', 'orderNumber');
        $this->addFieldHandler('fcsNotificationZakKD/href', 'href', function($value) {
                return  CHtml::link('Перейти на сайт', $value, ['target' => '_blank']);
            });
        $this->addFieldHandler('fcsNotificationZakKD/purchaseResponsible/responsibleOrg/fullName', 'customerName');
        $this->addFieldHandler('fcsNotificationZakKD/purchaseObjectInfo', 'orderName');
        $this->addFieldHandler('fcsNotificationZakKD/lots/lot/customerRequirements/customerRequirement/deliveryPlace', 'deliveryPlace');
        $this->addFieldHandler('fcsNotificationZakKD/lots/lot/purchaseObjects/totalSum', 'maxPrice');
        $this->addFieldHandler('fcsNotificationZakKD/purchaseDocumentation/payInfo/procedureInfo/stageOne/collecting/EndDate', 'bidsEndFillingDate',
            function($value){
                return date("d.m.Y", strtotime($value));
            });
        $this->addFieldHandler('fcsNotificationZakKD/purchaseDocumentation/payInfo/procedureInfo/stageOne/prequalification/date', 'qualifyFilter',
            function($value){
                return date("d.m.Y H:i", strtotime($value));
            });

        parent::__construct($bid);
    }
}