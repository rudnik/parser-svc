<?php
class BidMailPart94fz extends BidMailPart {

    protected function detectCustomer()
    {
        if (isset($this->_data['initiatorFullName']) && !empty($this->_data['initiatorFullName'])) {
            $this->_data['customerName'] = $this->_data['initiatorFullName'];
        } elseif (isset($this->_data['orgName']) && !empty($this->_data['orgName'])) {
            $this->_data['customerName'] = $this->_data['orgName'];
        }
    }
}