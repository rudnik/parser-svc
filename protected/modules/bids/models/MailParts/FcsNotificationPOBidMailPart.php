<?php
class FcsNotificationPOBidMailPart extends BidMailPart44fz
{
    /**
     * Конструктор
     * @param Bids $bid - заявка
     */
    public function __construct(Bids $bid) {
        # имя шаблона
        $this->_template = 'Zakupki44Part';

        # обработчики полей заявки
        $this->addFieldHandler('fcsNotificationPO/purchaseNumber', 'orderNumber');
        $this->addFieldHandler('fcsNotificationPO/href', 'href', function($value) {
                return  CHtml::link('Перейти на сайт', $value, ['target' => '_blank']);
            });
        $this->addFieldHandler('fcsNotificationPO/purchaseResponsible/responsibleOrg/fullName', 'customerName');
        $this->addFieldHandler('fcsNotificationPO/purchaseObjectInfo', 'orderName');
        $this->addFieldHandler('fcsNotificationPO/lot/customerRequirements/customerRequirement/deliveryPlace', 'deliveryPlace');
        $this->addFieldHandler('fcsNotificationPO/lot/purchaseObjects/totalSum', 'maxPrice');
        $this->addFieldHandler('fcsNotificationPO/procedureInfo/collecting/endDate', 'bidsEndFillingDate',
            function($value){
                return date("d.m.Y", strtotime($value));
            });

        $this->addFieldHandler('fcsNotificationPO/procedureInfo/selecting/date', 'preFilter',
            function($value){
                return date("d.m.Y H:i", strtotime($value));
            });

        parent::__construct($bid);
    }
}