<?php
class FcsNotificationZakKOUBidMailPart extends BidMailPart44fz
{
    /**
     * Конструктор
     * @param Bids $bid - заявка
     */
    public function __construct(Bids $bid) {
        # имя шаблона
        $this->_template = 'Zakupki44Part';

        # обработчики полей заявки
        $this->addFieldHandler('fcsNotificationZakKOU/purchaseNumber', 'orderNumber');
        $this->addFieldHandler('fcsNotificationZakKOU/href', 'href', function($value) {
                return  CHtml::link('Перейти на сайт', $value, ['target' => '_blank']);
            });
        $this->addFieldHandler('fcsNotificationZakKOU/purchaseResponsible/responsibleOrg/fullName', 'customerName');
        $this->addFieldHandler('fcsNotificationZakKOU/purchaseObjectInfo', 'orderName');
        $this->addFieldHandler('fcsNotificationZakKOU/lots/lot/customerRequirements/customerRequirement/deliveryPlace', 'deliveryPlace');
        $this->addFieldHandler('fcsNotificationZakKOU/lots/lot/purchaseObjects/totalSum', 'maxPrice');
        $this->addFieldHandler('fcsNotificationZakKOU/purchaseDocumentation/payInfo/procedureInfo/collecting/EndDate', 'bidsEndFillingDate',
            function($value){
                return date("d.m.Y", strtotime($value));
            });
        $this->addFieldHandler('fcsNotificationZakKOU/purchaseDocumentation/payInfo/procedureInfo/prequalification/date', 'qualifyFilter',
            function($value){
                return date("d.m.Y H:i", strtotime($value));
            });

        parent::__construct($bid);
    }
}