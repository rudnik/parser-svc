<?php
/**
 * Модель часть письма - Открытый конкурс
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013 BSTsoft
 */
class NotificationOKBidMailPart extends BidMailPart94fz {

     /**
     * Конструктор
     * @param Bids $bid - заявка
     */
    public function __construct(Bids $bid) {
	# имя шаблона
	$this->_template = 'notificationOKPart';

	# обработчики полей заявки
	# Дата окончания срока подачи заявок
	$this->addFieldHandler(
		'NotificationOK/competitiveDocumentProvisioning/deliveryTerm2',
		'bidsEndFillingDate',
		function($value){
		    return date("d.m.Y", strtotime($value));
		}
	);

    # Добавление обработчиков полей, общих для всех типов документов.
    # Ссылка на сайт госзакупок
    $this->addFieldHandler('NotificationOK/href', 'url');
    # Номер заказа
    $this->addFieldHandler('NotificationOK/notificationNumber', 'orderNumber');

    /*
     * Заказчик
     *
     * Заказчиком может быть, как инициатор, так и плейсер.
     * Кто из них заказчик определяется после парсинга всех полей заявки
     */
    $this->addFieldHandler('NotificationOK/order/initiatorOrgRole', 'initiatorOrgRole');
    $this->addFieldHandler('NotificationOK/order/initiator/regNum', 'initiatorRegNum');
    $this->addFieldHandler('NotificationOK/order/initiator/fullName', 'initiatorFullName');
    $this->addFieldHandler('NotificationOK/order/placerOrgType', 'placerOrgType');
    $this->addFieldHandler('NotificationOK/order/placer/regNum', 'placerRegNum');
    $this->addFieldHandler('NotificationOK/order/placer/fullName', 'placerFullName');
    $this->addFieldHandler('NotificationOK/contactInfo/orgName', 'orgName');

    # Наименование заказаOK
    $this->addFieldHandler('NotificationOK/orderName', 'orderName');

    # Место поставкиOK
    $this->addFieldHandler('NotificationOK/lots/lot/customerRequirements/customerRequirement/deliveryPlace', 'deliveryPlace');

    # Начальная (максимальная) ценаOK
    $this->addFieldHandler('NotificationOK/lots/lot/customerRequirements/customerRequirement/maxPrice', 'maxPrice');

	parent::__construct($bid);
    }
}
