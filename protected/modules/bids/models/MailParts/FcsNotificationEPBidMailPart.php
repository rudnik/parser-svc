<?php
class FcsNotificationEPBidMailPart extends BidMailPart44fz
{
    /**
     * Конструктор
     * @param Bids $bid - заявка
     */
    public function __construct(Bids $bid) {
        # имя шаблона
        $this->_template = 'Zakupki44Part';

        # обработчики полей заявки
        $this->addFieldHandler('fcsNotificationEP/purchaseNumber', 'orderNumber');
        $this->addFieldHandler('fcsNotificationEP/href', 'href', function($value) {
            return  CHtml::link('Перейти на сайт', $value, ['target' => '_blank']);
        });
        $this->addFieldHandler('fcsNotificationEP/purchaseResponsible/responsibleOrg/fullName', 'customerName');
        $this->addFieldHandler('fcsNotificationEP/purchaseObjectInfo', 'orderName');
        $this->addFieldHandler('no data', 'deliveryPlace');
        $this->addFieldHandler('fcsNotificationEP/lot/purchaseObjects/totalSum', 'maxPrice');
        $this->addFieldHandler('fcsNotificationEP/procedureInfo/collecting/endDate', 'bidsEndFillingDate',
            function($value){
                return date("d.m.Y", strtotime($value));
            });

        parent::__construct($bid);
    }
}