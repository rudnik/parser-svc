<?php

/**
 * Модель поисковой таблицы.
 *
 * The followings are the available columns in table 'bids_data_search':
 * @property integer $search_id Идентификатор поисковой записи
 * @property integer $bid_id Идентификатор заявки
 * @property integer $doctypeId Id типа документа
 * @property string $title Заголовок заказа
 * @property string $description Описание заказа
 * @property integer $region_id Регион заказа
 * @property integer $publication_date Дата публикации заказа
 * @property integer $quote_date_end Дата окончания подачи котировочных заявок
 * @property float $max_cost Начальная (максимальная) цена контракта
 * @property string $inn ИНН
 *
 * Отношения:
 * @property Bids $bid Заявка
 * @property Regions $region Регион
 * @property BidsDoctypes $doctype Тип документа
 *
 * @package Parser
 * @subpackage search
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013 BSTsoft
 */
class BidsDataSearch extends CActiveRecord {
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BidsDataSearch the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'bids_data_search';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return [
			//array('model_name, model_data_id, title, description', 'required'),
			['bid_id, region_id, doctypeId, publication_date, quote_date_end', 'numerical', 'integerOnly' => true],
			['title, description', 'safe'],
			['publication_date, quote_date_end', 'length', 'max' => 10],
			['max_cost', 'length', 'max' => 14],
			['inn', 'length', 'max' => 12],
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			['title, description, region_id, publication_date, quote_date_end, max_cost, inn', 'safe', 'on' => 'search'],
		];
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return [
			'bid'     => [self::BELONGS_TO, 'Bids',            'bid_id'],
			'region'  => [self::BELONGS_TO, 'Regions',         'region_id'],
			'doctype' => [self::BELONGS_TO, 'BidsDoctypes', 'doctypeId']
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return [
			'search_id'        => 'Идентификатор поисковой записи',
			'bid_id'           => 'Идентификатор заявки',
			'doctypeId'        => 'Id типа документа',
			'title'            => 'Заголовок заказа',
			'description'      => 'Описание заказа',
			'region_id'        => 'Регион заказа',
			'publication_date' => 'Дата публикации заказа',
			'quote_date_end'   => 'Дата окончания подачи котировочных заявок',
			'max_cost'         => 'Начальная (максимальная) цена контракта',
			'inn'         => 'ИНН',
		];
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		$criteria = new CDbCriteria;
		$criteria->compare('title', $this->title, true);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('region_id', $this->region_id);
		$criteria->compare('publication_date', $this->publication_date, true);
		$criteria->compare('quote_date_end', $this->quote_date_end, true);
		$criteria->compare('max_cost', $this->max_cost, true);
		$criteria->compare('inn', $this->inn, true);

		return new CActiveDataProvider($this, ['criteria' => $criteria]);
	}

	/**
	 * Возвращает заполненную модель исходных данных
	 *
	 * @return CActiveRecord
	 */
	public function getDataModel() {
		return Bids::model()->findByPk($this->bid_id);
	}

	public static function getBidIdsBySphinxIds(array $sphinxIds) {
		if (empty($sphinxIds)) {
			return [];
		}

		$bid_ids = [];

		$result = static::model()->findAll('search_id IN(' . implode(', ', $sphinxIds) . ')');

		foreach ($result as $search_model) {
			$bid_ids[] = $search_model->bid_id;
		}

		return $bid_ids;
	}
	
	
	/**
	 * Удаляет из поисковой таблицы записи, у которых дата окончания подачи 
	 * котировочных заявок меньше заданной временной границы
	 * @param midex $deadLine - временная граница (int, string)
	 */
	public static function cleanOldBids($deadLine = null) {
	    if(is_null($deadLine)) $deadLine = strtotime(date("Y-m-d 00:00:00", time()));
	    elseif(is_string($deadLine)) $deadLine = strtotime ($deadLine);
	    else $deadLine = (int) $deadLine;
	    
	    BidsDataSearch::model()->deleteAll('quote_date_end < :time', [':time' => $deadLine]);
	}
}
