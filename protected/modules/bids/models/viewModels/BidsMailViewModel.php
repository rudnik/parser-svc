<?php
/**
 * Контракт для представления //views/site/_subscr_email
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013 BSTsoft
 */
class BidsMailViewModel extends CFormModel
{
    /**
     * Заявки, преобразованные к виду-части письма
     * @var IBidMailPart[]
     */
    private $_data;

    /**
     * Заявки для рассылки
     * @var Bids[]
     */
    protected $_bids = [];

    /**
     * Устанавливает заявки для рассылки
     * @param Bids $bids
     * @return BidsMailViewModel
     */
    public function setBids(array $bids)
    {
        $this->_bids = $bids;
        return $this;
    }

    /**
     * Возвращает список моделей заявок (частей письма) для вставки в письмо
     * @return array ассоциативный массив вида:<br>
     *    'Название портала 1' => IBidMailPart[],<br>
     *    'Название портала 2' => IBidMailPart[],<br>
     *    ...<br>
     * 'Название портала N' => IBidMailPart[]
     *
     */
    public function getBidsList()
    {
        if (!empty($this->_data)) {
            return $this->_data;
        }

        foreach ($this->_bids as $bid) {
            $this->_data[$bid->portal->title][] = BidMailPart::factory($bid);
        }

        # @todo: вырезать мерж массивов (следующие 14 строк), когда 94ФЗ перестанет существовать
        if (!isset($this->_data['Портал госзакупок 94ФЗ'])) {
            $this->_data['Портал госзакупок 94ФЗ'] = [];
        }

        if (!isset($this->_data['Портал госзакупок 44ФЗ'])) {
            $this->_data['Портал госзакупок 44ФЗ'] = [];
        }

        $this->_data['Портал госзакупок 44ФЗ'] = array_merge(
            $this->_data['Портал госзакупок 94ФЗ'],
            $this->_data['Портал госзакупок 44ФЗ']
        );

        unset ($this->_data['Портал госзакупок 94ФЗ']);

        return $this->_data;
    }
}