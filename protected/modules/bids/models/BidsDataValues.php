<?php

/**
 * Модель значений данных закупки.
 * Таблица "bids_data_values".
 *
 * Доступные столбцы в таблице "bids_data_values":
 * @property integer $valueId Идентификатор значения.
 * @property integer $dataId Идентификатор данных.
 * @property string $value Значение.
 *
 * @package TendersParser
 * @subpackage bids
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013 BSTsoft
 */
class BidsDataValues extends CActiveRecord {
	/**
	 * Возвращает модель класса.
	 *
	 * @return BidsDataValues Модель класса.
	 */
	public static function model($className =__CLASS__) {
		return parent::model($className);
	}

	/**
	 * Возвращает имя таблицы в базе данных.
	 *
	 * @return string Имя таблицы в базе данных.
	 */
	public function tableName() {
		return 'bids_data_values';
	}

	/**
	 * Возвращает массив правил валидации.
	 * @return array правила валидации
	 */
	public function rules() {
		return [
			['dataId, value', 'required']
		];
	}

	/**
	 * Возращает связи с другими таблицами.
	 *
	 * @return array Правила отношений.
	 */
	public function relations() {
		return [
			'data' => [self::BELONGS_TO, 'BidsData', 'dataId']
		];
	}

	/**
	 * Список атрибутов модели.
	 *
	 * @return array Атрибуты.
	 */
	public function attributeLabels() {
		return [
			'valueId' => 'Идентификатор значения',
			'dataId'  => 'Идентификатор данных',
			'value'   => 'Значение'
		];
	}
}
