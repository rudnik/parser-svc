<?php
abstract class SearchResultTableRowBidData implements ISearchResultTableRowBidData
{
    private static $fields;

    /**
     * Экземпляр заявки
     * @var Bids
     */
    protected $bid;

    protected $href;
    protected $name;
    protected $price;
    protected $publishDateStart;
    protected $publishDateEnd;

    abstract protected function getHrefFields();

    abstract protected function getNameFields();

    abstract protected function getPriceFields();

    abstract protected function getPublishDateStartFields();

    abstract protected function getPublishDateEndFields();

    /**
     * @param $portalName имя портала
     * @return ISearchResultTableRowBidData|null
     */
    final public static function factory($portalName)
    {
        $instanceName = ucfirst($portalName) . 'SearchResultTableRowBidData';
        $instance = new $instanceName();

        if (!$instance instanceof ISearchResultTableRowBidData) {
            return null;
        }

        return $instance;
    }

    private static function extractValue($values)
    {
        if (!$values) {
            return null;
        }

        $lastValue = array_shift($values);
        return $lastValue->value;
    }

    private static function getTime($value) {
        try {
            $date = new DateTime($value);

            return $date->format('d.m.Y');
        }
        catch (Exception $e) {
            return '';
        }
    }

    public function parseBid(Bids $bid)
    {
        $this->bid = $bid;

        if (empty(self::$fields)) {
            self::$fields = BidsFields::model()->findAll(new CDbCriteria([
                'select' => ['fieldId','name','type'],
                'index' => 'fieldId',
            ]));
        }

        foreach ($this->bid->data as $data) {
            $field = self::$fields[$data->fieldId];
            $value = self::extractValue($data->values);

            if (!$value) {
                continue;
            }

            # href
            if (Utils::valueBelongsToSet($field->name, $this->getHrefFields())) {
                $this->href = $value;
                continue;
            }

            # name
            if (Utils::valueBelongsToSet($field->name, $this->getNameFields())) {
                if (!$this->name) {
                    $this->name = $value;
                }
                continue;
            }

            # price
            if (Utils::valueBelongsToSet($field->name, $this->getPriceFields())) {
                if (!$this->price) {
                    $this->price = $value;
                }
                continue;
            }

            # publish_date_start
            if (Utils::valueBelongsToSet($field->name, $this->getPublishDateStartFields())) {
                if (!$this->publishDateStart) {
                    $this->publishDateStart = self::getTime($value);
                }
                continue;
            }

            # publish_date_end
            if (Utils::valueBelongsToSet($field->name, $this->getPublishDateEndFields())) {
                if (!$this->publishDateEnd) {
                    $this->publishDateEnd = self::getTime($value);
                }
                continue;
            }
        }
    }
}