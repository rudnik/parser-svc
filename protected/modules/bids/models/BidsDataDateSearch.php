<?php

/**
 * Модель поиска значений дат данных закупки. Таблица "bids_data-date-search".
 *
 * Доступные столбцы в таблице "bids_data-date-search":
 * @property integer $valueId Идентификатор значения.
 * @property integer $dataId Идентификатор данных.
 * @property string $value Значение.
 *
 * @package TendersParser
 * @subpackage bids
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013 BSTsoft
 */
class BidsDataDateSearch extends BidsDataBase {
	/**
	 * Возвращает имя таблицы в базе данных.
	 *
	 * @return string Имя таблицы в базе данных.
	 */
	public function tableName() {
		return 'bids_data_date_search';
	}
}
