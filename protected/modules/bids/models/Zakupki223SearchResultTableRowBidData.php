<?php
class Zakupki223SearchResultTableRowBidData extends SearchResultTableRowBidData
{
    protected function getHrefFields()
    {
        return [
            '/registrationNumber',
        ];
    }

    protected function getNameFields()
    {

        return [
            '/purchaseNoticeData/name',
            '/purchaseNoticeOAData/name',
            '/purchaseNoticeOKData/name',
            '/purchaseNoticeAEData/name',
            '/purchaseNoticeZKData/name',
            '/purchaseNoticeEPData/name',
            '/lots/lot/subject',
        ];
    }

    protected function getPriceFields()
    {
        return [
            '/lots/lot/lotData/initialSum',
        ];
    }

    protected function getPublishDateStartFields()
    {
        return [
            '/purchaseNoticeData/createDateTime',
            '/purchaseNoticeOAData/createDateTime',
            '/purchaseNoticeOKData/createDateTime',
            '/purchaseNoticeAEData/createDateTime',
            '/purchaseNoticeZKData/createDateTime',
            '/purchaseNoticeEPData/createDateTime',
        ];
    }

    protected function getPublishDateEndFields()
    {
        return [
            '/purchaseNoticeData/submissionCloseDateTime',
            '/purchaseNoticeOAData/submissionCloseDateTime',
            '/purchaseNoticeOKData/submissionCloseDateTime',
            '/purchaseNoticeAEData/submissionCloseDateTime',
            '/purchaseNoticeZKData/submissionCloseDateTime',
            '/purchaseNoticeEPData/documentationDelivery/deliveryEndDateTime',
        ];
    }

    public function getHref()
    {
        return CHtml::link(
            'Перейти на сайт',
            'http://zakupki.gov.ru/epz/order/quicksearch/search.html?searchString=' . $this->href,
            ['target' => '_blank']
        );
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getPublishDateStart()
    {
        return $this->publishDateStart;
    }

    public function getPublishDateEnd()
    {
        return $this->publishDateEnd;
    }
}