<?php
class Zakupki44SearchResultTableRowBidData extends SearchResultTableRowBidData
{
    protected function getHrefFields()
    {
        return [
            '/href',
        ];
    }

    protected function getNameFields()
    {
        return [
            '/purchaseObjectInfo',
        ];
    }

    protected function getPriceFields()
    {
        return [
            '/lot/purchaseObjects/totalSum',
            '/lots/lot/maxPrice',
        ];
    }

    protected function getPublishDateStartFields()
    {
        return [
            '/docPublishDate'
        ];
    }

    protected function getPublishDateEndFields()
    {
        return [
            '/procedureInfo/collectingEndDate',
            '/stageOne/collecting/endDate',
            '/collecting/endDate',
        ];
    }

    public function getHref()
    {
        return CHtml::link('Перейти на сайт', $this->href, ['target' => '_blank']);
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getPublishDateStart()
    {
        return $this->publishDateStart;
    }

    public function getPublishDateEnd()
    {
        return $this->publishDateEnd;
    }
}