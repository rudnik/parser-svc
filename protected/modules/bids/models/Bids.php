<?php

/**
 * Модель закупки. Таблица "bids".
 *
 * Доступные столбцы в таблице "bids":
 * @property integer $bidId Идентификатор закупки.
 * @property integer $regionId Идентификатор региона.
 * @property integer $portalId Идентификатор портала.
 * @property integer $doctypeId Идентификатор типа документа.
 * @property string $id Уникальный идентификатор в типе документа.
 * @property timestamp $created Дата создания.
 * @property string $status Статус закупки.
 *
 * Доступные модели отношений:
 * @property Regions $region Регион.
 * @property Portals $portal Портал.
 * @property BidsDoctypes $doctype Тип закупки.
 * @property BidsData[] $data Данные закупки.
 * @property BidsDataSearch $searchData Поисковые данные.
 *
 * @package TendersParser
 * @subpackage bids
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013 BSTsoft
 */
class Bids extends CActiveRecord
{
    /**
     * Статус закупки "Отменено".
     *
     * @var string
     */
    const STATUS_CANCELED = 'canceled';

    /**
     * Типы полей для записи в БД.
     *
     * @var array
     */
    protected static $fieldsTypes = [
        'id'                                                         => 'num',
        'versionNumber'                                              => 'num',
        'price'                                                      => 'num',
        'priceChangeReason/id'                                       => 'num',
        'singleCustomerReason/id'                                    => 'num',
        'execution/month'                                            => 'num',
        'execution/year'                                             => 'num',
        'finances/budgetary/month'                                   => 'num',
        'finances/budgetary/year'                                    => 'num',
        'finances/budgetary/substageMonth'                           => 'num',
        'finances/budgetary/substageYear'                            => 'num',
        'finances/budgetary/price'                                   => 'num',
        'finances/extrabudgetary/month'                              => 'num',
        'finances/extrabudgetary/year'                               => 'num',
        'finances/extrabudgetary/substageMonth'                      => 'num',
        'finances/extrabudgetary/substageYear'                       => 'num',
        'finances/extrabudgetary/price'                              => 'num',
        'products/product/sid'                                       => 'num',
        'products/product/price'                                     => 'num',
        'products/product/quantity'                                  => 'num',
        'products/product/sum'                                       => 'num',
        'documentMetas/documentMeta/sid'                             => 'num',
        'lots/lot/customerRequirements/customerRequirement/maxPrice' => 'num',
        // ====
        'publishDate'                                                => 'date',
        'signDate'                                                   => 'date',
        'protocolDate'                                               => 'date',
        'notificationCommission/p1Date'                              => 'date',
        'notificationCommission/p2Date'                              => 'date',
        'competitiveDocumentProvisioning/deliveryTerm'               => 'date',
        'competitiveDocumentProvisioning/deliveryTerm2'              => 'date'
    ];

    /**
     * Поисковые поля.
     *
     * @var array
     */
    protected static $searchFieldsTypes = [
        'num'  => ['lots/lot/customerRequirements/customerRequirement/maxPrice'],
        'date' => [
            'publishDate',
            'notificationCommission/p1Date',
            'notificationCommission/p2Date',
            'competitiveDocumentProvisioning/deliveryTerm',
            'competitiveDocumentProvisioning/deliveryTerm2'
        ],
        'text' => [
            'orderName',
            'lots/lot/subject',
            'lots/lot/customerRequirements/customerRequirement/quantity'
        ]
    ];

    /**
     * Список моделей полей при создании заявки.
     *
     * @var array
     */
    protected static $fieldsModels = [];

    private static $fieldsCache;

    /**
     * Возвращает имя таблицы в базе данных.
     *
     * @return string Имя таблицы в базе данных.
     */
    public function tableName()
    {
        return 'bids';
    }

    /**
     * Возвращает модель класса.
     *
     * @return Bids Модель класса.
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Возвращает массив правил валидации.
     * @return array правила валидации
     */
    public function rules()
    {
        return [
            ['regionId, doctypeId, id', 'required'],
            ['status', 'safe'],
        ];
    }

    /**
     * Возращает связи с другими таблицами.
     *
     * @return array Правила отношений.
     */
    public function relations()
    {
        return [
            'region'     => [self::BELONGS_TO, 'Regions', 'regionId'],
            'portal'     => [self::BELONGS_TO, 'Portals', 'portalId'],
            'doctype'    => [self::BELONGS_TO, 'BidsDoctypes', 'doctypeId'],
            'data'       => [self::HAS_MANY, 'BidsData', 'bidId'],
            'searchData' => [self::BELONGS_TO, 'BidsDataSearch', ['bidId' => 'bid_id']],
        ];
    }

    /**
     * Список атрибутов модели.
     *
     * @return array Атрибуты.
     */
    public function attributeLabels()
    {
        return [
            'bidId'     => 'Идентификатор закупки',
            'regionId'  => 'Идентификатор региона',
            'portalId'  => 'Идентификатор портала',
            'doctypeId' => 'Идентификатор типа документа',
            'id'        => 'Уникальный идентификатор в типе документа',
            'created'   => 'Дата создания',
            'status'    => 'Статус закупки',
        ];
    }

    /**
     * Сохранение заявки.
     *
     * @param array $bidData Данные заявки.
     *
     * @return bool
     *
     * @throws ParserException
     * @throws Exception
     */
    public function saveBidData($bidData)
    {
        if (!isset($bidData['doctypeId'])) {
            throw new CException('Для заявки не указан тип документа.');
        }
        $bidDoctype = BidsDoctypes::model()->findByPk($bidData['doctypeId']);
        if (is_null($bidDoctype)) {
            throw new CException('Тип документа №' . $bidData['doctypeId'] . ' не найден в БД.');
        }

        try {
            $required_fields_errors = [
                'regionId'  => 'Не указан регион закупки.',
                'portalId'  => 'Не указан портал закупки.',
                'doctypeId' => 'Не указан тип документа.',
            ];

            // проверка обязательных полей
            foreach ($required_fields_errors as $field_name => $error_text) {
                if (!isset($bidData[$field_name])) {
                    throw new ParserException($error_text);
                }

                $bid_attrs[$field_name] = $bidData[$field_name];
                unset($bidData[$field_name]);
            }

            foreach ($bidData as $key => &$val) {
                if (false !== strpos($key, '*')) {
                    $bid_attrs['id']  = $bidData[$key][0];
                    $unique           = str_replace('*', '', $key);
                    $bidData[$unique] = $bidData[$key];
                    unset($bidData[$key]);

                    break;
                }
            }

            if (!isset($unique)) {
                throw new ParserException('Отсуствует уникальное поле. ' . var_export($bidData, true));
            }
        } catch (ParserException $e) {
            Yii::log($e->getMessage(), 'info', 'TendersParser');

            return false;
        }

        // нашли отмененные
        if ($bidDoctype->isCanceled()) {
            $bid = Bids::model()->findByAttributes($bid_attrs);

            if (!is_null($bid)) {
                $this->setBidsCanceled($bid);
            }

            return true;
        }

        $transaction = $this->getDbConnection()->beginTransaction();

        try {
            $bid                    = $this->saveBids($bid_attrs);
            $search_data            = new BidsDataSearch();
            $search_data->bid_id    = $bid->bidId;
            $search_data->doctypeId = $bid->doctypeId;
            $search_data->region_id = $bid->regionId;

            foreach ($bidData as $name => $values) {
                $field = $this->saveField(['doctypeId' => $bid->doctypeId, 'name' => $name], $unique);

                if (!$bid->isNewRecord && in_array($field->name, BidsFields::$notUpdatedFields)) {
                    continue;
                }

                $bid_data = $this->saveBidsData(['bidId' => $bid->bidId, 'fieldId' => $field->fieldId]);
                $this->saveBidsDataValues($field->name, $field->type, $bid_data->dataId, $values);
                $this->saveBidsDataSearch($bid, $name, $search_data, $values);
            }

            $this->saveBidsDataSearchModel($search_data);

            $transaction->commit();
        } catch (LogicException $e) {
            $transaction->rollback();
            Yii::log($e->getMessage(), 'info', 'TendersParser');

            return false;
        }

        return true;
    }

    /**
     * Сохранение основной модели заявки.
     *
     * @param array $attributes Атрибуты.
     *
     * @return Bids $bid
     *
     * @throws LogicException
     */
    protected function saveBids($attributes)
    {
        // проверяем закупку в базе
        $bid = Bids::model()->findByAttributes($attributes);

        // создаем закупку
        if (is_null($bid)) {
            $bid = new Bids();
            $bid->setAttributes($attributes, false);

            if (!$bid->save()) {
                throw new LogicException($this->getErrorsStr($bid->getErrors()));
            }
            $bid->setIsNewRecord(true);
        }

        return $bid;
    }

    /**
     * Сохранение модели поля.
     *
     * @param array  $attributes Атрибуты.
     * @param string $unique     Имя уникального поля.
     *
     * @return BidsFields $field
     *
     * @throws LogicException
     */
    protected function saveField($attributes, $unique)
    {
        // ключ в массиве закешированных моделей
        $key = $attributes['doctypeId'] . '=' . $attributes['name'];

        // если такую модель еще не запрашивали
        if (!isset(self::$fieldsModels[$key])) {
            // проверяем поле в базе
            $field = BidsFields::model()->findByAttributes($attributes);

            // создаем поле
            if (is_null($field)) {
                $attributes['unique'] = (int) ($attributes['name'] == $unique);
                $attributes['type']   = isset(self::$fieldsTypes[$attributes['name']])
                    ? self::$fieldsTypes[$attributes['name']]
                    : 'text';
                $field                = new BidsFields();
                $field->setAttributes($attributes, false);

                if (!$field->save()) {
                    throw new LogicException($this->getErrorsStr($field->getErrors()));
                }
            }

            self::$fieldsModels[$key] = $field;
        }

        return self::$fieldsModels[$key];
    }

    /**
     * Сохранение модели данных.
     *
     * @param array $attributes Атрибуты.
     *
     * @return BidsData $bid_data
     *
     * @throws LogicException
     */
    protected function saveBidsData($attributes)
    {
        // проверяем данные в базе
        $bid_data = BidsData::model()->findByAttributes($attributes);

        if (is_null($bid_data)) {
            $bid_data = new BidsData();
            $bid_data->setAttributes($attributes, false);

            if (!$bid_data->save()) {
                throw new LogicException($this->getErrorsStr($bid_data->getErrors()));
            }
        }

        return $bid_data;
    }

    /**
     * Сохранение модели значений данных.
     *
     * @param string  $fieldName Имя поля.
     * @param string  $fieldType Тип поля.
     * @param integer $dataId    Идентификатор данных.
     * @param array   $values    Значения.
     *
     * @throws LogicException
     *
     * @todo при длительной работе вставок/удалений оптимизировать путем выбора,
     * что удалять, а что обновлять
     */
    protected function saveBidsDataValues($fieldName, $fieldType, $dataId, $values)
    {
        /*// @todo как все данные будут переведены в общую таблицу, удалить
        // распределение по таблицам
        switch ($fieldType) {
            case 'text':
                $model = in_array($fieldName, self::$searchFieldsTypes[$fieldType])
                    ? new BidsDataTextSearch()
                    : new BidsDataText();

                break;
            case 'date':
                $model = in_array($fieldName, self::$searchFieldsTypes[$fieldType])
                    ? new BidsDataDateSearch()
                    : new BidsDataDate();

                break;
            case 'num':
                $model = in_array($fieldName, self::$searchFieldsTypes[$fieldType])
                    ? new BidsDataNumSearch()
                    : new BidsDataNum();

                break;
            default: break;
        }*/

        // удаляем данные текущего поля
        $this->dbConnection->createCommand(
            '
            DELETE FROM
                `' . BidsDataValues::model()->tableName() . '`
            WHERE
                `dataId`=:data_id
        '
        )->execute([':data_id' => $dataId]);
        /*$this->dbConnection->createCommand('
            DELETE FROM
                `'.$model->tableName().'`
            WHERE
                `dataId`=:data_id
        ')->execute([':data_id' => $dataId]);*/


        foreach ($values as $value) {
            $value = trim($value);

            if (!empty($value)) {
                if ('date' == $fieldType) {
                    $date  = new DateTime($value);
                    $value = $date->format('Y-m-d H:i:s');
                }

                $attrs = ['dataId' => $dataId, 'value' => $value];
                /*$model->setAttributes($attrs, false);

                if (!$model->save($attrs)) {
                    throw new LogicException($this->getErrorsStr($model->getErrors()));
                }*/

                $bid_data_values = new BidsDataValues();
                $bid_data_values->setAttributes($attrs, false);

                if (!$bid_data_values->save($attrs)) {
                    throw new LogicException($this->getErrorsStr($bid_data_values->getErrors()));
                }
            }
        }

        return true;
    }

    /**
     * Сохранение модели поисковых данных.
     *
     * @param Bids           $bid        Закупка.
     * @param string         $fieldName  Поле.
     * @param BidsDataSearch $searchData Поисковые данные.
     * @param array          $values     Значения.
     */
    protected function saveBidsDataSearch($bid, $fieldName, $searchData, $values)
    {
        $value = array_pop($values);
        $value = trim($value);

        if (!empty($value)) {
            switch ($fieldName) {

                # ====================== 94 ФЗ ====================== #
                case 'orderName':
                case 'lots/lot/subject':
                    $searchData->title .= $value . '; ';

                    break;
                case 'lots/lot/customerRequirements/customerRequirement/quantity':
                    $searchData->description = $value;

                    break;
                case 'publishDate':
                    if ('notificationOK' != $bid->doctype->name) {
                        $date                         = new DateTime($value);
                        $searchData->publication_date = (int) $date->getTimestamp();
                    }

                    break;
                case 'notificationCommission/p1Date':
                    if ('notificationEF' == $bid->doctype->name) {
                        $date                       = new DateTime($value);
                        $searchData->quote_date_end = (int) $date->getTimestamp();
                    }

                    break;
                case 'notificationCommission/p2Date':
                    if ('notificationZK' == $bid->doctype->name) {
                        $date                       = new DateTime($value);
                        $searchData->quote_date_end = (int) $date->getTimestamp();
                    }

                    break;
                case 'competitiveDocumentProvisioning/deliveryTerm':
                case 'competitiveDocumentProvisioning/deliveryTerm2':
                    if ('notificationOK' == $bid->doctype->name) {
                        $date  = new DateTime($value);
                        $value = (int) $date->getTimestamp();

                        if ('competitiveDocumentProvisioning/deliveryTerm' == $fieldName) {
                            $searchData->publication_date = $value;
                        } else {
                            $searchData->quote_date_end = $value;
                        }
                    }

                    break;
                case 'lots/lot/customerRequirements/customerRequirement/maxPrice':
                    $searchData->max_cost = $value;

                    break;

                # ====================== 223 ФЗ ====================== #
                // @todo -o s.utkin: вынести этот метод "наружу" и реализовать
                // для каждого портала свой

                // Наименование
                case 'purchaseNotice/body/item/purchaseNoticeData/name':
                case 'purchaseNoticeOA/body/item/purchaseNoticeOAData/name':
                case 'purchaseNoticeOK/body/item/purchaseNoticeOKData/name':
                case 'purchaseNoticeAE/body/item/purchaseNoticeAEData/name':
                case 'purchaseNoticeZK/body/item/purchaseNoticeZKData/name':
                case 'purchaseNoticeEP/body/item/purchaseNoticeEPData/name':
                    // Названия лотов
                case 'purchaseNotice/body/item/purchaseNoticeData/lots/lot/subject':
                case 'purchaseNoticeOA/body/item/purchaseNoticeOAData/lots/lot/subject':
                case 'purchaseNoticeOK/body/item/purchaseNoticeOKData/lots/lot/subject':
                case 'purchaseNoticeAE/body/item/purchaseNoticeAEData/lots/lot/subject':
                case 'purchaseNoticeZK/body/item/purchaseNoticeZKData/lots/lot/subject':
                case 'purchaseNoticeEP/body/item/purchaseNoticeEPData/lots/lot/subject':
                    $searchData->title .= $value . '; ';
                    break;

                // Дата публикации
                case 'purchaseNotice/body/item/purchaseNoticeData/createDateTime':
                case 'purchaseNoticeOA/body/item/purchaseNoticeOAData/createDateTime':
                case 'purchaseNoticeOK/body/item/purchaseNoticeOKData/createDateTime':
                case 'purchaseNoticeAE/body/item/purchaseNoticeAEData/createDateTime':
                case 'purchaseNoticeZK/body/item/purchaseNoticeZKData/createDateTime':
                case 'purchaseNoticeEP/body/item/purchaseNoticeEPData/createDateTime':
                    $searchData->publication_date = strtotime($value);
                    break;

                // Дата окончания срока подачи заявок
                case 'purchaseNotice/body/item/purchaseNoticeData/submissionCloseDateTime':
                case 'purchaseNoticeOA/body/item/purchaseNoticeOAData/submissionCloseDateTime':
                case 'purchaseNoticeOK/body/item/purchaseNoticeOKData/submissionCloseDateTime':
                case 'purchaseNoticeAE/body/item/purchaseNoticeAEData/submissionCloseDateTime':
                case 'purchaseNoticeZK/body/item/purchaseNoticeZKData/submissionCloseDateTime':
                case 'purchaseNoticeEP/body/item/purchaseNoticeEPData/documentationDelivery/deliveryEndDateTime':
                    $searchData->quote_date_end = strtotime($value);
                    break;

                // Сумма
                case 'purchaseNotice/body/item/purchaseNoticeData/lots/lot/lotData/initialSum':
                case 'purchaseNoticeOA/body/item/purchaseNoticeOAData/lots/lot/lotData/initialSum':
                case 'purchaseNoticeOK/body/item/purchaseNoticeOKData/lots/lot/lotData/initialSum':
                case 'purchaseNoticeAE/body/item/purchaseNoticeAEData/lots/lot/lotData/initialSum':
                case 'purchaseNoticeZK/body/item/purchaseNoticeZKData/lots/lot/lotData/initialSum':
                case 'purchaseNoticeEP/body/item/purchaseNoticeEPData/lots/lot/lotData/initialSum':
                    $searchData->max_cost = $value;
                    break;

                // ИНН Заказчика
                case 'purchaseNotice/body/item/purchaseNoticeData/customer/mainInfo/inn':
                case 'purchaseNoticeOA/body/item/purchaseNoticeOAData/customer/mainInfo/inn':
                case 'purchaseNoticeOK/body/item/purchaseNoticeOKData/customer/mainInfo/inn':
                case 'purchaseNoticeAE/body/item/purchaseNoticeAEData/customer/mainInfo/inn':
                case 'purchaseNoticeZK/body/item/purchaseNoticeZKData/customer/mainInfo/inn':
                case 'purchaseNoticeEP/body/item/purchaseNoticeZKData/customer/mainInfo/inn':
                    $searchData->inn = $value;
                    break;

                default:
                    break;
            }
        }

        return true;
    }

    /**
     * Сохранение модели поисковый данных.
     *
     * @param BidsDataSearch $searchData Поисковые данные.
     *
     * @return bool
     *
     * @throws LogicException
     */
    protected function saveBidsDataSearchModel($searchData)
    {
        // если дата окончания приема заявок меньше, чем сейчас, пропускаем
        if ($searchData->quote_date_end && $searchData->quote_date_end < time()) {
            return false;
        }

        if (!$searchData->save()) {
            throw new LogicException($this->getErrorsStr($searchData->getErrors()));
        }

        return true;
    }

    /**
     * Изменение статуса закупки "Отменена".
     *
     * @param Bids $bidCanceled Отмененная закупка (тип документа).
     *
     * @throws LogicException
     */
    public function setBidsCanceled($bidCanceled)
    {
        // помечаем
        $bids = Bids::model()->findAll(
            new CDbCriteria(
                [
                    'condition' => 'id=:id AND doctypeId!=:doctype_id',
                    'params'    => [':id' => $bidCanceled->id, ':doctype_id' => $bidCanceled->doctypeId]
                ]
            )
        );

        $canceledIds = [];
        foreach ($bids as $bid) {
            $bid->status = self::STATUS_CANCELED;

            if (!$bid->save()) {
                throw new LogicException($this->getErrorsStr($bid->getErrors()));
            }
            $canceledIds[] = $bid->id;
        }
        if (!empty($canceledIds)) {
            BidsDataSearch::model()->deleteAll('bid_id IN (' . implode(', ', $canceledIds) . ')');
        }

        return true;
    }

    /**
     * Получение списка ошибок.
     *
     * @param array $errors Ошибки.
     *
     * @return string Значения ошибок, склеенные через перевод каретки.
     */
    private function getErrorsStr($errors)
    {
        $error_str = [];

        foreach ($errors as $attr => $list) {
            $error_str[] = '(' . $attr . ') ' . implode(' ', $list);
        }

        return 'db_fail=' . implode("\n", array_values($error_str));
    }

    /**
     * Удаляет старые заявки
     *
     * @param integer $days     Если заявка создана раньше, чем $days дней назад - она удаляется
     * @param integer $limit    Лимит удаляемых заявок
     * @param integer $maxBidId Идентификатор последней заявки. Появляется в рекурсии.
     *
     * @return int
     */
    public static function removeOldBids($days = 60, $limit = null, $maxBidId = null)
    {
        $deletedBidsCount = 0;
        $bids             = self::model();
        // находим самый последний айдишник за указанный интервал
        $criteria = new CDbCriteria([
            'select'    => 'MAX(bidId) + 1 AS bidId',
            'condition' => 'created < DATE_SUB(DATE_FORMAT(NOW(), \'%Y-%m-%d 00:00:00\'), INTERVAL ' . $days . ' DAY)'
        ]);

        if ($bid = $bids->find($criteria)) {
            $criteria = new CDbCriteria([
                'condition' => 'bidId<' . $bid->bidId
            ]);
            if ($limit) {
                $criteria->limit  = $limit;
                $deletedBidsCount = $bids->deleteAll($criteria);
            } else {
                // удаляем по 100, ибо приемлимо быстро. каждый порядок увеличивает
                // время по неебической экспоненте
                $criteria->limit = 100;
                // общее количество заявок для удаления
                $count = Yii::app()->getDb()->createCommand(
                    'SELECT
                        COUNT(bidId)
                    FROM `' . $bids->tableName() . '`
                    WHERE
                        `created` < DATE_SUB(DATE_FORMAT(NOW(), \'%Y-%m-%d 00:00:00\'), INTERVAL ' . $days . ' DAY)'
                )->queryScalar();
                while ($count > 0) {
                    $del_count        = $bids->deleteAll($criteria);
                    $count            -= $del_count;
                    $deletedBidsCount += $del_count;
                }
            }
        }

        return $deletedBidsCount;
    }
}
