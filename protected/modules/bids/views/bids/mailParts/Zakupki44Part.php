<table width="100%" border="0" cellpadding="10" cellspacing="0" style="border: 1px solid #ccc">
    <? if (!empty($mailPart->href)) : ?>
        <tr>
            <td style="width: 30%; border-right: 1px solid #ccc; border-bottom: 1px solid #ccc;">Подробный просмотр</td>
            <td style="border-bottom: 1px solid #ccc;"><?= str_replace('<a ', '<a style="color: #248ADA; font-weight: bold;" ', $mailPart->href); ?></td>
        </tr>
    <? endif; ?>
    <?if( ! empty($mailPart->orderNumber)) : ?>
        <tr>
            <td style="width: 30%; border-right: 1px solid #ccc; border-bottom: 1px solid #ccc;">Номер заказа</td>
            <td style="border-bottom: 1px solid #ccc;"><?=$mailPart->orderNumber;?></td>
        </tr>
    <?endif;?>
    <?if( ! empty($mailPart->customerName)) : ?>
        <tr>
            <td style="width: 30%; border-right: 1px solid #ccc; border-bottom: 1px solid #ccc;">Заказчик</td>
            <td style="border-bottom: 1px solid #ccc;"><?=$mailPart->customerName;?></td>
        </tr>
    <?endif;?>
    <?if( ! empty($mailPart->orderName)) : ?>
        <tr>
            <td style="width: 30%; border-right: 1px solid #ccc; border-bottom: 1px solid #ccc;">Наименование заказа</td>
            <td style="border-bottom: 1px solid #ccc;">
                <p style="margin: 0 0 7px 0;"><?= $mailPart->orderName; ?></p>
            </td>
        </tr>
    <?endif;?>
    <?if( ! empty($mailPart->deliveryPlace)) : ?>
        <tr>
            <td style="width: 30%; border-right: 1px solid #ccc; border-bottom: 1px solid #ccc;">Место поставки</td>
            <td style="border-bottom: 1px solid #ccc;"><?=$mailPart->deliveryPlace;?></td>
        </tr>
    <?endif;?>
    <?if( ! empty($mailPart->maxPrice)) : ?>
        <tr>
            <td style="width: 30%; border-right: 1px solid #ccc; border-bottom: 1px solid #ccc;">Начальная (максимальная) цена</td>
            <td style="border-bottom: 1px solid #ccc;"><?=$mailPart->maxPrice;?></td>
        </tr>
    <?endif;?>
    <?if( ! empty($mailPart->bidsEndFillingDate)) : ?>
        <tr>
            <td style="width: 30%; border-right: 1px solid #ccc; border-bottom: 1px solid #ccc;">Дата окончания срока подачи заявок</td>
            <td style="border-bottom: 1px solid #ccc;"><?=$mailPart->bidsEndFillingDate;?></td>
        </tr>
    <?endif;?>
    <?if( ! empty($mailPart->auctionStartDate)) : ?>
        <tr>
            <td style="width: 30%; border-right: 1px solid #ccc; border-bottom: 1px solid #ccc;">Дата и время проведения аукциона</td>
            <td style="border-bottom: 1px solid #ccc;"><?=$mailPart->auctionStartDate;?></td>
        </tr>
    <?endif;?>
    <?if( ! empty($mailPart->qualifyFilter)) : ?>
        <tr>
            <td style="width: 30%; border-right: 1px solid #ccc; border-bottom: 1px solid #ccc;">Дата и время предквалификационного отбора</td>
            <td style="border-bottom: 1px solid #ccc;"><?=$mailPart->qualifyFilter;?></td>
        </tr>
    <?endif;?>
    <?if( ! empty($mailPart->preFilter)) : ?>
        <tr>
            <td style="width: 30%; border-right: 1px solid #ccc; border-bottom: 1px solid #ccc;">Дата и время предварительного отбора</td>
            <td style="border-bottom: 1px solid #ccc;"><?=$mailPart->preFilter;?></td>
        </tr>
    <?endif;?>
    <?if( ! empty($mailPart->closedAuctionStartDate)) : ?>
        <tr>
            <td style="width: 30%; border-right: 1px solid #ccc; border-bottom: 1px solid #ccc;">Дата и время проведения закрытого аукциона</td>
            <td style="border-bottom: 1px solid #ccc;"><?=$mailPart->closedAuctionStartDate;?></td>
        </tr>
    <?endif;?>
    <?if( ! empty($mailPart->placingOrderMethod)) : ?>
        <tr>
            <td style="width: 30%; border-right: 1px solid #ccc;">Способ размещения заказа</td>
            <td><?=$mailPart->placingOrderMethod;?></td>
        </tr>
    <?endif;?>
</table>
<hr style="margin: 20px 0 30px 0;">
