<?php

/**
 * Контроллер заявок
 */
class BidsController extends Controller {

    /**
     * @return array action filters
     */
    public function filters() {
	return ['accessControl'];
    }

    public function accessRules() {
	return [
	    ['allow',
		'roles' => ['manager'],
	    ],
	    ['deny', // deny all users
		'users' => ['*'],
	    ],
	];
    }

    /**
     * Вывод формы детализированной заявки
     * @param integer $id
     */
    public function actionDetails($id) {
	$model = Bids::model()->findByPk($id);
	if (is_null($model))
	    throw new CHttpException('Заявка не найдена', 404);
	
//	$viewModel = new BidsDetailsViewModel();
//	$viewModel->bid = $model;
//	if (!$viewModel->validate())
//	    throw new RuntimeException('Валидация модели завершилась с ошибкой. ' . var_export($viewModel->errors, true));
//	$this->render('details', ['viewModel' => $viewModel]);
	
	$this->render('details', ['bidMailPart' => BidMailPart::factory($model)]);
    }

}