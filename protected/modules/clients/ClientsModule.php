<?php
/**
 * Модуль 'Клиенты'
 */
class ClientsModule extends CWebModule {
    public function init(){
	Yii::import('application.modules.clients.models.*');
	$publishPath =  Yii::app()->assetManager->publish( Yii::getPathOfAlias('clients.assets'));
	Yii::app()->clientScript->registerScriptFile( $publishPath . '/js/moduleClients.js');
	Yii::app()->clientScript->registerCssFile( $publishPath . '/css/clients.css');
    }
}