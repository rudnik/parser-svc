<?php /* @var $dataProvider CActiveDataProvider */ ?>
<?
if (isset($_GET['pageSize'])) {
    Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
    unset($_GET['pageSize']);
}

$pageSize = Yii::app()->user->getState('pageSize');
$dataProvider->pagination->pageSize = $pageSize;
?>
<div>
    <br>

    <h3 style="margin-bottom: 0;">Найдено компаний: <?= $dataProvider->totalItemCount ?></h3>
    <?
    echo CHtml::openTag('div', ['style' => 'float:right']);
    echo CHtml::label('Количество элементов на странице:', 'pageSize');
    echo CHtml::dropDownList('pageSize', $pageSize, [30 => 30, 50 => 50, 100 => 100], ['class' => 'span3 pull-right']);
    echo CHtml::closeTag('div');
    echo CHtml::tag('br');
    ?>
    <?php
    $this->widget(
        'bootstrap.widgets.TbExtendedGridView',
        [
            'enableSorting' => false,
            'type' => 'bordered',
            'dataProvider' => $dataProvider,
            'rowCssClassExpression' => '
            $data->is_duplication ? "dublicate" : (
                $data->importance == "Край географии" ? "importance-far" : (
                    $data->importance == "Высокая важность" ? "importance-high" : (
                        $row % 2 ? "even" : "odd"
                    )
                )
            );
        ',
            'columns' => [
                [
                    'class' => 'LinkColumn',
                    'header' => 'Наименование',
                    'labelExpression' => '$data->title',
                    'urlExpression' => 'Yii::app()->createUrl("clients/clients/update", ["id" => $data->id])',
                    'renderLinkExpression' => '
                    Yii::app()->user->checkAccess(\'editClients\', [\'user_id\' => $data->user_id]) ||
                    (
                        isset(
                            Users::$usersRelations[Yii::app()->user->id]
                        ) &&
                        in_array(
                            (int)$data->user->userId,
                            Users::$usersRelations[Yii::app()->user->id]
                        )
                    )
                    ',
                ],
                [
                    'header' => 'E-mail',
                    'name' => 'clientEmails.email',
                    'value' => function ($data) {
                            $content = '';
                            foreach ($data->clientEmails as $item) {
                                $content .= $item->email . ', ';
                            }
                            return trim($content, ', ');
                        }
                ],
                [
                    'header' => 'Сайт',
                    'name' => 'website',
                ],
                [
                    'header' => 'Город',
                    'name' => 'city.OFFNAME',
                    'value' => 'isset($data->city->OFFNAME) ? $data->city->OFFNAME : \'\'',
                ],
                [
                    'header' => 'Менеджер',
                    'name' => 'user.name',
                    'value' =>
                        '(
                        Yii::app()->user->checkAccess("admin") ||
                        (
                            isset(
                                Users::$usersRelations[Yii::app()->user->id]
                            ) &&
                            in_array(
                                (int)$data->user->userId,
                                Users::$usersRelations[Yii::app()->user->id]
                            )
                        )
                    ) ? $data->user->name : ""',
                    'visible' => Yii::app()->user->checkAccess("admin") || isset(Users::$usersRelations[Yii::app(
                        )->user->id]),
                ],
                [
                    'header' => 'Протокол',
                    'value' => 'ClientProtocol::getLastItemsForSearchResultView($data->id)',
                    'visible' => Yii::app()->user->checkAccess('admin'),
                ],
                [
                    'name' => 'is_duplication',
                    'value' => '$data->is_duplication > 0 ? "Да" : "Нет"',
                ],
                [
                    'name' => 'contract_end_date',
                    'value' => '$data->contract_end_date > 0 ? date("d.m.Y", $data->contract_end_date) : ""',
                ],
                [
                    'header' => 'Дата контакта',
                    'value' => '$data->next_contact > 0 ? date("d.m.Y", $data->next_contact) : ""',
                ],
                [
                    'class' => 'LinkColumn',
                    'header' => 'Действия',
                    'labelExpression' => 'Yii::app()->user->checkAccess(\'editClients\', [\'user_id\' => $data->user_id]) ? "Удалить" : \'\'',
                    'urlExpression' => 'Yii::app()->createUrl("clients/clients/delete", ["id" => $data->id])',
                    'renderLinkExpression' => 'Yii::app()->user->checkAccess(\'editClients\', [\'user_id\' => $data->user_id])',
                    'linkHtmlOptions' => [
                        'class' => 'js-delete-link',
                        'token' => Yii::app()->request->csrfToken,
                        'onclick' => 'return false;'
                    ],
                ],
            ],
            'ajaxVar' => false,
            'ajaxUpdate' => false,
        ]
    );
    ?>
</div>