<?php
/* @var $form TbActiveForm */
/* @var $subscr ClientsSubscriptions */
Yii::app()->clientScript->registerCssFile('/css/jquery.checkboxtree.css');
Yii::app()->getClientScript()->registerCoreScript('jquery.ui');
Yii::app()->clientScript->registerCssFile('/css/jquery.tagedit.css');
Yii::app()->clientScript->registerScriptFile('/js/jquery.checkboxtree.js');
Yii::app()->clientScript->registerScriptFile('/js/jquery.autoGrowInput.js');
Yii::app()->clientScript->registerScriptFile('/js/jquery.tagedit.js');
Yii::app()->clientScript->registerScriptFile('/js/parser.js');
Yii::app()->clientScript->registerScriptFile( 
        Yii::app()->assetManager->publish(
                Yii::getPathOfAlias('clients.assets')
        ) . '/js/subscriptionsBidsList.js'
);

$regions = Regions::model()->findAll(new CDbCriteria([
    'condition' => 'parentId=0',
    'order'     => 'title'
]));

$branches = Branches::model()->findAll(new CDbCriteria([
    'order' => 'title'
]));

$subscrClone = clone($subscr);
$subscrClone->setScenario('view');
$subscrClone->validate(['emails']);
$subscr->emails = $subscrClone->emails;
if(strtotime($subscr->activedate_from) < 1)
    $subscr->activedate_from = date("Y-m-d");
if($subscr->isNewRecord) {
    $subscr->doctypeIds = [1,2,3];
    $subscr->active = true;
}

if ($subscr->isNewRecord) {
    $subscr->portalsIds = array_keys(CHtml::listData(Portals::model()->findAll(), 'portalId', 'title'));
    $subscr->demandTypesIds = array_keys(DemandType::getList());
}

$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'clients_subscriptions',
	// 'enableAjaxValidation' => true
));
?>
<style>
ul.check-tree {
    margin-left: 0;
    border: 0;
}
ul.check-tree li>span {
    float : left;
}
</style>

	<span class="danger"><?=$form->errorSummary($subscr, '');?></span>
	<?=$form->hiddenField($subscr, 'clientId');?>

<?
    $client = Clients::model()->findByPk(Yii::app()->request->getParam('clientId'));
?>

<?php if ($client !== null): ?>
    <div style="padding: 20px 0 20px 0">
        <a href="/clients/clients/">Список клиентов</a> /
        <a href="/clients/clients/update/<?php echo $client->id;?>">"<?php echo $client->title;?>"</a> /
        <a href="/clients/subscriptions?clientId=<?php echo $client->id;?>">Рассылки</a> /
        <span>"<?php echo $subscr->title;?>"</span>
    </div>
<?php endif; ?>

    <div class="row-fluid">
        <div class="span6">
            <?/*  Наименование  */?>
            <?=$form->textFieldRow($subscr, 'title', ['class' => 'span12']);?>
        </div>
        <div class="span6">
            <div class="span4">
                <?/*  Активность  */?>
                <label>&nbsp;</label>
                <?=$form->checkBoxRow($subscr, 'active');?>
            </div>
            <div class="span7">
                <?/*  Отправлять рассылку с дефолтного email  */?>
                <label>&nbsp;</label>
                <?=$form->checkBoxRow($subscr, 'send_from_default_email');?>
            </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <?/*  Список адресов рассылки  */?>
            <?=$form->textFieldRow($subscr, 'emails', ['class' => 'span12']);?>
        </div>
    </div>

<div class="row-fluid">
        <div class="span12">
            <?/*  Тема письма*/?>
            <?=$form->textFieldRow($subscr, 'theme', ['class' => 'span12']);?>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span6">
            <?/* Дата действия c  */?>
            <?=$form->labelEx($subscr, 'activedate_from');?>
            <?=$form->dateField($subscr, 'activedate_from', ['class' => 'span12']);?>
        </div>
        <div class="span6">
            <?/*  Дата действия по  */?>
            <?=$form->labelEx($subscr, 'activedate_to');?>
            <?=$form->dateField($subscr, 'activedate_to', ['class' => 'span12']);?>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <?= $form->labelEx($subscr, 'regions'); ?>
            <input type="hidden" name="<?= get_class($subscr); ?>[regions]" />
            <ul class="check-tree" style="list-style-type: none;">
                <?php
                $class = get_class($subscr);

                foreach ($regions as $okrug) {
                    ?>
                    <li><label style="float: none" class="checkbox"><input type="checkbox" name="<?= $class; ?>[regions][]" value="<?= $okrug->regionId; ?>"<?= in_array($okrug->regionId, $subscr->regionsIdAsArray) ? ' checked="checked"' : ''; ?> /> <?= $okrug->title; ?></label>
                    <ul style="list-style-type: none;"><?php foreach ($okrug->children as $region) { ?>
                            <li><label style="float: none" class="checkbox"><input type="checkbox" name="<?= $class; ?>[regions][]" value="<?= $region->regionId; ?>"<?= in_array($region->regionId, $subscr->regionsIdAsArray) ? ' checked="checked"' : ''; ?> /> <?= $region->title; ?></label></li><?php }
                        ?>
                    </ul>
                    </li><?php
                }
                ?>
            </ul>
            <?= $form->error($subscr, 'regionId'); ?>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span6">
            <?= $form->labelEx($subscr, 'keywords_inc'); ?>
            <?php
            $class = get_class($subscr);

            foreach (explode("\n", $subscr->keywords_inc) as $i => $tag) {
                echo '<input class="keywords_inc" id="' . $class . '_keywords_inc' . $i . '" type="text" name="' . $class . '[keywords_inc][]" value="' . $tag . '" />';
            }
            ?>
            <?= $form->error($subscr, 'keywords_inc'); ?>
        </div>
        <div class="span6">
            <?= $form->labelEx($subscr, 'keywords_exc'); ?>
            <?php
            foreach (explode("\n", $subscr->keywords_exc) as $i => $tag) {
                echo '<input class="keywords_exc" id="' . $class . '_keywords_exc' . $i . '" type="text" name="' . $class . '[keywords_exc][]" value="' . $tag . '" />';
            }
            ?>
            <?= $form->error($subscr, 'keywords_exc'); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span6">
            <?= $form->labelEx($subscr, 'portals'); ?>
            <?= $form->checkBoxList($subscr, 'portalsIds', CHtml::listData(Portals::model()->findAll(), 'portalId', 'title')); ?>
            <?= $form->error($subscr, 'portals'); ?>


            <label style="margin-top: 30px"  class="checkbox"><?=$form->checkBox($subscr, 'keywords_advance');?><label for="ClientsSubscriptions_keywords_advance">Искать в характеристиках</label></label>
            <?=$form->error($subscr, 'keywords_advance'); ?>
        </div>
        <div class="span6">
            <?= $form->labelEx($subscr, 'doctypes'); ?>
            <?= $form->checkBoxList($subscr, 'demandTypesIds', DemandType::getList()); ?>
            <?= $form->error($subscr, 'doctypes'); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span6">
            <label>Минимальная сумма <br><?=$form->textField($subscr, 'price_min', ['type' => 'number', 'class' => 'span12']);?></label>
            <?=$form->error($subscr, 'price_min');?>
        </div>
        <div class="span6">
            <label>Максимальная сумма <br><?=$form->textField($subscr, 'price_max', ['type' => 'number', 'class' => 'span12']);?></label>
            <?=$form->error($subscr, 'price_max');?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span6">
            <label>Дата публикации c <br><?=$form->dateField($subscr, 'pubdate_from_start', ['class' => 'span12']);?></label>
            <?=$form->error($subscr, 'pubdate_from_start');?>
        </div>
        <div class="span6">
            <label>Дата публикации по <br><?=$form->dateField($subscr, 'pubdate_from_end', ['class' => 'span12']);?></label>
            <?=$form->error($subscr, 'pubdate_from_end');?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span6">
            <label>Дата окончания срока подачи заявок с <br><?=$form->dateField($subscr, 'pubdate_to_start', ['class' => 'span12']);?></label>
            <?=$form->error($subscr, 'pubdate_to_end');?>
        </div>
        <div class="span6">
            <label>Дата окончания срока подачи заявок по <br><?=$form->dateField($subscr, 'pubdate_to_end', ['class' => 'span12']);?></label>
            <?=$form->error($subscr, 'pubdate_to_end');?>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span6">
            <?=$form->textFieldRow($subscr, 'inn', ['type' => 'number', 'class' => 'span12']);?></label>
        </div>
        <div class="span6">
        </div>
    </div>

	<div class="row-fluid">
        <div class="span12">
            <label>
                <?=CHtml::link('Посмотреть результаты', Yii::app()->createUrl('clients/subscriptions/search'), ['id' => 'look-search', 'class' => 'btn btn-success']);?>
                <img id="loading" style="display: none;" src="<?=Yii::app()->request->baseUrl;?>/images/spinner.gif" />
            </label>
        </div>
	</div>
	<div class="row-fluid">
        <div class="span12">
            <label>
		        <?=CHtml::submitButton($subscr->isNewRecord ? 'Добавить рассылку' : 'Сохранить', ['class' => 'btn btn-primary']); ?>
            </label>
        </div>
        <div id="search"></div>
        <div id='checkedBidsList'></div>
	</div>
</div>
<? $this->endWidget(); ?>

