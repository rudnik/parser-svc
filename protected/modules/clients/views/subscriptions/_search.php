<?php        
if (isset($error)) {
	echo $error;
}
else {
	$get_date = function($value) {
		try {
			$date = new DateTime($value);

			return $date->format('d.m.Y');
		}
		catch (Exception $e) {
			return '';
		}
	};

?>
<?
    if(count($bids) > 0)
	echo CHtml::submitButton('Отправить заявки', ['name' => 'send_email', 'class' => 'btn   ']);
?>
<br>
<?
    echo CHtml::openTag('div', ['style' => 'float:right']);
        echo CHtml::label('Количество элементов на странице:', 'pageSize');
        echo CHtml::dropDownList('pageSize', (int) Yii::app()->user->getState('pageSize'), [30 => 30, 50 => 50, 100 => 100], ['class' => 'span3 pull-right']);
    echo CHtml::closeTag('div');
    echo  CHtml::tag('br');
?>
<strong>Найдено: <?=$count;?></strong>
<table class="table table-bordered table-hover">
	<thead>
		<tr>
            <th><?= CHtml::checkBox('selectBidsOnPage', false, ['id' => 'selectBidsOnPage']); ?></th>
			<th>№</th>
			<th>Тип документа</th>
			<th>Регион</th>
			<th>Наименование</th>
			<th>Цена</th>
			<th>Дата публикации</th>
			<th>Дата окончания срока подачи</th>
			<th>Ссылка</th>
            <?if(!empty($sendedBids)):?>
                <th>Дата отправки</th>
            <?endif;?>
		</tr>
	</thead>
<tbody>
<?php
	foreach ($bids as $bid) {
        $bidTableDataSource = SearchResultTableRowBidData::factory($bid->portal->name);
        $bidTableDataSource->parseBid($bid);
		?>
	<tr class="odd" <?=isset($sendedBids[$bid->bidId]) ? ' style="background:#ffffcc" ' : ''?>>
            <td><?= CHtml::checkBox('checkedBids[' . $bid->bidId . ']', false, ['value' => $bid->bidId, 'class' => 'selectable-bids'] ); ?></td>
			<td><?=CHtml::link($bid->bidId, Yii::app()->createUrl('bids/bids/details', ['id' => $bid->bidId]), ['target' => '_blank']);?></td>
			<td><?=$bid->doctype->descr?></td>
			<td><?=$bid->region->title;?></td>
            <td><?=$bidTableDataSource->getName()?></td>
            <td><?=$bidTableDataSource->getPrice()?></td>
            <td><?=$bidTableDataSource->getPublishDateStart()?></td>
            <td><?=$bidTableDataSource->getPublishDateEnd()?></td>
            <td><?=$bidTableDataSource->getHref()?></td>
        <?if(!empty($sendedBids)):?>
            <td><?= isset($sendedBids[$bid->bidId]) ? date("d.m.Y H:i", strtotime($sendedBids[$bid->bidId]->date)) : ''?></td>
        <?endif;?>
	</tr>
<?php
	}
	?>
	</tbody>
</table>
<div class="pagination">
<?php
	$this->widget('CLinkPager', [
		'header' => '',
		'firstPageLabel' => '<<',
		'prevPageLabel' => '<',
		'nextPageLabel' => '>',
		'lastPageLabel' => '>>',
        'selectedPageCssClass' => 'active',
        'hiddenPageCssClass' => 'disabled',
		'pages' => $pages
	]);
}
?>
</div>

