<? 
    /* @var $this Controller */

    /** @var Clients $client */
    $client = Clients::model()->findByPk($clientId);
?>

<?php if ($client !== null): ?>
<div style="padding: 20px 0 20px 0">
    <a href="/clients/clients/">Список клиентов</a> / <a href="/clients/clients/update/<?php echo $client->id;?>">"<?php echo $client->title;?>"</a> / <span>Рассылки</span>
</div>
<?php endif; ?>

<?php
    $this->renderPartial('list', ['clientId' => $clientId]);
    echo CHtml::button('Добавить рассылку', ['onclick' => 'js:window.location="/clients/subscriptions/add?clientId=' . $clientId . '"', 'class' => 'btn btn-primary']);
?>
