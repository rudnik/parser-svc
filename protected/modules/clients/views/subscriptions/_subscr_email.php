<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Рассылка от SVCTender</title>
</head>
<body>
<table cellspacing="0" cellpadding="0" border="0" width="600" align="center"
       style="border-collapse:collapse; font-family: Tahoma, sans-serif; font-size: 12px;">
    <tr>
        <td><a href="http://svc-tender.ru/" target="_blank"><img style="margin: 10px 0 0 0;"
                                                                 src="http://svc-tender.ru/i/svctender.png"
                                                                 alt="svc-tender-logo" width="142" height="42"></a>
        </td>
    </tr>
    <tr>
        <td>
            <?
            /* @var $viewModel BidsMailViewModel */
            /* @var $mailParts IBidMailPart[] */
            /* @var $clientName string */
            /* @var $userSignature string */
            if (isset($mailSubject) && $mailSubject) {
                ?>
                <h4 style="margin: 10px 0 3px 0; line-height: 18px; font-size: 14px; color: #3b3b3b; font-weight: 500;">
                    <?php echo $mailSubject; ?></h4>
            <? } elseif ($clientName !== null) { ?>
                <h4 style="margin: 10px 0 3px 0; line-height: 18px; font-size: 14px; color: #3b3b3b; font-weight: 500;">
                    Рассылка для клиента</h4>
                <h3 style="margin: 3px 0 5px 0; padding: 0; line-height: 20px; font-size: 20px; color: #3b3b3b; font-weight: 500;">
                    <?php echo $clientName; ?>
                </h3>
            <?php } else { ?>
                <h4 style="margin: 10px 0 3px 0; line-height: 18px; font-size: 14px; color: #3b3b3b; font-weight: 500;">
                    По Вашей заявке найдены следующие тендеры:</h4>
            <?php } ?>
        </td>
    </tr>
    <?php
foreach ($viewModel->getBidsList() as $portalName => $mailParts) {
    if (empty($mailParts)) {
        continue;
    }
    ?>
    <tr>
        <td>
            <h5 style="margin: 10px 0 20px 0; padding: 0; line-height: 20px; font-size: 18px; color: #3b3b3b; font-weight: 500;">
                Источник: <?php echo $portalName; ?></h5>
        </td>
    </tr>
    <tr>
        <td>
    <?php
    foreach ($mailParts as $mailPart) {
        echo $mailPart->render();
    }
    ?>
        </td>
    </tr>
<?php } ?>
    <tr>
        <td>
            <?php
            # подпись менеджера
            echo nl2br($userSignature);
            ?>
        </td>
    </tr>
</table>
</body>
</html>
