<? 
    /* @var $this Controller */
?>
<div>
<?php
	$this->widget('bootstrap.widgets.TbExtendedGridView', [
        'enableSorting' => false,
        'type' => 'stripped bordered considered',
		'dataProvider' => new CActiveDataProvider('ClientsSubscriptions', [
			'criteria' => ['condition' => 'clientId=' . $clientId]
		]),
		'columns' => [
			[
				'class' => 'CLinkColumn',
				'header' => 'Название',
				'labelExpression' => '$data->title',
				'urlExpression' => 'Yii::app()->createUrl("clients/subscriptions/update", ["subscrId" => $data->subscrId, "clientId" => $data->clientId])'
			],			
			[
			    'header' => 'Дата действия "с"',
			    'name' => 'activedate_from',
			    'value' => 'strtotime($data->activedate_from) > 0 ? date("d.m.Y", strtotime($data->activedate_from)) : ""',
			],
			[
			    'header' => 'Дата действия "по"',
			    'name' => 'activedate_to',
			    'value' => 'strtotime($data->activedate_to) > 0 ? date("d.m.Y", strtotime($data->activedate_to)) : ""',
			],
			[
			    'name' => 'active',
			    'value' => '$data->active ? "Да" : "Нет"',
			],
			[
			    'class' => 'CButtonColumn',
			    'deleteConfirmation' => 'Вы уверены?',
			    'header' => 'Действия', 
			    'template' => '{update} {delete}',
			    'buttons' => [
                    'update' => [
                        'label' => 'Редактировать',
                        'url' => '"/clients/subscriptions/update?subscrId=" . $data->subscrId . "&clientId=" . $data->clientId',
                    ],
                    'delete' => [
                        'label' => 'Удалить',
                        'url' => '"/clients/subscriptions/delete?subscrId=" . $data->subscrId',
                    ]
			    ]
			],
		]
	]);
?>
</div>