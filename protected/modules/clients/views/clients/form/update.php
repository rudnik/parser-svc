<?php
    /* @var $model Clients */
    /* @var $form CActiveForm */
    /* @var $this Controller */

    if(is_null($model)) { echo 'Клиент не найден.'; return;}

    $model->setScenario($model->hasErrors() ? 'form_error' : 'edit');
    $model->validate(null, false);
    if( ! Yii::app()->request->isPostRequest) $model->clearErrors();
?>
<div style="padding: 20px 0 20px 0">
    <a href="/clients/clients/">Список клиентов</a> / <span>"<?php echo $model->title;?>"</span>
</div>
<?
    if( ! $model->isNewRecord) echo CHtml::link('Настройка рассылки', Yii::app()->createUrl('clients/subscriptions', ['clientId' => $model->id]), ['class' => 'btn btn-success']);
?>
<br>
<br>
<?php
    $regions = Regions::model()->findAll(new CDbCriteria(['condition' => 'parentId!=0', 'order' => 'title']));
    $branches = Branches::model()->findAll(new CDbCriteria(['order' => 'title']));
?>


<?php
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array( 'id' => 'clients', /*'enableAjaxValidation' => true*/));
    if( ! $model->isNewRecord) CHtml::hiddenField('id', $model->id);
    $form->errorSummary($model);
?>
<div class="row-fluid">
    <div class="span6">
        <? $this->renderPartial('form/update/company', ['model' => $model, 'form' => $form]); ?>
        <? $this->renderPartial('form/update/company_contacts', ['model' => $model, 'form' => $form]); ?>
    </div>
    <div class="span6">
        <? $this->renderPartial('form/update/detail_info', ['model' => $model, 'form' => $form]); ?>
    </div>
</div>

<div class="row-fluid">
    <? $this->renderPartial('form/update/sale', ['model' => $model, 'form' => $form]); ?>
</div>


<?=CHtml::submitButton('Сохранить', ['class' => 'btn btn-primary']);?>

<?php $this->endWidget(); ?>