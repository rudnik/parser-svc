<?php
/* @var $form TbActiveForm */
/* @var $model Clients */
/* @var $this Controller */
?>
	<?php
	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
		'id' => 'clients',
		'action' => $this->createUrl('clients/index'),
        'type' => 'horizontal',
        'htmlOptions' => ['class' => 'well'],
		/*'enableAjaxValidation' => true*/
	]);
?>
<div class="control-group">
    <div class="controls">
        <div class="row-fluid">
            <?= CHtml::button('Добавить клиента', ['name' => 'button_add', 'class' => 'btn btn-primary span3']);?>
        </div>
    </div>
</div>
<?
    echo $form->textFieldRow($model, 'title', ['class' => 'span9']);
    echo $form->textFieldRow($model, 'searchedEmails', ['class' => 'span9']);
    echo $form->textFieldRow($model, 'website', ['class' => 'span9']);
    echo $form->dropDownListRow($model, 'client_status', CMap::mergeArray(['' => "- любой -"],  Clients::getDropdownClientStatus(true)), ['class' => 'span9']);

?>
    <div class="control-group ">
        <?=$form->labelEx($model, 'city_id', ['class' => 'control-label'])?>
        <div class="controls">
            <?
            echo $form->hiddenField($model, 'region_id') . $form->hiddenField($model, 'city_id');
            $this->widget(
                'zii.widgets.jui.CJuiAutoComplete',
                [
                    'name' => 'city_title',
                    'value' => $model->city ? $model->city->OFFNAME . ' ' . $model->city->SHORTNAME : '',
                    'source' => 'js: function(request, response) {
            $.ajax({
            url: "/clients/clients/getCities/",
            dataType: "json",
            beforeSend: function() { $("#loading_cities").show(); },
            complete: function() { $("#loading_cities").hide(); },
            data: { region: "", title: request.term },
            success: function(data) {
            response(data
            ? $.map(data, function(item) {
            var title = item.title + " " + item.short_name,
            label = title + " (" + item.region_title + " " + item.region_short_name + ")";

            return { id: item.id, value: title, label: label, region_id: item.region_id }
            })
            : {}
            );
            }
            });
            }',
                    // при выборе города, вставляем и айдишник региона
                    'options' => [
                        'showAnim' => 'fold',
                        'select' => 'js: function(event, ui) {
            $("#' . get_class($model) . '_city_id").val(ui.item.id);
            $("#' . get_class($model) . '_region_id").val(ui.item.region_id);
            }',
                        'change' => 'js: function(event, ui) {
            if (!ui.item) {
            $("#' . get_class($model) . '_city_id").val("");
            $("#' . get_class($model) . '_region_id").val("");
            }
            }'
                    ],
                    'htmlOptions' => [
                        'class' => 'span9'
                        //'style' => 'width: 100%',
                    ],
                ]
            );
            ?><img id="loading_cities" style="display: none;" src="<?=Yii::app()->request->baseUrl; ?>/images/spinner.gif"/>
            <?echo $form->error($model, 'city_id');?>
        </div>
    </div>
    <div class="form-actions">
            <div class="row-fluid">
                    <?=$form->dropDownList($model, 'searchConditionType', ['AND' => 'Совпадают все поля', 'OR' => 'Должно совпасть одно из полей']);?>

            </div>
            <br>
            <div class="row-fluid">
                <?=CHtml::submitButton('Найти', ['name' => 'button_search', 'class' => 'btn btn-success']);?>
            </div>
    </div>
	<?php $this->endWidget(); ?>
