<?php
/* @var $form TbActiveForm */
/* @var $model Clients */
/* @var $this Controller */
?>

<div class="well well-small">Контактные данные</div>
<? /* ===== Телефон ===== */ ?>
<?= $form->textFieldRow($model, 'clientPhones', ['class' => 'span12']); ?>

<? /* ===== Email ===== */ ?>
<?= $form->textFieldRow($model, 'clientEmails', ['class' => 'span12', 'multiple' => true]); ?>

<? /* ===== E-mail (рассылка) ===== */ ?>
<?= $form->textFieldRow($model, 'clientSubscribeEmails', ['class' => 'span12', 'multiple' => true]); ?>

<? /* ===== Комментарий к почте ===== */ ?>
<?= $form->textFieldRow($model, 'email_comment', ['class' => 'span12']); ?>

<? $this->renderPartial('form/update/contact_name_fields', ['form' => $form, 'model' => $model]); ?>
