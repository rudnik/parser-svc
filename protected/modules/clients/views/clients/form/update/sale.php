<?php
/* @var $form TbActiveForm */
/* @var $model Clients */
/* @var $this Controller */
?>

    <div class="well well-small span12">Продажа</div>

    <div class="row-fluid">
        <div class="span6">
            <? /* ===== Менеджер ===== */ ?>
            <?if(Yii::app()->user->checkAccess('changeClientsManager')){?>
                <?= $form->dropDownListRow($model, 'user_id', CHtml::listData(Users::model()->findAll(['order' => 'name ASC']), 'userId', 'name'), ['class' => 'span6'])?>
            <?}elseif(Users::isMyUser(Yii::app()->user->id, $model->user_id)){?>
                <?= $form->dropDownListRow($model, 'user_id', CHtml::listData(Users::model()->findAll(['condition' => 'userId IN (' . implode(',', Users::$usersRelations[Yii::app()->user->id]) . ')','order' => 'name ASC']), 'userId', 'name'), ['class' => 'span6'])?>
            <?}else{?>
                <?=$model->user !== null ? $model->user->name : '' ?>
            <?}?>
        </div>
        <div class="span6">
            <? /* ===== Уровень важности ===== */ ?>
            <?=$form->dropDownListRow($model, 'importance', Clients::getDropdownImportancce(false), ['class' => 'span6']);?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span6">
            <? /* ===== Дата создания клиента ===== */ ?>
            <?=$form->labelEx($model, 'created')?>
            <?= CHtml::encode(date("d.m.Y H:i", strtotime($model->created))) ?>
        </div>
        <div class="span6">
            <? /* ===== Дата следующего контакта ===== */ ?>
            <?=$form->label($model, 'next_contact');?>
            <?=$form->dateField($model, 'next_contact', strtotime($model->next_contact) > 0 ? [] : ['value' => date("Y-m-d")]);?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span6">
            <? /* ===== Дата окончания контракта ===== */ ?>
            <?=$form->label($model, 'contract_end_date');?>
            <?=$form->dateField($model, 'contract_end_date', strtotime($model->contract_end_date) > 0 ? [] : ['value' => '']);?>
            <? /* ===== Дубль ===== */ ?><br>
            <?= $form->checkBoxRow($model, 'is_duplication'); ?>
            <br><br>
            <? /* ===== Статус клиента ===== */ ?>
            <?=$form->dropDownListRow($model, 'client_status', Clients::getDropdownClientStatus(false), ['class' => 'span6']);?>
            <? /* ===== Статус оплаты ===== */ ?>
            <?=$form->dropDownListRow($model, 'payment_status', Clients::getDropdownPaymentStatus(false), ['class' => 'span6']);?>
        </div>

        <div class="span6">
            <div class="span6">
                <? /* ===== Комментарий ===== */ ?>
                <?= $form->textAreaRow($model, 'comment', ['rows' => 5, 'class' => 'span12']); ?>
            </div>
            <div class="span6">
                <? /* ===== описание деятельности компании ===== */ ?>
                <?= $form->textAreaRow($model, 'action_description', ['rows' => 5, 'class' => 'span12']); ?>
            </div>
        </div>
    </div>

    <? /* ===== Протокол общения с клиентом ===== */ ?>
    <?php $this->renderPartial('form/update/protocol', [
            'model' => $model,
            'form' => $form,
        ]);?>
