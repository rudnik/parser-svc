<? /* ===== Регион ===== */ ?>
<?= $form->labelEx($model, 'region', ['class' => 'span12']); ?>
<?php
// скрытое поле с айдишником региона, который связан через внешний ключ
echo $form->hiddenField($model, 'region_id');
$this->widget(
    'zii.widgets.jui.CJuiAutoComplete',
    [
        'name' => 'region_title',
        'value' => $model->region ? $model->region->OFFNAME . ' ' . $model->region->SHORTNAME : '',
        'source' => 'js: function(request, response) {
				$.ajax({
					url: "/clients/clients/getRegions/",
					dataType: "json",
					beforeSend: function() { $("#loading_regions").show(); },
					complete: function() { $("#loading_regions").hide(); },
					data: { title: request.term },
					success: function(data) {
						response(data
							? $.map(data, function(item) {
								var title = item.title + " " + item.short_name;

								return { id: item.id, value: title, label: title, region_id: item.region_id };
							})
							: {}
						);
					}
				});
			}',
        'options' => [
            'showAnim' => 'fold',
            // при выборе региона обновляем скрытые поля с адишниками и обнуляем города
            'select' => 'js: function(event, ui) {
					$("#' . get_class($model) . '_region_id").val(ui.item.id);
					$("#city_region").val(ui.item.region_id);
					$("#' . get_class($model) . '_city_id").val("");
					$("#city_title").val("");
				}',
            // если в поле были изменения и потеряли фокус, обнуляем всё, ибо не соотвествует уже
            'change' => 'js: function(event, ui) {
					if (!ui.item) {
						$("#' . get_class($model) . '_region_id").val("");
						$("#city_region").val("");
						$("#' . get_class($model) . '_city_id").val("");
						$("#city_title").val("");
					}
				}'
        ],
        'htmlOptions' => ['class' => 'span12'],
    ]
);
?>
    <img id="loading_regions" style="display: none;" src="<?= Yii::app()->request->baseUrl; ?>/images/spinner.gif"/>
<?= $form->error($model, 'region'); ?>