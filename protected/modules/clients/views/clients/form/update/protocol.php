<?
/* @var $form CActiveForm */
/* @var $model Clients */
/* @var $this Controller */
/* @var $paginationRoute string */
?>
<?
Yii::app()->clientScript->registerScriptFile(
    Yii::app()->assetManager->publish(Yii::getPathOfAlias('clients.assets')) . '/js/moduleClients.js'
);
?>

<? /* ===== Протокол общения с клиентом ===== */ ?>
<div class="row-fluid">
    <div class="well well-small span12">Протокол общения с клиентом</div>
    <? $this->renderPartial('/clients/form/update/protocol_fields', ['model' => $model]); ?>
</div>