<? /* @var $model Clients */ ?>

    <div class="row-fluid">
        <? $this->renderPartial(
            '/clients/form/update/protocol/grid',
            [
                'model' => $model,
                'limit' => isset($limit) ? $limit : null
            ]
        )?>
    </div>

<? if ($model->user_id == Yii::app()->user->id || Yii::app()->user->checkAccess('admin') || Users::isMyUser(
        Yii::app()->user->id,
        $model->user_id
    )
): ?>
    <div class="row-fluid">
        <?= CHtml::openTag('div', ['style' => 'text-align:right']) ?>
        <?= CHtml::hiddenField('ClientProtocol[user_id]', Yii::app()->user->id) ?>
        <?= CHtml::hiddenField('ClientProtocol[client_id]', $model->id) ?>
        <?= CHtml::textArea('ClientProtocol[comment]', '', ['rows' => 3, 'class' => 'span6']) ?>
        <br>
        <?= CHtml::button(
            'Добавить сообщение',
            ['id' => 'js-client-protocol-message-submit-button', 'class' => 'btn btn-success']
        ) ?>
        <?= CHtml::closeTag('div') ?>
    </div>
<? endif; ?>