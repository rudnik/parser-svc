<?php
/* @var $model Clients */
/* @var $this Controller */
/* @var $dataProvider CActiveDataProvider */
/* @var $limit integer */

$appendParams = Yii::app()->urlManager->appendParams;
Yii::app()->urlManager->appendParams = false;
$dataProvider = (new ClientProtocol('search'))->getProtocolDataProvider($model->id, isset($limit) ? $limit : null);
if (!isset($limit)) {
    $dataProvider->pagination->pageSize = 5;
    $dataProvider->pagination->route = '/clients/clients/update/' . $model->id;
} else {
    $dataProvider->pagination = false;
}

$this->widget(
    'bootstrap.widgets.TbExtendedGridView',
    [
        'enableSorting' => false,
        'type' => 'stripped bordered considered',
        'id' => 'clientProtocol',
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'header' => 'Дата',
                'value' => 'date("d.m.Y H:i", strtotime($data->date))',
            ],
            [
                'header' => 'Менеджер',
                'value' => '$data->user->name',
            ],
            [
                'header' => 'Комментарий',
                'type' => 'raw',
                'htmlOptions' => ['style' => 'max-width: 200px'],
                'value' => '$data->messageIsEditable
				? "<span class=\'js-client-protocol-message-span\' item_id=\'$data->id\'>" . nl2br($data->comment) . "</span>" 
				: nl2br(htmlspecialchars($data->comment))
			    ',
            ],
            [
                'class' => 'CButtonColumn',
                'header' => 'Действия',
                'template' => '{update}',
                'buttons' => [
                    'update' => [
                        'label' => 'Редактировать комментарий',
                        'url' => '"#"',
                        'click' => 'function(){return false;}',
                        'visible' => '$data->messageIsEditable',
                    ],
                ],

            ],
        ],
        'ajaxVar' => false,
        'ajaxUpdate' => true,
    ]
);
Yii::app()->urlManager->appendParams = $appendParams;
