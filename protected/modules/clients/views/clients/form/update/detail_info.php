<?php
/* @var $form TbActiveForm */
/* @var $model Clients */
?>

<div class="well well-small">Подробная информация о компании</div>
<? /* ===== Отрасль ===== */ ?>
<?= $form->dropDownListRow(
    $model,
    'branch_id',
    CMap::mergeArray(
        ['' => '--- любая ---'],
        CHtml::listData(Branches::model()->findAll(['order' => 'title ASC']), 'branchId', 'title')
    ),
    ['class' => 'span12']
); ?>

<? /* ===== Род деятельности ===== */ ?>
<?= $form->textFieldRow($model, 'career', ['class' => 'span12']); ?>

<? /* ===== Сайт ===== */ ?>
<?= $form->textFieldRow($model, 'website', ['class' => 'span12']); ?>

<? $this->renderPartial('/clients/form/update/region_fields', ['form' => $form, 'model' => $model]); ?>

<? /* ===== Город ===== */ ?>
<?= $form->labelEx($model, 'city'); ?>
<?php
// скрытое поле с айдишником региона
echo CHtml::hiddenField('city_region', $model->region ? $model->region->AOGUID : '', ['id' => 'city_region']);
// скрытое поле с айдишником города, который связан через внешний ключ
echo $form->hiddenField($model, 'city_id');
$this->widget(
    'zii.widgets.jui.CJuiAutoComplete',
    [
        'name' => 'city_title',
        'value' => $model->city ? $model->city->OFFNAME . ' ' . $model->city->SHORTNAME : '',
        'source' => 'js: function(request, response) {
				$.ajax({
					url: "/clients/clients/getCities/",
					dataType: "json",
					beforeSend: function() { $("#loading_cities").show(); },
					complete: function() { $("#loading_cities").hide(); },
					//data: { region: $("#city_region").val(), title: request.term },
					data: { title: request.term },
					success: function(data) {
						response(data
							? $.map(data, function(item) {
								var title = item.title + " " + item.short_name;
                                var label = title + " (" + item.region_title + " " + item.region_short_name + ")";
                                var region_title = item.region_title + " " + item.region_short_name;

								return { id: item.id, value: title, label: label, region_id: item.region_id, region_title: region_title}
							})
							: {}
						);
					}
				});
			}',
        // additional javascript options for the autocomplete plugin
        'options' => [
            'showAnim' => 'fold',
            // при выборе города обновляем скрытые поля с адишниками
            'select' => 'js: function(event, ui) {
					$("#' . get_class($model) . '_city_id").val(ui.item.id);
                    $("#' . get_class($model) . '_region_id").val(ui.item.region_id);
					$("#region_title").val(ui.item.region_title);
				}',
            // если в поле были изменения и потеряли фокус, обнуляем всё, ибо не соотвествует уже
            'change' => 'js: function(event, ui) {
					if (!ui.item) {
						$("#' . get_class($model) . '_city_id").val("");
                        $("#' . get_class($model) . '_region_id").val("");
                        $("#region_title").val("");
					}
				}'
        ],
        'htmlOptions' => ['class' => 'span12'],
    ]
);
?>
<img id="loading_cities" style="display: none;" src="<?= Yii::app()->request->baseUrl; ?>/images/spinner.gif"/>
<?= $form->error($model, 'city'); ?>

<? $this->renderPartial('form/update/address_fields', ['form' => $form, 'model' => $model]); ?>

<? /* ===== ИНН ===== */ ?>
<?= $form->textFieldRow($model, 'inn', ['class' => 'span12']); ?>

<? /* ===== КПП ===== */ ?>
<?= $form->textFieldRow($model, 'kpp', ['class' => 'span12']); ?>
