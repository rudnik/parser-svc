<?php
/* @var Controller $this */

Yii::app()->clientScript->registerScriptFile(
    Yii::app()->assetManager->publish(
        Yii::getPathOfAlias('application.modules.clients.assets.js.clients_filter') . '.js'
    )
);

Yii::import('bootstrap.widgets.TbActiveForm');
$form = new TbActiveForm();
?>
<form>
    <? echo CHtml::tag(
        'input',
        [
            'id' => 'js-selected-rows-names',
            'name' => 'selected_rows',
            'type' => 'hidden',
            'value' => Yii::app()->session->get('selectedRows')
        ]
    ); ?>
    <div class="row-fluid">
        <?= CHtml::button(
            'Скрыть/показать фильтр',
            ['class' => 'btn btn-success', 'onclick' => '$("#js-filter-area").toggle();']
        ) ?>
        <br><br>
    </div>
    <div id="js-filter-area">
        <div class="row-fluid">
            <div id='js-available-filter-fields' class="span4">

                <div class="well well-small">Фильтры</div>
                <?php
                foreach (Clients::getFilterFieldNames() as $fieldName) {
                    # не выводим поле менеджер для простых пользователей
                    if ($fieldName == 'user_id') {
                        $usersList = Users::getDropdownList(Yii::app()->user);
                        if (empty($usersList)) {
                            continue;
                        }
                    }
                    ?>
                    <label class="checkbox" for="<?= $fieldName ?>">
                        <input id="<?= $fieldName ?>" type="checkbox"/>
                        <?= $model->getAttributeLabel($fieldName); ?>
                    </label>
                <?php
                }
                ?>
            </div>

            <div id='js-actual-filter' class="span8">
                <div class="well well-small">Значения</div>
                <? $this->renderPartial('form/rowPrototypes', ['form' => $form, 'model' => $model]) ?>
            </div>
        </div>
        <div class="row-fluid">
            <?= CHtml::submitButton('Найти', ['class' => 'btn btn-primary', 'name' => 'search_button']); ?>
        </div>
    </div>

</form>
