<?php
/*
 * @var $form TbActiveForm
 * @var $model Clients
 */
?>
<div class="row-fluid" name="js-row-next_contact" style="display:none">
    <? /* ===== Дата следующего контакта ===== */ ?>
    <div class="span3">
        <?= $form->labelEx($model, 'next_contact'); ?>
    </div>
    <div class="span9">
        <div class="span6">
            <span class="span2">с</span> <?= $form->dateField($model, 'next_contact_from', ['class' => 'span10']); ?>
            <?= $form->error($model, 'next_contact'); ?>
        </div>
        <div class="span6">
            <span class="span2">по</span> <?= $form->dateField($model, 'next_contact_to', ['class' => 'span10']); ?>
            <?= $form->error($model, 'next_contact'); ?>
        </div>
    </div>
</div>
<?
$usersList = Users::getDropdownList(Yii::app()->user);
if (!empty($usersList)) {
    ?>
    <div class="row-fluid" name="js-row-user_id" style="display:none">
        <? /* ===== Менеджер ===== */ ?>
        <div class="span3">
            <?= $form->labelEx($model, 'user'); ?>
        </div>
        <div class="span9">
            <?= $form->dropDownList($model, 'user_id', $usersList, ['class' => 'span12']); ?>
            <?= $form->error($model, 'user'); ?>
        </div>
    </div>
<?
}
?>
<div class="row-fluid" name="js-row-title" style="display:none">
    <? /* ===== Наименование клиента ===== */ ?>
    <div class="span3">
        <?= $form->labelEx($model, 'title'); ?>
    </div>
    <div class="span9">
        <?= $form->textField($model, 'title', ['class' => 'span12']); ?>
        <?= $form->error($model, 'title'); ?>
    </div>
</div>
<div class="row-fluid" name="js-row-contact_name" style="display:none">
    <? /* ===== Контактное лицо ===== */ ?>
    <div class="span3">
        <?= $form->labelEx($model, 'contact_name'); ?>
    </div>
    <div class="span9">
        <?= $form->textField($model, 'contact_name', ['class' => 'span12']); ?>
        <?= $form->error($model, 'contact_name'); ?>
    </div>
</div>

<div class="row-fluid" name="js-row-region_id" style="display:none">
    <? /* ===== Регион ===== */ ?>
    <div class="span3">
        <?= $form->labelEx($model, 'region'); ?>
    </div>
    <div class="span9">
        <?php
        echo $form->hiddenField($model, 'region_id');
        $this->widget(
            'zii.widgets.jui.CJuiAutoComplete',
            [
                'name' => 'region_title',
                'value' => $model->region ? $model->region->OFFNAME . ' ' . $model->region->SHORTNAME : '',
                'source' => 'js: function(request, response) {
					$.ajax({
						url: "/clients/clients/getRegions/",
						dataType: "json",
						beforeSend: function() { $("#loading_regions").show(); },
						complete: function() { $("#loading_regions").hide(); },
						data: { title: request.term },
						success: function(data) {
							response(data
								? $.map(data, function(item) {
									var title = item.title + " " + item.short_name;

									return { id: item.id, value: title, label: title, region_id: item.region_id };
								})
								: {}
							);
						}
					});
				}',
                // additional javascript options for the autocomplete plugin
                'options' => [
                    'showAnim' => 'fold',
                    'select' => 'js: function(event, ui) {
						$("#' . get_class($model) . '_region_id").val(ui.item.id);
						$("#city_region").val(ui.item.region_id);
						$("#' . get_class($model) . '_city_id").val("");
						$("#city_title").val("");
					}',
                    'change' => 'js: function(event, ui) {
						if (!ui.item) {
							$("#' . get_class($model) . '_region_id").val("");
							$("#city_region").val("");
						}

						$("#' . get_class($model) . '_city_id").val("");
						$("#city_title").val("");
					}'
                ],
                'htmlOptions' => [
                    'class' => 'span12'
                ],
            ]
        );
        ?>
        <img id="loading_regions" style="display: none;" src="<?= Yii::app()->request->baseUrl; ?>/images/spinner.gif"/>
        <?= $form->error($model, 'region_id'); ?>
    </div>
</div>

<div class="row-fluid" name="js-row-city_id" style="display:none">
    <? /* ===== Город ===== */ ?>
    <div class="span3">
        <?= $form->labelEx($model, 'city'); ?>
    </div>
    <div class="span9">
        <?php
        echo CHtml::hiddenField('city_region', $model->region ? $model->region->AOGUID : '', ['id' => 'city_region']);
        echo $form->hiddenField($model, 'city_id');
        $this->widget(
            'zii.widgets.jui.CJuiAutoComplete',
            [
                'name' => 'city_title',
                'value' => $model->city ? $model->city->OFFNAME . ' ' . $model->city->SHORTNAME : '',
                'source' => 'js: function(request, response) {
					$.ajax({
						url: "/clients/clients/getCities/",
						dataType: "json",
						beforeSend: function() { $("#loading_cities").show(); },
						complete: function() { $("#loading_cities").hide(); },
						data: { region: $("#city_region").val(), title: request.term },
						success: function(data) {
							response(data
								? $.map(data, function(item) {
									var title = item.title + " " + item.short_name,
										label = title + " (" + item.region_title + " " + item.region_short_name + ")";

									return { id: item.id, value: title, label: label }
								})
								: {}
							);
						}
					});
				}',
                // additional javascript options for the autocomplete plugin
                'options' => [
                    'showAnim' => 'fold',
                    'select' => 'js: function(event, ui) {
						$("#' . get_class($model) . '_city_id").val(ui.item.id);
					}',
                    'change' => 'js: function(event, ui) {
						if (!ui.item) {
							$("#' . get_class($model) . '_city_id").val("");
						}
					}'
                ],
                'htmlOptions' => [
                    'class' => 'span12'
                ],
            ]
        );
        ?>
        <img id="loading_cities" style="display: none;" src="<?= Yii::app()->request->baseUrl; ?>/images/spinner.gif"/>
        <?= $form->error($model, 'city_id'); ?>
    </div>
</div>

<div class="row-fluid" name="js-row-searchedPhones" style="display:none">
    <? /* ===== Телефон ===== */ ?>
    <div class="span3">
        <?= $form->labelEx($model, 'searchedPhones'); ?>
    </div>
    <div class="span9">
        <?= $form->textField($model, 'searchedPhones', ['class' => 'span12']); ?>
        <?= $form->error($model, 'searchedPhones'); ?>
    </div>
</div>

<div class="row-fluid" name="js-row-searchedEmails" style="display:none">
    <? /* ===== Email ===== */ ?>
    <div class="span3">
        <?= $form->labelEx($model, 'searchedEmails'); ?>
    </div>
    <div class="span9">
        <?= $form->textField($model, 'searchedEmails', ['class' => 'span12']); ?>
        <?= $form->error($model, 'searchedEmails'); ?>
    </div>
</div>

<div class="row-fluid" name="js-row-website" style="display:none">
    <? /* ===== Сайт ===== */ ?>
    <div class="span3">
        <?= $form->labelEx($model, 'website'); ?>
    </div>
    <div class="span9">
        <?= $form->textField($model, 'website', ['class' => 'span12']); ?>
        <?= $form->error($model, 'website'); ?>
    </div>
</div>

<div class="row-fluid" name="js-row-address" style="display:none">
    <? /* ===== Адрес ===== */ ?>
    <div class="span3">
        <?= $form->labelEx($model, 'address'); ?>
    </div>
    <div class="span9">
        <?= $form->textField($model, 'address', ['class' => 'span12']); ?>
        <?= $form->error($model, 'address'); ?>
    </div>
</div>

<div class="row-fluid" name="js-row-inn" style="display:none">
    <? /* ===== ИНН ===== */ ?>
    <div class="span3">
        <?= $form->labelEx($model, 'inn'); ?>
    </div>
    <div class="span9">
        <?= $form->textField($model, 'inn', ['class' => 'span12']); ?>
        <?= $form->error($model, 'inn'); ?>
    </div>
</div>

<div class="row-fluid" name="js-row-kpp" style="display:none">
    <? /* ===== КПП ===== */ ?>
    <div class="span3">
        <?= $form->labelEx($model, 'kpp'); ?>
    </div>
    <div class="span9">
        <?= $form->textField($model, 'kpp', ['class' => 'span12']); ?>
        <?= $form->error($model, 'kpp'); ?>
    </div>
</div>

<? /*  <div class="row-fluid" name="js-row-branch_id" style="display:none">  */ ?>
<? /* ===== Отрасль ===== */ ?>
<? /*  <div class="span3">
              <?=$form->labelEx($model, 'branch'); ?>
          </div>
          <div class="span9">
              <?=$form->dropDownList($model, 'branch_id', CMap::mergeArray(['' => '--- любая ---'], CHtml::listData(Branches::model()->findAll(), 'branchId', 'title')), ['class' => 'span12']);?>
              <?=$form->error($model, 'branch'); ?>
          </div>
      </div>  */
?>

<div class="row-fluid" name="js-row-career" style="display:none">
    <? /* ===== Род деятельности ===== */ ?>
    <div class="span3">
        <?= $form->labelEx($model, 'career'); ?>
    </div>
    <div class="span9">
        <?= $form->textField($model, 'career', ['class' => 'span12']); ?>
        <?= $form->error($model, 'career'); ?>
    </div>
</div>

<div class="row-fluid" name="js-row-protocol" style="display:none">
    <? /* ===== Протокол ===== */ ?>
    <div class="span3">
        <?= $form->labelEx($model, 'protocol'); ?>
    </div>
    <div class="span9">
        <?= $form->textField($model, 'protocol', ['class' => 'span12']); ?>
        <?= $form->error($model, 'protocol'); ?>
    </div>
</div>

<div class="row-fluid" name="js-row-protocol_last_date" style="display:none">
    <? /* ===== Дата последней записи в протоколе ===== */ ?>
    <div class="span3">
        <?= $form->labelEx($model, 'protocol_last_date'); ?>
    </div>
    <div class="span9">
        <div class="span6">
            <span class="span2">с</span> <?= $form->dateField(
                $model,
                'protocol_last_date_from',
                ['class' => 'span10']
            ); ?>
            <?= $form->error($model, 'protocol_last_date'); ?>
        </div>
        <div class="span6">
            <span class="span2">по</span> <?= $form->dateField(
                $model,
                'protocol_last_date_to',
                ['class' => 'span10']
            ); ?>
            <?= $form->error($model, 'protocol_last_date'); ?>
        </div>
    </div>
</div>

<div class="row-fluid" name="js-row-comment" style="display:none">
    <? /* ===== Комментарий ===== */ ?>
    <div class="span3">
        <?= $form->labelEx($model, 'comment'); ?>
    </div>
    <div class="span9">
        <?= $form->textField($model, 'comment', ['class' => 'span12']); ?>
        <?= $form->error($model, 'comment'); ?>
    </div>
</div>

<div class="row-fluid" name="js-row-created" style="display:none">
    <? /* ===== Дата регистрации ===== */ ?>
    <div class="span3">
        <?= $form->labelEx($model, 'created'); ?>
    </div>
    <div class="span9">
        <div class="span6">
            <span class="span2">с</span> <?= $form->dateField($model, 'created_from', ['class' => 'span10']); ?>
            <?= $form->error($model, 'created'); ?>
        </div>
        <div class="span6">
            <span class="span2">по</span> <?= $form->dateField($model, 'created_to', ['class' => 'span10']); ?>
            <?= $form->error($model, 'created'); ?>
        </div>
    </div>
</div>

<div class="row-fluid" name="js-row-contract_end_date" style="display:none">
    <? /* ===== Дата окончания контракта ===== */ ?>
    <div class="span3">
        <?= $form->labelEx($model, 'contract_end_date'); ?>
    </div>
    <div class="span9">
        <div class="span6">
            <span class="span2">с</span> <?= $form->dateField(
                $model,
                'contract_end_date_from',
                ['class' => 'span10']
            ); ?>
            <?= $form->error($model, 'contract_end_date'); ?>
        </div>
        <div class="span6">
            <span class="span2">по</span> <?= $form->dateField(
                $model,
                'contract_end_date_to',
                ['class' => 'span10']
            ); ?>
            <?= $form->error($model, 'contract_end_date'); ?>
        </div>
    </div>
</div>

<div class="row-fluid" name="js-row-importance" style="display:none">
    <? /* ===== Уровень важности ===== */ ?>
    <div class="span3">
        <?= $form->labelEx($model, 'importance'); ?>
    </div>
    <div class="span9">
        <?= $form->dropDownList($model, 'importance', Clients::getDropdownImportancce(), ['class' => 'span12']); ?>
        <?= $form->error($model, 'importance'); ?>
    </div>
</div>

<div class="row-fluid" name="js-row-is_duplication" style="display:none">
    <? /* ===== Дубль ===== */ ?>
    <div class="span3">
        <?= $form->labelEx($model, 'is_duplication'); ?>
    </div>
    <div class="span9 form-inline">
        <?=
        $form->radioButtonList(
            $model,
            'is_duplication',
            [
                0 => 'Не дубль',
                1 => 'Дубль',
                '' => 'Не важно',
            ],
            ['template' => '{input} {label}&nbsp;&nbsp;&nbsp;']
        );?>
        <?= $form->error($model, 'is_duplication'); ?>
    </div>
</div>

<div class="row-fluid" name="js-row-active_mailing" style="display:none">
    <? /* ===== Активная расслыка ===== */ ?>
    <div class="span3">
        <?= $form->labelEx($model, 'active_mailing'); ?>
    </div>
    <div class="span9 form-inline">
        <?=
        $form->radioButtonList(
            $model,
            'active_mailing',
            [
                0 => 'Нет',
                1 => 'Есть',
                2 => 'Не важно',
            ],
            ['template' => '{input} {label}&nbsp;&nbsp;&nbsp;']
        );?>
        <?= $form->error($model, 'active_mailing'); ?>
    </div>
</div>

<div class="row-fluid" name="js-row-client_status" style="display:none">
    <? /* ===== Статус клиента ===== */ ?>
    <div class="span3">
        <?= $form->labelEx($model, 'client_status'); ?>
    </div>
    <div class="span9">
        <?= $form->checkBoxList($model, 'client_status', Clients::getDropdownClientStatus()); ?>
        <?= $form->error($model, 'client_status'); ?>
    </div>
</div>

<div class="row-fluid" name="js-row-payment_status" style="display:none">
    <? /* ===== Статус оплаты ===== */ ?>
    <div class="span3">
        <?= $form->labelEx($model, 'payment_status'); ?>
    </div>
    <div class="span9">
        <?= $form->dropDownList($model, 'payment_status', Clients::getDropdownPaymentStatus(), ['class' => 'span12']); ?>
        <?= $form->error($model, 'payment_status'); ?>
    </div>
</div>

<div class="row-fluid" name="js-row-action_description" style="display:none">
    <? /* ===== Комментарий ===== */ ?>
    <div class="span3">
        <?= $form->labelEx($model, 'action_description'); ?>
    </div>
    <div class="span9">
        <?= $form->textField($model, 'action_description', ['class' => 'span12']); ?>
        <?= $form->error($model, 'action_description'); ?>
    </div>
</div>