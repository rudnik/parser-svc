<?php
/* @var $form CActiveForm */
/* @var $this Controller */
/* @var $model Clients */
?>
<div class="form">
<?php
$form = $this->beginWidget('CActiveForm', array(
        'id' => 'clients',
        'method' => 'GET',
        'action' => $this->createUrl('filter/index'),
        /*'enableAjaxValidation' => true*/));?>
<table>
<? /* ===== Наименование клиента ===== */ ?>
<tr>
    <td style="width:250px;"><?=$form->labelEx($model, 'title'); ?></td>
    <td>
        <?=$form->textField($model, 'title', ['style' => 'width: 100%']);?>
        <?=$form->error($model, 'title'); ?>
    </td>
</tr>
<? /* ===== Телефон ===== */ ?>
<tr>
    <td><?=$form->labelEx($model, 'searchedPhones'); ?></td>
    <td>
        <?=$form->textField($model, 'searchedPhones', ['style' => 'width: 100%']);?>
        <?=$form->error($model, 'searchedPhones'); ?>
    </td>
</tr>
<? /* ===== Email ===== */ ?>
<tr>
    <td><?=$form->labelEx($model, 'searchedEmails'); ?></td>
    <td>
        <?=$form->textField($model, 'searchedEmails', ['style' => 'width: 100%']);?>
        <?=$form->error($model, 'searchedEmails'); ?>
    </td>
</tr>

<? /* ===== Контактное лицо 1 ===== */ ?>
<tr>
    <td><?=$form->labelEx($model, 'contact_name1'); ?></td>
    <td>
        <?=$form->textField($model, 'contact_name1', ['style' => 'width: 100%']);?>
        <?=$form->error($model, 'contact_name1'); ?>
    </td>
</tr>

<? /* ===== Контактное лицо 2 ===== */ ?>
<tr>
    <td><?=$form->labelEx($model, 'contact_name2'); ?></td>
    <td>
        <?=$form->textField($model, 'contact_name2', ['style' => 'width: 100%']);?>
        <?=$form->error($model, 'contact_name2'); ?>
    </td>
</tr>

<? /* ===== Контактное лицо 3 ===== */ ?>
<tr>
    <td><?=$form->labelEx($model, 'contact_name3'); ?></td>
    <td>
        <?=$form->textField($model, 'contact_name3', ['style' => 'width: 100%']);?>
        <?=$form->error($model, 'contact_name3'); ?>
    </td>
</tr>

<? /* ===== Менеджер ===== */ ?>
<? if(Yii::app()->user->checkAccess('admin')) { ?>
    <tr>
        <td><?=$form->labelEx($model, 'user'); ?></td>
        <td>
            <?=$form->dropDownList($model, 'user_id', CMap::mergeArray(['' => '--- любой ---'], CHtml::listData(Users::model()->findAll(), 'userId', 'name')), ['style' => 'width: 100%']);?>
            <?=$form->error($model, 'user'); ?>
        </td>
    </tr>
<?}?>

<? /* ===== Отрасль ===== */ ?>
<tr>
    <td><?=$form->labelEx($model, 'branch'); ?></td>
    <td>
        <?=$form->dropDownList($model, 'branch_id', CMap::mergeArray(['' => '--- любая ---'], CHtml::listData(Branches::model()->findAll(), 'branchId', 'title')), ['style' => 'width: 100%']);?>
        <?=$form->error($model, 'branch'); ?>
    </td>
</tr>

<? /* ===== Род деятельности ===== */ ?>
<tr>
    <td><?=$form->labelEx($model, 'career'); ?></td>
    <td>
        <?=$form->textField($model, 'career', ['style' => 'width: 100%']);?>
        <?=$form->error($model, 'career'); ?>
    </td>
</tr>

<? /* ===== Сайт ===== */ ?>
<tr>
    <td><?=$form->labelEx($model, 'website'); ?></td>
    <td>
        <?=$form->textField($model, 'website', ['style' => 'width: 100%']);?>
        <?=$form->error($model, 'website'); ?>
    </td>
</tr>

<? /* ===== Регион ===== */ ?>
<tr>
    <td><?=$form->labelEx($model, 'region'); ?></td>
    <td>
        <?php
        echo $form->hiddenField($model, 'region_id');
        $this->widget('zii.widgets.jui.CJuiAutoComplete', [
                'name' => 'region_title',
                'value' => $model->region ? $model->region->OFFNAME.' '.$model->region->SHORTNAME : '',
                'source' => 'js: function(request, response) {
					$.ajax({
						url: "/clients/clients/getRegions/",
						dataType: "json",
						beforeSend: function() { $("#loading_regions").show(); },
						complete: function() { $("#loading_regions").hide(); },
						data: { title: request.term },
						success: function(data) {
							response(data
								? $.map(data, function(item) {
									var title = item.title + " " + item.short_name;

									return { id: item.id, value: title, label: title, region_id: item.region_id };
								})
								: {}
							);
						}
					});
				}',
                // additional javascript options for the autocomplete plugin
                'options' => [
                    'showAnim' => 'fold',
                    'select'   => 'js: function(event, ui) {
						$("#'.get_class($model).'_region_id").val(ui.item.id);
						$("#city_region").val(ui.item.region_id);
						$("#'.get_class($model).'_city_id").val("");
						$("#city_title").val("");
					}',
                    'change'   => 'js: function(event, ui) {
						if (!ui.item) {
							$("#'.get_class($model).'_region_id").val("");
							$("#city_region").val("");
						}

						$("#'.get_class($model).'_city_id").val("");
						$("#city_title").val("");
					}'
                ],
                'htmlOptions' => [
                    'style' => 'width: 100%'
                ],
            ]);
        ?>
        <img id="loading_regions" style="display: none;" src="<?=Yii::app()->request->baseUrl;?>/images/spinner.gif" />
        <?=$form->error($model, 'region_id'); ?>
    </td>
</tr>


<? /* ===== Адрес ===== */ ?>
<tr>
    <td><?=$form->labelEx($model, 'address'); ?></td>
    <td>
        <?=$form->textField($model, 'address', ['style' => 'width: 100%']);?>
        <?=$form->error($model, 'address'); ?>
    </td>
</tr>

<? /* ===== Юридический адрес ===== */ ?>
<tr>
    <td><?=$form->labelEx($model, 'legal_address'); ?></td>
    <td>
        <?=$form->textField($model, 'legal_address', ['style' => 'width: 100%']);?>
        <?=$form->error($model, 'legal_address'); ?>
    </td>
</tr>

<? /* ===== Город ===== */ ?>
<tr>
    <td><?=$form->labelEx($model, 'city'); ?></td>
    <td>
        <?php
        echo CHtml::hiddenField('city_region', $model->region ? $model->region->AOGUID : '', ['id' => 'city_region']);
        echo $form->hiddenField($model, 'city_id');
        $this->widget('zii.widgets.jui.CJuiAutoComplete', [
                'name' => 'city_title',
                'value' => $model->city ? $model->city->OFFNAME.' '.$model->city->SHORTNAME : '',
                'source' => 'js: function(request, response) {
					$.ajax({
						url: "/clients/clients/getCities/",
						dataType: "json",
						beforeSend: function() { $("#loading_cities").show(); },
						complete: function() { $("#loading_cities").hide(); },
						data: { region: $("#city_region").val(), title: request.term },
						success: function(data) {
							response(data
								? $.map(data, function(item) {
									var title = item.title + " " + item.short_name,
										label = title + " (" + item.region_title + " " + item.region_short_name + ")";

									return { id: item.id, value: title, label: label }
								})
								: {}
							);
						}
					});
				}',
                // additional javascript options for the autocomplete plugin
                'options' => [
                    'showAnim' => 'fold',
                    'select'   => 'js: function(event, ui) {
						$("#'.get_class($model).'_city_id").val(ui.item.id);
					}',
                    'change'   => 'js: function(event, ui) {
						if (!ui.item) {
							$("#'.get_class($model).'_city_id").val("");
						}
					}'
                ],
                'htmlOptions' => [
                    'style' => 'width: 100%'
                ],
            ]);
        ?>
        <img id="loading_cities" style="display: none;" src="<?=Yii::app()->request->baseUrl;?>/images/spinner.gif" />
        <?=$form->error($model, 'city_id'); ?>
    </td>
</tr>

<? /* ===== ИНН ===== */ ?>
<tr>
    <td><?=$form->labelEx($model, 'inn'); ?></td>
    <td>
        <?=$form->textField($model, 'inn', ['style' => 'width: 100%']);?>
        <?=$form->error($model, 'inn'); ?>
    </td>
</tr>

<? /* ===== КПП ===== */ ?>
<tr>
    <td><?=$form->labelEx($model, 'kpp'); ?></td>
    <td>
        <?=$form->textField($model, 'kpp', ['style' => 'width: 100%']);?>
        <?=$form->error($model, 'kpp'); ?>
    </td>
</tr>

<? /* ===== Дата окончания контракта ===== */ ?>
<tr>
    <td><?=$form->labelEx($model, 'contract_end_date'); ?></td>
    <td>
        <?=$form->dateField($model, 'contract_end_date');?>
        <?=$form->error($model, 'contract_end_date'); ?>
    </td>
</tr>

<? /* ===== Дата следующего контакта ===== */ ?>
<tr>
    <td><?=$form->labelEx($model, 'next_contact'); ?></td>
    <td>
        <?=$form->dateField($model, 'next_contact');?>
        <?=$form->error($model, 'next_contact'); ?>
    </td>
</tr>

<? /* ===== Уровень важности ===== */ ?>
<tr>
    <td><?=$form->labelEx($model, 'importance'); ?></td>
    <td>
        <?=$form->dropDownList($model, 'importance',
            [
                '' => '--- любой ---',
                'Минимальная важность' => 'Минимальная важность',
                'Высокая важность' => 'Высокая важность',
                'Край географии' => 'Край географии',
            ], ['style' => 'width: 100%']);?>
        <?=$form->error($model, 'importance'); ?>
    </td>
</tr>

<? /* ===== Дата последней записи в протоколе ===== */ ?>
<tr>
    <td><?=$form->labelEx($model, 'searchedLastProtocolRecordDate'); ?></td>
    <td>
        <?=$form->dateField($model, 'searchedLastProtocolRecordDate');?>
        <?=$form->error($model, 'searchedLastProtocolRecordDate'); ?>
    </td>
</tr>

<?/* ===== Тип поиска ===== */ ?>
<tr>
    <td>&nbsp;</td>
    <td>
        <?= $form->dropDownList($model, 'searchConditionType', ['AND' => 'Совпадают все поля', 'OR' => 'Должно совпасть одно из полей',])?>
        <?= CHtml::tag('br')?>
        <?= CHtml::submitButton('Найти', ['name' => 'button_search']);?>
    </td>
</tr>

</table>
<?php $this->endWidget(); ?>
</div>