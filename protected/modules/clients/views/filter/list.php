<?php /* @var $dataProvider CActiveDataProvider */ ?>
<?
if (empty($tableColumns)) {
    return;
}

if (isset($_GET['pageSize'])) {
    Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
    unset($_GET['pageSize']);
}

$pageSize = Yii::app()->user->getState('pageSize');
$dataProvider->pagination->pageSize = $pageSize;

?>
<div>
    <h3 style="margin-bottom: 0;">Найдено компаний: <?= $dataProvider->totalItemCount ?></h3>
    <?
    echo CHtml::openTag('div', ['style' => 'float:right']);
    echo CHtml::label('Количество элементов на странице:', 'pageSize');
    echo CHtml::dropDownList('pageSize', $pageSize, [30 => 30, 50 => 50, 100 => 100], ['class' => 'span3 pull-right']);
    echo CHtml::closeTag('div');
    echo CHtml::tag('br');
    ?>
    <?php
    $this->widget(
        'bootstrap.widgets.TbExtendedGridView',
        [
            'enableSorting' => false,
            'type' => 'bordered',
            'dataProvider' => $dataProvider,
            'rowCssClassExpression' => '
            $data->is_duplication ? "dublicate" : (
                $data->importance == "Край географии" ? "importance-far" : (
                    $data->importance == "Высокая важность" ? "importance-high" : (
                        $row % 2 ? "even" : "odd"
                    )
                )
            );
        ',
            'columns' => $tableColumns,
            'ajaxVar' => false,
            'ajaxUpdate' => false,
        ]
    );
    ?>

    <?php $this->beginWidget(
        'bootstrap.widgets.TbModal',
        [
            'id' => 'js-modal-window',
            'htmlOptions' => [
                'style' => '
                width: 900px;
                margin-left: -450px;
            ',
            ],
        ]
    ); ?>

    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <h4 id="js-modal-header">Modal header</h4>
    </div>

    <div id="js-modal-body" class="modal-body"></div>

    <div class="modal-footer">
        <?php $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'id' => 'js-modal-button-save',
                'type' => 'primary',
                'label' => 'Сохранить',
                'url' => '#',
                'htmlOptions' => array('data-dismiss' => 'modal'),
            )
        ); ?>
        <?php $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'id' => 'js-modal-button-cancel',
                'label' => 'Закрыть',
                'url' => '#',
                'htmlOptions' => array('data-dismiss' => 'modal'),
            )
        ); ?>
    </div>

    <?php $this->endWidget(); ?>
</div>