<?php

/**
 * This is the model class for table "clients_subscriptions_doctypes".
 *
 * The followings are the available columns in table 'clients_subscriptions_doctypes':
 * @property string $client_subscription_id
 * @property integer $bid_doctype_id
 */
class ClientsSubscriptionsDoctypes extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ClientsSubscriptionsDoctypes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'clients_subscriptions_doctypes';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('client_subscription_id, bid_doctype_id', 'required'),
			array('bid_doctype_id', 'numerical', 'integerOnly'=>true),
			array('client_subscription_id', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('client_subscription_id, bid_doctype_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'subscriptions' => [self::MANY_MANY,  'ClientSubscriptions', 'clients_subscriptions_doctypes(client_subscription_id, bid_doctype_id)'],
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'client_subscription_id' => 'Client Subscription',
			'bid_doctype_id' => 'Bid Doctype',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('client_subscription_id',$this->client_subscription_id,true);
		$criteria->compare('bid_doctype_id',$this->bid_doctype_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}