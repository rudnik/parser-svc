<?php

/**
 * This is the model class for table "clients".
 *
 * The followings are the available columns in table 'clients':
 *
 * @property string $id
 * @property string $title
 * @property string $email_comment
 * @property string $contact_name1
 * @property string $contact_name2
 * @property string $contact_name3
 * @property integer $branch_id
 * @property string $career
 * @property string $website
 * @property integer $region_id
 * @property string $city_id
 * @property string $address
 * @property string $legal_address
 * @property string $inn
 * @property string $kpp
 * @property integer $user_id
 * @property string $contract_end_date
 * @property integer $is_duplication
 * @property string $comment
 * @property string $importance
 * @property string $next_contact
 * @property string $created Дата создания клиента
 * @property string $sended_bids список отправленных заявок
 * @property string $client_status Статус клиента
 * @property string $action_description Описание деятельности компании
 * @property string $payment_status Статус оплаты
 *
 * The followings are the available model relations:
 * @property ClientEmails[] $clientEmails
 * @property ClientPhones[] $clientPhones
 * @property ClientProtocol[] $clientProtocols
 * @property ClientSubscribeEmails[] $clientSubscribeEmails
 * @property Addresses $region
 * @property Addresses $city
 * @property Branches $branch
 * @property Users $user
 */
class Clients extends CActiveRecord
{
    /**
     * Email'ы по которым ищем клиентов
     *
     * @var string
     */
    public $searchedEmails;

    /**
     * Название года, по которому ищем клиента
     *
     * @var string
     */
    public $searchedCity;

    /**
     * Номер телефона, по которому ищем клиента
     *
     * @var string
     */
    public $searchedPhones;

    /**
     * Дата последней записи в протоколе, по которой ищем
     *
     * @var string
     */
    public $searchedLastProtocolRecordDate;

    /**
     * Тип связки поисковых полей
     *
     * @var string 'OR' или 'AND'
     */
    public $searchConditionType = 'AND';

    /**
     * Фильтр.
     * Диапазон даты последнего контакта
     */
    public $next_contact_from;
    public $next_contact_to;

    /**
     * Фильтр.
     * Контактное лицо
     */
    public $contact_name;

    /**
     * Фильтр.
     * Протокол
     */
    public $protocol;

    /**
     * Фильтр.
     * Диапазон даты окончания контракта
     */
    public $contract_end_date_from;
    public $contract_end_date_to;

    /**
     * Фильтр.
     * Диапазон даты создания клиента
     */
    public $created_from;
    public $created_to;

    /**
     * Фильтр.
     * Дата последней записи в протоколе.
     */
    public $protocol_last_date;
    public $protocol_last_date_from;
    public $protocol_last_date_to;

    /**
     * Активные рассылки.
     * Используется в фильтре для выбора клиентов, у которых есть активные (включенные) рассылки.
     */
    public $active_mailing = 2;

    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     * @return Clients the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     *
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'clients';
    }

    /**
     *
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            // common
            [
                'title',
                'required'
            ],
            ['client_status', 'required'],
            [
                'branch_id, user_id, is_duplication',
                'numerical',
                'integerOnly' => true
            ],
            ['region_id, city_id', 'length', 'max' => 36],
            [
                'id, next_contact, contract_end_date',
                'length',
                'max' => 10
            ],
            [
                'client_status, payment_status, importance, title, email_comment, contact_name1, contact_name2, contact_name3, career, website, address, legal_address',
                'length',
                'max' => 256
            ],
            [
                'inn, kpp',
                'length',
                'max' => 12
            ],
            [
                'searchConditionType',
                'in',
                'range' => [
                    'AND',
                    'OR'
                ]
            ],
            // quick_insert
            ['searchedEmails', 'safe', 'on' => 'quick_insert'],
            // search
            [
                'active_mailing, protocol_last_date, protocol_last_date_from, protocol_last_date_to, contract_end_date_from, contract_end_date_to, created_from, created_to, protocol, contact_name, next_contact_from, next_contact_to, searchedLastProtocolRecordDate, searchedPhones, searchedCity, searchedEmails, clientEmails, id, title, email_comment, contact_name1, contact_name2, contact_name3, branch_id, career, website, region_id, city_id, address, legal_address, inn, kpp, user_id, is_duplication, comment, action_description, importance, client_status, payment_status',
                'safe',
                'on' => 'search'
            ],
            // edit
            [
                'title, clientEmails',
                'required',
                'on' => 'edit'
            ],
            [
                'clientPhones',
                'filter',
                'filter' => [
                    $this,
                    'filterPhonesToString'
                ],
                'on' => 'edit'
            ],
            [
                'clientEmails',
                'filter',
                'filter' => [
                    $this,
                    'filterEmailsToString'
                ],
                'on' => 'edit'
            ],
            [
                'clientSubscribeEmails',
                'filter',
                'filter' => [
                    $this,
                    'filterEmailsToString'
                ],
                'on' => 'edit'
            ],
            [
                'clientProtocols',
                'filter',
                'filter' => [
                    $this,
                    'filterClientProtocols'
                ],
                'on' => 'edit'
            ],
            [
                'contract_end_date, next_contact',
                'filter',
                'filter' => [
                    $this,
                    'filterUnixtimestampToDate'
                ],
                'on' => 'edit'
            ],
            // form_error Оставляем поля связанных моделей в виде "как есть"
            [
                'clientPhones, clientEmails, clientSubscribeEmails, clientProtocols',
                'safe',
                'on' => 'form_error'
            ],
            // form_add
            [
                'title, clientEmails, region',
                'required',
                'on' => 'form_add'
            ],
            [
                'clientPhones',
                'filter',
                'filter' => [
                    $this,
                    'filterStringToArray'
                ],
                'on' => 'form_add'
            ],
            [
                'clientEmails',
                'filter',
                'filter' => [
                    $this,
                    'filterStringToArray'
                ],
                'on' => 'form_add'
            ],
            [
                'clientSubscribeEmails',
                'filter',
                'filter' => [
                    $this,
                    'filterStringToArray'
                ],
                'on' => 'form_add'
            ],
            [
                'contract_end_date, next_contact',
                'filter',
                'filter' => 'strtotime',
                'on' => 'form_add'
            ],
            /*[
                'next_contact',
                'filter',
                'filter' => [
                        $this,
                        'filterCorrectJsTimestamp'
                ],
                'on' => 'form_add'
            ],*/
            ['created, sended_bids, comment, action_description', 'safe']
        ];
    }

    public function filterCorrectJsTimestamp($value)
    {
        return strtotime(date('Y-m-d', intval($value)));
    }

    public function filterPhonesToString($value)
    {
        return $this->ArrayToString($value, 'phone');
    }

    public function filterEmailsToString($value)
    {
        return $this->ArrayToString($value, 'email');
    }

    /**
     * Конвертирует дату из формата unixtimestamp в строку по шаблону "Y-m-d"
     *
     * @param integer $value Дата в формате unixtimestamp
     * @return string
     */
    public function filterUnixtimestampToDate($value)
    {
        return date("Y-m-d", $value);
    }

    /**
     * Преобразуем массив со строковыми значениями в строку, в которой значания
     * перечислены через запятую
     *
     * @param array $value - массив со строковыми значениями
     * @return string
     */
    public function filterStringToArray($value)
    {
        $data = array_unique(explode(',', trim($value . ',')));
        return array_filter($data, 'trim');
    }

    /**
     * Преобразует массив моделей с строку, в которой через запятую перечислены
     * значения указанного поля каждой модели
     *
     * @param CModel[] $value массив моделей
     * @param string $fieldName название поля
     * @return string
     * @throws InvalidArgumentException
     */
    public function arrayToString($value, $fieldName)
    {
        if (!is_array($value)) {
            throw new InvalidArgumentException('Агрумент должен быть массивом.');
        }
        $result = '';
        foreach ($value as $item) {
            $result .= ', ' . $item->$fieldName;
        }
        return trim($result, ', ');
    }

    /**
     * Преобразует массив моделей протоколов общения с клиентом к строкам вида:
     * дата ФИО_менеджера: комментарий
     *
     * @param array $value
     * @return string
     * @throws InvalidArgumentException
     */
    public function filterClientProtocols($value)
    {
        if (!is_array($value)) {
            throw new InvalidArgumentException('Агрумент должен быть массивом.');
        }
        $result = '';
        foreach ($value as $item) {
            $result .= date(
                    "d.m.Y H:i:s",
                    strtotime($item->date)
                ) . ' ' . $item->user->name . ': ' . $item->comment . "\n";
        }
        return $result;
    }

    /**
     *
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'clientEmails' => [
                self::HAS_MANY,
                'ClientEmails',
                'client_id'
            ],
            'clientPhones' => [
                self::HAS_MANY,
                'ClientPhones',
                'client_id'
            ],
            'clientProtocols' => [
                self::HAS_MANY,
                'ClientProtocol',
                'client_id'
            ],
            'clientSubscriptions' => [
                self::HAS_MANY,
                'ClientsSubscriptions',
                'clientId'
            ],
            'clientSubscribeEmails' => [
                self::HAS_MANY,
                'ClientSubscribeEmails',
                'client_id'
            ],
            'region' => [
                self::BELONGS_TO,
                'Addresses',
                'region_id',
                'condition' => 'PARENTGUID=""',
            ],
            'city' => [
                self::BELONGS_TO,
                'Addresses',
                'city_id',
                'condition' => 'PARENTGUID!=""'
            ],
            'branch' => [
                self::BELONGS_TO,
                'Branches',
                'branch_id'
            ],
            'user' => [
                self::BELONGS_TO,
                'Users',
                'user_id'
            ]
        ];
    }

    /**
     *
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'searchedLastProtocolRecordDate' => 'Дата последней записи в протоколе',
            'searchedPhones' => 'Телефон',
            'searchedEmails' => 'Email',
            'searchedCity' => 'Город',
            'clientEmails' => 'E-mail',
            'clientSubscribeEmails' => 'E-mail (рассылка)',
            'clientPhones' => 'Телефон',
            'clientProtocols' => 'Протокол общения с клиентом',
            'protocol' => 'Протокол',
            'city' => 'Город',
            'branch' => 'Отрасль',
            'importance' => 'Важность',
            'client_status' => 'Статус клиента',
            'payment_status' => 'Статус оплаты',
            'user' => 'Менеджер',
            'region' => 'Регион',
            'id' => 'id',
            'title' => 'Наименование',
            'email_comment' => 'Комментарий к e-mail',
            'contact_name' => 'Контактное лицо',
            'contact_name1' => 'Контактное лицо 1',
            'contact_name2' => 'Контактное лицо 2',
            'contact_name3' => 'Контактное лицо 3',
            'branch_id' => 'Отрасль',
            'career' => 'Род деятельности',
            'website' => 'Веб-сайт',
            'region_id' => 'Регион',
            'city_id' => 'Город',
            'address' => 'Адрес',
            'legal_address' => 'Юридический адрес',
            'inn' => 'ИНН',
            'kpp' => 'КПП',
            'user_id' => 'Менеджер',
            'contract_end_date' => 'Дата окончания контракта',
            'is_duplication' => 'Дубль',
            'comment' => 'Комментарий',
            'action_description' => 'Описание деятельности',
            'next_contact' => 'Дата следующего контакта',
            'created' => 'Дата регистрации',
            'protocol_last_date' => 'Дата последней записи в протоколе',
            'active_mailing' => 'Активные рассылки',
        ];
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }

        if (!Yii::app()->user->checkAccess('deleteClients', ['user_id' => $this->user_id])) {
            throw new CHttpException(403, 'Не хватает прав для удаления карточки клиента.');
        }

        return true;
    }

    public function beforeSave()
    {
        if (!parent::beforeSave()) {
            return false;
        }

        if (!Yii::app()->user->checkAccess('editClients', ['user_id' => $this->user_id]) &&
            !Users::isMyUser(Yii::app()->user->id, $this->user_id)
        ) {
            throw new CHttpException(403, 'Не хватает прав для редактирования карточки клиента.');
        }

        $fields = [
            'city_id',
            'region_id',
            'branch_id'
        ];

        foreach ($fields as $field) {
            $fieldValue = $this->$field;
            if (empty($fieldValue)) {
                $this->$field = null;
            }
        }

        return true;
    }

    // сохраняем связанные модели
    public function afterSave()
    {
        # quick insert
        if ($this->scenario == 'quick_insert') {
            $transaction = Yii::app()->db->beginTransaction();
            // Сохраняем email'ы
            // Удаляем существующие
            ClientEmails::model()->deleteAll(
                'client_id = :clientId',
                [
                    ':clientId' => $this->id
                ]
            );
            $this->clientEmails = explode(',', $this->searchedEmails);
            $emailValidator = new CEmailValidator();
            // Добавляем новые
            foreach ($this->clientEmails as $email) {
                $email = trim($email);
                if (!$emailValidator->validateValue($email)) {
                    continue;
                }

                $relatedModel = new ClientEmails();
                $relatedModel->client_id = $this->id;
                $relatedModel->email = $email;
                $relatedModel->save();
                if ($relatedModel->hasErrors()) {
                    foreach ($relatedModel->errors as $error) {
                        $this->addError('clientEmails', $error[0]);
                    }
                    $transaction->rollback();
                    return false;
                }
            }
            $transaction->commit();
        }

        # form_add
        if ($this->scenario !== 'form_add') {
            return true;
        }

        $transaction = Yii::app()->db->beginTransaction();
        // Сохраняем телефоны
        // Удаляем существующие
        ClientPhones::model()->deleteAll(
            'client_id = :clientId',
            [
                ':clientId' => $this->id
            ]
        );
        // Добавляем новые
        foreach ($this->clientPhones as $phone) {
            $relatedModel = new ClientPhones();
            $relatedModel->client_id = $this->id;
            $relatedModel->phone = trim($phone);
            $relatedModel->save();
            if ($relatedModel->hasErrors()) {
                foreach ($relatedModel->errors as $error) {
                    $this->addError('clientPhones', $error[0]);
                }
                $transaction->rollback();
                return false;
            }
        }

        // Сохраняем email'ы
        // Удаляем существующие
        ClientEmails::model()->deleteAll(
            'client_id = :clientId',
            [
                ':clientId' => $this->id
            ]
        );
        // Добавляем новые
        foreach ($this->clientEmails as $email) {
            $relatedModel = new ClientEmails();
            $relatedModel->client_id = $this->id;
            $relatedModel->email = trim($email);
            $relatedModel->save();
            if ($relatedModel->hasErrors()) {
                foreach ($relatedModel->errors as $error) {
                    $this->addError('clientEmails', $error[0]);
                }
                $transaction->rollback();
                return false;
            }
        }

        // Сохраняем рассылочные email'ы
        // Удаляем существующие
        ClientSubscribeEmails::model()->deleteAll(
            'client_id = :clientId',
            [
                ':clientId' => $this->id
            ]
        );
        // Добавляем новые
        foreach ($this->clientSubscribeEmails as $email) {
            $relatedModel = new ClientSubscribeEmails();
            $relatedModel->client_id = $this->id;
            $relatedModel->email = trim($email);
            $relatedModel->save();
            if ($relatedModel->hasErrors()) {
                foreach ($relatedModel->errors as $error) {
                    $this->addError('clientSibscribeEmails', $error[0]);
                }
                $transaction->rollback();
                return false;
            }
        }

        $transaction->commit();
        return parent::afterSave();
    }

    /**
     * Поиск по модели
     *
     * @param string $type тип поиска (simple | extended)
     * @param string $selectedRows Выбранные параметры. Остальные нужно пропустить при поиске.
     * @return \CActiveDataProvider
     * @throws BadMethodCallException
     */
    public function search($type = 'simple', $selectedRows = null)
    {
        $searchMethodName = "_{$type}Search";

        if (!method_exists($this, $searchMethodName)) {
            throw new BadMethodCallException("Неизвестный тип поиска: {$type}");
        }

        $criteria = $this->$searchMethodName($selectedRows);
        $dataProvider = new CActiveDataProvider($this, ['criteria' => $criteria ]);
        return $dataProvider;
    }

    /**
     * Сокращенный поиск.
     *
     * @param string $selectedRows Выбранные параметры. Остальные нужно пропустить при поиске.
     *
     * @return \CDbCriteria
     */
    protected function _simpleSearch($selectedRows = null)
    {
        // подготавливаем email'ы для поиска
        if (is_null($selectedRows) || false !== strpos($selectedRows, 'searchedEmails')) {
            $searchedEmails = explode(',', $this->searchedEmails);
        } else {
            $searchedEmails = [];
        }
        $trimmedSearchedEmails = array_filter($searchedEmails, 'trim');

        // подготавливаем название города для поиска
        //$this->searchedCity = trim($this->searchedCity);

        // создаем критерии выборки
        $criteria = new CDbCriteria();
        $criteria->group = 't.id';

        // поиск по названию
        $title = str_replace('_', '\_', $this->title);
        $title = '%' . str_replace([' ', '-'], '_', $title) . '%';
        if (is_null($selectedRows) || false !== strpos($selectedRows, 'title')) {
            $criteria->compare('t.title', $title, true, $this->searchConditionType, false);
        }

        // менеджер
        if (is_null($selectedRows) || false !== strpos($selectedRows, 'user_id')) {
            $criteria->compare('t.user_id', $this->user_id, false, $this->searchConditionType);
        }

        // поиск по email
        $searchedEmails = array_filter($searchedEmails);
        if (!empty($searchedEmails)) {
            $criteria->mergeWith(
                [
                    'with' => [
                        'clientEmails' => [
                            'select' => 'clientEmails.email',
                            'together' => true
                        ]
                    ]
                ]
            );
            foreach ($trimmedSearchedEmails as $index => $trimmedSearchedEmail) {
                $criteria->compare(
                    'clientEmails.email',
                    $trimmedSearchedEmail,
                    true,
                    $index > 0 ? 'OR' : $this->searchConditionType
                );
            }
        }

        // поиск по названию города
        if (is_null($selectedRows) || false !== strpos($selectedRows, 'city_id')) {
            if (!empty($this->city_id)) {
                $criteria->mergeWith(
                    [
                        'with' => [
                            'city' => [
                                'select'   => 'city.AOID, city.OFFNAME',
                                'together' => true
                            ]
                        ]
                    ]
                );
                $criteria->compare('city.AOID', $this->city_id, true, $this->searchConditionType);
            }
        }

        // поиск по адресу сайта
        if (is_null($selectedRows) || false !== strpos($selectedRows, 'website')) {
            $criteria->compare('t.website', $this->website, true, $this->searchConditionType);
        }

        // поиск по статусу
        if (is_null($selectedRows) || false !== strpos($selectedRows, 'client_status')) {
            $criteria->compare('t.client_status', $this->client_status, false, $this->searchConditionType);
        }

        // сортировка клиентов по убыванию в разрезе порядка добавления
        $criteria->mergeWith(
            [
                'order' => 't.id DESC'
            ]
        );

        return $criteria;
    }

    /**
     * Расширенный поиск
     *
     * @param string $selectedRows Выбранные параметры. Остальные нужно пропустить при поиске.
     *
     * @return \CDbCriteria
     */
    protected function _extendedSearch($selectedRows = null)
    {
        // создаем критерии выборки
        $criteria = new CDbCriteria();

        // Название, email, Город, Сайт
        $criteria->mergeWith($this->_simpleSearch());

        // дата следующего контакта
        $from = strtotime(date("Y-m-d 00:00:00", strtotime($this->next_contact_from)));
        $to = PHP_INT_MAX;
        if (is_null($selectedRows) || false !== strpos($selectedRows, 'next_contact')) {
            if (!empty($this->next_contact_to)) {
                $to = strtotime(date("Y-m-d 23:59:59", strtotime($this->next_contact_to)));
            }
            if (!empty($this->next_contact_from) || !empty($this->next_contact_to)) {
                $criteria->addBetweenCondition('t.next_contact', $from, $to, $this->searchConditionType);
            }
        }

        // контактное лицо
        if (is_null($selectedRows) || false !== strpos($selectedRows, 'contact_name')) {
            $criteria->compare('t.contact_name1', $this->contact_name, true, $this->searchConditionType);
            $criteria->compare('t.contact_name2', $this->contact_name, true, 'OR');
            $criteria->compare('t.contact_name3', $this->contact_name, true, 'OR');
        }

        // регион
        if (is_null($selectedRows) || false !== strpos($selectedRows, 'region_id')) {
            $criteria->compare('t.region_id', $this->region_id, false, $this->searchConditionType);
        }

        // поиск по телефону
        // подготавливаем телефоны для поиска
        if (is_null($selectedRows) || false !== strpos($selectedRows, 'searcedPhones')) {
            $searchedPhones = explode(',', $this->searchedPhones);
            $trimmedSearchedPhones = array_filter($searchedPhones, 'trim');
            $searchedPhones = array_filter($searchedPhones);
            if (!empty($searchedPhones)) {
                $criteria->mergeWith(
                    [
                        'with' => [
                            'clientPhones' => [
                                'select'   => 'clientPhones.phone',
                                'together' => true
                            ]
                        ]
                    ]
                );
                foreach ($trimmedSearchedPhones as $index => $trimmedSearchedPhone) {
                    $criteria->compare(
                        'clientPhones.phone',
                        $trimmedSearchedPhone,
                        true,
                        $index > 0 ? 'OR' : $this->searchConditionType
                    );
                }
            }
        }

        // адрес
        if (is_null($selectedRows) || false !== strpos($selectedRows, 'address')) {
            $criteria->compare('t.address', $this->address, true, $this->searchConditionType);
            $criteria->compare('t.legal_address', $this->address, true, 'OR');
        }

        // ИНН
        if (is_null($selectedRows) || false !== strpos($selectedRows, 'inn')) {
            $criteria->compare('t.inn', $this->inn, true, $this->searchConditionType);
        }

        // КПП
        if (is_null($selectedRows) || false !== strpos($selectedRows, 'kpp')) {
            $criteria->compare('t.kpp', $this->kpp, true, $this->searchConditionType);
        }

        // отрасль
        //$criteria->compare('t.branch_id', $this->branch_id, false, $this->searchConditionType);

        // род деятельности
        if (is_null($selectedRows) || false !== strpos($selectedRows, 'career')) {
            $criteria->compare('t.career', $this->career, true, $this->searchConditionType);
        }

        // Поиск по тексту протокола
        if (!empty($this->protocol)) {
            $criteria->mergeWith(
                [
                    'with' => [
                        'clientProtocols' => [
                            'select' => 'clientProtocols.comment',
                            'together' => true
                        ]
                    ]
                ]
            );
            $criteria->compare('clientProtocols.comment', $this->protocol, true, $this->searchConditionType);
        }

        // дата последней записи в протоколе
        $from = strtotime(date("Y-m-d 00:00:00", strtotime($this->protocol_last_date_from)));
        $to = PHP_INT_MAX;
        if (is_null($selectedRows) || false !== strpos($selectedRows, 'protocol_last_date')) {
            if (!empty($this->protocol_last_date_to)) {
                $to = strtotime(date("Y-m-d 23:59:59", strtotime($this->protocol_last_date_to)));
            }
            if (!empty($this->protocol_last_date_from) || !empty($this->protocol_last_date_to)) {
                $criteria->mergeWith(
                    [
                        'with' => [
                            'clientProtocols' => [
                                'select'   => 'clientProtocols.date',
                                'together' => true
                            ]
                        ]
                    ]
                );
                $criteria->addBetweenCondition(
                    'UNIX_TIMESTAMP(clientProtocols.date)',
                    $from,
                    $to,
                    $this->searchConditionType
                );
            }
        }

        // комментарий
        if (is_null($selectedRows) || false !== strpos($selectedRows, 'comment')) {
            $criteria->compare('t.comment', $this->comment, true, $this->searchConditionType);
        }

        // описание деятельности
        if (is_null($selectedRows) || false !== strpos($selectedRows, 'action_description')) {
            $criteria->compare('t.action_description', $this->action_description, true, $this->searchConditionType);
        }


        // дата регистрации
        $from = strtotime(date("Y-m-d 00:00:00", strtotime($this->created_from)));
        $to = PHP_INT_MAX;
        if (is_null($selectedRows) || false !== strpos($selectedRows, 'created')) {
            if (!empty($this->created_to)) {
                $to = strtotime(date("Y-m-d 23:59:59", strtotime($this->created_to)));
            }
            if (!empty($this->created_from) || !empty($this->created_to)) {
                $criteria->addBetweenCondition('UNIX_TIMESTAMP(t.created)', $from, $to, $this->searchConditionType);
            }
        }

        // дата окончания контракта
        $from = strtotime(date("Y-m-d 00:00:00", strtotime($this->contract_end_date_from)));
        $to = PHP_INT_MAX;
        if (is_null($selectedRows) || false !== strpos($selectedRows, 'contract_end_date')) {
            if (!empty($this->contract_end_date_to)) {
                $to = strtotime(date("Y-m-d 23:59:59", strtotime($this->contract_end_date_to)));
            }
            if (!empty($this->contract_end_date_from) || !empty($this->contract_end_date_to)) {
                $criteria->addBetweenCondition('t.contract_end_date', $from, $to, $this->searchConditionType);
            }
        }

        // важность
        if (is_null($selectedRows) || false !== strpos($selectedRows, 'importance')) {
            $criteria->compare('t.importance', $this->importance, false, $this->searchConditionType);
        }

        // статус клиента
        if (is_null($selectedRows) || false !== strpos($selectedRows, 'client_status')) {
            $criteria->compare('t.client_status', $this->client_status, false, $this->searchConditionType);
        }

        // статус оплаты
        if (is_null($selectedRows) || false !== strpos($selectedRows, 'payment_status')) {
            $criteria->compare('t.payment_status', $this->payment_status, false, $this->searchConditionType);
        }

        // дубль
        if (is_null($selectedRows) || false !== strpos($selectedRows, 'is_duplication')) {
            $criteria->compare('t.is_duplication', $this->is_duplication, false, $this->searchConditionType);
        }

        // активные рассылки
        if (is_null($selectedRows) || false !== strpos($selectedRows, 'active_mailing')) {
            if ($this->active_mailing < 2) {
                $criteria->together = true;
                $criteria->mergeWith(
                    [
                        'select' => 'SQL_CALC_FOUND_ROWS t.*, COUNT(clientSubscriptions.active) AS subscrCount',
                        'with'   => [
                            'clientSubscriptions' => [
                                'select'   => 'clientSubscriptions.active',
                                'joinType' => 'LEFT JOIN',
                                'join'     => 'AND clientSubscriptions.active = 1',
                            ],
                        ],
                    ]
                );
                $operator = $this->active_mailing == 0 ? '<' : '>=';
                $criteria->having = 'subscrCount ' . $operator . ' 1';
                $criteria->group = 't.id';
            }
        }
        $criteria->together = true;
        return $criteria;
    }

    /**
     * Пополняет список id заявок, отправленных клиенту
     * @param array $sendedBidIds - массив id заявок, отправленных клиенту
     * @param integer $clientId - id клиента
     */
    public static function addSendedBidsIds(array $sendedBidIds, $clientId)
    {
        # выбираем существующие отправленные заявки
        $existingSendedBidsIds = ClientSendedBids::getIds($clientId);

        # из переданного списка оставляем только те, которых нет в существующих
        if (!empty($existingSendedBidsIds)) {
            $sendedBidIds = array_diff($sendedBidIds, $existingSendedBidsIds);
        }

        # сохраняем сформированный список id заявок, отправленных клиенту
        $sendedDate = date("Y-m-d H:i:s");
        $sendedBidIds = array_unique($sendedBidIds);
        $transaction = Yii::app()->db->beginTransaction();
        foreach ($sendedBidIds as $sendedBidId) {
            $clientSendedBids = new ClientSendedBids();
            $clientSendedBids->attributes = [
                'clientId' => $clientId,
                'bidId' => $sendedBidId,
                'date' => $sendedDate,
            ];

            if (!$clientSendedBids->save()) {
                $transaction->rollback();
                Yii::log(
                    'Не удалось сохранить id отправленных заявок клиента №' . $clientId . '(' . var_export(
                        $sendedBidId,
                        true
                    ) . ')',
                    CLogger::LEVEL_WARNING
                );
            }

            unset($clientSendedBids);
        }
        $transaction->commit();

        return true;
    }

    /**
     * Возвращает список имен полей фильтра
     * @return array
     */
    public static function getFilterFieldNames()
    {
        return [
            'next_contact',
            'user_id',
            'title',
            'contact_name',
            'region_id',
            'city_id',
            'searchedPhones',
            'searchedEmails',
            'website',
            'address',
            'inn',
            'kpp',
            'career',
            'protocol_last_date',
            'comment',
            'created',
            'contract_end_date',
            'importance',
            'is_duplication',
            'client_status',
            'action_description',
            'payment_status',
            'active_mailing',
        ];
    }

    /**
     * Возвращает контент для выпадающего списка уровней важности клиента
     * @return array
     */
    public static function getDropdownImportancce($withEmptyCase = true)
    {
        $list = [
            'Минимальная важность' => 'Минимальная важность',
            'Высокая важность' => 'Высокая важность',
            'Край географии' => 'Край географии',
        ];

        if ($withEmptyCase) {
            $list  = CMap::mergeArray(
                ['' => '--- любой ---'],
                $list
            );
        }
        return $list;
    }

    /**
     * Возвращает контент для выпадающего списка статусов клиента
     * @return array
     */
    public static function getDropdownClientStatus($withEmptyCase = true)
    {
        $list = [
            'Лид' => 'Лид',
            'Клиент' => 'Клиент',
            'Клиент Сириус' => 'Клиент Сириус',
            'Клиент ИТК' => 'Клиент ИТК',
            'Клиент SVC' => 'Клиент SVC',
        ];

        if ($withEmptyCase) {
            $list  = CMap::mergeArray(
                //['' => '--- любой ---'],
                [],
                $list
            );
        }
        return $list;
    }

    /**
     * Возвращает контент для выпадающего списка статусов оплаты
     * @return array
     */
    public static function getDropdownPaymentStatus($withEmptyCase = true)
    {
        $list = [
            'Оплачено' => 'Оплачено',
            'Не оплачено' => 'Не оплачено',
        ];

        if ($withEmptyCase) {
            $list  = CMap::mergeArray(
                ['' => '--- любой ---'],
                $list
            );
        }
        return $list;
    }

    /**
     * Возвращает email`ы клиента, перечисленные через запятую
     * @param Clients $model
     * @return string
     */
    public static function getEmailsString(Clients $model)
    {
        $content = '';
        foreach ($model->clientEmails as $item) {
            $content .= $item->email . ", ";
        }
        return trim($content, ", ");
    }

    /**
     * Возвращает телефоны клиента, перечисленные через запятую
     * @param Clients $model
     * @return string
     */
    public static function getPhonesString(Clients $model)
    {
        $content = '';
        foreach ($model->clientPhones as $item) {
            $content .= $item->phone . ", ";
        }
        return trim($content, ", ");
    }

    /**
     * Возвращает контактные лица клиента, перечисленные через запятую
     * @param Clients $model
     * @return string
     */
    public static function getContactNameString(Clients $model)
    {
        return (empty($model->contact_name1) ? "" : $model->contact_name1) .
        (empty($model->contact_name2) ? "" : ", " . $model->contact_name2) .
        (empty($model->contact_name3) ? "" : ", " . $model->contact_name3);
    }

    /**
     * Возвращает алреса клиента, перечисленные через запятую
     * @param Clients $model
     * @return string
     */
    public static function getAddressString(Clients $model)
    {
        $content = empty($model->address) ? '' : $model->address;
        $content .= empty($model->legal_address) ? '' : ', ' . $model->legal_address;

        return $content;
    }

    /**
     * Сохраняет телефоны клиента, перечисленные через запятую
     * @param int $clientId - id клиента
     * @param string $phones - телефоны клиента, перечисленные через запятую
     * @return bool
     */
    public static function savePhonesString($clientId, $phones)
    {
        $phonesList = explode(',', $phones);
        $transaction = Yii::app()->db->beginTransaction();

        // Сохраняем телефоны
        // Удаляем существующие
        ClientPhones::model()->deleteAll(
            'client_id = :clientId',
            [
                ':clientId' => $clientId
            ]
        );

        // Добавляем новые
        foreach ($phonesList as $phone) {
            $relatedModel = new ClientPhones();
            $relatedModel->client_id = $clientId;
            $relatedModel->phone = trim($phone);
            $relatedModel->save();
            if ($relatedModel->hasErrors()) {
                $transaction->rollback();
                return false;
            }
        }

        $transaction->commit();
        return true;
    }

    /**
     * Сохраняет email`ы клиента, перечисленные через запятую
     * @param int $clientId - id клиента
     * @param string $emails - email`ы клиента, перечисленные через запятую
     * @return bool
     */
    public static function saveEmailsString($clientId, $emails)
    {
        $emailsList = explode(',', $emails);
        $transaction = Yii::app()->db->beginTransaction();

        // Сохраняем email'ы
        // Удаляем существующие
        ClientEmails::model()->deleteAll(
            'client_id = :clientId',
            [
                ':clientId' => $clientId
            ]
        );
        // Добавляем новые
        foreach ($emailsList as $email) {
            $relatedModel = new ClientEmails();
            $relatedModel->client_id = $clientId;
            $relatedModel->email = trim($email);
            $relatedModel->save();
            if ($relatedModel->hasErrors()) {
                $transaction->rollback();
                return false;
            }
        }

        $transaction->commit();
        return true;
    }

    public function isEditableForMe(CWebUser $user)
    {
        return Yii::app()->user->checkAccess('editClients', ['user_id' => $this->user_id]) ||
            (
                isset(
                    Users::$usersRelations[$user->id]
                ) &&
                in_array(
                    (int)$this->user->userId,
                    Users::$usersRelations[$user->id]
                )
            );
    }
}
