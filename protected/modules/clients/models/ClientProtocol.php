<?php

/**
 * This is the model class for table "clients_protocols".
 *
 * The followings are the available columns in table 'clients_protocols':
 * @property string $client_id
 * @property integer $user_id
 * @property string $date
 * @property string $comment
 *
 * The followings are the available model relations:
 * @property Clients $client
 * @property Users $user
 */
class ClientProtocol extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return ClientProtocol the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'clients_protocols';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_id', 'numerical', 'integerOnly' => true),
            array('client_id', 'length', 'max' => 10),
            array('date, comment', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('client_id, user_id, date, comment', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'client' => array(self::BELONGS_TO, 'Clients', 'client_id'),
            'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'client_id' => 'Client',
            'user_id' => 'User',
            'date' => 'Date',
            'comment' => 'Comment',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('client_id', $this->client_id, true);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('date', $this->date, true);
        $criteria->compare('comment', $this->comment, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Возвращает DataProvider протокола
     * @param integer $userId - id менеджера
     * @param integer $clientId - id клианта
     * @return \CActiveDataProvider
     */
    public function getProtocolDataProvider($clientId, $limit = null)
    {
        $criteria = new CDbCriteria;

        $criteria->compare('client_id', $clientId);
        $criteria->order = 'date DESC';
        if ($limit !== null) {
            $criteria->offset = 0;
            $criteria->limit = $limit;
        }

        return new CActiveDataProvider($this, ['criteria' => $criteria,]);
    }

    public function beforeSave()
    {
        if (!parent::beforeSave()) {
            return false;
        }

        # Обновление записи возможно в течение 12 часов после её добавления
        if (!$this->isNewRecord && !$this->messageIsEditable) {
            return false;
        }

        return true;
    }

    /**
     * Возможно ли обновление сообщения
     * @return boolean
     */
    public function getMessageIsEditable()
    {
        return $this->user_id == Yii::app()->user->id &&
        (time() - strtotime($this->date)) <= 12 * 3600;
    }

    /**
     * Рендерит содержимое последних добавленных сообщений
     * @param $clientId - id клиента
     * @param int $limit - количество выводимых последних записей
     * @param null $maxCommentLength - максимальное кол-во выводимых символов сообщения
     * @return string  -отрендеренная строка
     */
    public static function getLastItemsForSearchResultView($clientId, $limit = 1, $maxCommentLength = null)
    {
        $limit = (int)$limit;

        if ($limit < 1) {
            $limit = 1;
        }
        if ($maxCommentLength < 0) {
            $maxCommentLength = 0;
        }

        $lastItems = static::model()->findAll(
            [
                'condition' => 'client_id = :client_id',
                'order' => 'id DESC',
                'limit' => $limit,
                'params' => [
                    ':client_id' => $clientId,
                ],
            ]
        );

        if (count($lastItems) < 1) {
            return '';
        }

        $result = '';
        foreach ($lastItems as $lastItem) {
            $commentText = $maxCommentLength == null ? $lastItem->comment : substr(
                $lastItem->comment,
                0,
                $maxCommentLength
            );
            $result .= date("d.m.Y H:i", strtotime($lastItem->date)) . ': ' . $commentText . "\n";

        }

        return $result;
    }
}