<?php

/**
 * Модель расслок клиентов.
 * Таблица "clients_subscriptions".
 *
 * Доступные столбцы в таблице "clients_subscriptions":
 *
 * @property integer $subscrId Идентификатор рассылки.
 * @property integer $clientId Идентификатор клиента.
 * @property array $emails Список адресов рассылки.
 * @property string $title Наименование.
 * @property bool $active Активен.
 * @property timestamp $activedate_from Дата действия "с".
 * @property timestamp $activedate_to Дата действия "по".
 * @property string $keywords_inc Ключевые слова.
 * @property string $keywords_exc Слова-исключения.
 * @property bool $keywords_advance Искать в характеристиках.
 * @property integer $price_min Минимальная сумма.
 * @property integer $price_max Максимальная сумма.
 * @property timestamp $pubdate_from_start Дата размещения "с".
 * @property timestamp $pubdate_from_end Дата окончания размещения "с".
 * @property timestamp $pubdate_to_start Дата размещения "по".
 * @property timestamp $pubdate_to_end Дата окончания размещения "по".
 * @property string $bids_cache кэш заявок для рассылки.
 * @property string $inn ИНН заказчика.
 * @property string $theme тема письма рассылки.
 * @property int $send_from_default_email отправлять рассылку с дефолтного email.
 *
 * Доступные модели отношений:
 * @property Clients $client кэш заявок для рассылки.
 *
 * @package TendersParser
 * @subpackage clients
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013 BSTsoft
 */
class ClientsSubscriptions extends ActiveRecord
{
    public $doctypeIds;

    public $portalsIds;

    public $demandTypesIds;

    /**
     * Возвращает имя таблицы в базе данных.
     *
     * @return string Имя таблицы в базе данных.
     */
    public function tableName()
    {
        return 'clients_subscriptions';
    }

    /**
     * Возвращает модель класса.
     *
     * @return ClientsSubscriptions Модель класса.
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Возвращает массив правил валидации.
     *
     * @return array правила валидации
     */
    public function rules()
    {
        return [
            [
                'send_from_default_email, theme, doctypeIds, doctypes, clientId, subscrId, regions, active, activedate_from, ' .
                'activedate_to, keywords_exc, keywords_advance, price_min, price_max, ' .
                'pubdate_from_start, pubdate_from_start, pubdate_from_end, ' .
                'pubdate_to_start, pubdate_to_end, emails, demandTypes, portals, ' .
                'demandTypesIds, portalsIds, bids_cache, inn, keywords_inc',
                'safe'
            ],
            //['keywords_inc','required',],
            ['price_min, price_max', 'numerical', 'allowEmpty' => true],
            # Сценарий view
            ['emails', 'filter', 'filter' => [$this, 'filterArrayToString'], 'on' => 'view'],
            ['emails', 'required', 'on' => 'view'],
            # Сценарий insert
            ['emails, keywords_inc', 'safe', 'on' => 'insert'],
            [
                'title, emails',
                'required',
                'on' => 'insert'
            ],
            # Сценарий beforeSearch
            [
                'pubdate_from_start, pubdate_to_start',
                'filter',
                'filter' => [
                    $this,
                    'filterDateStart'
                ],
                'on' => 'beforeSearch'
            ],
            [
                'pubdate_from_end, pubdate_to_end',
                'filter',
                'filter' => [
                    $this,
                    'filterDateEnd'
                ],
                'on' => 'beforeSearch'
            ],
            [
                'price_max',
                'default',
                'setOnEmpty' => true,
                'value' => PHP_INT_MAX,
                'on' => 'beforeSearch'
            ],
            [
                'emails, title, keywords_inc',
                'safe',
                'on' => 'beforeSearch'
            ]
        ];
    }

    public function filterArrayToString($array)
    {
        if (!is_array($array))
            return '';
        $result = '';
        foreach ($array as $item) {
            $result .= ', ' . $item->email;
        }

        return (ltrim($result, ', '));
    }

    /**
     * Возвращает дату начала в формате unix timestamp
     *
     * @param string $value Дата начала (чего-либо)
     * @return int unix timestamp
     */
    public function filterDateStart($value)
    {
        return strtotime($value);
    }

    /**
     * Возвращает дату окончания в формате unix timestamp, прибавляя к ней 23 часа 59 минут 59 секунд.
     * Если дата окончания не задана - возвращает максимальное значение типа int
     *
     * @param type $value - дата окончания (чего-либо)
     * @return int - unix timestamp
     */
    public function filterDateEnd($value)
    {
        $value = $this->filterDateStart($value);

        if ($value < 1) {
            $value = PHP_INT_MAX;
        } else {
            $value += 86399;
        }

        return $value;
    }

    /**
     * Возращает связи с другими таблицами.
     *
     * @return array Правила отношений.
     */
    public function relations()
    {
        return [
            'client' => [
                self::BELONGS_TO,
                'Clients',
                'clientId'
            ],
            'emails' => [
                self::HAS_MANY,
                'ClientsSubscriptionsEmails',
                'client_subscription_id'
            ],
            'doctypes' => [
                self::MANY_MANY,
                'BidsDoctypes',
                'clients_subscriptions_doctypes(client_subscription_id, bid_doctype_id)'
            ],
            'regions' => [
                self::MANY_MANY,
                'Regions',
                'clients_subscriptions_regions(client_subscription_id, region_id)',
            ],
            'emailsCount' => [
                self::STAT, 'ClientsSubscriptionsEmails', 'client_subscription_id'
            ],
            'portals' => [
                self::HAS_MANY, 'ClientsSubscriptionsPortals', 'subscription_id'
            ],
            'demandTypes' => [
                self::HAS_MANY, 'ClientsSubscriptionsDemandTypes', 'subscription_id'
            ],
        ];
    }

    /**
     * Список атрибутов модели.
     *
     * @return array Атрибуты.
     */
    public function attributeLabels()
    {
        return [
            'subscrId' => 'Идентификатор рассылки',
            'regions' => 'Регионы',
            'clientId' => 'Идентификатор клиента',
            'emails' => 'Список адресов рассылки',
            'title' => 'Наименование',
            'active' => 'Активен',
            'activedate_from' => 'Дата действия рассылки "с"',
            'activedate_to' => 'Дата действия рассылки "по"',
            'keywords_inc' => 'Ключевые слова',
            'keywords_exc' => 'Слова-исключения',
            'keywords_advance' => 'Искать в характеристиках',
            'price_min' => 'Минимальная сумма',
            'price_max' => 'Максимальная сумма',
            'pubdate_from_start' => 'Дата публикации "с"',
            'pubdate_from_end' => 'Дата публикации "по"',
            'pubdate_to_start' => 'Дата окончания срока подачи заявок "с"',
            'pubdate_to_end' => 'Дата окончания срока подачи заявок "по"',
            'doctypes' => 'Тип спроса',
            'demandTypes' => 'Типы спроса',
            'portals' => 'Закон',
            'inn' => 'ИНН заказчика',
            'theme' => 'Тема письма',
            'send_from_default_email' => 'Отправлять рассылку с info@svc-tender.ru',
        ];
    }

    // Сохраняем связанные модели
    public function afterSave()
    {
        // пропускаем сохранение регионов и пр. при консольном вызове
        if ($this->scenario != 'insert') {
            return;
        }

        # ===== Сохраняем типы документов
        # Удаляем связи
        ClientsSubscriptionsDoctypes::model()->deleteAll('client_subscription_id = :suscrId', [
            ':suscrId' => $this->subscrId
        ]);

        # Сохраняем связанные типы документов
        if (!$this->doctypeIds) {
            $this->doctypeIds = [];
        }

        foreach ($this->doctypeIds as $doctype) {
            $subscriptionDoctype = new ClientsSubscriptionsDoctypes();
            $subscriptionDoctype->client_subscription_id = $this->subscrId;
            $subscriptionDoctype->bid_doctype_id = intval($doctype);

            if (!$subscriptionDoctype->save()) {
                exit();
            }
        }


        # ===== Сохраняем регионы
        # Удаляем связи
        ClientsSubscriptionsRegions::model()->deleteAll('client_subscription_id = :suscrId', [
            ':suscrId' => $this->subscrId
        ]);

        # Сохраняем связанные регионы
        if (!$this->regions) {
            $this->regions = [];
        }

        foreach ($this->regions as $regionId) {
            $subscriptionRegion = new ClientsSubscriptionsRegions();
            $subscriptionRegion->client_subscription_id = $this->subscrId;
            $subscriptionRegion->region_id = intval($regionId);

            if (!$subscriptionRegion->save()) {
                exit();
            }
        }


        # ===== Сохраняем email'ы
        # Удаляем старые
        ClientsSubscriptionsEmails::model()->deleteAll('client_subscription_id = :suscrId', [
            ':suscrId' => $this->subscrId
        ]);

        # Сохраняем связанные email'ы
        $emails = !is_array($this->emails) ? explode(',', $this->emails) : [];

        foreach ($emails as $email) {
            if (empty($email)) {
                continue;
            }

            $subscriptionEmails = new ClientsSubscriptionsEmails();
            $subscriptionEmails->client_subscription_id = $this->subscrId;
            $subscriptionEmails->email = trim($email);

            if (!$subscriptionEmails->save()) {
                exit();
            }
        }

        # ===== Сохраняем порталы
        # Удаляем старые
        ClientsSubscriptionsPortals::model()->deleteAll('subscription_id = :suscrId', [
            ':suscrId' => $this->subscrId
        ]);

        # Сохраняем связанные порталы
        if (!$this->portalsIds) {
            $this->portalsIds = [];
        }

        foreach ($this->portalsIds as $portal) {
            $subscriptionPortal = new ClientsSubscriptionsPortals();
            $subscriptionPortal->subscription_id = $this->subscrId;
            $subscriptionPortal->portal_id = intval($portal);

            if (!$subscriptionPortal->save()) {
                exit();
            }
        }

        # ===== Сохраняем типы спроса
        # Удаляем старые
        ClientsSubscriptionsDemandTypes::model()->deleteAll('subscription_id = :suscrId', [
            ':suscrId' => $this->subscrId
        ]);

        # Сохраняем связанные типы спроса
        if (!$this->demandTypesIds) {
            $this->demandTypesIds = [];
        }

        foreach ($this->demandTypesIds as $demandType) {
            $subscriptionDemandType = new ClientsSubscriptionsDemandTypes();
            $subscriptionDemandType->subscription_id = $this->subscrId;
            $subscriptionDemandType->demand_type_id = intval($demandType);

            if (!$subscriptionDemandType->save()) {
                exit();
            }
        }

        parent::afterSave();
        $this->refresh();
    }

    public function afterFind()
    {
        if (!empty($this->doctypes)) {
            foreach ($this->doctypes as $n => $doctype) {
                $this->doctypeIds[] = $doctype->doctypeId;
            }
        }

        if (!empty($this->portals)) {
            foreach ($this->portals as $n => $portal) {
                $this->portalsIds[] = $portal->portal_id;
            }
        }

        if (!empty($this->demandTypes)) {
            foreach ($this->demandTypes as $n => $demandType) {
                $this->demandTypesIds[] = $demandType->demand_type_id;
            }
        }

        parent::afterFind();
    }

    /**
     * Возвращает массив id регионов подписки
     * @return int[]
     */
    public function getRegionsIdAsArray()
    {
        $regionIdArray = [];
        if ($this->regions != null) {
            if (is_array($this->regions) && count($this->regions) > 0) {
                // если в поле regions сожержится массив чисел - возвращаем его
                if (is_numeric($this->regions[0])) {
                    return $this->regions;
                }
                if ($this->regions[0] instanceof Regions) {
                    foreach ($this->regions as $region) {
                        $regionIdArray[] = $region->regionId;
                    }
                }
            }
        }
        return $regionIdArray;
    }

    /**
     * Обновляет кэш отправляемых заявок
     * @param integer $limit - максимальное количество кэшируемых заявок
     * @param boolean $withoutSended - true, если нужно получить заяви без тех, которые уже были отправлены клиенту
     */
    public function refreshBidsCache($limit = 100, $withoutSended = true)
    {
        $sendedBidsIds = [];
        # если нужно получить заяви без тех, которые уже были отправлены
        if($withoutSended) {
            # выбираем список отправленных заявок по клиенту
            $sendedBidsIds = ClientSendedBids::getIds($this->clientId);
        }

        # Находим список заявок
        $bidsModels = (new BidsSearch())->search($this, $limit, $sendedBidsIds)[1];

        $bidsIds = [];
        foreach ($bidsModels as $bidModel) {
            $bidsIds[] = $bidModel->bidId;
        }

        $cachedIds = '';
        if (!empty($bidsIds)) {
            $cachedIds = implode(',', $bidsIds);
        }

        $this->bids_cache = $cachedIds;
        $this->saveAttributes(['bids_cache']);
    }

    public function getMailTheme(){
        $mailTheme = empty($this->theme) ? null : $this->theme;
        return $mailTheme;
    }
}
