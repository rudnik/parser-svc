<?php

/**
 * Модель таблицы "clients_sended_bids".
 *
 * Доступные поля таблицы "clients_sended_bids":
 * @property string $id .
 * @property string $clientId id клиента.
 * @property string $bidId id заявки.
 * @property string $date Время отправки заявки.
 *
 * Доступные отношения:
 * @property Clients $client
 * @property Bids $bid
 *
 * @package TendersParser
 * @subpackage clientsendedbids
 * @author s.utkin@bstsoft.ru
 * @copyright 2013 BSTsoft
 */
class ClientSendedBids extends CActiveRecord {
	/**
	 * @return string Таблица модели
	 */
	public function tableName() {
		return 'clients_sended_bids';
	}

	/**
	 * @return array Правила валидации.
	 */
	public function rules() {
		return [
			['clientId, bidId', 'required'],
			['clientId', 'length', 'max'=>11],
			['bidId', 'length', 'max'=>19],
            ['date', 'safe'],
			['id, clientId, bidId, date', 'safe', 'on'=>'search'],
		];
	}

	/**
	 * @return array Отношения модели.
	 */
	public function relations() {
		return [
			'client' => array(self::BELONGS_TO, 'Clients', 'clientId'),
			'bid' => array(self::BELONGS_TO, 'Bids', 'bidId'),
		];
	}

	/**
	 * @return array Атрибуты модели.
	 */
	public function attributeLabels() {
		return [
			'id' => '',
			'clientId' => 'id клиента',
			'bidId' => 'id заявки',
			'date' => 'Время отправки заявки',
		];
	}

	/**
	 * Построение условий поиска.
	 *
	 * @return CActiveDataProvider Модели с применением фильтров.
	 */
	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('clientId', $this->clientId, true);
		$criteria->compare('bidId', $this->bidId, true);
		$criteria->compare('date', $this->date, true);

		return new CActiveDataProvider($this, [
			'criteria' => $criteria,
		]);
	}

	/**
	 * Статический метод возвращения модели.
	 *
	 * @param string $className Имя класса.
	 * @return ClientSendedBids Модель.
	 */
	public static function model($className =__CLASS__) {
		return parent::model($className);
	}
    
    /**
     * Возвращает список отправленных клиенту заявок
     * @param integer $clientId
     * @return integer[]
     */
    public static function getIds($clientId) {
        $sendedBids = ClientSendedBids::model()->findAll(
                    [
                        'select' => 'bidId',
                        'condition' => 'clientId = :clientId',
                        'params' => [':clientId' => $clientId],
                        'index' => 'bidId',
                    ]
        );
        
        return array_keys($sendedBids);
    }
}
