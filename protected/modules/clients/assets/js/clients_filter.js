$(function () {
    /**
     * При установке "флажка" поля в фильтре (левая часть), соответствующее поле отображается в правой части фильтра
     * При снятии "флажка" - поле убирается
     */
    $('#js-available-filter-fields input').change(function () {
        var rowName = $(this).attr('id');
        var row_selector = 'div[name="js-row-' + rowName + '"]';

        if ($(this).is(':checked')) {
            $(row_selector).appendTo('#js-actual-filter');
            $(row_selector).show();
            var selectedRows = $('#js-selected-rows-names').val();
            selectedRows += ',' + rowName;
            $('#js-selected-rows-names').val(selectedRows);
        } else {
            var selectedRows = $('#js-selected-rows-names').val();
            selectedRows = selectedRows.replace(',' + rowName, '');
            $('#js-selected-rows-names').val(selectedRows);
            $(row_selector).hide();
        }
    });

    /**
     * при отправке формы фильтра сначала удаляем из сессии id "обработанных" клиентов,
     * а потом отправляем данные фильтра
     */
    $('input[name="search_button"]').on('click', function () {
        var button = $(this);
        $.post('/clients/filter/clearExcludedRecordsList', {}, function (data) {
            button.parents('form').submit();
        });
        return false;
    });

    showSelectedRows();
});

/**
 * Отображает в правой части фильтра видимые поля
 */
function showSelectedRows() {
    var itemList = $('#js-selected-rows-names').val().split(',');
    $('#js-selected-rows-names').val('');
    $.each(itemList, function (index, data) {
        if (data.length < 1) return;
        $('input#' + data).click();
    });
}

/**
 * Показывает модальное окно для редактирования составных полей таблицы в фильтре
 * @param windowTitle заголовок окна
 * @param contentUrl  адрес экшена, который возвращает форму и который сохраняет данные с этой формы
 * @param urlParams  параметры экшена
 * @param context  контекст ссылки, по которой клиннули
 * @param saveFunction  замыкание, выполняемое при получении ответа после сохранения формы
 * @param cancelFunction  замыкание, выполняемое при нажании на кнопку "Закрыть"
 */
function showEditDialog(windowTitle, contentUrl, urlParams, context, saveFunction, cancelFunction) {
    if (typeof saveFunction !== "undefined") {
        var saveButton = $('#js-modal-button-save');
        saveButton.off();
        saveButton.click(function () {
            var modalForm = $('#js-modal-form');
            $.post(modalForm.attr('action'), modalForm.serialize(), function (data) {
                saveFunction(context, data);
            });
        });
    }

    if (typeof cancelFunction !== "undefined") {
        var cancelButton = $('#js-modal-button-cancel');
        cancelButton.off();
        cancelButton.click(cancelFunction);
    }

    $('#js-modal-header').text(windowTitle);
    $('#js-modal-body').html('<div class="grid-view-loading"><div class="offset1">загрузка формы...</div></div>');
    $.get(contentUrl, urlParams, function (data) {
        $('#js-modal-body').html(data);
    });
}