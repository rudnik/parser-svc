var moduleClients = {};

$(function () {
    $('body').on('click', '.js-delete-link', function () {
        moduleClients.deleteRequest(this);
        return false;
    });
    $('body').on('click', '#js-client-protocol-message-submit-button', function () {
        moduleClients.addProtocolMessage(this);
        return false;
    });
    $('body').on('click', 'td.button-column>a.update', function () {
        moduleClients.showEditCommentUI(this);
        return false;
    });
    $('body').on('click', 'td.button-column>input.js-client-protocol-message-save', function () {
        moduleClients.saveProtocolMessage(this);
        return false;
    });
    $('body').on('change', 'select#pageSize', function () {
        if ($.fn.yiiGridView !== undefined) {
            $.fn.yiiGridView.update('yw0', { data: { pageSize: $(this).val() }});
        } else {
            refreshSearchResult($('#look-search'));
        }
    });

    $('input[name=button_add]').click(function (data) {
        $('input[name=button_search]').remove();
        $(this).parents('form').append('<input name="button_add" type="hidden" value="1"/>');
        $(this).parents('form').submit();
    });

    // Если на форме клиентов кликаем на кнопку "найти", то данные летят GET-запросом
    $('input[name=button_search]').click(function (data) {
        $('form#clients').attr('method', 'GET');
    });

});

/**
 * Удаление клиента.
 * @param {object} initiator - объект ссылки, по которой кликнул пользователь.
 * Ссылка должна содержать url экшена удаления в атрибуте href и csrf токен в
 * атрибуте token
 */
moduleClients.deleteRequest = function (initiator) {
    if (!confirm('Вы уверены, что хотите удалить элемент?')) return false;

    var url = $(initiator).attr('href');
    var token = $(initiator).attr('token');
    $.post(url, {token: token}, function () {
        window.location.reload();
    });

    return false;
};

/**
 * Дообавление сообщения в протокол общения с клиентом
 * @param {object} initiator - объект кнопки "отправить"
 */
moduleClients.addProtocolMessage = function (initiator) {
    var manager_id = $('#ClientProtocol_manager_id').attr('value');
    var client_id = $('#ClientProtocol_client_id').attr('value');
    var formData = $(initiator).parents('form').serialize();

    if ($('#ClientProtocol_comment').val().length > 0) {
        $('#ClientProtocol_comment').attr('value', '');
        $.post('/clients/protocols/addMessage', formData, function (data) {
            if (data.length > 0) alert(data);
            moduleClients.updateGrid(manager_id, client_id);
        });
    }
};

/**
 * Обновляет таблицу с протоколом общения с клиентом
 * @param {integer} manager_id
 * @param {integer} client_id
 */
moduleClients.updateGrid = function (manager_id, client_id) {
    $.get('/clients/protocols/grid', {manager_id: manager_id, client_id: client_id}, function (data) {
        $('#clientProtocol').replaceWith(data);
    });
};

/**
 * Отображение textarea для редактирования комментария менеджера
 * @param {object} initiator
 * @returns {Boolean}
 */
moduleClients.showEditCommentUI = function (initiator) {
    var span = $(initiator).parents('tr:first').find('td>span.js-client-protocol-message-span');
    var message = $(span).text();
    var item_id = $(span).attr('item_id');
    $(span).replaceWith("<textarea class='js-client-protocol-message' item_id='" + item_id + "' rows='3' cols='40'>" + message + "</textarea>");
    $(initiator).after('<input item_id="' + item_id + '" class="js-client-protocol-message-save" type="button" value="сохранить"/>');
    $(initiator).hide();
    return false;
};

/**
 * Сохранение сообщения протокола общения с клиентом
 * @param {object} initiator
 */
moduleClients.saveProtocolMessage = function (initiator) {
    var textarea = $(initiator).parents('tr:first').find('td>textarea.js-client-protocol-message');
    var message = $(textarea).val();
    var item_id = $(textarea).attr('item_id');

    $.post('/clients/protocols/updateMessage', {id: item_id, value: message}, function (data) {
        $(textarea).replaceWith('<span class="js-client-protocol-message-span" item_id="' + item_id + '">' + data + '</span>');
        $(initiator).prev('a').show();
        $(initiator).remove();
    });

};
