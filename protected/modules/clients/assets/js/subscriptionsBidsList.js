var subscriptionsBidsList = {};
var selectedBids = [];

$(function(){
    $('body').on('change', '.selectable-bids', function(){subscriptionsBidsList.onChangeBidSelectedStatus(this);});
    $('body').on('click', 'input[name=send_email]', function(){subscriptionsBidsList.beforeFormSend();});
    $('body').on('change', 'input#selectBidsOnPage', function(){subscriptionsBidsList.selectBidsOnPage(this);});
});

/**
 * Добавляет заявку в список выбранных
 * @param {object} initiator
 */
subscriptionsBidsList.onChangeBidSelectedStatus = function(initiator, updateTopCheckbox) {
    var checked = $(initiator).is(':checked');
    var value = $(initiator).attr('value');
    if(checked) {
	var elementIndex = selectedBids.indexOf(value);
	if(elementIndex < 0) {
	    selectedBids.push(value);
	}
    } else {
	var elementIndex = selectedBids.indexOf(value);
	if(elementIndex >= 0) {
	    delete selectedBids[elementIndex];
	}
    }
    
    if(updateTopCheckbox !== false) {
	subscriptionsBidsList.updateTopCheckBox();
    }
};

/**
 * Помечает выбранные заявки на очередной странице в списке результатов поиска
 */
subscriptionsBidsList.updateBidsSelectedStatus = function() {
    for(i = 0; i < selectedBids.length; i++) {
	var element = $('#checkedBids_' + selectedBids[i]);
	if(undefined === element) {
	    continue;
	}
	element.attr('checked', true);
    }
    subscriptionsBidsList.updateTopCheckBox();
};

/**
 * Если на странице выбраны все элементы, тогалочку в заголовке таблицы тоже отмечаем
 */
subscriptionsBidsList.updateTopCheckBox = function() {
    var checked_all = true;
    var bidsInputs = $('input[id*=checkedBids_]');
    
    if(bidsInputs === undefined) return;
    
    bidsInputs.each(function(index, value){
	if(!$(this).is(':checked')) {
	    checked_all = false;
	}
    });
    
    $('#selectBidsOnPage').attr('checked', checked_all);
}

/**
 * Перед отправкой формы преврящаем накопленные идентификаторы заявок в инпуты
 * @returns {undefined}
 */
subscriptionsBidsList.beforeFormSend = function() {
    for(i = 0; i < selectedBids.length; i++) {
	$('#checkedBidsList').append('<input type="hidden" name="checkedBidsList[' + selectedBids[i] + ']" value="' + selectedBids[i] + '" />');
    }
};

/**
 * Отмечает/снимает отметку со всех заявок на странице
 */
subscriptionsBidsList.selectBidsOnPage = function(initiator) {
    var checked = $(initiator).is(':checked');
    $('input[id*=checkedBids_]').each(function(index, value){
	$(this).attr('checked', checked);
	subscriptionsBidsList.onChangeBidSelectedStatus(this, false);
    });	    
};