<?php

/**
 * Контроллер клиентов
 */
class ClientsController extends Controller
{

    /**
     * @return array action filters
     */
    public function filters()
    {
        return ['accessControl'];
    }

    public function accessRules()
    {
        return [
            ['allow',
                'roles' => ['manager'],
            ],
            ['deny', // deny all users
                'users' => ['*'],
            ],
        ];
    }

    /**
     * Карточка клиента
     */
    public function actionIndex()
    {
        $this->title = 'Клиенты';
        $clientSearchModel = new Clients('search');
        $clientSearchModel->unsetAttributes();
        if (!Yii::app()->user->checkAccess('admin')) {
            $clientSearchModel->user_id = Yii::app()->user->id;
        }

        // если нажали кнопку 'Найти'
        if (Yii::app()->request->getParam('button_search')) {
            $requestData = Yii::app()->request->getParam('Clients', []);
            $clientSearchModel->attributes = $requestData;
        } elseif (Yii::app()->request->getPost('button_add')) {
            // если нажали кнопку 'Добавить'
            $requestData = Yii::app()->request->getPost('Clients', []);
            $clientModel = new Clients('quick_insert');
            $clientModel->attributes = $requestData;
            $clientModel->user_id = Yii::app()->user->id;
            $clientModel->save();
        }

        $this->render('index', ['model' => $clientSearchModel]);
    }

    /**
     * Обновление модели клиентов
     * @return type
     */
    public function actionUpdate()
    {
        $this->title = 'Карточка клиента';
        $id = Yii::app()->request->getParam('id');
        $clientModel = $id !== null ? Clients::model()->findByPk($id) : new Clients();

        if (is_null($clientModel)) {
            throw new CHttpException(404, 'Модель не найдена');
        }

        if (Yii::app()->request->isPostRequest) {
            $postData = Yii::app()->request->getPost('Clients');
            if ($postData === null)
                throw new RuntimeException('Данные модели Clients отсутствуют в запросе.');

            $clientModelErrors = clone ($clientModel);
            $clientModelErrors->setScenario('form_error');
            $clientModelErrors->attributes = $postData;

            $clientModel->setScenario('form_add');
            $clientModel->attributes = $postData;

            if($clientModel->isNewRecord) {
                $clientModel->next_contact = time();
            }

            if (isset($postData['user_id']) && !Yii::app()->user->checkAccess('changeClientsManager') &&  !Users::isMyUser(Yii::app()->user->id, $postData['user_id'])) {
                throw new CHttpException(403, 'Не хватает прав на изменение менеджера.');
            }

            $clientModel->save();

            if ($clientModel->hasErrors()) {
                $clientModelErrors->addErrors($clientModel->errors);
                $this->render('update', ['model' => $clientModelErrors]);
                return;
            }

            Yii::app()->request->redirect('/clients/clients/update/' . $id);
        }

        if (!Yii::app()->user->checkAccess('editClients', ['user_id' => $clientModel->user_id]) &&
                !Users::isMyUser(Yii::app()->user->id, $clientModel->user_id)) {
            throw new CHttpException(403, 'Не хватает прав для редактирования карточки клиента.');
        }

        $this->render('update', ['model' => $clientModel]);
    }

    public function actionDelete()
    {
        $id = Yii::app()->request->getParam('id');

        if (is_null($id)) {
            throw new CHttpException('Не задан id', 500);
        }

        $model = Clients::model()->findByPk($id);

        if (is_null($model)) {
            throw new CHttpException(404, 'Модель не найдена.');
        }

        $model->delete();
        exit;
    }

    /**
     * Список регионов для поля с автозаполнением.
     *
     * @return json
     */
    public function actionGetRegions()
    {
        $regions = [];

        if (!empty($_GET['title'])) {
            $models = Addresses::findAllRegionsByTitle($_GET['title']);

            foreach ($models as $region) {
                $regions[] = [
                    'id' => $region->AOID,
                    'region_id' => $region->AOGUID,
                    'short_name' => $region->SHORTNAME,
                    'title' => $region->OFFNAME
                ];
            }
        }

        echo CJSON::encode($regions);

        Yii::app()->end();
    }

    /**
     * Список населенных пунктов для поля с автозаполнением.
     *
     * @return json
     */
    public function actionGetCities()
    {
        $cities = [];
        $models = Addresses::findAllCitiesByTitle(
                        !empty($_GET['region']) ? $_GET['region'] : '', !empty($_GET['title']) ? $_GET['title'] : ''
        );

        foreach ($models as $city) {
            $cities[] = [
                'id' => $city->AOID,
                'short_name' => $city->SHORTNAME,
                'title' => $city->OFFNAME,
                'region_id' => $city->region !== null ? $city->region->AOID : null,
                'region_short_name' => $city->region !== null ? $city->region->SHORTNAME : '',
                'region_title' => $city->region !== null ? $city->region->OFFNAME : '',
            ];
        }

        echo CJSON::encode($cities);

        Yii::app()->end();
    }

}
