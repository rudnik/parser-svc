<?php

/**
 * Контроллер фильтра клиентов
 */
class FilterController extends Controller
{

    /**
     * @return array action filters
     */
    public function filters()
    {
        return ['accessControl'];
    }

    public function accessRules()
    {
        return [
            [
                'allow',
                'roles' => ['manager'],
            ],
            [
                'deny', // deny all users
                'users' => ['*'],
            ],
        ];
    }

    /**
     * Отображение фильтра и результатов фильтрации.
     */
    public function actionIndex()
    {
        $this->title = 'Фильтр';
        $requestData = Yii::app()->request->getParam('Clients', []);
        $model = new Clients('search');
        // Убираем статус клиента "Лид" по умолчанию
        $model->client_status = null;
        $model->attributes = $requestData;
        # Менеджеры видят своих клиентов, "руководители отделов" - и своих клиентов и клиентов своих подчиненных
        if (!Yii::app()->user->checkAccess('admin')) {
            if (isset(Users::$usersRelations[Yii::app()->user->id])) {
                $model->dbCriteria->mergeWith(
                    [
                        'condition' => 'user_id IN (' . implode(',', Users::$usersRelations[Yii::app()->user->id]) . ')',
                    ]
                );
            } else {
                $model->user_id = Yii::app()->user->id;
            }
        }

        if (!isset($requestData['importance'])) {
            $model->importance = '';
        }

        # Формируем список столбцов таблицы
        $tableColumns = [];
        $selectedRows = Yii::app()->request->getParam('selected_rows', Yii::app()->session->get('selectedRows'));
        if (!empty($selectedRows)) {
            Yii::app()->session->add('selectedRows', $selectedRows);
            $tableColumns = $this->getTableColumns(explode(',', $selectedRows));
        }

        # Исключаем "обработанных" клиентов
        $dataProvider = $model->search('extended', $selectedRows);
        $dataProvider->getCriteria()->mergeWith(WebUser::getExcludedRecordsListCriteria());

        /* да, это не айс, но иначе totalItemCount, при фильтрации по активным рассылкам, содержит количество
        клиентов без учета фильррации по активным рассылкам */
        $dataProvider->setTotalItemCount(count($model->findAll($dataProvider->criteria)));

        $this->render(
            'index',
            [
                'dataProvider' => $dataProvider,
                'tableColumns' => $tableColumns,
            ]
        );
    }

    /**
     * Сохранение поля при inline-редактировании
     * @throws CHttpException
     */
    public function actionSaveField()
    {
        $name = Yii::app()->request->getParam('name');
        $value = Yii::app()->request->getParam('value');
        $pk = (int)Yii::app()->request->getParam('pk');

        $model = Clients::model()->findByPk($pk);
        if ($model == null) {
            throw new CHttpException(404, "Элемент #$pk не найден");
        }

        # конвертирует дату в unix timestamp.
        if (in_array($name, ['next_contact', 'contract_end_date'])) {
            $value = strtotime($value);
        }

        if (!$model->saveAttributes([$name => $value])) {
            throw new CHttpException(500, "Не удалось сохранить элемент #$pk");
        }
    }

    /**
     * Возвращает модель клиента по id
     * @param $id - идентификатор клиента
     * @return Clients
     * @throws CHttpException
     */
    protected function getModel($id)
    {
        $model = Clients::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, "Модель #$id не найдена.");
        }

        return $model;
    }

    /**
     * Наполняет и сохраняет модель, полученную из запроса
     * @param $model Clients - модель клиента
     * @return bool
     * @throws CHttpException
     */
    protected function saveModelFromRequest(Clients $model)
    {
        $model->attributes = Yii::app()->request->getPost('Clients', []);
        if (!$model->save()) {
            throw new CHttpException(500, "Не удалось сохранить модель #{$model->id}}");
        }

        return true;
    }

    /**
     * При GET-запросе рендерит форму редактирования контактных лиц
     * При POST-запросе - сохраняет контактные лица и рендерит обновленное содержимое ячейки
     * @param $id - идентивикатор клиента
     */
    public function actionGetContactNameForm($id)
    {
        $model = $this->getModel($id);

        if (Yii::app()->request->isPostRequest) {
            $this->saveModelFromRequest($model);
            $content = Clients::getContactNameString($model);
            echo empty($content) ? 'нет данных' : $content;
            Yii::app()->end();
        }

        Yii::import('bootstrap.widgets.TbActiveForm');
        $form = $this->beginWidget(
            'TbActiveForm',
            ['id' => 'js-modal-form', 'htmlOptions' => ['style' => 'max-width: 600px']]
        );
        $this->renderPartial('/clients/form/update/contact_name_fields', ['form' => $form, 'model' => $model]);
        $this->endWidget();
    }

    /**
     * При GET-запросе рендерит форму редактирования адресов
     * При POST-запросе - сохраняет адреса и рендерит обновленное содержимое ячейки
     * @param $id - идентивикатор клиента
     */
    public function actionGetAddressForm($id)
    {
        $model = $this->getModel($id);

        if (Yii::app()->request->isPostRequest) {
            $this->saveModelFromRequest($model);
            $content = Clients::getAddressString($model);
            echo empty($content) ? 'нет данных' : $content;
            Yii::app()->end();
        }

        Yii::import('bootstrap.widgets.TbActiveForm');
        $form = $this->beginWidget(
            'TbActiveForm',
            ['id' => 'js-modal-form', 'htmlOptions' => ['style' => 'max-width: 600px']]
        );
        $this->renderPartial('/clients/form/update/address_fields', ['form' => $form, 'model' => $model]);
        $this->endWidget();
    }

    /**
     * При GET-запросе рендерит форму редактирования email
     * При POST-запросе - сохраняет email и рендерит обновленное содержимое ячейки
     * @param $id - идентивикатор клиента
     * @throws CHttpException
     */
    public function actionGetEmailForm($id)
    {
        $model = $this->getModel($id);

        if (Yii::app()->request->isPostRequest) {
            $model->setScenario('search');
            $model->attributes = Yii::app()->request->getPost('Clients', []);

            if (!Clients::saveEmailsString($model->id, $model->searchedEmails)) {
                throw new CHttpException(500, "Не удалось сохранить модель #$id");
            }

            $content = Clients::getEmailsString($model);
            echo empty($content) ? 'нет данных' : $content;
            Yii::app()->end();
        }

        Yii::import('bootstrap.widgets.TbActiveForm');
        $form = $this->beginWidget(
            'TbActiveForm',
            ['id' => 'js-modal-form', 'htmlOptions' => ['style' => 'max-width: 600px']]
        );
        echo $form->textField(
            $model,
            'searchedEmails',
            ['value' => Clients::getEmailsString($model), 'class' => 'span12']
        );
        $this->endWidget();
    }

    /**
     * При GET-запросе рендерит форму редактирования телефонов
     * При POST-запросе - сохраняет телефоны и рендерит обновленное содержимое ячейки
     * @param $id - идентивикатор клиента
     * @throws CHttpException
     */
    public function actionGetPhoneForm($id)
    {
        $model = $this->getModel($id);

        if (Yii::app()->request->isPostRequest) {
            $model->setScenario('search');
            $model->attributes = Yii::app()->request->getPost('Clients', []);

            if (!Clients::savePhonesString($model->id, $model->searchedPhones)) {
                throw new CHttpException(500, "Не удалось сохранить модель #$id");
            }

            $content = Clients::getPhonesString($model);
            echo empty($content) ? 'нет данных' : $content;
            Yii::app()->end();
        }

        Yii::import('bootstrap.widgets.TbActiveForm');
        /* @var $form TbActiveForm */
        $form = $this->beginWidget(
            'TbActiveForm',
            ['id' => 'js-modal-form', 'htmlOptions' => ['style' => 'max-width: 600px']]
        );
        echo $form->textField(
            $model,
            'searchedPhones',
            ['value' => Clients::getPhonesString($model), 'class' => 'span12']
        );
        $this->endWidget();
    }


    /**
     * При GET-запросе рендерит форму протокола общения с клиентом
     * При POST-запросе - рендерит обновленное содержимое ячейки
     * @param $id - идентивикатор клиента
     * @throws CHttpException
     */
    public function actionGetProtocolForm($id)
    {
        $model = Clients::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, "Модель #$id не найдена.");
        }

        if (Yii::app()->request->isPostRequest) {
            $content = ClientProtocol::getLastItemsForSearchResultView($model->id);
            echo empty($content) ? 'нет данных' : $content;
            Yii::app()->end();
        }

        Yii::import('bootstrap.widgets.TbActiveForm');
        $this->beginWidget(
            'TbActiveForm',
            ['id' => 'js-modal-form', 'htmlOptions' => ['style' => 'max-width: 1100px']]
        );
        $this->renderPartial(
            '/clients/form/update/protocol_inline_form_fields',
            [
                'model' => $model,
                'limit' => 999
            ]
        );
        $this->endWidget();
    }

    /**
     * Добавляет клиента в список "обработанных" для текущего пользователя
     * @throws CHttpException
     */
    public function actionExcludeRecord()
    {
        $id = (int)Yii::app()->request->getPost('id');

        if (!WebUser::addExcludedRecordId($id)) {
            throw new CHttpException(404, "Модель #$id не найдена");
        }
    }

    /**
     * Очищает список "обработанных" клиентов текущего пользователя
     * @throws CHttpException
     */
    public function actionClearExcludedRecordsList()
    {
        WebUser::сlearExcludedRecordsList();
    }


    /**
     * Фромирование списка столбцов таблицы результатов фильтрации
     * @param array $columnsList - список имен столбцов
     * @return array
     */
    protected function getTableColumns(array $columnsList)
    {
        $columns = [
            'searchedEmails' => [
                'class' => 'LinkColumn',
                'header' => 'E-mail',
                'labelExpression' => 'strlen($content = Clients::getEmailsString($data)) > 0 ? $content : "нет данных";',
                'urlExpression' => '$data->id',
                'linkHtmlOptions' => [
                    'class' => 'editable-click',
                    'token' => Yii::app()->request->csrfToken,
                    'data-toggle' => 'modal',
                    'data-target' => '#js-modal-window',
                    'onclick' => 'showEditDialog(
                        "E-mail",
                        "/clients/filter/getEmailForm",
                        {id:$(this).attr("href")},
                        this,
                        function (context, data){
                            $(context).text(data);
                        }
                    );'
                ],
            ],
            'searchedPhones' => [
                'class' => 'LinkColumn',
                'header' => 'Телефон',
                'labelExpression' => 'strlen($content = Clients::getPhonesString($data)) > 0 ? $content : "нет данных";',
                'urlExpression' => '$data->id',
                'linkHtmlOptions' => [
                    'class' => 'editable-click',
                    'token' => Yii::app()->request->csrfToken,
                    'data-toggle' => 'modal',
                    'data-target' => '#js-modal-window',
                    'onclick' => 'showEditDialog(
                        "Телефоны клиента",
                        "/clients/filter/getPhoneForm",
                        {id:$(this).attr("href")},
                        this,
                        function (context, data){
                            $(context).text(data);
                        }
                    );'
                ],
            ],
            'contact_name' => [
                'class' => 'LinkColumn',
                'header' => 'Контактное лицо',
                'labelExpression' => 'strlen($content = Clients::getContactNameString($data)) > 0 ? $content : "нет данных";',
                'urlExpression' => '$data->id',
                'linkHtmlOptions' => [
                    'class' => 'editable-click',
                    'token' => Yii::app()->request->csrfToken,
                    'data-toggle' => 'modal',
                    'data-target' => '#js-modal-window',
                    'onclick' => 'showEditDialog(
                        "Контактное лицо",
                        "/clients/filter/getContactNameForm",
                        {id:$(this).attr("href")},
                        this,
                        function (context, data){
                            $(context).text(data);
                        }
                    );'
                ],
            ],
            'address' => [
                'class' => 'LinkColumn',
                'header' => 'Адрес',
                'labelExpression' => 'strlen($content = Clients::getAddressString($data)) > 0 ? $content : "нет данных";',
                'urlExpression' => '$data->id',
                'linkHtmlOptions' => [
                    'class' => 'editable-click',
                    'token' => Yii::app()->request->csrfToken,
                    'data-toggle' => 'modal',
                    'data-target' => '#js-modal-window',
                    'onclick' => 'showEditDialog(
                        "Адрес",
                        "/clients/filter/getAddressForm",
                        {id:$(this).attr("href")},
                        this,
                        function (context, data){
                            $(context).text(data);
                        }
                    );'
                ],
            ],
            'inn' => [
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'editable' => [
                    'type' => 'text',
                    'model' => 'Clients',
                    'attribute' => 'inn',
                    'url' => '/clients/filter/saveField',
                    'emptytext' => 'нет данных',
                ],
                'name' => 'inn',
            ],
            'kpp' => [
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'editable' => [
                    'type' => 'text',
                    'model' => 'Clients',
                    'attribute' => 'kpp',
                    'url' => '/clients/filter/saveField',
                    'emptytext' => 'нет данных',
                ],
                'name' => 'kpp',
            ],
            'career' => [
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'editable' => [
                    'type' => 'text',
                    'model' => 'Clients',
                    'attribute' => 'career',
                    'url' => '/clients/filter/saveField',
                    'emptytext' => 'нет данных',
                ],
                'name' => 'career',
            ],
            'comment' => [
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'editable' => [
                    'type' => 'textarea',
                    'model' => 'Clients',
                    'attribute' => 'comment',
                    'url' => '/clients/filter/saveField',
                    'emptytext' => 'нет данных',
                ],
                'name' => 'comment',
            ],
            'action_description' => [
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'editable' => [
                    'type' => 'textarea',
                    'model' => 'Clients',
                    'attribute' => 'action_description',
                    'url' => '/clients/filter/saveField',
                    'emptytext' => 'нет данных',
                ],
                'name' => 'action_description',
            ],
            'created' => 'created',
            'importance' => [
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'editable' => [
                    'type' => 'select',
                    'model' => 'Clients',
                    'attribute' => 'importance',
                    'url' => '/clients/filter/saveField',
                    'emptytext' => 'нет данных',
                    'source' => Clients::getDropdownImportancce(),
                ],
                'name' => 'importance',
            ],
            'client_status' => [
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'editable' => [
                    'type' => 'select',
                    'model' => 'Clients',
                    'attribute' => 'client_status',
                    'url' => '/clients/filter/saveField',
                    'emptytext' => 'нет данных',
                    'source' => Clients::getDropdownClientStatus(),
                ],
                'name' => 'client_status',
            ],
            'payment_status' => [
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'editable' => [
                    'type' => 'select',
                    'model' => 'Clients',
                    'attribute' => 'payment_status',
                    'url' => '/clients/filter/saveField',
                    'emptytext' => 'нет данных',
                    'source' => Clients::getDropdownPaymentStatus(false),
                ],
                'name' => 'payment_status',
            ],
            'contract_end_date' => [
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'editable' => [
                    'type' => 'date',
                    'model' => 'Clients',
                    'attribute' => 'contract_end_date',
                    'url' => '/clients/filter/saveField',
                    'emptytext' => 'нет данных',
                    'value' => '($data->contract_end_date > 0 ? date("d.m.Y", $data->contract_end_date) : "")',
                ],
                'name' => 'contract_end_date',
                'value' => '$data->contract_end_date > 0 ? date("d.m.Y", $data->contract_end_date) : ""'
            ],
            'website' => [
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'editable' => [
                    'type' => 'text',
                    'model' => 'Clients',
                    'attribute' => 'website',
                    'url' => '/clients/filter/saveField',
                    'emptytext' => 'нет данных',
                ],
                'header' => 'Сайт',
                'name' => 'website',
            ],
            'city_id' => [
                'class' => 'LinkColumn',
                'header' => 'Город',
                'labelExpression' => 'isset($data->city->OFFNAME) ? $data->city->OFFNAME : \'\'',
                'urlExpression' => 'Yii::app()->createUrl("clients/clients/update", ["id" => $data->id])',
                'linkHtmlOptions' => ['target' => '_blank'],
                'renderLinkExpression' => '$data->isEditableForMe(Yii::app()->user);',
            ],
            'region_id' => [
                'class' => 'LinkColumn',
                'header' => 'Регион',
                'labelExpression' => 'isset($data->region->OFFNAME) ? $data->region->OFFNAME : \'\'',
                'urlExpression' => 'Yii::app()->createUrl("clients/clients/update", ["id" => $data->id])',
                'linkHtmlOptions' => ['target' => '_blank'],
                'renderLinkExpression' => '$data->isEditableForMe(Yii::app()->user);',
            ],
            'user_id' => [
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'editable' => [
                    'type' => 'select',
                    'model' => 'Clients',
                    'attribute' => 'user_id',
                    'url' => '/clients/filter/saveField',
                    'emptytext' => 'нет данных',
                    'source' => Users::getDropdownList(Yii::app()->user),
                ],
                'header' => 'Менеджер',
                'name' => 'user_id',
                'value' => '$data->isEditableForMe(Yii::app()->user) ? $data->user->name : ""',
                'visible' => Yii::app()->user->checkAccess("admin") || isset(Users::$usersRelations[Yii::app(
                    )->user->id]),
            ],
            'protocol_last_date' => [
                'class' => 'LinkColumn',
                'header' => 'Протокол',
                'labelExpression' => 'strlen($content = ClientProtocol::getLastItemsForSearchResultView($data->id, 1, 100)) > 0 ? $content : "нет данных";',
                'urlExpression' => '$data->id',
                'htmlOptions' => [
                    'style' => 'width: 200px',
                ],
                'linkHtmlOptions' => [
                    'class' => 'editable-click',
                    'token' => Yii::app()->request->csrfToken,
                    'data-toggle' => 'modal',
                    'data-target' => '#js-modal-window',
                    'onclick' => 'showEditDialog(
                        "Протокол общения с клиентом",
                        "/clients/filter/getProtocolForm",
                        {id:$(this).attr("href")},
                        this,
                        function (context, data){
                            $(context).text(data);
                        },
                        function (){
                            $("#js-modal-button-save").click();
                        }
                    );'
                ],
            ],
            'is_duplication' => [
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'editable' => [
                    'type' => 'select',
                    'model' => 'Clients',
                    'attribute' => 'is_duplication',
                    'url' => '/clients/filter/saveField',
                    'emptytext' => 'нет данных',
                    'source' => [0 => 'Нет', 1 => 'Да'],
                ],
                'name' => 'is_duplication',
                'value' => '$data->is_duplication > 0 ? "Да" : "Нет"',
            ],
            [
                'name' => 'contract_end_date',
                'value' => '$data->contract_end_date > 0 ? date("d.m.Y", $data->contract_end_date) : ""',
            ],
            'next_contact' => [
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'editable' => [
                    'type' => 'date',
                    'model' => 'Clients',
                    'attribute' => 'next_contact_dt',
                    'url' => '/clients/filter/saveField',
                    'emptytext' => 'нет данных',
                    'format' => 'yyyy-mm-dd',
                    'viewformat' => 'dd.mm.yyyy',
                ],
                'name' => 'next_contact',
                'header' => 'Дата контакта',
                'value' => '$data->next_contact > 0 ? date("d.m.Y", $data->next_contact) : ""',
            ],
            [
                'class' => 'LinkColumn',
                'header' => 'Действия',
                'labelExpression' => 'Yii::app()->user->checkAccess(\'editClients\', [\'user_id\' => $data->user_id]) ? "Удалить" : \'\'',
                'urlExpression' => 'Yii::app()->createUrl("clients/clients/delete", ["id" => $data->id])',
                'renderLinkExpression' => 'Yii::app()->user->checkAccess(\'editClients\', [\'user_id\' => $data->user_id])',
                'linkHtmlOptions' => [
                    'class' => 'js-delete-link',
                    'token' => Yii::app()->request->csrfToken,
                    'onclick' => 'return false;'
                ],
            ]
        ];

        $tableColumns = [

            [
                'class' => 'LinkColumn',
                'header' => 'id',
                'labelExpression' => '$data->id',
                'urlExpression' => 'Yii::app()->createUrl("clients/clients/update", ["id" => $data->id])',
                'renderLinkExpression' => '$data->isEditableForMe(Yii::app()->user);',
            ],
            [
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'editable' => [
                    'type' => 'text',
                    'model' => 'Clients',
                    'attribute' => 'title',
                    'url' => '/clients/filter/saveField',
                    'emptytext' => 'нет данных',
                ],
                'header' => 'Наименование',
                'name' => 'title',

                /*
                'class' => 'LinkColumn',
                'labelExpression' => '$data->title',
                'urlExpression' => 'Yii::app()->createUrl("clients/clients/update", ["id" => $data->id])',
                'renderLinkExpression' => '$data->isEditableForMe(Yii::app()->user)',
                */
            ],

        ];

        foreach ($columnsList as $columnName) {

            if (isset($columns[$columnName])) {
                $tableColumns[] = $columns[$columnName];
            }
        }

        $tableColumns[] = [
            'class' => 'LinkColumn',
            'header' => 'Действия',
            'label' => '<i class="icon-ok-sign" title="Завершить работу с записью"></i>',
            'urlExpression' => '$data->id',
            'htmlOptions' => [
                'style' => 'text-align:center; vertical-align: middle',
            ],
            'linkHtmlOptions' => [
                'onclick' => '
                    var button = $(this);
                    $.post("/clients/filter/excludeRecord", {id: $(this).attr("href")}, function(data) {
                        button.parents("tr").remove();
                    });
                    return false;
                ',
            ],
        ];

        return $tableColumns;
    }
}
