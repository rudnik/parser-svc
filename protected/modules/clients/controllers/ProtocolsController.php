<?php

/**
 * Контроллер опрераций с сообщениями протокола общения с клиентом
 */
class ProtocolsController extends Controller
{

    /**
     * @return array action filters
     */
    public function filters()
    {
        return ['accessControl'];
    }

    public function accessRules()
    {
        return [
            ['allow',
                'roles' => ['manager'],
            ],
            ['deny', // deny all users
                'users' => ['*'],
            ],
        ];
    }

    /**
     * Добавление сообщения
     * @throws CHttpException
     */
    public function actionAddMessage()
    {
        $newProtocolMessage = new ClientProtocol();
        $newProtocolMessage->attributes = Yii::app()->request->getPost('ClientProtocol');

        if (!$newProtocolMessage->validate()) {
            throw new CHttpException(500, 'Проверка параметров сообщения протокола завершилась с ошибками. ' . var_export($newProtocolMessage->errors, TRUE));
        }

        if ($newProtocolMessage->client->user_id == Yii::app()->user->id || Yii::app()->user->checkAccess('admin') || Users::isMyUser(Yii::app()->user->id, $newProtocolMessage->client->user_id)) {
            $newProtocolMessage->save();
        }

        exit;
    }

    /**
     * Обновляет сообщение протокола общения с клиентами
     * @throws CHttpException
     */
    public function actionUpdateMessage()
    {
        if (!Yii::app()->request->isPostRequest) {
            throw new CHttpException(403, 'Неверный тип запроса');
        }

        $model = ClientProtocol::model()->findByPk(Yii::app()->request->getPost('id'));
        
        if (is_null($model) || $model->user_id != Yii::app()->user->id) {
            throw new CHttpException(403, 'Не удалось сохранить модель.');
        }
        
        $model->comment = Yii::app()->request->getPost('value');
        
        if (!$model->validate()) {
            throw new CHttpException(500, var_export($model->errors, true));
        }

        if (!$model->save()) {
            throw new CHttpException(500, 'Не удалось сохранить модель.');
        }

        $model->refresh();
        echo nl2br(htmlspecialchars($model->comment));
    }

    /**
     * Рендерит протокол общения с заданным клиентом
     * @param integer $client_id
     */
    public function actionGrid($client_id)
    {
        $model = new Clients('search');
        $model->id = $client_id;
        $this->renderPartial('application.modules.clients.views.clients.form.update.protocol.grid', ['model' => $model]);
    }

}