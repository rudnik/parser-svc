<?php

class SubscriptionsController extends Controller
{

    /**
     *
     * @return array action filters
     */
    public function filters()
    {
        return [
            'accessControl'
        ];
    }

    public function accessRules()
    {
        return [
            [
                'allow',
                'roles' => [
                    'manager'
                ]
            ],
            [
                'deny', // deny all users
                'users' => [
                    '*'
                ]
            ]
        ];
    }

    /**
     * Поиск.
     */
    public function actionSearch()
    {
        if (!$this->checkAuth(false)) {
            $data = [
                'error' => 'Необходима авторизация.'
            ];
        } else {
            if (isset($_GET['ClientsSubscriptions'])) {
                $attributes = $_GET['ClientsSubscriptions'];

                foreach ([
                    'keywords_inc',
                    'keywords_exc'
                ] as $k) {
                    if (isset($attributes[$k])) {
                        $attributes[$k] = implode("\n", $attributes[$k]);
                    }
                }

                $subscr = new ClientsSubscriptions('search');
                $subscr->attributes = $attributes;
                $data = [];

                if (!$subscr->validate()) {
                    $errors = $subscr->getErrors();

                    if (isset($errors['keywords_inc'])) {
                        $data['error'] = array_shift($errors['keywords_inc']);
                    }
                }

                if (!isset($data['error'])) {
                    $search = new BidsSearch();

                    if (isset($_GET['pageSize'])) {
                        Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
                        unset($_GET['pageSize']);
                    }

                    $pageSize = Yii::app()->user->getState('pageSize');

                    try {
                        list ($count, $bids, $pages) = $search->search($subscr, $pageSize);

                        $fields_models = BidsFields::model()->findAll(new CDbCriteria([
                            'select' => [
                                'fieldId',
                                'name',
                                'type'
                            ]
                        ]));
                        $fields_cache = [];
                        foreach ($fields_models as $k => $field) {
                            $fields_cache[$field->fieldId] = $field;
                        }

                        unset($fields_models);

                        $data = [
                            'bids' => $bids,
                            'pages' => $pages,
                            'count' => $count,
                            'fields' => $fields_cache
                        ];
                    } catch (LogicException $e) {
                        $data = [
                            'error' => $e->getMessage()
                        ];
                    }
                }

                $data['sendedBids'] = ClientSendedBids::model()->findAll(
                    [
                        'select' => 'bidId, date',
                        'condition' => 'clientId = :clientId',
                        'params' => [':clientId' => $subscr->clientId],
                        'index' => 'bidId',
                    ]
                );
            } else {
                $data['error'] = 'Отсуствуют параметры рассылки.';
            }
        }

        $this->renderPartial('_search', $data);
    }

    /**
     * Выводит список рассылок клиента
     *
     * @param type $clientId
     *            - id клиента
     * @throws CHttpException
     */
    public function actionIndex($clientId = null)
    {
        if (is_null($clientId)) {
            throw new CHttpException(404, 'Не указан клиент.');
        }

        $clientModel = Clients::model()->findByPk((int) $clientId);
        if (is_null($clientModel)) {
            throw new CHttpException(404, 'Клиент не найден.');
        }

        $this->title = 'Настройка рассылки для клиента ' . CHtml::link($clientModel->title, '/clients/clients/update/' . $clientId);

        $this->render('index', [
            'clientId' => $clientId
        ]);
    }

    /**
     * Обновление параметров подписки
     *
     * @param integer $clientId - id клиента
     * @param integer $subscrId - id подписки
     * @throws CHttpException
     */
    public function actionUpdate($clientId, $subscrId = null)
    {
        $clientModel = Clients::model()->findByPk($clientId);
        if (is_null($clientModel)) {
            throw new CHttpException(404, 'Клиент не найден.');
        }

        // если рассылка пренадлежит другому менеджеру
        if (!Yii::app()->user->checkAccess('editClients', [
                    'user_id' => $clientModel->user_id
                ])
                && !Users::isMyUser(Yii::app()->user->id, $clientModel->user_id)
        ) {
            throw new CHttpException(403, 'У вас нет прав для редактирования этой рассылки');
        }

        // пробуем загрузить существующую рассылку
        $subscr = ClientsSubscriptions::model()->find([
            'condition' => 'subscrId = :subscrId AND clientId = :clientId',
            'params' => [
                ':subscrId' => $subscrId,
                ':clientId' => $clientModel->id
            ],
            'with' => [
                'client' => [
                    'with' => [
                        'user'
                    ],
                ],
            ],
        ]);

        // если рассылки нет - создаем
        if (is_null($subscr)) {
            $subscr = new ClientsSubscriptions();
        }

        $this->title = ($subscr->isNewRecord ? 'Добавление ' : 'Изменение ') . CHtml::link('рассылки', '/clients/subscriptions?clientId=' . $clientId, [
                    'title' => 'Список рассылок клиента'
                ]) . ' для клиента ' . CHtml::link($clientModel->title, '/clients/clients/update/' . $clientId, [
                    'title' => 'Карточка клиента'
        ]);
        $subscr->setScenario('insert');

        // получаем данные с формы
        $postData = Yii::app()->request->getPost('ClientsSubscriptions');

        // если пришли данные
        if (!is_null($postData)) {
            $postData['clientId'] = $clientModel->id;
            if (isset($postData['subscrId'])) {
                unset($postData['subscrId']);
            }

            # если удалось сохранить параметры рассылки
            if ($this->saveSubscription($subscr, $postData)) {
                # если нажата кнопка "отправить"
                if (isset($_POST['send_email'])) {
                    # отправляем только выбранные заявки, саму рассылку не отправляем
                    $checkedBids = Yii::app()->request->getPost('checkedBidsList');
                    if(!empty($checkedBids)) {
                        $emails = [];
                        foreach($subscr->emails as $emailModel) {
                            $emails[] = $emailModel->email;
                        }
                        $params = [
                            'clientName' => $subscr->client->title,
                            'userSignature' => $subscr->client->user->mail_signature,
                            'fromEmail' => $subscr->send_from_default_email ? 'info@svc-tender.ru' : $subscr->client->user->email,
                        ];
                        (new EmailSender())->sendBids($checkedBids, $emails, $subscr->clientId, $params, $subscr->getMailTheme());
                    }
                    # отправляем рассылку
                    else {
                        $subscr->refreshBidsCache();
                        (new EmailSender())->sendSubscriptions([$subscr]);
                    }
                }
                $subscr->refresh();
                $this->redirect('/clients/subscriptions/update?subscrId=' . $subscr->subscrId . '&clientId=' . $subscr->clientId);
            }
        }

        $this->renderSubscr($subscr);
    }

    public function actionAdd($clientId)
    {
        $this->actionUpdate($clientId, null);
    }

    /**
     * Удаление подписки
     *
     * @param integer $subscrId - id подписки
     * @throws CHttpException
     */
    public function actionDelete($subscrId)
    {
        $subscriptionModel = ClientsSubscriptions::model()->with('client')->findByPk($subscrId);
        if (is_null($subscriptionModel)) {
            throw new CHttpException(404, 'Рассылка не найдена');
        }

        if (!Yii::app()->user->checkAccess('editClients', [
                    'user_id' => $subscriptionModel->client->user_id
                ]) && !Users::isMyUser(Yii::app()->user->id, $subscriptionModel->client->user_id)) {
            throw new CHttpException(403, 'У вас нет прав для удаления этой рассылки');
        }

        $subscriptionModel->delete();
    }

    /**
     * Сохранение и вывод рассылки.
     *
     * @param ClientsSubscriptions $subscr
     *            Рассылка.
     */
    private function saveSubscription($subscr, $postData)
    {
        $attributes = $postData;

        foreach ([
            'keywords_inc',
            'keywords_exc'
        ] as $k) {
            if (isset($attributes[$k])) {
                $attributes[$k] = implode("\n", $attributes[$k]);
            } else {
                $attributes[$k] = '';
            }
        }

        $subscr->attributes = $attributes;

        foreach ([
            'activedate_from',
            'pubdate_from_start',
            'pubdate_to_start'
        ] as $field) {
            if (!empty($subscr->$field)) {
                $subscr->$field .= ' 00:00:00';
            }
        }

        foreach ([
            'activedate_from',
            'pubdate_from_start',
            'pubdate_to_start'
        ] as $field) {
            if (!empty($subscr->$field)) {
                $subscr->$field .= ' 23:59:59';
            }
        }

        return $subscr->save();
    }

    /**
     *
     * @param ClientsSubscriptions $subscr
     *            Рассылка.
     */
    private function renderSubscr($subscr)
    {
        foreach ([
            'activedate_from',
            'pubdate_from_start',
            'pubdate_from_end',
            'activedate_to',
            'pubdate_to_start',
            'pubdate_to_end'
        ] as $field) {
            $subscr->$field = str_replace([
                ' 00:00:00',
                ' 23:59:59'
                    ], '', $subscr->$field);
        }
        $this->render('subscrs', [
            'subscr' => $subscr
        ]);
    }

    /**
     * Проверка доступа.
     *
     * @param bool $redirect
     *            Перенаправить в случае отсутствия доступа.
     *
     * @return bool
     */
    private function checkAuth($redirect = true)
    {
        if (Yii::app()->user->isGuest) {
            if (!$redirect) {
                return false;
            }
            $this->redirect(Yii::app()->createUrl('site/login'));
        }
        return true;
    }

}
