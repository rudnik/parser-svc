<?php

/**
 * Консольное приложение конвертера данных парсера в сводную таблицу для поиска.
 *
 * @package Parser
 * @subpackage search
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013 BSTsoft
 */
class SearchConverterCommand extends CConsoleCommand
{

    /**
     * Запускает конвертер для указанной модели.
     *
     * @param type $limit Максимальное количество импортируемых заявок.
     *
     * @deprecated
     */
    public function actionConvert($limit = null)
    {
        // Вычисляем  id посдедней импортированной записи
        $response = Yii::app()->db->createCommand('
			SELECT
				MAX(`bid_id`) AS `lastId`
			FROM
				`bids_data_search`'
                )->queryRow();
        $lastId = intval($response['lastId']);

        // Выбираем свежие заявки
        $sql_new_bids = "
			SELECT
				`bidId`,
				`regionId`,
				`doctypeId`
			FROM
				`bids`
			WHERE
				`bidId`>{$lastId}
				AND `doctypeId` IN (
					SELECT
						`doctypeId`
					FROM
						`bis_doctypes`
					WHERE
						`name` IN('notificationOK', 'notificationEF', 'notificationZK')
				)
				AND `status`!='" . Bids::STATUS_CANCELED . "'";

        if (!is_null($limit)) {
            $sql_new_bids .= " LIMIT 0, $limit";
        }

        $result_new_bids = Yii::app()->db->createCommand($sql_new_bids)->queryAll();

        # Вывод информации о последнем id модели, имеющемся в сводной таблице и количеству импортируемых заявок
        echo "Last id = $lastId\n";
        echo "New bids count = " . count($result_new_bids) . "\n";

        # Массив, в котором харнятся комбинации пустых полей заявки и id заявки, где обнаружены пустые данные
        $_data_missing_array = [];

        # Выбираем данные по каждой заявкке и сохраняем в модель SphinxData
        foreach ($result_new_bids as $new_bid) {
            $model_search_data = new BidsDataSearch();

            # Ключ массива $_data_missing_array
            $_data_missing_key = '';

            # Id записи в моделе данных
            $model_search_data->bid_id = $new_bid['bidId'];
            # Id типа документа
            $model_search_data->doctypeId = $new_bid['doctypeId'];

            # Название закза
            $sql_order_title = "
				SELECT
					GROUP_CONCAT(`bids_data_text_search_orderName`.`value` SEPARATOR '; ') AS `value`
				FROM
					`bids_data_values` AS `bids_data_text_search_orderName`
					LEFT JOIN
						`bids_data` AS `bids_data_text_orderName`
						ON `bids_data_text_search_orderName`.`dataId` = `bids_data_text_orderName`.`dataId`
						AND `bids_data_text_orderName`.`fieldId` IN (SELECT `fieldId` FROM `bids_fields` WHERE `name` IN('orderName', 'lots/lot/subject'))
				WHERE
					`bids_data_text_orderName`.`bidId` = {$new_bid['bidId']}
			";
            $result_order_title = Yii::app()->db->createCommand($sql_order_title)->queryRow();

            # не найдено название заказа
            if (empty($result_order_title['value'])) {
                $_data_missing_key .= ' title ';
            }

            $model_search_data->title = $result_order_title['value'];

            # Описание заказа
            $sql_order_description = "
				SELECT
					GROUP_CONCAT(`bids_data_text_search_productName`.`value` SEPARATOR '; ') AS `value`
				FROM
					`bids_data_values` AS `bids_data_text_search_productName`
					LEFT JOIN
						`bids_data` AS `bids_data_text_productName`
						ON `bids_data_text_search_productName`.`dataId` = `bids_data_text_productName`.`dataId`
						AND `bids_data_text_productName`.`fieldId` IN (
							SELECT `fieldId` FROM `bids_fields` WHERE `name`='lots/lot/customerRequirements/customerRequirement/quantity'
						)
				WHERE
					`bids_data_text_productName`.`bidId` = {$new_bid['bidId']}
			";
            $result_order_description = Yii::app()->db->createCommand($sql_order_description)->queryRow();

            # не найдено описание заказа
            if (empty($result_order_description['value'])) {
                $_data_missing_key .= ' descrpition ';
            }

            $model_search_data->description = $result_order_description['value'];
            # Регион заказа
            $model_search_data->region_id = $new_bid['regionId'];

            # Дата публикации заказа
            $sql_publication_date = "
				SELECT
					UNIX_TIMESTAMP(`bids_data_date_search_publicationDate`.`value`) AS `value`
				FROM
					`bids_data_values` AS `bids_data_date_search_publicationDate`
					LEFT JOIN
						`bids_data` AS `bids_data_publicationDate`
						ON `bids_data_date_search_publicationDate`.`dataId` = `bids_data_publicationDate`.`dataId`
						AND `bids_data_publicationDate`.`fieldId` IN (
							SELECT `fieldId` FROM `bids_fields` WHERE `name`='%s'
						)
				WHERE
					`bids_data_publicationDate`.`bidId`=%s
				ORDER BY
					`bids_data_date_search_publicationDate`.`valueId`%s
				LIMIT 1
			";
            $sql_pd = sprintf($sql_publication_date, 1 == $model_search_data->doctypeId ? 'competitiveDocumentProvisioning/deliveryTerm' : 'publishDate', $new_bid['bidId'], 1 == $model_search_data->doctypeId ? ' DESC' : ''
            );
            $result_publication_date = Yii::app()->db->createCommand($sql_pd)->queryRow();

            if (1 == $model_search_data->doctypeId && empty($result_publication_date['value'])) {
                $sql_pd = sprintf($sql_publication_date, 'publishDate', $new_bid['bidId'], '');
                $result_publication_date = Yii::app()->db->createCommand($sql_pd)->queryRow();
            }

            # не найдена дата публикации заказа
            if (empty($result_publication_date['value'])) {
                $_data_missing_key .= ' publication_date ';
            }

            $model_search_data->publication_date = $result_publication_date['value'];

            // выбираем дату окончания подачи согласно типу документа
            $field_name = 1 == $model_search_data->doctypeId ? 'competitiveDocumentProvisioning/deliveryTerm2' : (2 == $model_search_data->doctypeId ? 'notificationCommission/p1Date' : 'notificationCommission/p2Date');

            # Дата окончания подачи котировочных заявок
            $sql_quote_date_end = "
				SELECT
					UNIX_TIMESTAMP(`bids_data_date_search_dateTo`.`value`) AS `value`
				FROM
					`bids_data_values` AS `bids_data_date_search_dateTo`
					LEFT JOIN `bids_data` AS `bids_data_dateTo`
						ON `bids_data_date_search_dateTo`.`dataId` = `bids_data_dateTo`.`dataId`
						AND `bids_data_dateTo`.`fieldId` IN (
							SELECT `fieldId` FROM `bids_fields` WHERE `name`='$field_name'
						)
				WHERE
					`bids_data_dateTo`.`bidId` = {$new_bid['bidId']}
				ORDER BY
					`bids_data_date_search_dateTo`.`valueId` DESC
				LIMIT 1
			";
            $result_quote_date_end = Yii::app()->db->createCommand($sql_quote_date_end)->queryRow();

            # не найдена дата окончания подачи котировочных заявок
            if (empty($result_quote_date_end['value'])) {
                $_data_missing_key .= ' quote_date_end ';
            }

            $model_search_data->quote_date_end = $result_quote_date_end['value'];

            // проверяем дату окончания подачи. если меньше сегодня, исключаем из поиска
            if ((int) $model_search_data->quote_date_end && $model_search_data->quote_date_end < time()) {
                continue;
            }

            # Начальная (максимальная) цена контракта
            $sql_max_cost = "
				SELECT
					`bids_data_num_search`.`value` AS `value`
				FROM
					`bids_data-num-search` AS `bids_data_num_search`
					LEFT JOIN `bids_data` AS `bids_data_price`
						ON `bids_data_num_search`.`dataId` = `bids_data_price`.`dataId`
						AND `bids_data_price`.`fieldId` IN (
							SELECT `fieldId` FROM `bids_fields` WHERE `name`='lots/lot/customerRequirements/customerRequirement/maxPrice'
						)
				WHERE
					`bids_data_price`.`bidId` = {$new_bid['bidId']}
				ORDER BY
					`bids_data_num_search`.`valueId` DESC
				LIMIT 1

			";
            $result_max_cost = Yii::app()->db->createCommand($sql_max_cost)->queryRow();

            # не найдена начальная (максимальная) цена контракта
            if (empty($result_max_cost['value'])) {
                $_data_missing_key .= ' max_cost ';
            }

            $model_search_data->max_cost = $result_max_cost['value'];

            # формирование статистики по пустым полям
            if (!empty($_data_missing_key)) {
                $_data_missing_key = $new_bid['doctypeId'] . '_' . trim($_data_missing_key);
                $_data_missing_array[$_data_missing_key][] = $new_bid['bidId'];
            }

            # Сохраняем данные
            if (!$model_search_data->save()) {
                print 'Не удалось сохранить даные: ' . var_export($model_search_data->getErrors(), true) . var_export($model_search_data->getAttributes(), true) . "\n";
            }
        }

        # Вывод статистики по пустым полям
        if (!empty($_data_missing_array)) {
            echo "Вывод статистики по пустым полям: \n";

            foreach ($_data_missing_array as $key => $value_array) {
                echo $key . ': ' . count($value_array) . "\n";
            }
        }
    }

    /**
     * Конвертирует заявки из в поисковую таблицу
     * @param int $limit
     */
    public function actionUniversalConvert($limit = 1000)
    {
        /* @var $bidList Bids[] */
        $bidList = Bids::model()->with('portal')->findAll([
            'limit' => $limit,
        ]);

        if (empty($bidList)) {
            echo 'В БД нет заявок для конвертации';
            return;
        }

        foreach ($bidList as $bid) {
            # при помощи фабрики создаем конвертер, соответствующий типу закупки.
            $bidConverter = BidsToSearchDataConverter::factory($bid->portal->name);

            # конвертируем закупку
            $searchData = $bidConverter->convert($bid);

            if ($searchData === null) {
                continue;
            }

            # сохраняем в поисковой таблице сконвертированную закупку
            $searchData->save();
        }
    }

    public function actionCorrectDuplication()
    {
        $dupliactionBids = Yii::app()->db->createCommand("
            SELECT  `bid_id` , COUNT(  `bid_id` ) AS  `cnt`
            FROM  `bids_data_search`
            GROUP BY  `bid_id`
            HAVING  `cnt` > 1
            ORDER BY  `cnt` DESC
        ")->queryAll();

        # перебираем все повторяющихся идентификаторы закупок
        foreach ($dupliactionBids as $bidInfo) {
            /* @var $searchBids BidsDataSearch[] */
            $searchBids = BidsDataSearch::model()->findAll([
                'condition' => 'bid_id = :bid_id',
                'order' => 'search_id',
                'params' => [':bid_id' => $bidInfo['bid_id']],
                ]);

            $newSearchBid = new BidsDataSearch();

            # перебираем все повторяющиеся записи в пределах очередного идентификатора закупки
            $counter = 0;
            foreach($searchBids as $searchBid) {
                $newSearchBid->bid_id = $searchBid->bid_id;
                $newSearchBid->doctypeId = $searchBid->doctypeId;
                $newSearchBid->title = $searchBid->title;
                $newSearchBid->description = $searchBid->description;
                $newSearchBid->region_id = $searchBid->region_id;

                if($counter == 0) {
                    $newSearchBid->publication_date = $searchBid->publication_date;
                }

                $newSearchBid->quote_date_end = $searchBid->quote_date_end;
                $newSearchBid->max_cost = $searchBid->max_cost;
                $newSearchBid->inn = $searchBid->inn;

                $counter++;
            }

            BidsDataSearch::model()->deleteAll('bid_id = :bid_id', [':bid_id' => $bidInfo['bid_id']]);
            $newSearchBid->save();
        }
    }
}
