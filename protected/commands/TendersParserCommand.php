<?php
Yii::import('application.components.*');
Yii::import('application.controllers.*');
Yii::import('application.extensions.TendersParser.*');
Yii::import('application.extensions.TendersParser.models.*');
Yii::import('application.models.*');

/**
 * Консольное приложение парсера закупок.
 *
 * @package TendersParser
 * @subpackage launcher
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013, 2014 BSTsoft
 */
class TendersParserCommand extends CConsoleCommand
{
    /**
     * Загрузчик парсера.
     *
     * @var ParserLauncher
     */
    private $launcher = null;

    /**
     * Загрузка файлов по FTP и распаковка архивов.
     * @param string $period
     *            Период, за который сохранять файлы:
     *            <ul>
     *            <li>today. За день. Если не указан, то последний;</li>
     *            <li>month. За месяц. Если не указан, то последний;</li>
     *            </ul>
     * @param int $periodNum Номер дня, месяца, года.
     * @param bool $sendReportEmail - отправлять ли отчеты по загрузке на email разработчиков
     * @return bool
     */
    public function actionSave($period = 'day', $periodNum = 0, $sendReportEmail = false)
    {
        Yii::app()->getDb()
            ->createCommand('TRUNCATE TABLE `logs`')
            ->execute();
        $this->getLauncher();
        $this->launcher->save($period, $periodNum, 0, $sendReportEmail);

        print 'Done.' . "\n";

        return true;
    }

    /**
     * Парсинг загруженных файлов.
     * @param bool $rmFiles Удалять файлы после парсинга.
     * @param bool $sendReportEmail отправлять ли отчеты по парсингу на email разработчиков
     * @return bool
     */
    public function actionParse($rmFiles = false, $sendReportEmail = false)
    {
        $this->getLauncher();
        $this->launcher->parse($rmFiles, $sendReportEmail);

        print 'Done.' . "\n";

        return true;
    }

    /**
     * Отправка заявок по почте.
     *
     * @param bool $trueSend Отправлять почту, иначе все выполнится, кроме вызова самой отправки.
     *
     * @throws CException
     */
    public function actionEmail($trueSend = true)
    {
        $criteria = new CDbCriteria();
        $criteria->condition = 't.active=1';
        $criteria->with = [
            'emailsCount',
            'client' => ['with' => 'user']
        ];

        $subscriptions = ClientsSubscriptions::model()->findAll($criteria);

        $sender = new EmailSender();
        $sender->trueSend = (bool) $trueSend;
        $sender->sendSubscriptions($subscriptions);

        echo $sender->getReportBuilder()->getReport();
    }

    /**
     * Подготовка загрузчика парсера.
     *
     * @return ParserLauncher Загрузкчик.
     *
     * @throws RuntimeException
     */
    private function getLauncher()
    {
        $this->launcher = new ParserLauncher();

        if (! $this->launcher->isReady()) {
            throw new RuntimeException('Launcher is not ready.');
        }

        $this->launcher->useException = false;
        $this->launcher->debug = true;
        $this->launcher->getPortalParsers();
    }

    /**
     * Справка.
     *
     * @see CConsoleCommand::getHelp()
     */
    public function getHelp()
    {
        return <<<EOD
ИСПОЛЬЗОВАНИЕ
  yiic tendersparser [action] [parameter]

ОПИСАНИЕ
  Консольный запуск парсера. Позволяет запускать парсер, используя планировщик
  задач. Может быть запущен параллельно с разными параметрами. В консольном
  режиме исключения парсинга не вызываются. Все ошибочные и успешные файлы при
  этом записываются в лог - в таблицу 'logs' базы данных.

ПРИМЕРЫ
 * yiic tendersparser save
   Загрузка файлов по FTP.

   Параметры:
   - period. Период, за который сохранять файлы:
     - day. За день. Если не указан, то текущий;
     - month. За месяц. Если не указан, то текущий;
   - periodNum. Номер дня, месяца;
   - sendReportEmail отправлять ли отчеты по парсингу на email разработчиков. Default=false

 * yiic tendersparser parse
   Парсинг загруженных файлов.

   Параметры:
   - rmFiles. Удалять файлы после парсинга.
   - sendReportEmail отправлять ли отчеты по парсингу на email разработчиков. Default=false

* yiic tendersparser runPortal
    Запускает парсеры порталов

    Параметры:
    - name - имя портала. Если указано - то запускается парсинг указанного портала
    - action - действие (save | parse)
    - period - период (day|month). Default=day
    - rmFiles - удалять ли файлы. Default=true
    - periodNum - номер дня, месяца; Default=0
    - sendReportEmail отправлять ли отчеты по загрузке и парсингу файлов на email разработчиков. Default=false

 * yiic tendersparser removeOldBids
    Удаляет старые заявки

    Параметры:
    - days. если заявка создана раньше, чем days дней назад - она удаляется.
    - limit. лимит удаляемых заявок. Default=null

EOD;
    }

    /**
     * Запускает парсеры порталов
     * @param null $name - имя портала. Если указано - то запускается парсинг указанного портала
     * @param null $action - действие (save | parse)
     * @param string $period - период (day|month)
     * @param boolean $rmFiles - удалять ли файлы
     * @param int $periodNum - номер дня, месяца;
     * @param bool $sendReportEmail отправлять ли отчеты по загрузке и парсингу файлов на email разработчиков
     * @throws RuntimeException
     */
    public function actionRunPortal($name = null, $action = null, $period = 'day', $rmFiles = true, $periodNum = 0, $sendReportEmail = false)
    {
        $parsersLauncher = new ParserLauncher();
        if (! $parsersLauncher->isReady()) {
            throw new RuntimeException('Launcher is not ready.');
        }

        $parsersLauncher->useException = false;
        $parsersLauncher->debug = true;
        $parsersLauncher->getPortalParsers($name);

        if (is_null($action) || $action == "save") {
            $parsersLauncher->save($period, $periodNum, $sendReportEmail);
        }

        if (is_null($action) || $action == "parse") {
            $parsersLauncher->parse($rmFiles, $sendReportEmail);
        }
    }

    /**
     * Обновляет кэш активных отправляемых заявок
     */
    public function actionRefreshSubscrCache()
    {
        # !!! логирование кэша заявок клиента 2707
        $loggedClientId = 2707;
        $log = date("d.m.Y H:i:s") . "\n";

        $criteria = new CDbCriteria();
        $criteria->condition = 't.active=1';
        $criteria->with = [
            'emailsCount'
        ];

        /* @var $subscrs ClientsSubscriptions[] */
        $subscrs = ClientsSubscriptions::model()->findAll($criteria);
        if (! empty($subscrs)) {
            foreach ($subscrs as $subscr) {

                # !!! логирование кэша заявок клиента 2707
                if ($subscr->clientId == $loggedClientId) {
                    $log .= $subscr->subscrId . ":\n" . $subscr->bids_cache . "\n";
                }

                $subscr->refreshBidsCache();

                # !!! логирование кэша заявок клиента 2707
                if ($subscr->clientId == $loggedClientId) {
                    $log .= $subscr->bids_cache . "\n\n";
                }
            }

            # !!! логирование кэша заявок клиента 2707
            $logFileName = Yii::getPathOfAlias('application.runtime') . '/subscr_log.txt';
            file_put_contents($logFileName, $log, FILE_APPEND);
        }
    }

    /**
     * Удаляет старые заявки
     * @param integer $days - если заявка создана раньше, чем $days дней назад - она удаляется
     * @param integer $limit - лимит удаляемых заявок
     */
    public function actionRemoveOldBids($days, $limit = null)
    {
        $removedBids =  Bids::removeOldBids($days, $limit);
        echo "RemoveOldBids report: remove $removedBids bids\n";
    }
}
