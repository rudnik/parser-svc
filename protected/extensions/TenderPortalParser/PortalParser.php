<?php
/**
 * Парсер порталов.
 */
class PortalParser implements IPortalParser
{
    /**
     * Стратегия загрузки файлов
     * @var IPortalFilesDownloadStrategy
     */
    private $downloadStrategy;

    /**
     * Стратегия парсинга файлов
     * @var IPortalParseStrategy
     */
    private $parseStrategy;

    /**
     * Конструктор.
     * @param IPortalFilesDownloadStrategy $downloadStrategy  Стратегия загрузки файлов
     * @param IPortalParseStrategy $parseStrategy  Стратегия парсинга файлов
     */
    public function __construct(IPortalFilesDownloadStrategy $downloadStrategy, IPortalParseStrategy $parseStrategy)
    {
        $this->downloadStrategy = $downloadStrategy;
        $this->parseStrategy = $parseStrategy;
    }

    /**
     * Загружает файлы при помощи стратегии загрузки. Именно стратегия загрузки знает что, откуда и куда загружать.
     * @param string $period  преиод, за который загружаются файлы [day|month]
     * @param int $number  номер дня или месяца, за который загружаются файлы
     * @return void
     */
    public function saveFiles($period, $number = null)
    {
        $this->downloadStrategy->getFiles($period, $number);

        # реконнект к БД. Когда парсинг файлов длится долго, то приложение теряет коннект с сервером БД.
        // @todo это залипень. нужно передать правильные параметры для pdo
        Yii::app()->db->setActive(false);
        Yii::app()->db->setActive(true);
    }

    /**
     * Парсит файлы при помощи стратегии парсинга файлов. Именно стратегия парсинга занет, где лежат файлы для парсинга,
     * как их парсить и куда сохранять.
     * @param bool $removeFiles  удалять ли после парсинга загруженные файлы
     * @return bool
     */
    public function parse($removeFiles = true)
    {
        $this->parseStrategy->parse($removeFiles);
    }

    public function getDownloadStrategy()
    {
        return $this->downloadStrategy;
    }

    public function setDownloadStrategy(IPortalFilesDownloadStrategy $downloadStrategy)
    {
        $this->downloadStrategy = $downloadStrategy;
    }

    public function getParseStrategy()
    {
        return $this->parseStrategy;
    }

    public function setParseStrategy(IPortalParseStrategy $parseStrategy)
    {
        $this->parseStrategy = $parseStrategy;
    }
}
