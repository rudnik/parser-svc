<?php
/**
 * Хранилище заявок. На данном этапе умеет только сохранять распарсенные заявки.
 */
class ZakupkiBidStorage implements IBidStorage, ILoggable
{
    /**
     * Статус закупки "Отменено".
     * @var string
     */
    const STATUS_CANCELED = 'canceled';

    /**
     * Информация о типе заявки. Содержит информацию, необходимую для корректного сохранения заявки.
     * @var IZakupkiBidTypeInfo
     */
    private $bidTypeInfo;

    /**
     * Логгер
     * @var IParserLogger
     */
    private $logger;

    public function getLogger()
    {
        return $this->logger;
    }

    public function setLogger(IParserLogger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Устанавливает информацию о типе заявки портала госуслуг.
     * @param \IZakupkiBidTypeInfo $bidTypeInfo
     */
    public function setBidTypeInfo(IZakupkiBidTypeInfo $bidTypeInfo)
    {
        $this->bidTypeInfo = $bidTypeInfo;
    }

    /**
     * Возвращает информацию о типе заявки портала госуслуг.
     * @return \IZakupkiBidTypeInfo
     */
    public function getBidTypeInfo()
    {
        return $this->bidTypeInfo;
    }

    #******************************************************************************************
    # Код, приведенный ниже, скопирован из старой версии парсера с минимальными изменениями.
    # Его рефаторинг требует много времени, поэтому не сейчас ;o)
    #******************************************************************************************

    /**
     * Сохраняет заявку в БД.
     * @param array $bidData  массив с распарсенными данными заявки
     * @return bool
     * @throws ParserException
     */
    public function saveBid(array $bidData)
    {
        if ($this->bidTypeInfo == null) {
            $this->logger->log('Не установлен класс с информацией и типе заявки.', CLogger::LEVEL_ERROR);
            return false;
        }

        if (!isset($bidData['doctypeId'])) {
            $this->logger->log('Для заявки не указан тип документа.', CLogger::LEVEL_ERROR);
            return false;
        }

        # подгружаем из базы тип документа
        /* @var BidsDoctypes $bidDoctype */
        $bidDoctype = BidsDoctypes::model()->findByPk($bidData['doctypeId']);

        if (is_null($bidDoctype)) {
            $this->logger->log('Тип документа №' . $bidData['doctypeId'] . ' не найден в БД.', CLogger::LEVEL_ERROR);
            return false;
        }

        # проверка обязательных и уникальных полей
        try {
            $requiredFieldsErrors = [
                'regionId' => 'Не указан регион закупки.',
                'portalId' => 'Не указан портал закупки.',
                'doctypeId' => 'Не указан тип документа.',
            ];

            $bidAttrs = [];

            // проверка обязательных полей
            foreach ($requiredFieldsErrors as $fieldName => $errorText) {
                if (!isset($bidData[$fieldName])) {
                    $this->logger->log($errorText, CLogger::LEVEL_ERROR);
                    return false;
                }

                $bidAttrs[$fieldName] = $bidData[$fieldName];
                unset($bidData[$fieldName]);
            }

            foreach ($bidData as $key => $val) {
                if (false !== strpos($key, '*')) {
                    $bidAttrs['id'] = $bidData[$key][0];
                    $unique = str_replace('*', '', $key);
                    $bidData[$unique] = $bidData[$key];
                    unset($bidData[$key]);

                    break;
                }
            }

            if (!isset($unique)) {
                $this->logger->log('Отсуствует уникальное поле. ' . var_export($bidData, true), CLogger::LEVEL_ERROR);
                return false;
            }
        } catch (ParserException $e) {
            $this->logger->log($e->getMessage());
            return false;
        }

        // нашли отмененные
        if (in_array($bidDoctype->name, $this->bidTypeInfo->getCancelDoctypeNames())) {
            $id = isset($bidAttrs['id']) ? $bidAttrs['id'] : null;

            if ($id === null) {
                $this->logger->log('Отсуствует идентификатор докумена, отменяющего закупку. ' . var_export($bidAttrs, true), CLogger::LEVEL_ERROR);
            }

            /* @var $bid Bids */
            $bid = Bids::model()->find('id=:id', [':id' => $id]);

            if (!is_null($bid)) {
                $this->setBidsCanceled($bid->id);
            }

            return true;
        }

        $transaction = Yii::app()->db->beginTransaction();

        try {
            $bid = $this->saveBids($bidAttrs);

            if($bid === false) {
                return false;
            }

            $searchData = BidsDataSearch::model()->find('bid_id = :bid_id', [':bid_id' => $bid->bidId]);

            if ($searchData === null) {
                $searchData = new BidsDataSearch();
                $searchData->bid_id    = $bid->bidId;
                $searchData->doctypeId = $bid->doctypeId;
                $searchData->region_id = $bid->regionId;
            }

            foreach ($bidData as $name => $values) {
                $field = $this->saveField(['doctypeId' => $bid->doctypeId, 'name' => $name], $unique);

                if(!$bid->isNewRecord && Utils::valueBelongsToSet($field->name, $this->bidTypeInfo->getNotUpdatedFields())) {
                    continue;
                }

                $bid_data = $this->saveBidsData(['bidId' => $bid->bidId, 'fieldId' => $field->fieldId]);
                $this->saveBidsDataValues($field->type, $bid_data->dataId, $values);
                $this->bidTypeInfo->setSearchInfo($bid, $name, $searchData, $values);
            }

            $this->saveBidsDataSearchModel($searchData);

            $transaction->commit();
        }
        catch (Exception $e) {
            $transaction->rollback();
            $this->logger->log($e->getMessage());
            return false;
        }

        return true;
    }

    /**
     * Сохранение основной модели заявки.
     *
     * @param array $attributes Атрибуты.
     *
     * @return Bids $bid
     *
     * @throws LogicException
     */
    protected function saveBids($attributes) {
        // проверяем закупку в базе
        $bid = Bids::model()->findByAttributes($attributes);

        // создаем закупку
        if (is_null($bid)) {
            $bid = new Bids();
            $bid->setAttributes($attributes, false);

            if (!$bid->save()) {
                $this->logger->log($this->getErrorsStr($bid->getErrors()), CLogger::LEVEL_ERROR);
                return false;
            }
            $bid->setIsNewRecord(true);
        }

        return $bid;
    }

    /**
     * Сохранение модели поля.
     *
     * @param array $attributes Атрибуты.
     * @param string $unique Имя уникального поля.
     *
     * @return BidsFields $field
     *
     * @throws LogicException
     */
    protected function saveField($attributes, $unique) {
        static $fieldsModels = [];

        // ключ в массиве закешированных моделей
        $key = $attributes['doctypeId'].'='.$attributes['name'];

        // если такую модель еще не запрашивали
        if (!isset($fieldsModels[$key])) {
            // проверяем поле в базе
            $field = BidsFields::model()->findByAttributes($attributes);

            // создаем поле
            if (is_null($field)) {
                $attributes['unique'] = (int) ($attributes['name'] == $unique);
                $attributes['type']  = 'text';
                $field = new BidsFields();
                $field->setAttributes($attributes, false);

                if (!$field->save()) {
                    $this->logger->log($this->getErrorsStr($field->getErrors()), CLogger::LEVEL_ERROR);
                    return false;
                }
            }

            $fieldsModels[$key] = $field;
        }

        return $fieldsModels[$key];
    }

    /**
     * Получение списка ошибок.
     *
     * @param array $errors Ошибки.
     *
     * @return string Значения ошибок, склеенные через перевод каретки.
     */
    private function getErrorsStr($errors) {
        $error_str = [];

        foreach ($errors as $attr => $list) {
            $error_str[] = '('.$attr.') '.implode(' ', $list);
        }

        return 'db_fail='.implode("\n", array_values($error_str));
    }

    /**
     * Сохранение модели данных.
     *
     * @param array $attributes Атрибуты.
     *
     * @return BidsData $bid_data
     *
     * @throws LogicException
     */
    protected function saveBidsData($attributes) {
        // проверяем данные в базе
        $bid_data = BidsData::model()->findByAttributes($attributes);

        if (is_null($bid_data)) {
            $bid_data = new BidsData();
            $bid_data->setAttributes($attributes, false);

            if (!$bid_data->validate()) {
                $this->logger->log("Некорректные данные для вставки.");
                return [];
            }

            if (!$bid_data->save()) {
                throw new LogicException($this->getErrorsStr($bid_data->getErrors()));
            }
        }

        return $bid_data;
    }

    /**
     * Изменение статуса закупки "Отменена".
     *
     * @param Bids $bidCanceled Отмененная закупка (тип документа).
     *
     * @throws LogicException
     */
    private function setBidsCanceled($bidUniqueId) {
        /* @var $bid Bids */
        $bid = Bids::model()->find('id=:id', [':id' => $bidUniqueId]);

        if ($bid == null) {
            return false;
        }

        $bid->status = self::STATUS_CANCELED;
        if (!$bid->save()) {
            $this->logger->log($this->getErrorsStr($bid->getErrors()), CLogger::LEVEL_ERROR);
        }

        BidsDataSearch::model()->deleteAll('bid_id = :bidId', [':bidId' => $bid->bidId]);

        return true;
    }

    /**
     * Сохранение модели значений данных.
     *
     * @param string $fieldType Тип поля.
     * @param integer $dataId Идентификатор данных.
     * @param array $values Значения.
     *
     * @throws LogicException
     */
    protected function saveBidsDataValues($fieldType, $dataId, $values) {
        // удаляем данные текущего поля
        Bids::model()->dbConnection->createCommand('
            DELETE FROM
                `'.BidsDataValues::model()->tableName().'`
            WHERE
                `dataId`=:data_id
        ')->execute([':data_id' => $dataId]);

        foreach ($values as $value) {
            $value = trim($value);

            if (!empty($value)) {
                if ('date' == $fieldType) {
                    $date = new DateTime($value);
                    $value = $date->format('Y-m-d H:i:s');
                }

                $attrs = ['dataId' => $dataId, 'value' => $value];

                $bid_data_values = new BidsDataValues();
                $bid_data_values->setAttributes($attrs, false);

                if (!$bid_data_values->save($attrs)) {
                    throw new LogicException($this->getErrorsStr($bid_data_values->getErrors()));
                }
            }
        }

        return true;
    }

    /**
     * Сохранение модели поисковый данных.
     *
     * @param BidsDataSearch $searchData Поисковые данные.
     *
     * @return bool
     *
     * @throws LogicException
     */
    protected function saveBidsDataSearchModel($searchData) {
        // если дата окончания приема заявок меньше, чем сейчас, пропускаем
        if ($searchData->quote_date_end && $searchData->quote_date_end < time()) {
            return false;
        }

        if (!$searchData->save()) {
            throw new LogicException($this->getErrorsStr($searchData->getErrors()));
        }

        return true;
    }
}