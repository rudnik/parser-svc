<?php
/**
 * Поведение стратегии загрузки файлов для загрузки файлов за указанный месяц текущего года
 */
class Zakupki223DownloadStrategyMonthBehavior extends ZakupkiDownloadStrategyBehavior
{
    /**
     * Инициализация поведения. Заходит в каталог с файлами выгрузки.
     * @param mixed $params не используется
     * @return bool
     */
    public function init($params = null)
    {
        # в некоторых регионах нет папки full
        if (!($this->ftp->chdir('daily'))) {
            return false;
        }

        return true;
    }

    /**
     * Финализация поведения. Выходит из каталога с файлами выгрузки.
     * @param null $params не используется
     * @return bool
     */
    public function finalize($params = null)
    {
        if (!($this->ftp->chdir('..'))) {
            $this->logger->log('Не удается вернуться в родительский каталог', CLogger::LEVEL_ERROR);
            $this->ftp->disconnect();
            return false;
        }
    }

    /**
     * Определяет, соответствует ли файл с указанной $fileInfo месяцу $number текущего года
     * @param string $fileInfo raw информация о файле
     * @param int $number номер месяца текущего года
     * @return bool
     */
    public function fileMayBeSaved($fileInfo, $number)
    {
        if ($number == null || $number < 1) {
            $number = date('m');
        }

        return ($fileInfo['month'] == $number) &&
        ($fileInfo['year'] == (int)date("Y") || $fileInfo['year'] == 0);
    }
}
