<?php
/**
 * Стратегия загрузки файлов с портала госзакупок по 223ФЗ
 */
class Zakupki223DownloadStrategy extends ZakupkiDownloadStrategy
{
    /**
     * Загрузка файлов
     * @param string $period [day|month]
     * @param int $number номер дня или месяца
     * @return bool
     */
    public function getFiles($period, $number = null)
    {
        // инициализируем ftp-соединение
        $this->ftp = new Ftp('ftp.zakupki.gov.ru', 'fz223free', 'fz223free');

        // Подгружаем портал с его типами документов
        $portalFz223 = Portals::model()->with('doctypes')->find('`t`.`name`="zakupki223"');
        if (null === $portalFz223) {
            $this->logger->log('Портал zakupki223 не найден.', CLogger::LEVEL_ERROR);
            $this->ftp->disconnect();
            return false;
        }


        // Заходим в папку /out/published
        if (!$this->ftp->chdir('out/published')) {
            $this->logger->log('Не удалось открыть ftp-каталог /out/published', CLogger::LEVEL_ERROR);
            $this->ftp->disconnect();
            return false;
        }

        # Получаем список папок с региональными выгрузками
        if (!$regions = $this->ftp->ls()) {
            $this->logger->log('Не удалось получить содержимое ftp-каталога /out/published', CLogger::LEVEL_ERROR);
            $this->ftp->disconnect();
            return false;
        }

        // Создем нужное поведение стратегии
        $strategyBehaviorName = 'Zakupki223DownloadStrategy' . ucfirst($period) . 'Behavior';
        /** @var IZakupkiDownloadStrategyBehavior|ILoggable $strategyBehavior */
        $strategyBehavior = new $strategyBehaviorName($this->ftp);

        if (!$strategyBehavior instanceof IZakupkiDownloadStrategyBehavior) {
            $this->logger->log('$strategyBehavior must be an instance of IZakupkiDownloadStrategyBehavior', CLogger::LEVEL_ERROR);
            $this->ftp->disconnect();
            return false;
        }

        // Устанавливаем поведению логгер
        $strategyBehavior->setLogger($this->logger);

        // Пробегаемся по всем регионам (/out/published/{REGION_NAME})
        /** @var array $regions */
        foreach ($regions as $region) {
            if (in_array($region, ['undefined', 'archive'], true)) {
                continue;
            }

            if (!$this->ftp->chdir($region)) {
                $this->logger->log('Не удается зайти в каталог ' . $region, CLogger::LEVEL_ERROR);
                $this->ftp->disconnect();
                return false;
            }

            # В каждом регионе пробегаемся по нужным типам документов (/out/published/{REGION_NAME}/{DOCTYPE_NAME})
            foreach ($portalFz223->doctypes as $doctype) {
                if (!$this->ftp->chdir($doctype->name)) {
                    $this->logger->log('Не удается зайти в каталог ' . $region . '/' . $doctype->name, CLogger::LEVEL_ERROR);
                    continue;
                }

                // Скачиваем файлы при помощи нужного поведения стратегии
                $this->downloadFiles($strategyBehavior, $number);

                if (!$this->ftp->chdir('..')) {
                    $this->logger->log('Не удается вернуться в родительский каталог', CLogger::LEVEL_ERROR);
                    $this->ftp->disconnect();
                    return false;
                }
            }

            if (!$this->ftp->chdir('..')) {
                $this->logger->log('Не удается вернуться в родительский каталог', CLogger::LEVEL_ERROR);
                $this->ftp->disconnect();
                return false;
            }
        }
        $this->ftp->disconnect();
    }

    /**
     * @inheritdoc
     */
    protected function downloadFiles(IZakupkiDownloadStrategyBehavior $strategyBehavior, $number)
    {
        // если инициализация поведения завершилось с ошибкой - уходим
        if (!$strategyBehavior->init()) {
            return false;
        }

        return parent::downloadFiles($strategyBehavior, $number);
    }
}
