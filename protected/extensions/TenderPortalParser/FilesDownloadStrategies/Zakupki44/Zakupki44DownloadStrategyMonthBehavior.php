<?php
/**
 * Поведение стратегии загрузки файлов для загрузки файлов за текущий или предыдущий месяц текущего года
 */
class Zakupki44DownloadStrategyMonthBehavior extends ZakupkiDownloadStrategyBehavior
{
    /**
     * Инициализация поведения. Заходит в каталог с файлами выгрузки.
     * @param mixed $params $params['number'] - номер месяца текущего года
     * @return bool
     */
    public function init($params = null)
    {
        if (!isset($params['number'])) {
            $this->logger->log('Не установлен номер периода', CLogger::LEVEL_ERROR);
            $this->ftp->disconnect();
            return false;
        }

        $monthNumber = (int) $params['number'];

        # если скачивание за запрашиваемый месяц не возможно - уходим
        $currentMonth = date("m");
        if ($monthNumber > $currentMonth || $monthNumber < ($currentMonth - 1)) {
            $this->logger->log('Скачивание за запрашиваемый месяц не возможно', CLogger::LEVEL_ERROR);
            $this->ftp->disconnect();
            return false;
        }

        /**
         * Загружать файлы из папки за текущий или предыдущий месяц
         */
        $downloadFromPreviosMonth = $monthNumber === ($currentMonth - 1);

        if ($downloadFromPreviosMonth) {
            # переходим в каталог с файлами за предыдущий месяц
            if (!$this->ftp->chdir('prevMonth')) {
                $this->logger->log('Не удается перейти в каталог prevMonth', CLogger::LEVEL_ERROR);
                $this->ftp->disconnect();
                return false;
            }
        } else {
            # переходим в каталог с файлами за текущий месяц
            if (!$this->ftp->chdir('currMonth')) {
                $this->logger->log('Не удается перейти в каталог currMonth', CLogger::LEVEL_ERROR);
                $this->ftp->disconnect();
                return false;
            }
        }

        return true;
    }

    /**
     * Финализация поведения. Выходит из каталога с файлами выгрузки.
     * @param null $params не используется
     * @return bool
     */
    public function finalize($params = null)
    {
        if (!$this->ftp->chdir('..')) {
            $this->logger->log('Не удается вернуться в родительский каталог', CLogger::LEVEL_ERROR);
            $this->ftp->disconnect();
            return false;
        }
    }

    /**
     * Загружаем все файлы
     * @param string $fileInfo raw информация о файле
     * @param int $number номер меяса текущего года
     * @return bool
     */
    public function fileMayBeSaved($fileInfo, $number)
    {
        return !in_array($fileInfo['fileName'], ['.', '..']);
    }
}