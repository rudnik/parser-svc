<?php
/**
 * Стратегия загрузки файлов с портала госзакупок по 44ФЗ
 */
class Zakupki44DownloadStrategy extends ZakupkiDownloadStrategy
{
    /**
     * Загрузка файлов
     * @param string $period [day|month]
     * @param int $number номер дня или месяца
     * @return bool
     */
    public function getFiles($period, $number = null)
    {
        // Инициализируем ftp-соединение
        $this->ftp = new Ftp('ftp.zakupki.gov.ru', 'free', 'free');

        // Подгружаем портал с его типами документов
        $portalFz44 = Portals::model()->with('doctypes')->find('`t`.`name`="zakupki44"');
        if (null === $portalFz44) {
            $this->logger->log('Портал zakupki44 не найден.', CLogger::LEVEL_ERROR);
            $this->ftp->disconnect();
            return false;
        }

        // Заходим в папку /fcs_regions
        if (!$this->ftp->chdir('fcs_regions')) {
            $this->logger->log('Не удалось открыть ftp-каталог /fcs_regions', CLogger::LEVEL_ERROR);
            $this->ftp->disconnect();
            return false;
        }

        // Получаем список папок с региональными выгрузками
        if (!$regions = $this->ftp->ls()) {
            $this->logger->log('Не удалось получить содержимое ftp-каталога /fcs_regions', CLogger::LEVEL_ERROR);
            $this->ftp->disconnect();
            return false;
        }

        // Создем нужное поведение стратегии
        $strategyBehaviorName = 'Zakupki44DownloadStrategy' . ucfirst($period) . 'Behavior';
        /** @var IZakupkiDownloadStrategyBehavior|ILoggable $strategyBehavior */
        $strategyBehavior = new $strategyBehaviorName($this->ftp);

        if (!$strategyBehavior instanceof IZakupkiDownloadStrategyBehavior) {
            $this->logger->log('$strategyBehavior must be an instance of IZakupkiDownloadStrategyBehavior', CLogger::LEVEL_ERROR);
            $this->ftp->disconnect();
            return false;
        }
        // Устанавливаем поведению логгер
        $strategyBehavior->setLogger($this->logger);

        # Пробегаемся по всем регионам (/fcs_regions/{REGION_NAME})
        /** @var array $regions */
        foreach ($regions as $region) {
            if ($region === '_logs' || preg_match('/\.zip/', $region)) {
                continue;
            }

            # В каждом регионе заходим в папку notifications (/fcs_regions/{REGION_NAME}/notifications)
            if (!$this->ftp->chdir($region . '/notifications')) {
                $this->logger->log('Не удается зайти в каталог "' . $region . '/notifications"', CLogger::LEVEL_ERROR);
                continue;
            }

            // Скачиваем файлы при помощи нужного поведения стратегии
            $this->downloadFiles($strategyBehavior, $number);

            if (!$this->ftp->chdir('../..')) {
                $this->logger->log('Не удается вернуться в родительский каталог', CLogger::LEVEL_ERROR);
                continue;
            }
        }

        if (!$this->ftp->chdir('..')) {
            $this->logger->log('Не удается вернуться в родительский каталог', CLogger::LEVEL_ERROR);
            $this->ftp->disconnect();
            return false;
        }

        $this->ftp->disconnect();
    }

    /**
     * @inheritdoc
     */
    protected function downloadFiles(IZakupkiDownloadStrategyBehavior $strategyBehavior, $number)
    {
        // если инициализация поведения завершилось с ошибкой - уходим
        if (!$strategyBehavior->init(['number' => $number])) {
            return false;
        }

        if (!parent::downloadFiles($strategyBehavior, $number)) {
            return false;
        }

        return $this->downloadFilesFirstDay($strategyBehavior, $number);
    }

    /**
     * В первое число месяца файлы могут быть директории как текущего, так и прошлого месяца.
     * Тут получаем из прошлого.
     *
     * @param IZakupkiDownloadStrategyBehavior $strategyBehavior поведение стратегии
     * @param integer                          $number           номер для или месяца, в зависимости от поведения стратегии
     *
     * @return bool
     */
    private function downloadFilesFirstDay(IZakupkiDownloadStrategyBehavior $strategyBehavior, $number)
    {
        if ($strategyBehavior instanceof Zakupki44DownloadStrategyMonthBehavior) {
            return true;
        }

        if (!$strategyBehavior->init(['number' => -1])) {
            return false;
        }

        return parent::downloadFiles($strategyBehavior, $number);
    }
}
