<?php
/**
 * Поведение стратегии загрузки файлов для загрузки файлов за указанный день текущего месяца
 */
class Zakupki44DownloadStrategyDayBehavior extends ZakupkiDownloadStrategyBehavior
{
    /**
     * Инициализация поведения. Заходит в каталог с файлами выгрузки.
     * @param mixed $params
     * @return bool
     */
    public function init($params = null)
    {
        if (!isset($params['number']) || (int) $params['number'] === 0) {
            $params['number'] = date('d');
        }

        // файлы, датированные первым числом месяца, иногда располагаются в каталоге с файлами за предыдущий месяц
        $directoryName = (int) $params['number'] === -1 ? 'prevMonth' : 'currMonth';

        if (!$this->ftp->chdir($directoryName)) {
            $this->logger->log('Не удается войти в каталог ' . $directoryName, CLogger::LEVEL_ERROR);
            $this->ftp->disconnect();
            return false;
        }

        return true;
    }

    /**
     * Финализация поведения. Выходит из каталога с файлами выгрузки.
     * @param null $params не используется
     * @return bool
     */
    public function finalize($params = null)
    {
        if (!$this->ftp->chdir('..')) {
            $this->logger->log('Не удается вернуться в родительский каталог', CLogger::LEVEL_ERROR);
            $this->ftp->disconnect();
            return false;
        }
    }

    /**
     * Определяет, соответствует ли файл с указанной $fileInfo дню $number текущего месяца
     * @param string $fileInfo raw информация о файле
     * @param int $number номер дня текущего месяца
     * @return bool
     */
    public function fileMayBeSaved($fileInfo, $number)
    {
        if ($number == null || $number < 1) {
            $number = (int)date('d');
        }

        return ($fileInfo['day'] == $number) &&
        ($fileInfo['month'] == (int)date('m')) &&
        ($fileInfo['year'] == (int)date("Y") || $fileInfo['year'] == 0);
    }
}
