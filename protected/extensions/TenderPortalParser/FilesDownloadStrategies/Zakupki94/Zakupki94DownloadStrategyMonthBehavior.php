<?php
/**
 * Поведение стратегии загрузки файлов для загрузки файлов за указанный месяц текущего года
 */
class Zakupki94DownloadStrategyMonthBehavior extends ZakupkiDownloadStrategyBehavior
{
    public function init($params = null)
    {
        return true;
    }

    public function finalize($params = null)
    {
        return true;
    }

    /**
     * Определяет, соответствует ли файл с указанной $fileInfo месяцу $number текущего года
     * @param string $fileInfo raw информация о файле
     * @param int $number номер месяца текущего года
     * @return bool
     */
    public function fileMayBeSaved($fileInfo, $number)
    {
        if ($number == null || $number < 1) {
            $number = date('m');
        }

        return ($fileInfo['month'] == $number) &&
        ($fileInfo['year'] == (int)date("Y") || $fileInfo['year'] == 0);
    }
}
