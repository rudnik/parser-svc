<?php
/**
 * Стратегия загрузки файлов с портала госзакупок по 94ФЗ
 */
class Zakupki94DownloadStrategy extends ZakupkiDownloadStrategy
{
    /**
     * Загрузка файлов
     * @param string $period [day|month]
     * @param int $number номер дня или месяца
     * @return bool
     */
    public function getFiles($period, $number = null)
    {
        # инициализируем ftp-соединение
        $this->ftp = new Ftp('ftp.zakupki.gov.ru', 'free', 'free');

        # Подгружаем портал с его типами документов
        $portalFz94 = Portals::model()->with('doctypes')->find('`t`.`name`="zakupki94"');
        if (is_null($portalFz94)) {
            $this->logger->log('Портал zakupki94 не найден.', CLogger::LEVEL_ERROR);
            $this->ftp->disconnect();
            return false;
        }

        # Заходим в папку /94fz
        if (!$this->ftp->chdir('94fz')) {
            $this->logger->log('Не удалось открыть ftp-каталог /94fz', CLogger::LEVEL_ERROR);
            $this->ftp->disconnect();
            return false;
        }

        # Получаем список папок с региональными выгрузками
        if (!$regions = $this->ftp->ls()) {
            $this->logger->log('Не удалось получить содержимое ftp-каталога /94fz', CLogger::LEVEL_ERROR);
            $this->ftp->disconnect();
            return false;
        }

        # Пробегаемся по всем регионам (/94fz/{REGION_NAME})
        foreach ($regions as $region) {
            if (in_array($region, ['_FCS_nsi', 'nsi']) || strpos($region, '.') !== false) {
                continue;
            }

            # В каждом регионе заходим в папку notifications (/94fz/{REGION_NAME}/notifications/daily)
            if (!$this->ftp->chdir($region . '/notifications')) {
                $this->logger->log('Не удается зайти в каталог ' . $region . '/notifications', CLogger::LEVEL_ERROR);
                $this->ftp->disconnect();
                return false;
            }

            # Создем нужное поведение стратегии
            $strategyBehaviorName = 'Zakupki94DownloadStrategy' . ucfirst($period) . 'Behavior';
            $strategyBehavior = new $strategyBehaviorName($this->ftp);

            if (!$strategyBehavior instanceof IZakupkiDownloadStrategyBehavior) {
                $this->logger->log('$strategyBehavior must be an instance of IZakupkiDownloadStrategyBehavior', CLogger::LEVEL_ERROR);
                $this->ftp->disconnect();
                return false;
            }

            # Устанавливаем поведению логгер
            $strategyBehavior->setLogger($this->logger);

            # Скачиваем файлы при помощи нужного поведения стратегии
            $this->downloadFiles($strategyBehavior, $number);

            if (!$this->ftp->chdir('../..')) {
                $this->logger->log('Не удается вернуться в родительский каталог', CLogger::LEVEL_ERROR);
                $this->ftp->disconnect();
                return false;
            }
        }

        if (!$this->ftp->chdir('..')) {
            $this->logger->log('Не удается вернуться в родительский каталог', CLogger::LEVEL_ERROR);
            $this->ftp->disconnect();
            return false;
        }

        $this->ftp->disconnect();
    }
}
