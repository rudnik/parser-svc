<?php
/**
 * Базовый класс стратегии загрузки.
 */
abstract class ZakupkiDownloadStrategy implements IPortalFilesDownloadStrategy, IReportable, ILoggable
{
    /**
     * Ftp клиент
     * @var Ftp
     */
    protected $ftp;

    /**
     * Папка для сохранения файлов заявок
     * @var string
     */
    protected $outputDir;

    /**
     * Логгер
     * @var IParserLogger
     */
    protected $logger;

    /**
     * Строитель отчетов
     * @var IEntityProcessReportBuilder
     */
    private $reportBuilder;

    public function setOutputDir($outputDir)
    {
        $this->outputDir = $outputDir;
    }

    public function getOutputDir()
    {
        return $this->outputDir;
    }

    public function getReportBuilder()
    {
        return $this->reportBuilder;
    }

    public function setReportBuilder(IEntityProcessReportBuilder $reportBuilder)
    {
        $this->reportBuilder = $reportBuilder;
    }

    public function getLogger()
    {
        return $this->logger;
    }

    public function setLogger(IParserLogger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Возвращает массив с информацией о файле
     * @param $infoString  строка, описывающая файл (ftp raw)
     * @return array массив с информацией о файле:<br>
     *  $info['day'] - день создания файла<br>
     *  $info['month'] - месяц создания файла<br>
     *  $info['year'] - год создания файла<br>
     *  $info['fileName'] - имя файла<br>
     */
    protected function parseFileInfo($infoString)
    {
        $fileInfo = [];

        /*
         *  Строка, описывающая файл имеет следующий вид:
         * -rw-rw-r--    1 572      572           272 Jan 01  2013 attachedOrderClause_Adygeya_Resp_20121231_000000_20121231_235959_daily_001.xml.zip
         */
        $chunks = preg_split('/\s+/', $infoString);
        $fileInfo['day'] = (int)$chunks[6];
        $fileInfo['month'] = (int)date('m', strtotime($chunks[5]));
        $fileInfo['year'] = strpos($chunks[7], ':') !== false ? (int)date('Y') : (int)$chunks[7];
        $fileInfo['fileName'] = end($chunks);
        return $fileInfo;
    }

    /**
     * Загружает файлы при помощи поведения стратегии
     * @param IZakupkiDownloadStrategyBehavior $strategyBehavior  поведение стратегии
     * @param integer $number номер для или месяца, в зависимости от поведения стратегии
     * @return bool
     */
    protected function downloadFiles(IZakupkiDownloadStrategyBehavior $strategyBehavior, $number)
    {
        # получаем список файлов для загрузки (переход в нужный каталог обеспечивает $strategyBehavior->init)
        $filesList = $this->ftp->lsRaw();

        if ($filesList === false) {
            $this->logger->log('Не удается получить список файлов', CLogger::LEVEL_WARNING);
            return false;
        }

        # перебираем файлы
        /** @var array $filesList */
        foreach ($filesList as $fileName) {
            $fileInfo = $this->parseFileInfo($fileName);
            $fileName = $fileInfo['fileName'];

            # если поведение стратегии не позволяет загрузить файл - переходим к следующему файлу
            if (!$strategyBehavior->fileMayBeSaved($fileInfo, $number)) {
                continue;
            }

            # построитель отчетов увличивает счетчик общего числа затронутых файлов
            if ($this->reportBuilder !== null) {
                $this->reportBuilder->addTotalCount();
            }

            # в случае неудачи при скачивании - увеличиваем счетчик fail'ов.
            if (!$this->ftp->get($this->outputDir . '/' . $fileName, $fileName)) {
                if ($this->reportBuilder !== null) {
                    $this->reportBuilder->addFail();
                }
                $this->logger->log('Не удается сохранить файл ' . $fileName, CLogger::LEVEL_WARNING);
                continue;
            }

            # в случае успешного скачивания - увеличиваем соответствующий счетчик
            if ($this->reportBuilder !== null) {
                $this->reportBuilder->addSuccess();
            }
        }

        # проводим финализацию поведения
        $strategyBehavior->finalize(['number' => $number]);

        return true;
    }

}
