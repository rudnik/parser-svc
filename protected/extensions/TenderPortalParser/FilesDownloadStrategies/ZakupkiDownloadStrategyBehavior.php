<?php
/**
 * Базовый класс поведения стратегии загрузки файлов
 */
abstract class ZakupkiDownloadStrategyBehavior implements IZakupkiDownloadStrategyBehavior, ILoggable
{
    /**
     * Логгер
     * @var IParserLogger
     */
    protected $logger;

    /**
     * Ftp клиент
     * @var Ftp
     */
    protected $ftp;

    /**
     * Конструктор
     * @param Ftp $ftp Ftp клиент
     */
    public function __construct(Ftp $ftp)
    {
        $this->ftp = $ftp;
    }

    public function getLogger()
    {
        return $this->logger;
    }

    public function setLogger(IParserLogger $logger)
    {
        $this->logger = $logger;
    }
}