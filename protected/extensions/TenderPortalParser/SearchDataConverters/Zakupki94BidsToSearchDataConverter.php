<?php
/**
 * Конвертер закупок по 94ФЗ
 */
class Zakupki94BidsToSearchDataConverter extends ZakupkiBidsToSearchDataConverter
{
    public function __construct()
    {
        $this->bidTypeInfo = ZakupkiBidsTypeInfo::factory('Zakupki94');
        parent::__construct();
    }
}
