<?php
/**
 * Конвертер закупок по 44ФЗ
 */
class Zakupki44BidsToSearchDataConverter extends ZakupkiBidsToSearchDataConverter
{
    public function __construct()
    {
        $this->bidTypeInfo = ZakupkiBidsTypeInfo::factory('Zakupki44');
        parent::__construct();
    }
}