<?php
/**
 * Конвертер закупок по 223ФЗ
 */
class Zakupki223BidsToSearchDataConverter extends ZakupkiBidsToSearchDataConverter
{
    public function __construct()
    {
        $this->bidTypeInfo = ZakupkiBidsTypeInfo::factory('Zakupki223');
        parent::__construct();
    }
}