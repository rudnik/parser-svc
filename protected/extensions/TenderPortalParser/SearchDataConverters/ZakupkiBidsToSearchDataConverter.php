<?php
/**
 * Конвертер закупок портала zakupki.gov.ru
 */
class ZakupkiBidsToSearchDataConverter implements IBidsToSearchDataConverter
{
    /**
     * Ассоциативный массив с информацией о полях закупки<br>
     * 'fieldId1' =>  BidsFields object1,<br>
     * 'fieldId2' =>  BidsFields object2,<br>
     * ...<br>
     *
     * @var BidsFields[]
     */
    static protected $_fields;

    /**
     * Информация о закупках по определенному ФЗ
     * @var IZakupkiBidTypeInfo
     */
    protected $bidTypeInfo = null;

    public function __construct()
    {
        if ($this->bidTypeInfo === null) {
            throw new CException('Не установлен объект с информацией о закупках.');
        }

        # Заполнение справочника моделей полей заявки
        if(is_null(static::$_fields)) {
            static::$_fields = BidsFields::model()->findAll(new CDbCriteria([
                    'select' => ['fieldId', 'name', 'type'],
                    'index' => 'fieldId',
                ]));
        }
    }

    /**
     * Конвертер закупки в модель поисковой таблицы
     * @param Bids $bid экземпляр закупки
     * @return BidsDataSearch модель записи поисковой таблицы
     */
    public function convert(Bids $bid)
    {
        $searchData = new BidsDataSearch();

        # перебираем значения по каждому полю
        foreach($bid->data as $data) {
            /* @var $field BidsFields */
            $field = static::$_fields[$data->fieldId];
            $this->bidTypeInfo->setSearchInfo($bid, $field->name, $searchData, $data->values);
        }

        $searchData->bid_id = $bid->bidId;
        $searchData->region_id = $bid->regionId;
        $searchData->doctypeId = $bid->doctypeId;

        return $searchData;
    }
}