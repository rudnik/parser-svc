<?php
/**
 * Фабрика конвертеров закупок в модель записи поисковой таблицы
 */
abstract class BidsToSearchDataConverter
{
    /**
     * Фабрика конвертеров
     * @param $portalName имя портала, к которому относится конвертируемая закупка
     * @return IBidsToSearchDataConverter
     */
    public static function factory($portalName) {
        $className = ucfirst($portalName) . 'BidsToSearchDataConverter';
        $classInstance =  new $className;

        if (!$classInstance instanceof IBidsToSearchDataConverter) {
            return null;
        }

        return $classInstance;
    }
}