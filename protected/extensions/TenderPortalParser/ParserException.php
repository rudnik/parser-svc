<?php

/**
 * Класс исключений парсера.
 *
 * @package TendersParser
 * @subpackage ParserException
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013
 */
class ParserException extends CException {
	public function __construct($message = null, $code = null, $previous = null) {
		parent::__construct($message, $code, $previous);
	}
}
