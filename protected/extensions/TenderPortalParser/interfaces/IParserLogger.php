<?php
/**
 * Интерфейс логгера
 */
interface IParserLogger
{
    /**
     * Добавляет сообщение в лог
     * @param string $message сообщение
     * @param string $level уровень сообщения. Один из уровней описанный в CLogger::
     * @return void
     */
    public function log($message, $level = CLogger::LEVEL_INFO);
}