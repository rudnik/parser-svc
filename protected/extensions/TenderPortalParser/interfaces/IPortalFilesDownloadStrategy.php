<?php
/**
 * Интерфейс стратегии загрузки файлов
 */
interface IPortalFilesDownloadStrategy
{
    /**
     * Устанавливает папку для сохранения скачанных файлов
     * @param string $dir путь к папке, в которую будут сохраняться скачанные файлы
     * @return void
     */
    public function setOutputDir($dir);

    /**
     * Возвращает путь к папке, в которую будут сохраняться скачанные файлы.
     * @return string
     */
    public function getOutputDir();

    /**
     * Скачивает нужные файлы
     * @param string $period название периода, за который качаем файлы
     * @param int $number параметр периода
     * @return bool
     */
    public function getFiles($period, $number);
}