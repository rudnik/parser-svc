<?php
/**
 * Интерфейс объекта, поддерживающего отчеты
 */
interface IReportable
{
    /**
     * Возвращает строитель отчетов
     * @return IEntityProcessReportBuilder
     */
    public function getReportBuilder();

    /**
     * Устанавливает строитель отчетов
     * @param IEntityProcessReportBuilder $reportBuilder
     * @return void
     */
    public function setReportBuilder(IEntityProcessReportBuilder $reportBuilder);
}