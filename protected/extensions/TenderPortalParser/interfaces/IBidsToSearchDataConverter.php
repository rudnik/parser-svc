<?php
/**
 * Интерфейс конвертера модели закупки в модель записи поисковой таблицы
 */
interface IBidsToSearchDataConverter
{
    /**
     * Конвертер закупки в модель поисковой таблицы
     * @param Bids $bid экземпляр закупки
     * @return BidsDataSearch модель записи поисковой таблицы
     */
    public function convert(Bids $bid);
}