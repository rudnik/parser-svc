<?php
/**
 * Интерфейс строителя парсера
 */
interface IParserBuilder
{
    /**
     * Возвращает построенный парсер
     * @return IPortalParser
     */
    public function build();
}