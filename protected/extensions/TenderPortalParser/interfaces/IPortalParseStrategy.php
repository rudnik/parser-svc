<?php
/**
 * Интерфейс стратегии парсинга
 */
interface IPortalParseStrategy
{
    /**
     * Конструктор
     * @param IBidStorage $bidStorage экземпляр хранилища заявок
     */
    public function __construct(IBidStorage $bidStorage);

    /**
     * Устанавливает папку с файлами для парсинга
     * @param string $dir путь к папке с файлами для парсинга
     * @return void
     */
    public function setInputDir($dir);

    /**
     * Возвращает путь к папке с файлами для парсинга
     * @return string
     */
    public function getInputDir();

    /**
     * Парсит файлы
     * @param bool $removeFiles удалять ли скачанные файлы после парсинга
     * @return void
     */
    public function parse($removeFiles = true);
}