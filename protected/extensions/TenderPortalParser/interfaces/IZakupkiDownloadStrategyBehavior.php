<?php
/**
 * Интерфейс поведения стратегии загрузки файлов.
 */
interface IZakupkiDownloadStrategyBehavior
{
    /**
     * Инициализация стратегии
     * @param null $params параметры
     * @return bool
     */
    public function init($params = null);

    /**
     * Финализация стратегии
     * @param null $params параметры
     * @return bool
     */
    public function finalize($params = null);

    /**
     * Определение, соответствует ли очередной найденный файл правилам данной стратегии загрузки.
     * @param string $fileInfo raw информация о файле
     * @param int $number число, параметризующее стратегию загрузки. Например номер дня или месяца, в зависимости от стратегии.
     * @return bool
     */
    public function fileMayBeSaved($fileInfo, $number);
}