<?php
/**
 * Интерфейс хранилища заявок
 */
interface IBidStorage
{
    /**
     * Сохраняет заявку в БД.
     * @param array $bidData  массив с распарсенными данными заявки
     * @return bool
     */
    public function saveBid(array $bidData);
}