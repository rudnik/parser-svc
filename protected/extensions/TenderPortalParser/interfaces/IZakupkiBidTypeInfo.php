<?php
/**
 * Интерфейс объекта, содержащего информацию о типе заявки портала госуслуг.
 * Информация нужна для парсинга и сохранения заявок.
 */
interface IZakupkiBidTypeInfo
{
    /**
     * Возвращает Список имен уникальных полей документа.
     * @return array
     */
    public function getUniqueFields();

    /**
     * Возвращает ассоциативный массив, используемый для определения региона заявки.
     * Элемент массив имеет вид: 'название_региональной_папки_на_ftp' => id_региона_в_БД
     * @return array
     */
    public function getRegions();

    /**
     * Возвращает регулярное выражение, при помощи которого вычисляется имя региона по имени zip архива или xml документа.
     * @return string
     */
    public function getRegionDetectRegexp();

    /**
     * Возвращает список типов документов, отменяющих закупку
     * @return array
     */
    public function getCancelDoctypeNames();

    /**
     * Возвращает список полей, которые не обновляются при парсинге.
     * @return array
     */
    public function getNotUpdatedFields();

    /**
     * Список игнорируемых тэгов в xml документе. (парсер разбирает данные тэгов, вложенных в игнорируемые)
     * @return array
     */
    public function getIgnoredTags();

    /**
     * Заполняет модель записи поисковой таблицы по данным заявки.
     * @param Bids $bid экземпляр сохраненной заявки
     * @param string $fieldName название поля заявки, по которому определяется, как и куда сохранить значение заявки в результирующем объекте
     * @param BidsDataSearch $searchData результирующий наполняемый объект
     * @param array $valueArray массив значений поля
     * @return bool
     */
    public function setSearchInfo(Bids $bid, $fieldName, BidsDataSearch $searchData, $valueArray);
}