<?php
/**
 * Интерфейс журналируемого объекта
 */
interface ILoggable
{
    /**
     * Возвращает логгер
     * @return IParserLogger
     */
    public function getLogger();

    /**
     * Устанавливает логгер
     * @param IParserLogger $logger
     * @return mixed
     */
    public function setLogger(IParserLogger $logger);
}