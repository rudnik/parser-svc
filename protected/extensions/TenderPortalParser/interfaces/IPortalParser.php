<?php
/**
 * Интерфейс парсера порталов
 */
interface IPortalParser
{
    /**
     * Конструктор.
     * @param IPortalFilesDownloadStrategy $downloadStrategy стратегия загрузки файлов
     * @param IPortalParseStrategy $parseStrategy стратегия парсинга файлов
     */
    public function __construct(IPortalFilesDownloadStrategy $downloadStrategy, IPortalParseStrategy $parseStrategy);

    /**
     * Скачивает файлы при помощи стратегии загрузки. Именно стратегия загрузки знает что, откуда загружать и куда сохранять.
     * @param mixed $period см. конкретную реализацию
     * @param null $number
     * @return void
     */
    public function saveFiles($period, $number = null);

    /**
     * Парсит файлы при помощи стратегии парсинга файлов. Именно стратегия парсинга занет, где лежат файлы для парсинга,
     * как их парсить и куда сохранять.
     * @param bool $removeFiles удалять ли скачанные файлы после парсинга
     * @return void
     */
    public function parse($removeFiles = true);

    /**
     * Возвращает стратегию загрузки файлов
     * @return IPortalFilesDownloadStrategy
     */
    public function getDownloadStrategy();

    /**
     * Устанавливает стратегию загрузки файлов
     * @param IPortalFilesDownloadStrategy $downloadStrategy стратегия загрузки
     * @return void
     */
    public function setDownloadStrategy(IPortalFilesDownloadStrategy $downloadStrategy);

    /**
     * Возвращает стратегию парсинга файлов
     * @return IPortalParseStrategy
     */
    public function getParseStrategy();

    /**
     * Устанавливает стратегию парсинга файлов
     * @param IPortalParseStrategy $parseStrategy стратегия парсинга
     * @return void
     */
    public function setParseStrategy(IPortalParseStrategy $parseStrategy);

}