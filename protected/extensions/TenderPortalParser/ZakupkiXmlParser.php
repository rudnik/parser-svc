<?php

/**
 * Механизм парсинга xml-файлов портала zakupki.gov.ru
 */
class ZakupkiXmlParser implements IReportable, ILoggable
{
    /**
     * XML-парсер
     * @var resource
     */
    protected $parser = null;

    /**
     * Название типа разбираемого документа
     * @var string
     */
    protected $docType = null;

    /**
     * Массив с разобранными данными
     * @var array
     */
    protected $data = [];

    /**
     * Цепочка тэгов, по которой строится ключ данных в массиве разбираемого документа
     * @var array
     */
    protected $tagPath = [];

    /**
     * Накапливает данные разбираемого в данный момент тэга
     * @var string
     */
    protected $currentTagData = '';

    /**
     * Массив документов, разобранных в рамках текущего разбираемого xml-файла
     * @var array
     */
    protected $bids = [];

    /**
     * Содержит имя неизвестного типа документов. Данные соответствующего тега (и вложенных в него) не парсятся.
     * @var string
     */
    protected $skippingDocType  = null;

    /**
     * Список ключей, определяющий, какой тэг в документе содержит уникальный идентификатор данных документа
     * @var array
     */
    protected $uniqueFields = [];

    protected $ignoreTags = [];

    /**
     * Класс, формирующий отчет
     * @var IEntityProcessReportBuilder
     */
    protected $reportBuilder = null;

    /**
     * логгер
     * @var IParserLogger
     */
    protected $logger = null;

    public function getLogger()
    {
        return $this->logger;
    }

    public function setLogger(IParserLogger $logger)
    {
        $this->logger = $logger;
    }

    public function setReportBuilder(IEntityProcessReportBuilder $reportBuilder)
    {
        $this->reportBuilder = $reportBuilder;
    }

    public function getReportBuilder()
    {
        return $this->reportBuilder;
    }

    /**
     * Конструктор
     * @param array $uniqueFields - Список ключей, определяющий, какой тэг в документе содержит уникальный идентификатор данных документа
     * $params['reportBuilder'] - класс, формирующий отчет (IEntityProcessReportBuilder)
     * @param array $ignoreTags - Список игнорируемых тэгов. (парсер разбирает данные тэгов, вложенных в игнорируемые)
     */
    public function __construct(array $uniqueFields, array $ignoreTags = [])
    {
        $this->parser = xml_parser_create();
        xml_parser_set_option($this->parser, XML_OPTION_CASE_FOLDING, 0);
        xml_set_object($this->parser, $this);
        xml_set_element_handler($this->parser, 'tagOpen', 'tagClose');
        xml_set_character_data_handler($this->parser, 'tagData');

        $this->uniqueFields = $uniqueFields;
        $this->ignoreTags = $ignoreTags;
    }

    /**
     * Возвращает массив документов, разобранных в рамках текущего разбираемого xml-файла
     * @return array
     */
    public function getBidsData()
    {
        return $this->bids;
    }

    /**
     * Разбирает xml-файл
     * @param string $filePath - путь к xml-файлу, который нужно разобрать
     * @return bool
     */
    public function parse($filePath)
    {
        if (!($filePtr = @fopen($filePath, 'r'))) {
            Yii::log('Не удалось открыть файл. ' . $filePath, CLogger::LEVEL_ERROR, 'TendersParser');
            return false;
        }

        while (($data = fread($filePtr, 5242880))) {
            if (!xml_parse($this->parser, $data, feof($filePtr))) {
                $error = xml_get_error_code($this->parser);
                if ($error) {
                    if ($this->reportBuilder !== null) {
                        $this->reportBuilder->addFail();
                    }
                    $this->logger->log('XML parse error ' . $error, CLogger::LEVEL_WARNING);
                    xml_parser_free($this->parser);
                    fclose($filePtr);
                    $this->bids = [];
                    return;
                }
            }
        }

        xml_parser_free($this->parser);
        fclose($filePtr);
    }

    /**
     * Обработчик открывающегося тэга
     * @param resource $parser - экземпляр парсера
     * @param string $tag - название тэга
     * @param array $attributes - атрибуты тэга
     */
    private function tagOpen($parser, $tag, $attributes)
    {
        $tag = static::cleanTagName($tag);

        if (in_array($tag, $this->ignoreTags)) {
            return;
        }

        if (!is_null($this->skippingDocType)) {
            return;
        }

        # если тип документа не установлен - первый тэг идентифицирует тип документа
        if (is_null($this->docType)) {
            $this->docType = $tag;

            if ($this->reportBuilder !== null) {
                $this->reportBuilder->addTotalCount();
            }

            /* @var $doctypeModel BidsDoctypes[] */
            $doctypeModel = BidsDoctypes::model()->find('name = :name', [':name' => $this->docType]);

            # если тип документа не найден
            if (is_null($doctypeModel)) {
                $this->docType = null;
                # если пропускаемый тег отсутствует
                if (is_null($this->skippingDocType)) {
                    if ($this->reportBuilder !== null) {
                        $this->reportBuilder->addFail();
                    }
                    $this->skippingDocType = $tag;
                    $this->logger->log('Неизвестный тип документа (' . $tag . ').', CLogger::LEVEL_ERROR);
                }

                return;
            }

            $this->data['doctypeId'] = $doctypeModel->doctypeId;
            $this->data['portalId'] = $doctypeModel->portalId;
        }

        # помечаем уникальный идентификатор заявки
        $dataKey = implode('/', $this->tagPath) . "/$tag";
        if (Utils::valueBelongsToSet($dataKey, $this->uniqueFields)) {
            $tag .= '*';
        }

        $this->tagPath[] = $tag;
    }

    /**
     * Обработчик содержимого тэга
     * @param resource $parser - экземпляр парсера
     * @param string $data - данные тэга
     */
    private function tagData($parser, $data)
    {
        if (is_null($this->docType)) {
            return;
        }

        $this->currentTagData .= $data;
    }

    /**
     * Обработчик закрывающегося тэга
     * @param resource $parser - экземпляр парсера
     * @param string $tag - название тэга
     */
    private function tagClose($parser, $tag)
    {
        # Очищаем название тэга от префикса пространства имен
        $tag = static::cleanTagName($tag);

        if (in_array($tag, $this->ignoreTags)) {
            $this->currentTagData = '';
            return;
        }

        if (!is_null($this->skippingDocType) && $this->skippingDocType == $tag) {
            $this->skippingDocType = null;
            return;
        }

        $this->currentTagData = trim($this->currentTagData);

        # Перед проверкой зачищаем уникальное поле от звездочки
        $dataKey = implode('/', $this->tagPath);

        # Закрывается тэг типа документа
        if ($this->docType == $tag) {
            $this->bids[] = $this->data;
            $this->data = [];
            $this->tagPath = [];
            $this->currentTagData = '';
            $this->docType = null;

            if ($this->reportBuilder !== null) {
                $this->reportBuilder->addSuccess();
            }
        } /**
         * Закрывается тэг, вложенный в тэг типа документа
         * Если в закрываемом теге есть данные -
         * записываем данные тэга в массив данных разбираемого документа
         */ elseif (!empty($this->currentTagData)) {
            # Если ключа еще нет в списке - создаем его и записываем данные
            if (!isset($this->data[$dataKey])) {
                $this->data[$dataKey] = [trim($this->currentTagData)];
            } # Если ключ уже есть в списке - добавляем данные
            else {
                $this->data[$dataKey][] = trim($this->currentTagData);
            }
        }

        # Очищаем переменную с содержимым закрываемого тэга
        $this->currentTagData = '';
        # Удаляем тэг из массива, формирующего ключ данных массива разбираемого документа
        array_pop($this->tagPath);
    }

    /**
     * Очищает имя тэга от идентификатора пространства имен
     * @param string $tagName - имя тэга
     * @return string
     */
    protected static function cleanTagName($tagName)
    {
        $tagNameParts = explode(':', $tagName);
        return trim(end($tagNameParts));
    }

}