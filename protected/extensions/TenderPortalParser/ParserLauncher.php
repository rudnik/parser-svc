<?php
/**
 * Класс запуска парсера.
 *
 * Запускает прасеры различных источников по параметрам.
 *
 * @package TendersParser
 * @subpackage launcher
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013 BSTsoft
 */
final class ParserLauncher
{
    /**
     * Вызывать исключения в случае ошибок парсинга.
     *
     * @var bool
     */
    public $useException = true;

    /**
     * Режим отладки.
     *
     * позволяет выводить справочную информацию в поток вывода.
     *
     * @var bool
     */
    public $debug = false;

    /**
     * Подключение к БД.
     *
     * @var CDbConnection
     */
    private $db = null;

    /**
     * Список порталов.
     *
     * @var IPortalParser[]
     */
    private $portalParsers = null;


    /**
     * Инициализация.
     *
     * @param string $portal Имя портала для парсинга. По умолчанию все.
     */
    public function __construct($portal = null)
    {

    }

    /**
     * Готовность к запуску.
     *
     * @return bool
     */
    public function isReady()
    {
        return true;
    }

    /**
     * Получение списка парсеров порталов.
     *
     * @return array Массив экземпляров
     */
    public function getPortalParsers($portalName = null)
    {
        $portalCondition = is_null($portalName) ? '' : ['condition' => 'name = :portalName', 'params' => [':portalName' => $portalName]];
        $portalsList = Portals::model()->findAll($portalCondition);
        foreach ($portalsList as $portal) {
            $portalParser = ParserBuilder::factory($portal->name)->build();
            $this->portalParsers[] = $portalParser;
        }

        return $this->portalParsers;
    }

    /**
     * Загрузка и сохранение файлов порталов.
     *
     * @param string $period Период, за который сохранять файлы:
     * <ul>
     * 	<li>day. За последний день;</li>
     * 	<li>month. За последний месяц;</li>
     * </ul>
     * @param int $periodNum номер дня или месяца
     * @param bool $sendReportEmail отправлять ли отчеты по загрузке на email разработчиков
     * @return bool
     */
    public function save($period = 'day', $periodNum = 0, $sendReportEmail = false)
    {
        foreach ($this->portalParsers as $portal) {
            $portal->saveFiles($period, $periodNum);
            $downloadStrategy = $portal->getDownloadStrategy();

            if (!$downloadStrategy instanceof IReportable) {
                continue;
            }

            $reportBuilder = $downloadStrategy->getReportBuilder();

            if ($reportBuilder === null) {
                continue;
            }

            # выводим на дисплей и отправляем на email отчет, если стратегия загрузки поддерживает отчеты
            $reportString = $reportBuilder->getReport();
            echo $reportString;

            if ($sendReportEmail) {
                mail('n.rudakov@itsirius.ru', 'Parser report ' . date("Y-m-d H:i"), $reportString);
            }
        }
        return true;
    }

    /**
     * Запуск парсинга порталов.
     *
     * @param bool $rmFiles Удалять архивы после распаковки.
     * @param bool $sendReportEmail отправлять ли отчеты по парсингу на email разработчиков
     * @return bool
     */
    public function parse($rmFiles = false, $sendReportEmail = false)
    {
        foreach ($this->portalParsers as $portal) {
            $portal->parse($rmFiles);
            $parseStrategy = $portal->getParseStrategy();

            if (!$parseStrategy instanceof IReportable) {
                continue;
            }

            $reportBuilder = $parseStrategy->getReportBuilder();

            if ($reportBuilder === null) {
                continue;
            }

            # выводим на дисплей и отправляем на email отчет, если стратегия парсинга поддерживает отчеты
            $reportString = $reportBuilder->getReport();
            echo $reportString;

            if ($sendReportEmail) {
                mail('n.rudakov@itsirius.ru', 'Parser report ' . date("Y-m-d H:i"), $reportString);
            }
        }

        return true;
    }

}
