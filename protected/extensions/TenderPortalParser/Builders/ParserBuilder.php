<?php

/**
 * Абстрактный строитель парсеров.
 * При помощи абстрактной фабрики возвращает конкретного строителя, который и строит нужный парсер.
 *
 * Пример: $parser223 = ParserBuilder::factory('zakupki223')->build();
 */
abstract class ParserBuilder implements IParserBuilder
{
    /**
     * Права на директорию по умолчанию.
     *
     * @var string
     */
    const DIR_PERMS = 0775;

    /**
     * Папка, в которой будут лежать файлы для парсинга. В эту папку стратегия загрузки сохраняет файлы, а стратегия
     * парсинга берет в этой папке файлы и парсит.
     *
     * @var string
     */
    protected $exchangeDir;

    /**
     * Инициализация.
     */
    public function __construct()
    {
        if (!file_exists($this->exchangeDir)) {
            mkdir($this->exchangeDir, self::DIR_PERMS, true);
        }
    }

    /**
     * Фабрика строителей
     * @param $name имя строителя парсера. Для парсеров портала закупок совпадает с именем портала (Например, zakupki44).
     * @return IParserBuilder
     */
    public static function factory($name)
    {
        $builderName = ucfirst($name) . 'ParserBuilder';
        $builder = new $builderName();

        if (!$builder instanceof IParserBuilder) {
            return null;
        }

        return $builder;
    }

    /**
     * Строит и возвращает стратегию загрузки
     * @return IPortalFilesDownloadStrategy
     */
    abstract protected function getDownloadStrategy();

    /**
     * Строит и возвращает стратегию парсинга
     * @return IPortalParseStrategy
     */
    abstract protected function getParseStrategy();

    /**
     * Строит парсер.
     * @return PortalParser
     */
    public function build()
    {
        $downloadStrategy = $this->getDownloadStrategy();
        $parseStrategy = $this->getParseStrategy();
        return new PortalParser($downloadStrategy, $parseStrategy);
    }
}
