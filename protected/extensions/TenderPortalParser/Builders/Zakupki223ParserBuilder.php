<?php

/**
 * Строитель парсера портала гос.закупок для 223ФЗ.
 */
class Zakupki223ParserBuilder extends ParserBuilder
{
    /**
     * Конструктор
     */
    public function __construct()
    {
        $this->exchangeDir = Yii::getPathOfAlias('application.data.tendersparser.zakupki223');
        parent::__construct();
    }

    /**
     * Строит стратегию загрузки 223ФЗ
     * @return IPortalFilesDownloadStrategy
     */
    protected function getDownloadStrategy()
    {
        $downloadStrategy = new Zakupki223DownloadStrategy();

        # устанавливаем папку для сохранения файлов
        $downloadStrategy->setOutputDir($this->exchangeDir);

        # устанавливаем построитель отчетов, если стратегия поддерживает отчеты
        if ($downloadStrategy instanceof IReportable) {
            $reportBuilder = new EntityProcessReportBuilder();
            $reportBuilder->template  = "Downloading files 223FZ\n";
            $reportBuilder->template .= "Total: {total}, Success: {success}, Fail: {fail}\n\n";

            $downloadStrategy->setReportBuilder($reportBuilder);
        }

        # устанавливаем логгер, если стратегия поддерживает журналирование
        if ($downloadStrategy instanceof ILoggable) {
            $downloadStrategy->setLogger(new ParserLogger());
        }

        return $downloadStrategy;
    }

    /**
     * Строит стратегию парсинга 223ФЗ
     * @return IPortalParseStrategy
     */
    protected function getParseStrategy()
    {
        $parserLogger = new ParserLogger();

        # информация о заявках 223ФЗ
        $bidTypeInfo = new Zakupki223BidsTypeInfo();

        # механизм сохранения заявок
        $bidStorage = new ZakupkiBidStorage();
        $bidStorage->setBidTypeInfo($bidTypeInfo);
        $bidStorage->setLogger($parserLogger);

        # парсер xml-файлов с информацией о заявках
        $parseStrategy = new ZakupkiParseStrategy($bidStorage);
        # каталог, в котором хранятся скачанные zip-архивы
        $parseStrategy->setInputDir($this->exchangeDir);
        $parseStrategy->setBidTypeInfo($bidTypeInfo);

        # устанавливаем построитель отчетов, если стратегия поддерживает отчеты
        if ($parseStrategy instanceof IReportable) {
            $reportBuilder = new EntityProcessReportBuilder();
            $reportBuilder->template  = "Parsing files 223FZ\n";
            $reportBuilder->template .= "Total: {total}, Success: {success}, Fail: {fail}\n\n";

            $parseStrategy->setReportBuilder($reportBuilder);
        }

        # устанавливаем логгер, если стратегия поддерживает журналирование
        if ($parseStrategy instanceof ILoggable) {
            $parseStrategy->setLogger($parserLogger);
        }

        return $parseStrategy;
    }
}
