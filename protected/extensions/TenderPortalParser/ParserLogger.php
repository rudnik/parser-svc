<?php
/**
 * Логгер парсера.
 */
class ParserLogger implements IParserLogger
{
    /**
     * Записывает в БД или в файл сообщение с трассировкой вызова.
     * @param string $message  сообщение
     * @param string $level  уровень сообщения. Один из уровней описанный в CLogger::
     * @return void
     */
    public function log($message, $level = CLogger::LEVEL_INFO)
    {
        $trace = debug_backtrace();
        $trace_text = [];
        foreach ($trace as $i => $call) {
            if (isset($call['object']) && is_object($call['object'])) {
                $call['object'] = 'CONVERTED OBJECT OF CLASS ' . get_class($call['object']);
            }
            if (is_array($call['args'])) {
                foreach ($call['args'] as &$arg) {
                    if (is_object($arg)) {
                        $arg = 'CONVERTED OBJECT OF CLASS ' . get_class($arg);
                    } elseif (is_array($arg)) {
                        $items = '';
                        foreach ($arg as $item) {
                            if (is_array($item)) {
                                $items .= implode('|', $item);
                            } else {
                                $items .= '|' . $item;
                            }
                        }
                        $arg = $items;
                    }
                }
            }

            $trace_text[$i] = '#' . $i . ' ';
            $trace_text[$i] .= isset($call['file']) ? $call['file'] : '';
            $trace_text[$i] .= isset($call['line']) ? '(' . $call['line'] . ') ' : '';
            $trace_text[$i] .= isset($call['object']) ? $call['object'] . $call['type'] : '';
            $trace_text[$i] .= $call['function'] . '(' . implode(', ', $call['args']) . ')';
        }

        Yii::log($message . PHP_EOL . PHP_EOL . implode(PHP_EOL, $trace_text), $level, 'TendersParser');
    }
}