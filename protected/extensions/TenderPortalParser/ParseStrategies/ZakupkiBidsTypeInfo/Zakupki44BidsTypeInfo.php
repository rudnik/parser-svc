<?php
/**
 * Класс с информацией о заявках по 44ФЗ на сайте госзакупок
 */
class Zakupki44BidsTypeInfo extends ZakupkiBidsTypeInfo
{
    public function getUniqueFields()
    {
        return [
            'purchaseNumber',
        ];
    }

    public function getRegions()
    {
        static $regionsIds = null;

        if ($regionsIds !== null) {
            return $regionsIds;
        }

        $regionsRecords = Yii::app()->db->createCommand()
            ->select(['regionId', 'title'])
            ->from('regions')
            ->where('parentId!=0 OR title=:title', [':title' => 'Байконур'])
            ->queryAll();

        $regionsArray = [];
        foreach ($regionsRecords as $region) {
            $regionsArray[$region['regionId']] = $region['title'];
        }

        $regions = array_flip($regionsArray);
        $regionsIds = [
            'Adygeja_Resp' => $regions['Республика Адыгея'],
            'Altaj_Resp' => $regions['Республика Алтай'],
            'Altajskij_kraj' => $regions['Алтайский край'],
            'Amurskaja_obl' => $regions['Амурская область'],
            'Arkhangelskaja_obl' => $regions['Архангельская область'],
            'Astrakhanskaja_obl' => $regions['Астраханская область'],
            'Bajkonur_g' => $regions['Байконур'],
            'Bashkortostan_Resp' => $regions['Республика Башкортостан'],
            'Belgorodskaja_obl' => $regions['Белгородская область'],
            'Brjanskaja_obl' => $regions['Брянская область'],
            'Burjatija_Resp' => $regions['Республика Бурятия'],
            'Chechenskaja_Resp' => $regions['Чеченская Республика'],
            'Cheljabinskaja_obl' => $regions['Челябинская область'],
            'Chukotskij_AO' => $regions['Чукотский автономный округ'],
            'Chuvashskaja_Resp' => $regions['Чувашская Республика'],
            'Dagestan_Resp' => $regions['Республика Дагестан'],
            'Evrejskaja_Aobl' => $regions['Еврейская автономная область'],
            'Ingushetija_Resp' => $regions['Республика Ингушетия'],
            'Irkutskaja_obl' => $regions['Иркутская область'],
            'Irkutskaja_obl_Ust-Ordynskij_Burjatskij_okrug' => $regions['Иркутская область'],
            'Ivanovskaja_obl' => $regions['Ивановская область'],
            'Jamalo-Neneckij_AO' => $regions['Ямало-Ненецкий автономный округ'],
            'Jaroslavskaja_obl' => $regions['Ярославская область'],
            'Kabardino-Balkarskaja_Resp' => $regions['Кабардино-Балкарская Республика'],
            'Kaliningradskaja_obl' => $regions['Калининградская область'],
            'Kalmykija_Resp' => $regions['Республика Калмыкия'],
            'Kaluzhskaja_obl' => $regions['Калужская область'],
            'Kamchatskij_kraj' => $regions['Камчатский край'],
            'Karachaevo-Cherkesskaja_Resp' => $regions['Карачаево-Черкесская Республика'],
            'Karelija_Resp' => $regions['Республика Карелия'],
            'Kemerovskaja_obl' => $regions['Кемеровская область'],
            'Khabarovskij_kraj' => $regions['Хабаровский край'],
            'Khakasija_Resp' => $regions['Республика Хакасия'],
            'Khanty-Mansijskij_AO-Jugra_AO' => $regions['Ханты-Мансийский автономный округ — Югра'],
            'Kirovskaja_obl' => $regions['Кировская область'],
            'Komi_Resp' => $regions['Республика Коми'],
            'Kostromskaja_obl' => $regions['Костромская область'],
            'Krasnodarskij_kraj' => $regions['Краснодарский край'],
            'Krasnojarskij_kraj' => $regions['Красноярский край'],
            'Krim_Resp' => $regions['Республика Крым'],
            'Kurganskaja_obl' => $regions['Курганская область'],
            'Kurskaja_obl' => $regions['Курская область'],
            'Leningradskaja_obl' => $regions['Ленинградская область'],
            'Lipeckaja_obl' => $regions['Липецкая область'],
            'Magadanskaja_obl' => $regions['Магаданская область'],
            'Marij_El_Resp' => $regions['Республика Марий Эл'],
            'Mordovija_Resp' => $regions['Республика Мордовия'],
            'Moskovskaja_obl' => $regions['Московская область'],
            'Moskva' => $regions['Город Москва'],
            'Murmanskaja_obl' => $regions['Мурманская область'],
            'Neneckij_AO' => $regions['Ненецкий автономный округ'],
            'Nizhegorodskaja_obl' => $regions['Нижегородская область'],
            'Novgorodskaja_obl' => $regions['Новгородская область'],
            'Novosibirskaja_obl' => $regions['Новосибирская область'],
            'Omskaja_obl' => $regions['Омская область'],
            'Orenburgskaja_obl' => $regions['Оренбургская область'],
            'Orlovskaja_obl' => $regions['Орловская область'],
            'Penzenskaja_obl' => $regions['Пензенская область'],
            'Permskij_kraj' => $regions['Пермский край'],
            'Primorskij_kraj' => $regions['Приморский край'],
            'Pskovskaja_obl' => $regions['Псковская область'],
            'Rjazanskaja_obl' => $regions['Рязанская область'],
            'Rostovskaja_obl' => $regions['Ростовская область'],
            'Sakha_Jakutija_Resp' => $regions['Республика Саха (Якутия)'],
            'Sakhalinskaja_obl' => $regions['Сахалинская область'],
            'Samarskaja_obl' => $regions['Самарская область'],
            'Sankt-Peterburg' => $regions['Санкт-Петербург'],
            'Saratovskaja_obl' => $regions['Саратовская область'],
            'Sevastopol_g' => $regions['Город Севастополь'],
            'Severnaja_Osetija-Alanija_Resp' => $regions['Республика Северная Осетия — Алания'],
            'Smolenskaja_obl' => $regions['Смоленская область'],
            'Stavropolskij_kraj' => $regions['Ставропольский край'],
            'Sverdlovskaja_obl' => $regions['Свердловская область'],
            'Tambovskaja_obl' => $regions['Тамбовская область'],
            'Tatarstan_Resp' => $regions['Республика Татарстан'],
            'Tjumenskaja_obl' => $regions['Тюменская область'],
            'Tomskaja_obl' => $regions['Томская область'],
            'Tulskaja_obl' => $regions['Тульская область'],
            'Tverskaja_obl' => $regions['Тверская область'],
            'Tyva_Resp' => $regions['Республика Тыва'],
            'Udmurtskaja_Resp' => $regions['Удмуртская Республика'],
            'Uljanovskaja_obl' => $regions['Ульяновская область'],
            'Vladimirskaja_obl' => $regions['Владимирская область'],
            'Volgogradskaja_obl' => $regions['Волгоградская область'],
            'Vologodskaja_obl' => $regions['Вологодская область'],
            'Voronezhskaja_obl' => $regions['Воронежская область'],
            'Zabajkalskij_kraj' => $regions['Забайкальский край'],
            'Zabajkalskij_kraj_Aginskij_Burjatskij_okrug' => $regions['Забайкальский край'],
        ];

        return $regionsIds;
    }

    public function getRegionDetectRegexp()
    {
        return '/[a-zA-Z0-9]+_([a-zA-Z_-]+)_\d+_\d+_\d+.xml/';
    }

    public function getCancelDoctypeNames()
    {
        return [
            'fcsPurchaseDocumentCancel',
        ];
    }

    public function getNotUpdatedFields()
    {
        return [
            'docPublishDate',
        ];
    }

    public function setSearchInfo(Bids $bid, $fieldName, BidsDataSearch $searchData, $valueArray)
    {
        $value = array_pop($valueArray);

        if ($value instanceof BidsDataValues) {
            $value = trim($value->value);
        }
        else {
            $value = trim($value);
        }

        if (empty($value)) {
            return;
        }

        # title
        $fieldsArray = [
            'purchaseObjectInfo',
        ];

        if (Utils::valueBelongsToSet($fieldName, $fieldsArray)) {
            $searchData->title .= $value.'; ';
            return;
        }


        # description
        $fieldsArray = [
            'purchaseObject/name',
        ];

        if (Utils::valueBelongsToSet($fieldName, $fieldsArray)) {
            $searchData->description = $value;
            return;
        }

        # max_cost
        $fieldsArray = [
            'lot/maxPrice',
        ];

        if (Utils::valueBelongsToSet($fieldName, $fieldsArray)) {
            $searchData->max_cost = $value;
            return;
        }

        # publication_date
        $fieldsArray = [
            'docPublishDate'
        ];

        if (Utils::valueBelongsToSet($fieldName, $fieldsArray)) {
            $date = new DateTime($value);
            $searchData->publication_date = (int) $date->getTimestamp();
            return;
        }

        # quote_date_end
        $fieldsArray = [
            'stageOne/collecting/endDate',
            'collecting/endDate',
        ];

        if (Utils::valueBelongsToSet($fieldName, $fieldsArray)) {
            $date = new DateTime($value);
            $searchData->quote_date_end = (int) $date->getTimestamp();
            return;
        }

        # inn
        $fieldsArray = [
            'responsibleOrg/INN',
        ];

        if (Utils::valueBelongsToSet($fieldName, $fieldsArray)) {
            $searchData->inn = $value;
            return;
        }
    }

    public function getIgnoredTags()
    {
        return [
            'export',
        ];
    }
}
