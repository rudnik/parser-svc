<?php
/**
 * Базовый класс с информацией о заявках на сайте госзакупок
 */
abstract class ZakupkiBidsTypeInfo implements IZakupkiBidTypeInfo
{
    /**
     * Возвращает класс с информацией о типе документа закупки
     * @param $portalName - имя портала на госзакупках
     * @return IZakupkiBidTypeInfo
     */
    public static function factory($portalName) {
        $className = ucfirst($portalName) . 'BidsTypeInfo';
        $classInstance =  new $className;

        if (!$classInstance instanceof IZakupkiBidTypeInfo) {
            return null;
        }

        return $classInstance;
    }
}