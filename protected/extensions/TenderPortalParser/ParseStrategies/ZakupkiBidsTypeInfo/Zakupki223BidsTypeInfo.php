<?php
/**
 * Класс с информацией о заявках по 223ФЗ на сайте госзакупок
 */
class Zakupki223BidsTypeInfo  extends ZakupkiBidsTypeInfo
{
    public function getUniqueFields()
    {
        return [
            'registrationNumber',
            'cancelNoticeRegistrationNumber',
        ];
    }

    public function getRegions()
    {
        static $regionsIds = null;

        if ($regionsIds !== null) {
            return $regionsIds;
        }

        $regionsRecords = Yii::app()->db->createCommand()
            ->select(['regionId', 'title'])
            ->from('regions')
            ->where('parentId!=0 OR title=:title', [':title' => 'Байконур'])
            ->queryAll();

        $regionsArray = [];
        foreach ($regionsRecords as $region) {
            $regionsArray[$region['regionId']] = $region['title'];
        }

        $regions = array_flip($regionsArray);
        $regionsIds = [
            'Adygeya_Resp' => $regions['Республика Адыгея'],
            'Altay_Resp' => $regions['Республика Алтай'],
            'Altayskii__krai' => $regions['Алтайский край'],
            'Amurskaya_obl' => $regions['Амурская область'],
            'Arhangelskaya_obl' => $regions['Архангельская область'],
            'Astrahanskaya_obl' => $regions['Астраханская область'],
            'Baikonur_g' => $regions['Байконур'],
            'Bashkortostan_Resp' => $regions['Республика Башкортостан'],
            'Belgorodskaya_obl' => $regions['Белгородская область'],
            'Brianskaya_obl' => $regions['Брянская область'],
            'Buryatiya_Resp' => $regions['Республика Бурятия'],
            'Chechenskaya_Resp' => $regions['Чеченская Республика'],
            'Cheliabinskaya_obl' => $regions['Челябинская область'],
            'Chukotskii_AO' => $regions['Чукотский автономный округ'],
            'Chuvashskaya_Respublika' => $regions['Чувашская Республика'],
            'Dagestan_Resp' => $regions['Республика Дагестан'],
            'Evreiskaya_Aobl' => $regions['Еврейская автономная область'],
            'Habarovskii_krai' => $regions['Хабаровский край'],
            'Hakasiia_Resp' => $regions['Республика Хакасия'],
            'Hanty-Mansiiskii_AO_Iugra_AO' => $regions['Ханты-Мансийский автономный округ — Югра'],
            'Ingushetiya_Resp' => $regions['Республика Ингушетия'],
            'Irkutskaya_obl' => $regions['Иркутская область'],
            'Irkutskaya_obl_Ust-Ordynskii_Buriatskii_okrug' => 1,
            'Ivanowskaya_obl' => $regions['Иркутская область'],
            'Jamalo-Nenetckii_AO' => $regions['Ямало-Ненецкий автономный округ'],
            'Jaroslavskaya_obl' => $regions['Ярославская область'],
            'Kabardino-Balkarskaya_Resp' => $regions['Кабардино-Балкарская Республика'],
            'Kaliningradskaya_obl' => $regions['Калининградская область'],
            'Kalmykiya_Resp' => $regions['Республика Калмыкия'],
            'Kaluzhskaya_obl' => $regions['Калужская область'],
            'Kamchatskii_krai' => $regions['Камчатский край'],
            'Karachaevo-Cherkesskaya_Resp' => $regions['Карачаево-Черкесская Республика'],
            'Kareliya_Resp' => $regions['Республика Карелия'],
            'Kemerowskaya_obl' => $regions['Кемеровская область'],
            'Kirowskaya_obl' => $regions['Кировская область'],
            'Komi_Resp' => $regions['Республика Коми'],
            'Kostromskaya_obl' => $regions['Костромская область'],
            'Krasnodarskii_krai' => $regions['Краснодарский край'],
            'Krasnoyarskii_krai' => $regions['Красноярский край'],
            'Krym_Resp' => $regions['Республика Крым'],
            'Kurganskaya_obl' => $regions['Курганская область'],
            'Kurskaya_obl' => $regions['Курская область'],
            'Leningradskaya_obl' => $regions['Ленинградская область'],
            'Lipetckaya_obl' => $regions['Липецкая область'],
            'Magadanskaya_obl' => $regions['Магаданская область'],
            'Marii_El_Resp' => $regions['Республика Марий Эл'],
            'Mordoviya_Resp' => $regions['Республика Мордовия'],
            'Moskovskaya_obl' => $regions['Московская область'],
            'Moskva' => $regions['Город Москва'],
            'Murmanskaya_obl' => $regions['Мурманская область'],
            'Nenetckii_AO' => $regions['Ненецкий автономный округ'],
            'Nizhegorodskaya_obl' => $regions['Нижегородская область'],
            'Novgorodskaya_obl' => $regions['Новгородская область'],
            'Novosibirskaya_obl' => $regions['Новосибирская область'],
            'Omskaya_obl' => $regions['Омская область'],
            'Orenburgskaya_obl' => $regions['Оренбургская область'],
            'Orlovskaya_obl' => $regions['Орловская область'],
            'Penzenskaya_obl' => $regions['Пензенская область'],
            'Permskii_krai' => $regions['Пермский край'],
            'Primorskii_krai' => $regions['Приморский край'],
            'Pskovskaya_obl' => $regions['Псковская область'],
            'Rostovskaya_obl' => $regions['Ростовская область'],
            'Ryazanskaya_obl' => $regions['Рязанская область'],
            'Saha_Jakutiya_Resp' => $regions['Республика Саха (Якутия)'],
            'Sahalinskaya_obl' => $regions['Сахалинская область'],
            'Samarskaya_obl' => $regions['Самарская область'],
            'Sankt-Peterburg' => $regions['Санкт-Петербург'],
            'Saratovskaya_obl' => $regions['Саратовская область'],
            'Sevastopol' => $regions['Город Севастополь'],
            'Severnaia_Osetiya_Alaniia_Resp' => $regions['Республика Северная Осетия — Алания'],
            'Smolenskaya_obl' => $regions['Смоленская область'],
            'Stavropolskii_krai' => $regions['Ставропольский край'],
            'Sverdlovskaya_obl' => $regions['Свердловская область'],
            'Tambovskaya_obl' => $regions['Тамбовская область'],
            'Tatarstan_Resp' => $regions['Республика Татарстан'],
            'Tiumenskaya_obl' => $regions['Тюменская область'],
            'Tomskaya_obl' => $regions['Томская область'],
            'Tulskaya_obl' => $regions['Тульская область'],
            'Tverskaya_obl' => $regions['Тверская область'],
            'Tyva_Resp' => $regions['Республика Тыва'],
            'Udmurtskaya_Resp' => $regions['Удмуртская Республика'],
            'Ulianovskaya_obl' => $regions['Ульяновская область'],
            'Vladimirskaya_obl' => $regions['Владимирская область'],
            'Volgogradskaya_obl' => $regions['Волгоградская область'],
            'Vologodskaya_obl' => $regions['Вологодская область'],
            'Voronezhskaya_obl' => $regions['Воронежская область'],
            'Zabaikalskii_krai' => $regions['Забайкальский край'],
            'Zabaikalskii_krai_Aginskii_Buriatskii_okrug' => $regions['Забайкальский край'],
        ];

        return $regionsIds;
    }

    public function getRegionDetectRegexp()
    {
        return '/[a-zA-Z0-9]+_([a-zA-Z_-]+)_\d+_\d+_.*(daily|full).*.xml/';
    }

    public function getCancelDoctypeNames()
    {
        return [
            'purchaseRejection',
        ];
    }

    public function getNotUpdatedFields()
    {
        return [
            'purchaseNoticeData/createDateTime',
            'purchaseNoticeOAData/createDateTime',
            'purchaseNoticeOKData/createDateTime',
            'purchaseNoticeAEData/createDateTime',
            'purchaseNoticeZKData/createDateTime',
            'purchaseNoticeEPData/createDateTime',
        ];
    }

    public function setSearchInfo(Bids $bid, $fieldName, BidsDataSearch $searchData, $valueArray)
    {
        $value = array_pop($valueArray);

        if ($value instanceof BidsDataValues) {
            $value = trim($value->value);
        }
        else {
            $value = trim($value);
        }

        if (empty($value)) {
            return true;
        }

        switch ($fieldName) {
            // Наименование
            case 'purchaseNotice/body/item/purchaseNoticeData/name':
            case 'purchaseNoticeOA/body/item/purchaseNoticeOAData/name':
            case 'purchaseNoticeOK/body/item/purchaseNoticeOKData/name':
            case 'purchaseNoticeAE/body/item/purchaseNoticeAEData/name':
            case 'purchaseNoticeZK/body/item/purchaseNoticeZKData/name':
            case 'purchaseNoticeEP/body/item/purchaseNoticeEPData/name':
                // Названия лотов
            case 'purchaseNotice/body/item/purchaseNoticeData/lots/lot/subject':
            case 'purchaseNoticeOA/body/item/purchaseNoticeOAData/lots/lot/subject':
            case 'purchaseNoticeOK/body/item/purchaseNoticeOKData/lots/lot/subject':
            case 'purchaseNoticeAE/body/item/purchaseNoticeAEData/lots/lot/subject':
            case 'purchaseNoticeZK/body/item/purchaseNoticeZKData/lots/lot/subject':
            case 'purchaseNoticeEP/body/item/purchaseNoticeEPData/lots/lot/subject':
                $searchData->title .= $value.'; ';
                break;

            // Дата публикации
            case 'purchaseNotice/body/item/purchaseNoticeData/createDateTime':
            case 'purchaseNoticeOA/body/item/purchaseNoticeOAData/createDateTime':
            case 'purchaseNoticeOK/body/item/purchaseNoticeOKData/createDateTime':
            case 'purchaseNoticeAE/body/item/purchaseNoticeAEData/createDateTime':
            case 'purchaseNoticeZK/body/item/purchaseNoticeZKData/createDateTime':
            case 'purchaseNoticeEP/body/item/purchaseNoticeEPData/createDateTime':
                $searchData->publication_date = strtotime($value);
                break;

            // Дата окончания срока подачи заявок
            case 'purchaseNotice/body/item/purchaseNoticeData/submissionCloseDateTime':
            case 'purchaseNoticeOA/body/item/purchaseNoticeOAData/submissionCloseDateTime':
            case 'purchaseNoticeOK/body/item/purchaseNoticeOKData/submissionCloseDateTime':
            case 'purchaseNoticeAE/body/item/purchaseNoticeAEData/submissionCloseDateTime':
            case 'purchaseNoticeZK/body/item/purchaseNoticeZKData/submissionCloseDateTime':
            case 'purchaseNoticeEP/body/item/purchaseNoticeEPData/documentationDelivery/deliveryEndDateTime':
                $searchData->quote_date_end = strtotime($value);
                break;

            // Сумма
            case 'purchaseNotice/body/item/purchaseNoticeData/lots/lot/lotData/initialSum':
            case 'purchaseNoticeOA/body/item/purchaseNoticeOAData/lots/lot/lotData/initialSum':
            case 'purchaseNoticeOK/body/item/purchaseNoticeOKData/lots/lot/lotData/initialSum':
            case 'purchaseNoticeAE/body/item/purchaseNoticeAEData/lots/lot/lotData/initialSum':
            case 'purchaseNoticeZK/body/item/purchaseNoticeZKData/lots/lot/lotData/initialSum':
            case 'purchaseNoticeEP/body/item/purchaseNoticeEPData/lots/lot/lotData/initialSum':
                $searchData->max_cost = $value;
                break;

            // ИНН Заказчика
            case 'purchaseNotice/body/item/purchaseNoticeData/customer/mainInfo/inn':
            case 'purchaseNoticeOA/body/item/purchaseNoticeOAData/customer/mainInfo/inn':
            case 'purchaseNoticeOK/body/item/purchaseNoticeOKData/customer/mainInfo/inn':
            case 'purchaseNoticeAE/body/item/purchaseNoticeAEData/customer/mainInfo/inn':
            case 'purchaseNoticeZK/body/item/purchaseNoticeZKData/customer/mainInfo/inn':
            case 'purchaseNoticeEP/body/item/purchaseNoticeZKData/customer/mainInfo/inn':
                $searchData->inn = $value;
                break;

            default:
                break;
        }

        return true;
    }

    public function getIgnoredTags()
    {
        return [];
    }
}
