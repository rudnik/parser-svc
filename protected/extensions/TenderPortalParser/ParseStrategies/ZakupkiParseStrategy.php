<?php
/**
 * Стратегия парсинга файлов портала госзакупок
 */
class ZakupkiParseStrategy implements IPortalParseStrategy, IReportable, ILoggable
{
    /**
     * Логгер
     * @var IParserLogger
     */
    protected $logger;

    /**
     * Папка, где лежат файлы для парсинга
     * @var string
     */
    protected $inputDir;

    /**
     * Строитель отчетов
     * @var IEntityProcessReportBuilder
     */
    protected $reportBuilder = null;

    /**
     * Хранилище заявок
     * @var IBidStorage
     */
    protected $bidStorage = null;

    /**
     * Информация о типе заявки портала госзакупок
     * @var IZakupkiBidTypeInfo
     */
    protected $bidTypeInfo;

    public function getLogger()
    {
        return $this->logger;
    }

    public function setLogger(IParserLogger $logger)
    {
        $this->logger = $logger;
    }

    public function __construct(IBidStorage $bidStorage)
    {
        $this->setBidStorage($bidStorage);
    }

    public function setBidTypeInfo(IZakupkiBidTypeInfo $bidTypeInfo)
    {
        $this->bidTypeInfo = $bidTypeInfo;
    }

    public function getBidTypeInfo()
    {
        return $this->bidTypeInfo;
    }

    public function setInputDir($inputDir)
    {
        $this->inputDir = $inputDir;
    }

    public function getInputDir()
    {
        return $this->inputDir;
    }

    public function getReportBuilder()
    {
        return $this->reportBuilder;
    }

    public function setReportBuilder(IEntityProcessReportBuilder $reportBuilder)
    {
        $this->reportBuilder = $reportBuilder;
    }

    /**
     * Устанавливает хранилище заявок
     * @param IBidStorage $bidStorage
     */
    public function setBidStorage(IBidStorage $bidStorage)
    {
        $this->bidStorage = $bidStorage;
    }

    /**
     * Возвращает хранилище заявок
     * @return IBidStorage
     */
    public function getBidStorage()
    {
        return $this->bidStorage;
    }

    /**
     * Парсит zip архивированные xml файлы портала госзакупок
     * @param bool $removeFiles
     */
    public function parse($removeFiles = true)
    {
        $regions = $this->bidTypeInfo->getRegions();

        # получаем список архивов
        $archiveList = glob($this->inputDir . '/*.zip');
        foreach ($archiveList as $archiveFilePath) {
            $zip = new ZipArchive();

            # открываем архив
            if (!$zip->open($archiveFilePath)) {
                $this->logger->log('Не удается открыть архив', CLogger::LEVEL_ERROR);
                if ($removeFiles) {
                    @unlink($archiveFilePath);
                }
                continue;
            }

            # распаковываем архив
            $zip->extractTo($this->inputDir . '/');
            $zip->close();

            # удаляем архив, если нужно
            if ($removeFiles) {
                unlink($archiveFilePath);
            }

            # получаем список распакованных из архивов xml файлов
            $xmlList = glob($this->inputDir . '/*.xml');

            # перебираем все xml файлы
            foreach ($xmlList as $xmlFilePath) {
                # инициализируем xml парсер файлов с портала госзакупок
                $zakupkiXmlDocumentParser = new ZakupkiXmlParser(
                    $this->bidTypeInfo->getUniqueFields(),
                    $this->bidTypeInfo->getIgnoredTags()
                );

                # устанавливаем строитель отчетов
                if($zakupkiXmlDocumentParser instanceof IReportable) {
                    $zakupkiXmlDocumentParser->setReportBuilder($this->reportBuilder);
                }

                if($zakupkiXmlDocumentParser instanceof ILoggable) {
                    $zakupkiXmlDocumentParser->setLogger($this->logger);
                }

                # парсим распакованные xml-данные о закупках
                $zakupkiXmlDocumentParser->parse($xmlFilePath);

                # получаем результаты парсинга в виде массива ['путь/к/значению' => значение]
                $parsedBidsData = $zakupkiXmlDocumentParser->getBidsData();

                # если в файле нет заявок - переходим к разбору следующего файла
                if (!is_array($parsedBidsData) || count($parsedBidsData) < 1) {
                    unlink($xmlFilePath);
                    continue;
                }

                # признак, по которому удаляются только те xml, в которых все заявки успешно сохранились
                $allBidsSaved = true;

                # сохраняем разобранную информацию о закупке
                foreach ($parsedBidsData as $bidData) {
                    /*
                     * Проставляем регион, если он не определен при парсинге.
                     * На 06.09.2013 механизм определения региона поставки при
                     * парсинге не реализован, поэтому условие срабатыват всегда.
                     */
                    if (!isset($bidData['regionId'])) {
                        $matchesCount = 0;

                        # имя региона может содержаться как в имени zip архива, так и в имени xml файла
                        foreach ([$xmlFilePath, $archiveFilePath] as $fileName) {
                            $matchesCount = preg_match($this->bidTypeInfo->getRegionDetectRegexp(), $fileName, $matches);

                            if ($matchesCount > 0) {
                                break;
                            }
                        }

                        # если имя региона на определено -  переходим к парсингу следующего файла
                        if ($matchesCount < 1) {
                            $this->logger->log('Не удается распознать регион.', CLogger::LEVEL_ERROR);
                            break;
                        }

                        $regionName = $matches[1];

                        # если id региона на определен -  переходим к парсингу следующего файла
                        if (!isset($regions[$regionName])) {
                            $this->logger->log('Не удается найти id региона.', CLogger::LEVEL_ERROR);
                            break;
                        }

                        $bidData['regionId'] = $regions[$regionName];
                    }

                    # сохраняем заявку
                    if( !$this->bidStorage->saveBid($bidData)) {
                        $allBidsSaved = false;
                    }
                }

                # Удаляем только те xml, в которых все заявки успешно сохранились
                if ($allBidsSaved) {
                    unlink($xmlFilePath);
                }
                else {
                    rename($xmlFilePath, $xmlFilePath . '.err');
                }
            }
        }
    }
}