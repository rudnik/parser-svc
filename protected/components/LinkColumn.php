<?php

class LinkColumn extends CLinkColumn
{
    /**
     * PHP код, возвращаюший значение типа boolean, определяющее, выводить ли ссылку
     * @var string
     */
    public $renderLinkExpression;

    protected function renderDataCellContent($row, $data)
    {
        if ($this->renderLinkExpression !== null) {
            $renderLink = $this->evaluateExpression($this->renderLinkExpression, array('data' => $data, 'row' => $row));
        } else {
            $renderLink = $this->label;
        }
            
        if ($renderLink) {
            parent::renderDataCellContent($row, $data);
            return;
        }

        $label = '';
        if ($this->labelExpression !== null) {
            $label = $this->evaluateExpression($this->labelExpression, array('data' => $data, 'row' => $row));
        } else {
            $label = $this->label;
        }
        
        echo CHtml::encode($label);
    }

}