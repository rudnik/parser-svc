<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    //public $layout = '//layouts/column1';
    public $layout = '//layouts/bootstrap';

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();

    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();

    /**
     * Заголовок страницы
     * @var string
     */
    public $title = '';

    /**
     * Initializes the controller.
     * This method is called by the application before the controller starts to execute.
     */
    public function init()
    {
        parent::init();
        if (Yii::app()->user->isGuest) {
            $_SESSION['last_visited_url'] = Yii::app()->request->url;
            $this->redirect(Yii::app()->createUrl('site/login'));
        }

        if (!Users::isActive(Yii::app()->user->id)) {
            throw new CHttpException(403, 'Пользователь деактивирован. Доступ запрещен.');
        }

        # подключаем jQuery
        Yii::app()->clientScript->registerCoreScript('jquery');

        $pageSize = Yii::app()->user->getState('pageSize');
        if ($pageSize === null) {
            Yii::app()->user->setState('pageSize', Yii::app()->params['defaultPageSize']);
        }
    }

}
