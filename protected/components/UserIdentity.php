<?php

class UserIdentity extends CUserIdentity
{
    protected $_id;

    public function authenticate()
    {
        $user = Users::model()->find('LOWER(email)=?', array(strtolower($this->username)));
        
        if (($user === null) || (md5($this->password) !== $user->password)) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } else {
            $this->_id = $user->userId;
            $this->username = $user->name;
            $this->errorCode = self::ERROR_NONE;
        }
        return !$this->errorCode;
    }

    public function getId()
    {
        return $this->_id;
    }

}
