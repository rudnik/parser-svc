<?php
class ActiveRecord extends CActiveRecord {
    public function fieldAsArray($fieldName, $delimiter = ',', $trim = true)
    {
        if (!$this->hasAttribute($fieldName)) {
            throw new InvalidArgumentException("Атрибут $fieldName отсутствует в модели " . get_class($this));
        }

        $values = explode($delimiter, $this->$fieldName);

        if ($trim) {
            array_walk($values, function(&$value){$value = trim($value);});
        }

        $values = array_filter($values);

        return $values;
    }
}