<?php

/**
 * Менеджер FTP-соединения
 * 
 * @package TendersParser
 * @subpackage Ftp
 * @author Sergey Utkin s.utkin@itsirius.ru
 * @copyright 2013 BSTsoft
 */
class Ftp extends CComponent
{
    /**
     * Соединяться ли с хостом при создании экземпляра класса
     * @var boolean
     */
    protected $autoConnect = false;

    /**
     * Ссылка на соединение
     * @var resource
     */
    protected $connection;

    /**
     * Адрес хоста
     * @var string
     */
    protected $host;

    /**
     * Порт
     * @var intger
     */
    protected $port;

    /**
     * Логин
     * @var string
     */
    protected $login;

    /**
     * Пароль
     * @var string
     */
    protected $password;

    /**
     * Колчество секунд, в течение которых одидается ответ от сервера
     * @var integer
     */
    protected $timeout;

    /**
     * Использовать ли пассивный режим
     * @var boolean
     */
    protected $passive;

    /**
     * Конструктор
     * @param string $host - Адрес хоста
     * @param string $login - Логин
     * @param string $password - Пароль
     * @param integer $port - Порт
     * @param integer $timeout - Колчество секунд, в течение которых одидается ответ от сервера
     * @param boolean $passive - Использовать ли пассивный режим
     * @param boolean $autoConnect - Соединяться ли с хостом при создании экземпляра класса
     */
    public function __construct($host, $login, $password, $port = 21, $timeout = 90, $passive = true, $autoConnect = true)
    {
        $this->host = $host;
        $this->login = $login;
        $this->password = $password;
        $this->port = $port;
        $this->timeout = $timeout;
        $this->passive = $passive;
        $this->autoConnect = $autoConnect;

        if ($this->autoConnect) {
            $this->connect();
        }
    }

    /**
     * Деструктор. Разрывает соединение
     */
    public function __destruct()
    {
        $this->disconnect();
    }

    /**
     * Возвращает ссылку на соединение
     * @return resource
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * Подключается к серверу
     * @return boolean - в случае успеха - true
     * @throws CException
     */
    public function connect()
    {
        if ($this->connection !== null) {
            $this->disconnect();
        }

        $this->connection = @ftp_connect($this->host, $this->port, $this->timeout);
        if ($this->connection === false) {
            throw new CException("Can't connect to FTP");
        }

        if (!@ftp_login($this->connection, $this->login, $this->password)) {
            throw new CException("Can't login to FTP.");
        }

        @ftp_pasv($this->connection, $this->passive);

        return true;
    }

    /**
     * Разрывает соединение с сервером
     * @return boolean - в случае успеха - true
     */
    public function disconnect()
    {
        if (!is_null($this->connection)) {
            $closingResult = @ftp_close($this->connection);

            if ($closingResult) {
                $this->connection = null;
            }

            return $closingResult;
        }

        return true;
    }

    /**
     * Возвращает список названий файлов  и папок в заданной папке
     * @param string $dir - имя папки
     * @return mixed - массив файлов и папок или false
     */
    public function ls($dir = '.')
    {
        return @ftp_nlist($this->connection, $dir);
    }

    /**
     * Возвращает список с расширенной информацией о  файлах и папках в заданной папке
     * @param string $dir - имя папки
     * @param boolean $recursive - учет вложенных папок
     * @return mixed - массив файлов и папок или false
     */
    public function lsRaw($dir = '.', $recursive = false)
    {
        return @ftp_rawlist($this->connection, $dir, $recursive);
    }

    /**
     * Смена текушего каталога
     * @param string $dir - имя папки
     * @return boolean - в случае успеха - true
     */
    public function chdir($dir)
    {
        return @ftp_chdir($this->connection, $dir);
    }

    /**
     * Скачивает файл с FTP
     * @param string $localFile - путь к локальному файлу
     * @param string $remoteFile - путь к удаленному (скачиваемому) файлу
     * @param integer $mode - режим скачивания 
     * @param integer $resumePos - позиция, с которой нужно докачивать файл
     * @return boolean - в случае успеха - true
     */
    public function get($localFile, $remoteFile, $mode = FTP_BINARY, $resumePos = 0)
    {
        return @ftp_get($this->connection, $localFile, $remoteFile, $mode, $resumePos);
    }

    /**
     * Возвращает имя текущей папки
     * @return mixed - имя текущей папки или false
     */
    public function pwd()
    {
        return @ftp_pwd($this->connection);
    }

}