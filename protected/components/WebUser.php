<?php

/**
 * Пользователь.
 *
 * @property Roles $role Роль.
 */
class WebUser extends CWebUser
{
    private $_model = null;

    public function getRole()
    {
        if ($user = $this->getModel()) {
            return $user->role->code;
        }
    }

    private function getModel()
    {
        if (!$this->isGuest && $this->_model === null) {
            $this->_model = Users::model()->findByPk($this->id, array('select' => 'role_id'));
        }
        return $this->_model;
    }

    /**
     * Добавить клиента в список "обработанных"
     * (клиент не отображается в результатах фильтра в пределах одной поисковой сессии)
     * @param bool $id - идентификатор клиента
     * @return bool
     */
    public static function addExcludedRecordId($id)
    {
        $model = Clients::model()->findByPk($id, ['select' => 'id']);

        if ($model === null) {
            return false;
        }

        $hiddenRecordIds = Yii::app()->session->get('hiddenRecordIds', []);
        $hiddenRecordIds[] = $id;
        Yii::app()->session->add('hiddenRecordIds', $hiddenRecordIds);

        return true;
    }

    /**
     * Очистить список "обработанных" клиентов
     */
    public static function clearExcludedRecordsList()
    {
        Yii::app()->session->add('hiddenRecordIds', []);
    }

    /**
     * Возвращает условия выборки клиентов, исключая "обработанных"
     * @return CDbCriteria
     */
    public static function getExcludedRecordsListCriteria()
    {
        $excludedRecordIds = Yii::app()->session->get('hiddenRecordIds', []);
        $criteria = new CDbCriteria();

        if (!empty($excludedRecordIds)) {
            $criteria->condition = 'id NOT IN (' . implode(',', $excludedRecordIds) . ')';
        }

        return $criteria;
    }
}
