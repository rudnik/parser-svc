<?php
define('SELENIUM_TEST_URL', 'http://parser.loc');

return CMap::mergeArray(
    require(dirname(__FILE__) . '/main.php'), array(
            'components' => array(
                'fixture' => array(
                    'class' => 'system.test.CDbFixtureManager',
                ),
            ),
    )
);
