<?php
return [
    # ========== РОЛИ
    'guest' => [
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Гость',
        'bizRule' => null,
        'data' => null,
    ],
    'manager' => [
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Менеджер',
        'children' => [
            'guest',
            'editOwnClients',
            'deleteOwnClients',
            'viewMainMenu',
            'viewOwnManagersClients',
        ],
        'bizRule' => null,
        'data' => null,
    ],
    'admin' => [
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Администратор',
        'children' => [
            'manager',
            'changeClientsManager',
            'editClients',
            'deleteClients',
            'manageUsers',
        ],
        'bizRule' => null,
        'data' => null,
    ],
    # отображение главного меню
    'viewMainMenu' => array(
        'type' => CAuthItem::TYPE_OPERATION,
        'description' => 'Проверка активности пользователя',
        'children' => null,
        'bizRule' => 'return Users::isActive($params["userId"]);',
        'data' => null
    ),
    # возможность видеть клиентов подчиненных менеджеров
    'viewOwnManagersClients' => array(
        'type' => CAuthItem::TYPE_OPERATION,
        'description' => 'Возможность видеть клиентов подчиненных менеджеров',
        'children' => null,
        'bizRule' => 'return Users::isMyUser($params["userId"], $params["user_id"]);',
        'data' => null
    ),
    # редактиорвание карточки клиента
    'changeClientsManager' => array(
        'type' => CAuthItem::TYPE_OPERATION,
        'description' => 'Изменение менеджера в карточке клиента',
        'children' => null,
        'bizRule' => null,
        'data' => null
    ),
    'editOwnClients' => array(
        'type' => CAuthItem::TYPE_TASK,
        'description' => 'Редактирование только своих карточек клиентов',
        'children' => ['editClients'],
        'bizRule' => 'return $params["userId"]==$params["user_id"];',
        'data' => null
    ),
    'editClients' => array(
        'type' => CAuthItem::TYPE_OPERATION,
        'description' => 'Редактирование карточки клиента',
        'children' => null,
        'bizRule' => null,
        'data' => null
    ),
    # удаление карточки клиента
    'deleteOwnClients' => array(
        'type' => CAuthItem::TYPE_TASK,
        'description' => 'Удаление только своих карточек клиентов',
        'children' => ['deleteClients'],
        'bizRule' => 'return $params["userId"]==$params["user_id"];',
        'data' => null
    ),
    'deleteClients' => array(
        'type' => CAuthItem::TYPE_OPERATION,
        'description' => 'Удаление карточки клиента',
        'children' => null,
        'bizRule' => null,
        'data' => null
    ),
    # Доступ к CRUD интерфейсу пользователей (менеджеров)
    'manageUsers' => array(
        'type' => CAuthItem::TYPE_OPERATION,
        'description' => 'Доступ к CRUD интерфейсу пользователей (менеджеров)',
        'children' => null,
        'bizRule' => null,
        'data' => null
    ),
];
