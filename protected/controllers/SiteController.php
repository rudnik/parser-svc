<?php

class SiteController extends CController
{
    public $breadcrumbs = [];

    //public $layout = '//layouts/column1';
    public $layout = '//layouts/bootstrap';

    public $menu = [];

    public $title;

    /**
     * @return array action filters
     */
    public function filters()
    {
        return ['accessControl'];
    }

    public function accessRules()
    {
        return [
            ['allow',
                'actions' => ['login', 'index', 'page'],
            ],
            ['allow',
                'roles' => ['manager'],
            ],
            ['deny', // deny all users
                'users' => ['*'],
            ],
        ];
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        $this->render('index');
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if (!($error = Yii::app()->errorHandler->error)) {
            return;
        }
            
        if (Yii::app()->request->isAjaxRequest) {
            echo $error['message'];
        } else {
            $this->render('error', $error);
        }
    }

    /**
     * Displays the login page
     */
    public function actionLogin()
    {
        $model = new LoginForm();

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {
                $this->redirect( isset($_SESSION['last_visited_url']) ? 
                    'http://' . Yii::app()->request->serverName . $_SESSION['last_visited_url'] 
                    : 
                    Yii::app()->user->returnUrl
                );
            }
        }
        // display the login form
        $this->render('login', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }
    
    public function actionTest() {
//        $subscr = ClientsSubscriptions::model()->findByPk(9);
//        $subscr->refreshBidsCache();
         
    }
}
