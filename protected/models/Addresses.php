<?php

/**
 * Модель таблицы "addresses".
 *
 * Доступные поля таблицы "addresses":
 * @property string $AOID Идентификатор.
 * @property string $AOGUID Глобальный уникальный идентификатор адресного объекта.
 * @property string $PARENTGUID Идентификатор объекта родительского объекта.
 * @property string $OFFNAME Официальное наименование.
 * @property string $SHORTNAME Краткое наименование типа объекта.
 * @property integer $AOLEVEL Уровень адресного объекта.
 * @property integer $REGIONCODE Код региона.
 * @property integer $AREACODE Код района.
 * @property integer $AUTOCODE Код автономии.
 * @property integer $CITYCODE Код города.
 * @property integer $CTARCODE Код внутригородского района.
 * @property integer $PLACECODE Код населенного пункта.
 * @property integer $STREETCODE Код улицы.
 * @property integer $EXTRCODE Код дополнительного адресообразующего элемента.
 * @property integer $SEXTCODE Код подчиненного дополнительного адресообразующего элемента.
 * @property string $NEXTID Идентификатор записи связывания с последующей исторической записью.
 * @property string $PREVID Идентификатор записи связывания с предыдушей исторической записью.
 * @property integer $POSTALCODE Почтовый индекс.
 *
 * Доступные отношения:
 * @property Clients $clients Клиенты.
 * @property Addresses $region Регион. Доступно, если текущая модель не регион.
 * @property Addresses $cities Населенные пункты. Доступно, если текущая модель является регионом.
 *
 * @package TendersParser
 * @subpackage addresses
 * @author Nikolay Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013 BSTsoft
 */
class Addresses extends CActiveRecord
{
    /**
     * Регион населенного пункта.
     * Доступен, если текущая модель не регион.
     *
     * @var Addresses
     */
    private $parentRegion = null;

    /**
     * Список населенных пунктов.
     * Доступны, если текущая модель является регионом.
     *
     * @var array
     */
    private $regionCities = null;

    /**
     * Получение дополнительных отношений.
     *
     * @param string $name Имя отношения.
     */
    public function __get($name)
    {
        switch ($name) {
            case 'region':
                if ($this->PARENTGUID && is_null($this->parentRegion)) {
                    $region = self::model()->findByAttributes([
                        'AOGUID' => $this->PARENTGUID
                    ]);
                    // глубина вложенности может быть любая.
                    // идем вверх до самого региона мимо районов
                    if ($region !== null) {
                            $this->parentRegion = !$region->PARENTGUID ? $region : $region->region;
                    }
                }

                return $this->parentRegion;
            case 'cities':
                if (is_null($this->regionCities)) {
                    $this->regionCities = self::model()->findAllByAttributes(
                            ['PARENTGUID' => $this->AOGUID], new CDbCriteria([
                        'condition' => 'SHORTNAME!=:short_name',
                        'params' => [':short_name' => 'р-н'],
                        'order' => 'OFFNAME',
                        'limit' => 50
                            ])
                    );
                }

                return $this->regionCities;
            default:
                return parent::__get($name);
        }
    }

    /**
     * @return string Таблица модели
     */
    public function tableName()
    {
        return 'addresses';
    }

    /**
     * @return array Правила валидации.
     */
    public function rules()
    {
        return [
            ['AOID', 'required'],
            ['AOLEVEL, REGIONCODE, AREACODE, AUTOCODE, CITYCODE, CTARCODE, PLACECODE, STREETCODE, EXTRCODE, SEXTCODE, POSTALCODE', 'numerical', 'integerOnly' => true],
            ['AOID, AOGUID, PARENTGUID, NEXTID, PREVID', 'length', 'max' => 36],
            ['OFFNAME', 'length', 'max' => 145],
            ['SHORTNAME', 'length', 'max' => 14],
            ['AOID, AOGUID, PARENTGUID, OFFNAME, SHORTNAME, AOLEVEL, REGIONCODE, AREACODE, AUTOCODE, CITYCODE, CTARCODE, PLACECODE, STREETCODE, EXTRCODE, SEXTCODE, NEXTID, PREVID, POSTALCODE', 'safe', 'on' => 'search'],
        ];
    }

    /**
     * @return array Отношения модели.
     */
    public function relations()
    {
        return [
            'clients' => [self::HAS_MANY, 'Clients', 'AOGUID']
        ];
    }

    /**
     * @return array Атрибуты модели.
     */
    public function attributeLabels()
    {
        return [
            'AOID' => 'Идентификатор',
            'AOGUID' => 'Глобальный уникальный идентификатор адресного объекта',
            'PARENTGUID' => 'Идентификатор объекта родительского объекта',
            'OFFNAME' => 'Официальное наименование',
            'SHORTNAME' => 'Краткое наименование типа объекта',
            'AOLEVEL' => 'Уровень адресного объекта',
            'REGIONCODE' => 'Код региона',
            'AREACODE' => 'Код района',
            'AUTOCODE' => 'Код автономии',
            'CITYCODE' => 'Код города',
            'CTARCODE' => 'Код внутригородского района',
            'PLACECODE' => 'Код населенного пункта',
            'STREETCODE' => 'Код улицы',
            'EXTRCODE' => 'Код дополнительного адресообразующего элемента',
            'SEXTCODE' => 'Код подчиненного дополнительного адресообразующего элемента',
            'NEXTID' => 'Идентификатор записи связывания с последующей исторической записью',
            'PREVID' => 'Идентификатор записи связывания с предыдушей исторической записью',
            'POSTALCODE' => 'Почтовый индекс',
        ];
    }

    /**
     * Построение условий поиска.
     *
     * @return CActiveDataProvider Модели с применением фильтров.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('AOID', $this->AOID, true);
        $criteria->compare('AOGUID', $this->AOGUID, true);
        $criteria->compare('PARENTGUID', $this->PARENTGUID, true);
        $criteria->compare('OFFNAME', $this->OFFNAME, true);
        $criteria->compare('SHORTNAME', $this->SHORTNAME, true);
        $criteria->compare('AOLEVEL', $this->AOLEVEL);
        $criteria->compare('REGIONCODE', $this->REGIONCODE);
        $criteria->compare('AREACODE', $this->AREACODE);
        $criteria->compare('AUTOCODE', $this->AUTOCODE);
        $criteria->compare('CITYCODE', $this->CITYCODE);
        $criteria->compare('CTARCODE', $this->CTARCODE);
        $criteria->compare('PLACECODE', $this->PLACECODE);
        $criteria->compare('STREETCODE', $this->STREETCODE);
        $criteria->compare('EXTRCODE', $this->EXTRCODE);
        $criteria->compare('SEXTCODE', $this->SEXTCODE);
        $criteria->compare('NEXTID', $this->NEXTID, true);
        $criteria->compare('PREVID', $this->PREVID, true);
        $criteria->compare('POSTALCODE', $this->POSTALCODE);

        return new CActiveDataProvider($this, [
            'criteria' => $criteria,
        ]);
    }

    /**
     * Статический метод возвращения модели.
     *
     * @param string $className Имя класса.
     * @return Addresses Модель.
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Список регионов.
     *
     * @param string $title Название региона (можно часть) для поиска.
     *
     * @return array
     */
    public static function findAllRegionsByTitle($title = '')
    {
        $criteria = new CdbCriteria([
            'condition' => 'PARENTGUID=""',
            'order' => 'OFFNAME'
        ]);

        if ($title) {
            $criteria->condition .= ' AND OFFNAME LIKE :title';
            $criteria->params = [':title' => $title . '%'];
        }

        return self::model()->findAll($criteria);
    }

    /**
     * Список населенных пунктов.
     *
     * @param string $region Идентификатор региона.
     * @param string $title Название населенного пункта (можно часть) для поиска.
     *
     * @return array
     */
    public static function findAllCitiesByTitle($region = '', $title = '')
    {
        $criteria = new CdbCriteria([
            'condition' => 'SHORTNAME!=:short_name',
            'params' => [':short_name' => 'р-н'],
            'order' => 'OFFNAME',
            'limit' => 50
        ]);

        if (!empty($region)) {
            $criteria->condition .= ' AND PARENTGUID=:parent';
            $criteria->params[':parent'] = $region;
        } else {
            $criteria->condition .= ' AND PARENTGUID!=:parent';
            $criteria->params[':parent'] = '';
        }

        if ($title) {
            $criteria->condition .= ' AND OFFNAME LIKE :title';
            $criteria->params[':title'] = $title . '%';
        }

        $models = self::model()->findAll($criteria);
        $names = [];

        foreach ($models as $i => $model) {
            $cityKey = $model->OFFNAME . '_' . $model->SHORTNAME;
            if (isset($names[$cityKey])) {
                unset($models[$i]);
            } else {
                $names[$cityKey] = true;
            }
        }

        return $models;
    }

}
