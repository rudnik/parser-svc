<?php

/**
 * This is the model class for table "regions".
 *
 * The followings are the available columns in table 'regions':
 * @property integer $regionId
 * @property integer $parentId
 * @property string $title
 *
 * The followings are the available model relations:
 * @property Bids[] $bids
 * @property Clients[] $clients
 */
class Regions extends CActiveRecord
{
    /**
     * Регионы округов.
     *
     * @var array
     */
    private $children = null;

    public function __get($name)
    {
        if ('children' != $name) {
            return parent::__get($name);
        }

        if (is_null($this->children)) {
            $this->children = Regions::model()->findAll(new CDbCriteria([
                    'condition' => 'parentId=' . $this->regionId,
                    'order' => 'title'
                ]));
        }

        return $this->children;
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'regions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            ['title', 'required'],
			array('parentId', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('regionId, parentId, title', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bids' => array(self::HAS_MANY, 'Bids', 'regionId'),
			'clients' => array(self::HAS_MANY, 'Clients', 'regionId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'regionId' => 'Region',
			'parentId' => 'Parent',
			'title' => 'Title',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('regionId',$this->regionId);
		$criteria->compare('parentId',$this->parentId);
		$criteria->compare('title',$this->title,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Regions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
