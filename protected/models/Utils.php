<?php
class Utils
{
    /**
     * Определяет, содержит ли указанное поле в качестве подстроки хотябы один элемент из переданного массива строк.
     * @param string $value имя поля
     * @param array $valuesSet
     * @return bool
     */
    public static function valueBelongsToSet($value, array $valuesSet)
    {
        foreach ($valuesSet as $item) {
            $pos = strpos($value, $item);

            if($pos === false) {
                continue;
            }

            if (($pos + strlen($item)) == strlen($value)) {
                return true;
            }
        }

        return false;
    }
}