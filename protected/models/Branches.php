<?php

/**
 * Модель отраслей. Таблица "branches".
 *
 * Доступные столбцы в таблице "branches":
 * @property integer $branchId Идентификатор отрасли.
 * @property integer $parentId Идентификатор родительской отрасли.
 * @property string $title Идентификатор портала.
 *
 * Доступные модели отношений:
 *
 * @package TendersParser
 * @subpackage branches
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013 BSTsoft
 */
class Branches extends CActiveRecord
{

    /**
     * Возвращает имя таблицы в базе данных.
     *
     * @return string Имя таблицы в базе данных.
     */
    public function tableName()
    {
        return 'branches';
    }

    /**
     * Возвращает модель класса.
     *
     * @return Branches Модель класса.
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Возвращает массив правил валидации.
     * @return array правила валидации
     */
    public function rules()
    {
        return [
            ['parentId, title', 'required']
        ];
    }

    /**
     * Возращает связи с другими таблицами.
     *
     * @return array Правила отношений.
     */
    public function relations()
    {
        return [
            'region' => [self::BELONGS_TO, 'Regions', 'regionId'],
            'clients' => [self::HAS_MANY, 'Clients', 'clientId']
        ];
    }

    /**
     * Список атрибутов модели.
     *
     * @return array Атрибуты.
     */
    public function attributeLabels()
    {
        return [
            'branchId' => 'Идентификатор закупки',
            'parentId' => 'Идентификатор родительской отрасли',
            'title' => 'Наименование'
        ];
    }

}
