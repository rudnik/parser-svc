<?php

/**
 * Все, что отправляется по email - отправляется методами данного класса
 */
final class EmailSender
{
    /**
     * Построитель отчетов.
     *
     * @return IEntityProcessReportBuilder
     */
    protected $reportBuilder = null;

    /**
     * Отправка почты.
     *
     * @var bool
     */
    public $trueSend = true;

    /**
     * Список клиентов, для которых нужно сохранять заявки для внешних источников.
     *
     * @var array
     */
    protected static $clientsExport = [2192];

    /**
     * @return IEntityProcessReportBuilder|null Построитель отчетов.
     */
    public function getReportBuilder()
    {
        if (is_null($this->reportBuilder)) {
            $builder = new EntityProcessReportBuilder();
            $builder->template  = 'Subscriptions mails:' . "\n";
            $builder->template .= 'Total: {total}, Success: {success}, Fail: {fail}'."\n\n";
            $this->setReportBuilder($builder);
        }

        return $this->reportBuilder;
    }

    /**
     * @param IEntityProcessReportBuilder $reportBuilder Построитель отчетов.
     */
    public function setReportBuilder(IEntityProcessReportBuilder $reportBuilder)
    {
        $this->reportBuilder = $reportBuilder;
    }

    /**
     * Отправляет email
     *
     * @param string $to                   адрес получателя
     * @param string $subject              тема письма
     * @param string $message              сообщение
     * @param string $additionalHeaders    дополнительные заголовки
     * @param string $additionalParameters дополнительные параметры
     *
     * @return boolean
     */
    public function sendMail($to, $subject, $message, $additionalHeaders = null, $additionalParameters = null)
    {
        return $this->trueSend ? mail($to, $subject, $message, $additionalHeaders, $additionalParameters) : true;
    }

    /**
     * Отправляет отдельные заявки на указанные email
     *
     * @param array $bidsIds Cписок id заявок
     * @param array $emails Список email'ов, куда отправлять
     * @param integer $clientId id клиента, которому добавляются отправленные заявки
     * @param array $params Параметры.
     * @param string $mailSubject Тема письма.
     *
     * @return boolean - true в случае успеха
     */
    public function sendBids(array $bidsIds, array $emails, $clientId, array $params = null, $mailSubject = null)
    {
        if (empty($bidsIds) || empty($emails) || empty($clientId)) {
            return false;
        }

        $bidsIds = array_filter(
            $bidsIds,
            function ($value) {
                return (int) $value;
            }
        );

        $bidsUniqueIds = array_unique($bidsIds);

        if (empty($bidsUniqueIds)) {
            return false;
        }

        $bidsUniqueIdsString = implode(',', $bidsUniqueIds);

        $bids = Bids::model()->findAll('bidId IN (' . $bidsUniqueIdsString . ')');

        if (empty($bids)) {
            return false;
        }

        // инициализируем заявками механизм формирования содержимого письма рассылки
        $viewModel = new BidsMailViewModel();
        $viewModel->setBids($bids);

        // получаем содержимое присьма рассылки
        $clientName = isset($params['clientName']) ? trim($params['clientName']) : null;
        $controller = new CController('EmailSender');
        $message = $controller->renderInternal(
            Yii::getPathOfAlias('application.modules.clients.views.subscriptions') . '/_subscr_email.php',
            [
                'viewModel'     => $viewModel,
                'clientName'    => $clientName,
                'mailSubject'   => $mailSubject,
                'userSignature' => (isset($params['userSignature']) ? trim($params['userSignature']) : ''),
            ],
            true
        );

        // отправляем рассылку на собранные email
        if ($mailSubject !== null) {
            $subject = $mailSubject;
        } else {
            $subject = $clientName !== null ? 'Рассылка для клиента: ' . $clientName : 'Тендеры по вашей заявке';
        }

        $headers = [
            'From: ' . (isset($params['fromEmail']) ? trim($params['fromEmail']) : 'info@svc-tender.ru'),
            'Reply-To: info@svc-tender.ru',
            'Return-Path: info@svc-tender.ru',
            'X-PHP-Originating-Script: ',
            'MIME-Version: 1.0',
            'Content-Type: text/html; charset=UTF-8'
        ];

        $to = implode(',', $emails);

        $this->getReportBuilder()->addTotalCount();

        if (!$this->sendMail($to, $subject, $message, implode("\r\n", $headers))) {
            $this->getReportBuilder()->addFail();
            return false;
        }

        $this->getReportBuilder()->addSuccess();

        // сохраняем идентификатиоры отправленных заявок
        Clients::addSendedBidsIds($bidsUniqueIds, $clientId);
        // сохраняем заявки для внешних источников
        $export = BidsExport::model();
        if (in_array($clientId, self::$clientsExport, false)) {
            $export->saveExport($bidsUniqueIds, $clientId);
        }

        return true;
    }

    /**
     * Отправляет рассылки
     *
     * @param ClientsSubscriptions[] $subscriptions список рассылок
     * @throws CException
     * @throws CDbException
     */
    public function sendSubscriptions(array $subscriptions)
    {
        $today = strtotime(date('Y-m-d 00:00:00'));

        // пробегаемся по рассылкам
        foreach ($subscriptions as $subscription) {
            if (! $subscription instanceof ClientsSubscriptions) {
                throw new CException('Рассылка должна быть экземпляром класса ClientsSubscriptions');
            }

            $start = 0;
            $end = PHP_INT_MAX;

            if ('0000-00-00 00:00:00' != $subscription->activedate_from) {
                $start = strtotime($subscription->activedate_from);
            }

            if ('0000-00-00 00:00:00' != $subscription->activedate_to) {
                $end = strtotime($subscription->activedate_to);
            }

            /*
             * если текущая дата выходит за рамки активности рассылки - переходим к обработке следующей рассылки
             */
            if ($today < $start || $today > $end) {
                // если рассылка уже закончилась - убираем флаг активности рассылки
                if ($today > $end) {
                    $subscription->active = 0;
                    $subscription->saveAttributes(['active']);
                }

                continue;
            }



            // выбираем заявки из кэша
            $bidsIdsCacheArray = explode(',', trim($subscription->bids_cache));

            $emails = [];
            foreach ($subscription->emails as $emailAddress) {
                $emails[] = $emailAddress->email;
            }

            // отправляем заявки
            $params = [
                'clientName'    => $subscription->client->title,
                'userSignature' => $subscription->client->user->mail_signature,
                'fromEmail'     => $subscription->send_from_default_email
                    ? 'info@svc-tender.ru'
                    : $subscription->client->user->email,
            ];
            $this->sendBids(
                $bidsIdsCacheArray,
                $emails,
                $subscription->clientId,
                $params,
                $subscription->getMailTheme()
            );
        }
    }

    public function sendUsersPassword($email, $password)
    {
        $siteUrl = 'http://' . Yii::app()->request->serverName;
        $loginPage = $siteUrl . '/site/login';

        $subject = 'Данные для авторизации на сайте ' . $siteUrl;
        $headers = [
            'From: info@svc-tender.ru',
            'Reply-To: info@svc-tender.ru',
            'Return-Path: info@svc-tender.ru',
            'X-PHP-Originating-Script: ',
            'MIME-Version: 1.0',
            'Content-Type: text/html; charset=UTF-8'
        ];

        $message = <<<MESSAGE
Страница авторизации: {$loginPage}<br />
Логин: {$email}<br />
Пароль: {$password}<br />
MESSAGE;
        return $this->sendMail($email, $subject, $message, implode("\r\n", $headers));
    }
}
