<?php

/**
 * Модель порталов. Таблица "portals".
 *
 * Доступные столбцы в таблице "portals":
 * @property integer $portalId Идентификатор портала.
 * @property string $name Имя.
 * @property string $title Заголовок.
 * @property string $url Ссылка на портал.
 *
 * Доступные модели отношений:
 *
 * @package TendersParser
 * @subpackage portals
 * @author Nikolaj Rudakov n.rudakov@bstsoft.ru
 * @copyright 2013 BSTsoft
 */
class Portals extends CActiveRecord
{

    /**
     * Возвращает имя таблицы в базе данных.
     *
     * @return string Имя таблицы в базе данных.
     */
    public function tableName()
    {
        return 'portals';
    }

    /**
     * Возвращает модель класса.
     *
     * @return Portals Модель класса.
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Возвращает массив правил валидации.
     * @return array правила валидации
     */
    public function rules()
    {
        return [
            ['name, title, url', 'required']
        ];
    }

    /**
     * Возращает связи с другими таблицами.
     *
     * @return array Правила отношений.
     */
    public function relations()
    {
        return [
            'bids' => [self::HAS_MANY, 'Bids', 'bidId'],
            'doctypes' => [self::HAS_MANY, 'BidsDoctypes', 'portalId'],
        ];
    }

    /**
     * Список атрибутов модели.
     *
     * @return array Атрибуты.
     */
    public function attributeLabels()
    {
        return [
            'portalId' => 'Идентификатор портала',
            'name' => 'Имя',
            'title' => 'Заголовок',
            'url' => 'Ссылка на портал'
        ];
    }

}
