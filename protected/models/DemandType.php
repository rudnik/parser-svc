<?php

/**
 * Модель типов спроса
 * @todo рефакторить при первой возможности
 */
class DemandType
{
    /**
     * Типы спроса
     * @var array
     */
    protected static $demandTypes = [
        1 => 'Открытый конкурс',
        2 => 'Открытый аукцион в электронной форме',
        3 => 'Запрос котировок',
        4 => 'Закупка у единственного поставщика',
        5 => 'Иной способ',

        6  => 'Электронный аукцион',
        7  => 'Открытый двухэтапный конкурс',
        8  => 'Открытый конкурс с ограниченным участием',
        9  => 'Предварительный отбор',
        10 => 'Закрытый аукцион',
        11 => 'Закрытый двухэтапный конкурс',
        12 => 'Закрытый конкурс с ограниченным участием',
        13 => 'Закрытый конкурс',
        14 => 'Общая информация об объекте закупки (ЗК-БИ)',
        15 => 'Запрос предложений',
    ];

    /**
     * Соответствие типов спроса типам документов ФЗ 94, 223 и 44
     * @var array[]
     */
    protected static $doctypesAccordance = [
        'zakupki94' => [
            1 => ['notificationOK'],
            2 => ['notificationEF'],
            3 => ['notificationZK'],
            4 => ['noAnySearch'],
            5 => ['noAnySearch'],
            6  => ['noAnySearch'],
            7  => ['noAnySearch'],
            8  => ['noAnySearch'],
            9  => ['noAnySearch'],
            10 => ['noAnySearch'],
            11 => ['noAnySearch'],
            12 => ['noAnySearch'],
            13 => ['noAnySearch'],
            14 => ['noAnySearch'],
            15 => ['noAnySearch'],
        ],
        'zakupki223' => [
            1 => ['purchaseNoticeOK'],
            2 => ['purchaseNoticeOA', 'purchaseNoticeAE'],
            3 => ['purchaseNoticeZK'],
            4 => ['noAnySearch'],
            5 => ['purchaseNotice'],
            6  => ['noAnySearch'],
            7  => ['noAnySearch'],
            8  => ['noAnySearch'],
            9  => ['noAnySearch'],
            10 => ['noAnySearch'],
            11 => ['noAnySearch'],
            12 => ['noAnySearch'],
            13 => ['noAnySearch'],
            14 => ['noAnySearch'],
            15 => ['noAnySearch'],
        ],

        'zakupki44' => [
            1 => ['fcsNotificationOK'],
            2 => ['noAnySearch'],
            3 => ['fcsNotificationZK'],
            4 => ['fcsNotificationEP'],
            5 => ['fcsNotificationISM', 'fcsNotificationISO'],
            6  => ['fcsNotificationEF'],
            7  => ['fcsNotificationOKD'],
            8  => ['fcsNotificationOKOU'],
            9  => ['fcsNotificationPO'],
            10 => ['fcsNotificationZakA'],
            11 => ['fcsNotificationZakKD'],
            12 => ['fcsNotificationZakKOU'],
            13 => ['fcsNotificationZakK'],
            14 => ['fcsNotificationZKBI'],
            15 => ['fcsNotificationZP'],
        ],
    ];

    /**
     * Возвращает список типов спроса
     * @return array
     */
    public static function getList()
    {
        return static::$demandTypes;
    }

    /**
     * Возвращает список идентификторов типов документов для поиска в соответствии с
     * заданными порталами и типами спроса
     * @param array $portalIds - массив идентификаторов порталов
     * @param array $demandTypes - массив идентификаторов типов спроса
     * @return int[]
     */
    public static function getPortalDemandsDoctypeIds($portalIds, $demandTypes)
    {
        if (empty($portalIds)) {
            $portalIds = [];
        } elseif (!is_array($portalIds)) {
            $portalIds = [(int) $portalIds];
        }

        if (empty($demandTypes)) {
            $demandTypes = array_keys(static::$demandTypes);
        } elseif (!is_array($demandTypes)) {
            $demandTypes = [(int) $demandTypes];
        }

        $portalcondition = empty($portalIds) ? '' : 't.portalId IN (' . implode(',', $portalIds) . ')';
        $portals = Portals::model()->with('doctypes')->findAll($portalcondition);

        $doctypesIds = [];
        foreach ($portals as $portal) {
            foreach ($portal->doctypes as $doctype) {
                foreach ($demandTypes as $demandType) {
                    if (in_array($doctype->name, static::$doctypesAccordance[$portal->name][$demandType])) {
                        $doctypesIds[] = $doctype->doctypeId;
                    }
                }
            }
        }
        return empty($doctypesIds) ? [0] : $doctypesIds;
    }

}