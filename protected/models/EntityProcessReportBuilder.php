<?php
/**
 * Класс, формирующий отчет о результате обработки сущностей
 *
 * @property string $template  - Шаблон вывода отчета
 */
class EntityProcessReportBuilder implements IEntityProcessReportBuilder
{
    /**
     * Cчетчик общего числа сущностей, подлежащих обработке
     * @var integer
     */
    protected $total = 0;

    /**
     * Cчетчик количества успешных обработок
     * @var integer
     */
    protected $success = 0;

    /**
     * Cчетчик количества обработок со сбоем
     * @var integer
     */
    protected $fail = 0;

    /**
     * Шаблон отчета
     * @var string
     */
    public $template = "Total: {total}, Success: {success}, Fail: {fail}\n";

    public function addFail($count = 1)
    {
        $this->fail += $count;
    }

    public function addSuccess($count = 1)
    {
        $this->success += $count;
    }

    public function addTotalCount($count = 1)
    {
        $this->total += $count;
    }

    public function getSuccessCount()
    {
        return $this->success;
    }

    public function getFailCount()
    {
        return $this->fail;
    }

    public function getTotalCount()
    {
        return $this->total;
    }

    public function getReport(array $params = null)
    {
        return str_replace(
            ['{total}', '{success}', '{fail}'],
            [$this->total, $this->success, ($this->fail ? '!!! ' . $this->fail . ' !!!' : $this->fail)],
            $this->template);
    }

    public function reset()
    {
        $this->total = 0;
        $this->fail = 0;
        $this->success = 0;
    }
}
