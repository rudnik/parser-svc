<?php

/**
 * Change the following URL based on your server configuration
 * Make sure the URL ends with a slash so that we can use relative URLs in test cases
 */
define('TEST_BASE_URL', 'http://localhost/testdrive/index-test.php/');

/**
 * The base class for functional test cases.
 * In this class, we set the base URL for the test application.
 * We also provide some common methods to be used by concrete test classes.
 */
class WebTestCase extends CWebTestCase {

    protected function setUp() {
	parent::setUp();

	$this->screenshotPath = 'c:\OpenServer\domains\parser.svc.itsirius.loc\screenshots';
	$this->screenshotUrl = SELENIUM_TEST_URL . "/screenshots";
	$this->captureScreenshotOnFailure = true;

	$this->setBrowser('firefox');
	$this->setBrowserUrl(SELENIUM_TEST_URL);
    }
    
    /**
     * Авторизация
     */
    protected function login($login, $password) {
	$this->open('/site/login');
	$this->type("LoginForm[username]", $login); 
	$this->type("LoginForm[password]", $password);
	$this->clickAndWait('yt0');
    }
    
    /**
     * Проверка URL на отсутствие ошибок
     * @param type $location
     */
    protected function testLocation($location) {
	$this->open($location);
	$this->assertLocation(SELENIUM_TEST_URL . $location);
	$this->testNoErrors();
    }
    
    /**
     * Проверка на отсутствие ошибок
     */
    protected function testNoErrors(){
	$this->assertElementPresent("//div[@id='header']");
	$this->assertTextNotPresent('Error 404');
	$this->assertTextNotPresent('Error 403');
    }
    
    
    

}
