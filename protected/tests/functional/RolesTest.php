<?php
/**
 * @group Selenium
 * @group Selenium_RolesAccess
 */
class RolesTest extends WebTestCase {
    public function testGuestAccess() {
	$this->open('/users/users');
	$this->assertLocation(SELENIUM_TEST_URL . '/site/login');
	$this->open('/clients/clients');
	$this->assertLocation(SELENIUM_TEST_URL . '/site/login');
	$this->open('/clients/filter');
	$this->assertLocation(SELENIUM_TEST_URL . '/site/login');
    }
    
    public function testUserAccess() {
	$this->login('tender@tender.svc', 'TpCLYdfv9L');
	$this->open('/users/users');
	$this->assertTextPresent('Error 403');
	$this->open('/clients/clients');
	$this->testNoErrors();
	$this->open('/clients/filter');
	$this->testNoErrors();
    }
    
    public function testAdminAccess() {
	$this->login('admin@tender.svc', 'TpCLYdfv9L1');
	$this->open('/users/users');
	$this->testNoErrors();
	$this->open('/clients/clients');
	$this->testNoErrors();
	$this->open('/clients/filter');
	$this->testNoErrors();
    }
}