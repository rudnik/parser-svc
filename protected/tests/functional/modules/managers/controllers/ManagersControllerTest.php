<?php
/**
 * @group Selenium
 * @group Selenium_UsersController
 */
class UsersControllerTest extends WebTestCase {
    
    /**
     * Тест представлений
     */
    public function testView() {
	$this->login('admin@tender.svc', 'TpCLYdfv9L1');
	
	# карточки
	$this->testLocation('/users/users');
	# список
	$this->testLocation('/users/users/admin');
	# форма добавления
	$this->testLocation('/users/users/create');
    }
    
    /**
     * Тест добавления менеджера
     */
    public function testCRUD() {
	$this->login('admin@tender.svc', 'TpCLYdfv9L1');
	
	# добавление
	$this->clickAndWait("//a[text()='Менеджеры']");
	$this->clickAndWait("//a[text()='Добавить']");
	$this->type("Users[name]", 'selenium Менеджер');
	$this->type("Users[email]", 'selenuim@mail.loc');
	$this->type("Users[password]", '123321');
	$this->type("Users[password_confirm]", '123321');
	$this->select("Users[role_id]", 'value=2');
	$this->clickAndWait('yt0');
	$this->testNoErrors();
	$this->assertElementPresent("//td[text()='selenium Менеджер']");
	$this->assertElementPresent("//td[text()='selenuim@mail.loc']");
	$this->assertElementPresent("//td[text()='Менеджер']");
	
	# редактирование
	$this->clickAndWait("//a[text()='Редактировать']");
	$this->type("Users[name]", 'selenium Менеджер1');
	$this->type("Users[email]", 'selenuim1@mail.loc');
	$this->type("Users[password]", '321123');
	$this->type("Users[password_confirm]", '321123');
	$this->select("Users[role_id]", 'value=1');
	$this->clickAndWait('yt0');
	$this->testNoErrors();
	$this->assertElementPresent("//td[text()='selenium Менеджер1']");
	$this->assertElementPresent("//td[text()='selenuim1@mail.loc']");
	$this->assertElementPresent("//td[text()='Администратор']");
	
	# удаление
	$this->clickAndWait("//a[text()='Удалить']");
	$this->testNoErrors();
    }
}