<?php

Yii::import('application.extensions.TendersParser.*');
Yii::import('application.extensions.TendersParser.models.*');

use Db\Db;

/**
 * @group Unit
 * @group Unit_ZakupkiParser
 */
class ZakupkiParserTest extends CTestCase {
	/**
	 * Загрузчик парсера порталов.
	 *
	 * @var ParserLauncher
	 */
	protected $launcher;

	/**
	 * Портал госзакупок.
	 *
	 * @var ZakupkiParser
	 */
	protected $portal;

	protected function setUp() {
		$this->launcher = new ParserLauncher();
		$portals = $this->launcher->getPortalParsers();
		$this->portal = array_shift($portals);
		$ftp_conn = new ReflectionMethod($this->portal, 'getFtpConnection');
		$ftp_conn->setAccessible(true);
		$ftp_conn->invoke($this->portal);
	}

	/**
	 * @covers Parser::getFtpConnection<protected>
	 */
	public function testFtpConnection() {
		$ftp_conn = new ReflectionMethod($this->portal, 'getFtpConnection');
		$ftp_conn->setAccessible(true);

		try {
			$ftp_conn->invoke($this->portal);
		}
		catch (\TendersParser\Parser\Exception\ParserException $e) {
			$this->fail($e->getMessage());
		}
	}

	/**
	 * @covers Parser::getFtpConnection<protected>
	 * @covers Parser::getFtpList<protected>
	 */
	public function testFtpList() {
		//$this->markTestSkipped();
		$method = new ReflectionMethod($this->portal, 'getFtpList');
		$method->setAccessible(true);
		$list = $method->invokeArgs($this->portal, []);
		$this->assertContains('/Adygeja_Resp', $list);
	}

	/**
	 * @covers Parser::isFtpDir<protected>
	 */
	public function testContractsDir() {
		/* $this->markTestSkipped();
		$method = new ReflectionMethod($this->portal, 'isFtpDir');
		$method->setAccessible(true);
		$is_dir = $method->invokeArgs($this->portal, ['/Adygeja_Resp/contracts/daily']);
		$this->assertTrue($is_dir); */
	}

	/**
	 * @covers Parser::isFtpDir<protected>
	 */
	public function testNotificationsDir() {
		//$this->markTestSkipped();
		$method = new ReflectionMethod($this->portal, 'isFtpDir');
		$method->setAccessible(true);
		$is_dir = $method->invokeArgs($this->portal, ['/Adygeja_Resp/notifications/daily']);
		$this->assertTrue($is_dir);
	}

	/**
	 * @covers Parser::isFtpDir<protected>
	 */
	public function testProtocolsDir() {
		/* $this->markTestSkipped();
		$method = new ReflectionMethod($this->portal, 'isFtpDir');
		$method->setAccessible(true);
		$is_dir = $method->invokeArgs($this->portal, ['/Adygeja_Resp/protocols/daily']);
		$this->assertTrue($is_dir); */
	}

	/**
	 * @covers ZakupkiParser::getTodayFiles<protected>
	 * @covers ZakupkiParser::checkFilenameFormat<protected>
	 */
	public function testTodayList() {
		//$this->markTestSkipped();
		$method = new ReflectionMethod($this->portal, 'getDayFiles');
		$method->setAccessible(true);

		try {
			$list = $method->invokeArgs($this->portal, ['/Adygeja_Resp/notifications/daily', 2]);
			$this->assertNotEmpty($list);
		}
		catch (ParserException $e) {
			$this->fail($e->getMessage());
		}
	}

	/**
	 * @covers ZakupkiParser::getMonthFiles<protected>
	 * @covers ZakupkiParser::checkFilenameFormat<protected>
	 */
	public function testMonthList() {
		$this->markTestSkipped();
		$method = new ReflectionMethod($this->portal, 'getMonthFiles');
		$method->setAccessible(true);

		try {
			$list = $method->invokeArgs($this->portal, ['/Adygeja_Resp/notifications', 2]);
			$this->assertNotEmpty($list);
		}
		catch (ParserException $e) {
			$this->fail($e->getMessage());
		}
	}

	/**
	 * @covers ZakupkiParser::getYearFiles<protected>
	 * @covers ZakupkiParser::checkFilenameFormat<protected>
	 */
	public function testYearList() {
		$this->markTestSkipped();
		$method = new ReflectionMethod($this->portal, 'getYearFiles');
		$method->setAccessible(true);
		$this->portal->useException = false;

		try {
			$list = $method->invokeArgs($this->portal, ['/Adygeja_Resp/notifications', 2013]);
			$this->assertNotEmpty($list);
		}
		catch (ParserException $e) {
			$this->fail($e->getMessage());
		}
	}

	/**
	 * @covers ZakupkiParser::saveFiles
	 */
	public function testSaveFiles() {
		$this->markTestSkipped();
		$this->assertTrue($this->portal->saveFiles());
	}

	/**
	 * @covers ZakupkiParser::parse
	 */
	public function testParse() {
		//$this->portal->useException = false;
		$this->assertTrue($this->portal->parse(false));
	}
}
