<?php
/**
 * @group Unit
 * @group Unit_Zakupki223Parser
 */
class Zakupki223ParserTest extends DbTestCase {
    
    /**
     * Проверка корректности парсинга
     */
    public function testParseXmlFile() {
	$xmlFilePath = realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'testFz223Xml.xml');
	$zakupkiXmlDocumentParser = new ZakupkiXmlDocumentParser(Zakupki223Parser::$uniqueFields, Zakupki223Parser::$parsedFields);
	$zakupkiXmlDocumentParser->parse($xmlFilePath);
	$parsedBidsData = $zakupkiXmlDocumentParser->getBidsData();
	
	$this->assertArrayHasKey('doctypeId', $parsedBidsData[0]);
	$this->assertGreaterThan(0, $parsedBidsData[0]['doctypeId']);
	unset($parsedBidsData[0]['doctypeId']);
	$this->assertArrayHasKey('portalId', $parsedBidsData[0]);
	$this->assertGreaterThan(0, $parsedBidsData[0]['portalId']);
	unset($parsedBidsData[0]['portalId']);
	
	$expectedResult = [
	    [
		'purchaseNoticeAE/body/item/purchaseNoticeAEData/createDateTime' => ['2013-09-03T15:20:02'],
		'purchaseNoticeAE/body/item/purchaseNoticeAEData/registrationNumber*' => ['31300529300'],
		'purchaseNoticeAE/body/item/purchaseNoticeAEData/name' => ['№ 6354/ОАЭ-ОАО "ЭЛТЕЗА"/2013/Д  на право заключения договора поставки оборудования автоматики и телемеханики для нужд ОАО «ЭЛТЕЗА» в 2013 году'],
		'purchaseNoticeAE/body/item/purchaseNoticeAEData/customer/mainInfo/shortName' => ['ОАО "ЭЛТЕЗА"'],
		'purchaseNoticeAE/body/item/purchaseNoticeAEData/lots/lot/subject' => ['№ 6354/ОАЭ-ОАО "ЭЛТЕЗА"/2013/Д на право заключения договора поставки оборудования автоматики и телемеханики (далее – Оборудование) для нужд ОАО «ЭЛТЕЗА» в 2013 году'],
		'purchaseNoticeAE/body/item/purchaseNoticeAEData/lots/lot/initialSum' => ['874863.94'],
		'purchaseNoticeAE/body/item/purchaseNoticeAEData/lots/lot/deliveryPlace' => ['Краснодарский край'],
		'purchaseNoticeAE/body/item/purchaseNoticeAEData/submissionCloseDateTime' => ['2013-09-24T11:00:00'],
		],
	];
	
	$this->assertEquals($expectedResult, $parsedBidsData);
    }
    
    /**
     * Прокерка удаленной структуры каталогов на соответствие ожидаемой
     * парсером
     */
    public function testDirTreeStructure() {
	$ftp = new Ftp(Zakupki223Parser::$ftp_host, Zakupki223Parser::$ftp_login, Zakupki223Parser::$ftp_password, Zakupki223Parser::$ftp_port, Zakupki223Parser::$ftp_timeout, Zakupki223Parser::$ftp_passiveMode);	
	$portal = Portals::model()->with('doctypes')->find('t.name = "zakupki223"');
	$this->assertNotNull($portal);
	$this->assertTrue(is_array($portal->doctypes));
	$this->assertTrue(count($portal->doctypes) > 0);
	
	$this->assertEquals($ftp->ls(), ['out']);
	$this->assertTrue($ftp->chdir('out/published'));
	$regions = $ftp->ls();
	$this->assertTrue(is_array($regions));
	$this->assertTrue(count($regions) > 0);
	Zakupki223Parser::prepareRegions();
	$localRegions = array_keys(Zakupki223Parser::$regionsIds);
	$regionsExclude = ['undefined', 'archive'];
	$localRegionsWithTempFolders = CMap::mergeArray($localRegions, $regionsExclude);
	foreach($regions as $region) {
	    $this->assertTrue(in_array($region, $localRegionsWithTempFolders));
	    if(in_array($region, $regionsExclude)) continue;
	    $this->assertTrue($ftp->chdir($region));
	    foreach ($portal->doctypes as $doctype) {
		$this->assertTrue($ftp->chdir($doctype->name));
		    $this->assertTrue($ftp->chdir('daily'));
		    $this->assertTrue($ftp->chdir('..'));
		    // в некоторых регионах нет папки full		    
//		    $this->assertTrue($ftp->chdir('full'));
//		    $this->assertTrue($ftp->chdir('..'));
		$this->assertTrue($ftp->chdir('..'));
	    }
	    $this->assertTrue($ftp->chdir('..'));
	}
	
	$ftp->disconnect();
    }
}