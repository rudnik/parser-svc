<?php

/**
 * @group Unit
 * @group Unit_Ftp
 */
class FtpTest extends CTestCase
{

    public function testAutoconnect()
    {
        $ftp = new Ftp(Zakupki223Parser::$ftp_host, Zakupki223Parser::$ftp_login, Zakupki223Parser::$ftp_password, Zakupki223Parser::$ftp_port, Zakupki223Parser::$ftp_timeout, Zakupki223Parser::$ftp_passiveMode, false);
        $this->assertNull($ftp->getConnection());
        unset($ftp);

        $ftp = new Ftp(Zakupki223Parser::$ftp_host, Zakupki223Parser::$ftp_login, Zakupki223Parser::$ftp_password, Zakupki223Parser::$ftp_port, Zakupki223Parser::$ftp_timeout, Zakupki223Parser::$ftp_passiveMode, true);
        $this->assertNotNull($ftp->getConnection());
        unset($ftp);
    }

    public function testConnect()
    {
        $ftp = new Ftp(Zakupki223Parser::$ftp_host, Zakupki223Parser::$ftp_login, Zakupki223Parser::$ftp_password, Zakupki223Parser::$ftp_port, Zakupki223Parser::$ftp_timeout, Zakupki223Parser::$ftp_passiveMode, false);
        $this->assertNull($ftp->getConnection());
        $this->assertTrue($ftp->connect());
        $this->assertNotNull($ftp->getConnection());
        unset($ftp);
    }

    public function testDisconnect()
    {
        $ftp = new Ftp(Zakupki223Parser::$ftp_host, Zakupki223Parser::$ftp_login, Zakupki223Parser::$ftp_password);
        $this->assertNotNull($ftp->getConnection());
        $ftp->disconnect();
        $this->assertNull($ftp->getConnection());
    }

}