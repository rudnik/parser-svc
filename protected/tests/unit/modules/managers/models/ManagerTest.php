<?php
/**
 * @group Unit
 * @group Unit_Users
 */
class UsersTest extends DbTestCase {
	/**
	 * Экземпляр тестируемой модели
	 * @var User 
	 */
	protected $model;
	
	protected function setUp() {
	    parent::setUp();
	    $this->model = new Users();
	}
	
	protected function testRequiredField($fieldName) {
	    $this->model->$fieldName = '';
	    $this->assertFalse($this->model->validate([$fieldName]));
	}
	
	protected function testMaxLength($fieldName, $length) {
	    $this->model->$fieldName = str_repeat(rand(0,9), $length);
	    $this->assertTrue($this->model->validate([$fieldName]));
	    $this->model->$fieldName = str_repeat(rand(0,9), $length + 1);
	    $this->assertFalse($this->model->validate([$fieldName]));
	}
	
	# ========== COMMON ========== #
	public function testNameMaxLengthIs255() {
	    $this->testMaxLength('name', 255);
	}
	
	public function testEmailFormat() {
	    $this->model->email = 'asd';
	    $this->assertFalse($this->model->validate(['email']));
	    
	    $this->model->email = 'asd@asd.asd';
	    $this->assertTrue($this->model->validate(['email']));
	}

	public function testPasswordMaxLengthIs32() {
	    $this->testMaxLength('password', 32);
	}

	public function testPasswordConfirmMaxLengthIs32() {
	    $this->testMaxLength('password_confirm', 32);
	}

	public function testRoleId() {
	    $this->model->role_id = -1;
	    $this->assertFalse($this->model->validate(['role_id']));
	    
	    $this->model->role_id = 0;
	    $this->assertTrue($this->model->validate(['role_id']));
	    
	    $this->model->role_id = 255;
	    $this->assertTrue($this->model->validate(['role_id']));
	    
	    $this->model->role_id = 256;
	    $this->assertFalse($this->model->validate(['role_id']));
	    
	    $this->model->role_id = 1.5;
	    $this->assertFalse($this->model->validate(['role_id']));
	    
	    $this->model->role_id = 'asd';
	    $this->assertFalse($this->model->validate(['role_id']));
	}

	# ========== ADD SCENARIO ========== #
	public function test_scenario_add_NameRequired() {
	    $this->model->setScenario('add');
	    $this->testRequiredField('name');
	}
	
	public function test_scenario_add_EmailRequired() {
	    $this->model->setScenario('add');
	    $this->testRequiredField('email');
	}
	
	public function test_scenario_add_PasswordRequired() {
	    $this->model->setScenario('add');
	    $this->testRequiredField('password');
	}
	
	public function test_scenario_add_PasswordConfirmRequired() {
	    $this->model->setScenario('add');
	    $this->testRequiredField('password_confirm');
	}
	
	public function test_scenario_add_PasswordsEqualsRequired() {
	    $this->model->setScenario('add');
	    $this->model->password = '1';
	    $this->model->password_confirm = '2';
	    $this->assertFalse($this->model->validate(['password_confirm']));
	    $this->model->password_confirm = '1';
	    $this->assertTrue($this->model->validate(['password_confirm']));
	}
	
	public function test_scenario_add_RoleIdRequired() {
	    $this->model->setScenario('add');
	    $this->testRequiredField('role_id');
	}
	
	# ========== UPDATE SCENARIO ========== #
	public function test_scenario_update_NameRequired() {
	    $this->model->setScenario('update');
	    $this->testRequiredField('name');
	}
	
	public function test_scenario_update_EmailRequired() {
	    $this->model->setScenario('update');
	    $this->testRequiredField('email');
	}
	
	public function test_scenario_update_RoleIdRequired() {
	    $this->model->setScenario('update');
	    $this->testRequiredField('role_id');
	}
	
	public function test_scenario_update_PasswordsEqualsRequired() {
	    $this->model->setScenario('update');
	    $this->model->password = '1';
	    $this->model->password_confirm = '2';
	    $this->assertFalse($this->model->validate(['password_confirm']));
	    $this->model->password_confirm = '1';
	    $this->assertTrue($this->model->validate(['password_confirm']));
	}
	
	# ========== EDIT SCENARIO ========== #
	public function test_scenario_edit_PasswordFilter() {
	    $this->model->setScenario('edit');
	    $this->model->password = '1';
	    $this->model->validate(['password']);
	    $this->assertEmpty($this->model->password);
	}
	
	# ========== RELATIONS ========== #
	public function testBelongsToRole() {
	    $user = Users::model()->findByPk(1);
	    $this->assertInstanceOf('Roles', $user->role);
	}
}