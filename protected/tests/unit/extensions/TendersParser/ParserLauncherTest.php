<?php

Yii::import('application.extensions.TendersParser.*');

/**
 * @group Unit
 * @group Unit_ParserLauncher
 */
class ParserLauncherTest extends CTestCase {
	protected $launcher;

	protected function setUp() {
		$this->launcher = new ParserLauncher();
	}

	/**
	 * @covers ParserLauncher::isReady
	 */
	public function testLauncherReady() {
		$this->assertEquals(true, $this->launcher->isReady());
	}

	public function testPortalsList() {
		$this->assertNotEmpty($this->launcher->getPortalParsers());
	}

	public function testIsObjectInPortals() {
		$portals = $this->launcher->getPortalParsers();
		$this->assertContainsOnlyInstancesOf('Parser', $portals);
	}
}
