$(document).ready(function() {
	$('#look-search').on('click', function(event) {
		event.preventDefault();
		var a = $(this);
		refreshSearchResult(a);

	});

	$(document).on('click', 'ul.yiiPager > li > a', function(event) {
		event.preventDefault();
		$.get($(this).attr('href'), function(data) {
		    $('#search').html(data);
		    if(subscriptionsBidsList !== undefined) {
			subscriptionsBidsList.updateBidsSelectedStatus();
		    }
		});
	});

	$('.check-tree').tree({
		collapseImage: '/images/downArrow.gif',
		expandImage: '/images/rightArrow.gif',
		initializeChecked: 'expanded',
		initializeUnchecked: 'collapsed'
	});

	var tagedit_texts = {
		removeLinkTitle: 'Удалить из списка',
		saveEditLinkTitle: 'Сохранить изменения',
		deleteLinkTitle: 'Удалить',
		deleteConfirmation: 'Вы уверены, что хотите удалить?',
		deletedElementTitle: 'Элемент будет удален',
		breakEditLinkTitle: 'Отмена',
		editTag: 'Редактировать'
	};
	$('.keywords_inc').tagedit({
		texts: tagedit_texts
	});
	$('.keywords_exc').tagedit({
		texts: tagedit_texts
	});
});

function refreshSearchResult(initiator) {
    $.ajax(initiator.attr('href'), {
	    method: 'get',
	    data: initiator.parents('form').serialize(),
	    beforeSend: function() { $('#loading').show(); },
	    success: function(data) { $('#search').html(data); },
	    complete: function() { $('#loading').hide(); }
    });
}
